//
//  PHATextView.m
//  多级菜单
//
//  Created by 杨毅 on 16/12/19.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "PHATextView.h"

@implementation PHATextView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _contentColor = [UIColor blackColor];
        _editing = NO;
        _placeholderColor = [UIColor lightGrayColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startEditing:) name:UITextViewTextDidBeginEditingNotification object:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishEditing:) name:UITextViewTextDidEndEditingNotification object:self];
        
    }
    return self;
}


#pragma mark - super

- (void)setTextColor:(UIColor *)textColor {
    [super setTextColor:textColor];
    _contentColor = textColor;
}

- (NSString *)text {
    if ([super.text isEqualToString:_placeholder] && super.textColor == _placeholderColor) {
        return @"";
    }
    return [super text];
}

- (void)setText:(NSString *)string {
    if (string == nil || string.length == 0) {
        return;
    }
    super.textColor = _contentColor;
    [super setText:string];
}


#pragma mark - setting

- (void)setPlaceholder:(NSString *)string {
    _placeholder = string;
    [self finishEditing:nil];
}


#pragma mark - notification

- (void)startEditing:(NSNotification *)notification {
    _editing = YES;
    if ([super.text isEqualToString:_placeholder] && super.textColor == _placeholderColor) {
        
        super.textColor = _contentColor;
        super.text = @"";
    }
}

- (void)finishEditing:(NSNotification *)notification {
    _editing = NO;
    if (super.text.length == 0) {
        
        super.textColor = _placeholderColor;
        super.text = _placeholder;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
