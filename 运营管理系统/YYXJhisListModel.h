//
//  YYXJhisListModel.h
//  XJHistory
//
//  Created by 杨毅 on 16/12/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYXJhisListModel <NSObject>

@end

@interface YYXJhisListModel : JSONModel
@property (nonatomic,assign)NSInteger inspectId;
@property (nonatomic,strong)NSString *inspectUser;
@property (nonatomic,assign)NSInteger inspectUserId;
@property (nonatomic,assign)NSInteger stationId;
@property (nonatomic,strong)NSString *stationName;

//@property (nonatomic,strong)NSString *operationCenter;
@property (nonatomic,strong)NSString *startTime;
@property (nonatomic,strong)NSString *stopTime;
@property (nonatomic,assign)NSInteger resultStatus;
@property (nonatomic,strong)NSString *photos;

@property (nonatomic,strong)NSString *resultDesc;
@property (nonatomic,assign)NSInteger faultId;
@property (nonatomic,strong)NSString *commitTime;
@property (nonatomic,strong)NSString *commitLocation;
@property (nonatomic,strong)NSString *stationLocation;
@end
/*
 "stationId": 2380,
 "resultStatus": 1,
 "resultDesc": "填我哦哦哦",
 "commitLocation": "长沙市雨花区环保中路188号",
 "endTime": "2016-12-19 14:11:43",
 "faultId": "",
 "inspectId": 111,
 "stationLocation": "长沙市雨花区环保中路188号",
 "photos": "",
 "startTime": "2016-12-19 14:11:41",
 "inspectUserId": 1101,
 "commitTime": "2016-12-19 14:11:37",
 "inspectUser": "郑博",
 "stationName": "武威市供排水公司污水处理厂进水口"
 */
