//
//  SectionOneFaultModel.h
//  WSDropMenuView
//
//  Created by 杨毅 on 16/11/29.
//  Copyright © 2016年 Senro Wong. All rights reserved.
//

#import "JSONModel.h"
#import "SectionTowFaultModel.h"
@protocol SectionOneFaultModel <NSObject>

@end

@interface SectionOneFaultModel : JSONModel
@property (nonatomic,strong)NSString *code;
@property (nonatomic,strong)NSArray <SectionTowFaultModel> *faultTypes;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *remark;
@property (nonatomic,assign)NSInteger type;
@end
/*
 "code": "0002",
 "faultTypes": [],
 "id": 13,
 "name": "软件故障",
 "remark": "",
 "type": 0
 */
