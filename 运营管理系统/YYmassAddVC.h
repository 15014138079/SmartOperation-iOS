//
//  YYmassAddVC.h
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MassMessageView.h"
#import "AFNetworking.h"
#import "XJEqDataModel.h"//设备模型
#import "AppDelegate.h"
#import "MHActionSheet.h"
//时间选择
#import "FDAlertView.h"
#import "RBCustomDatePickerView.h"

#import "MBProgressHUD.h"
#import "YXKit.h"
@interface YYmassAddVC : UIViewController<MassMessageViewDelegate,sendTheValueDelegate>
@property(nonatomic,assign)NSInteger stationId;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSString *custName;
@end
