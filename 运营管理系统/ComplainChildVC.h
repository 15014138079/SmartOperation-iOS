//
//  ComplainChildVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestionAndComplainAddVC.h"
#import "SuggesOrComplainCell.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "SugAndComDataModel.h"
#import "SugAndComDetailVC.h"
#import "UnusualBaseView.h"
@interface ComplainChildVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UnusualBaseViewDelegate,SuggesOrComplainCellDelegate>

@end
