//
//  GZcountPsonCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/18.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PsonModel.h"
@interface GZcountPsonCell : UITableViewCell
- (void)fillCellWithModel:(PsonModel *)model;
@end
