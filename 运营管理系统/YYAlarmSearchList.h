//
//  YYAlarmSearchList.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"


@protocol YYAlarmSearchList <NSObject>

@end

@interface YYAlarmSearchList : JSONModel
@property (nonatomic, strong)NSString *time;
@property (nonatomic, assign)NSInteger state;//故障状态
@property (nonatomic, strong)NSString *discrib;//故障描述
@property (nonatomic, strong)NSString *name;
@property (nonatomic, assign)NSInteger gzID;
@end
/*
 "time": "2017-01-12 18:29:46",
 "discrib": "说一下",
 "name": "睡觉咯",
 "gzid": 459,
 "state": 0
 */
