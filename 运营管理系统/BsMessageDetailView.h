//
//  BsMessageDetailView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BsMessageDetailView;
@protocol BsMessageDetailViewDelegate <NSObject>
- (void)didSelectedImageURL:(NSArray *)arr AndIndex:(NSInteger)index;
- (void)returnBSMessageViewHeight:(CGFloat)height;
@end

@interface BsMessageDetailView : UIView
- (instancetype)initWithFrame:(CGRect)frame AndDataDictionray:(NSDictionary *)dic MaxY:(CGFloat)Y WithImageUrl:(NSArray *)imageUrlArray;
@property(nonatomic,weak)id <BsMessageDetailViewDelegate> delegate;
@end
