//
//  YYFgsPartVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "PartDataModel.h"
#import "YYpartCell.h"
#import "PartDetailVC.h"
#import "PartSearchVC.h"
#import "MBProgressHUD.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
#import "MJRefresh.h"
@interface YYFgsPartVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,NothingViewDelegate,FallLineDelegate,ServerUnsualDelegate>
@property (nonatomic,strong)NSString *NavTitle;
@property (nonatomic,assign)NSInteger ID;
@end
