//
//  PartMtsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol PartMtsModel <NSObject>

@end
@interface PartMtsModel : JSONModel
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *unit;
@property (nonatomic,assign)NSInteger numb;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSString *logId;
@end
/*
 "id": 0,
 "unit": "台",
 "numb":2,
 "discrib": "裸机/化学需氧量在线自动监测仪/YX-CODCr-Ⅱ/分光光度法/九通阀/PLC/蓝色/COD机型",
 "logId": "19301003000001"
 */