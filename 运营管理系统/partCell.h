//
//  partCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/13.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MslistModel.h"
@interface partCell : UITableViewCell
- (void)fillCellWithModel:(MslistModel *)model;
@end
