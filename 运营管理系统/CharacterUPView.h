//
//  CharacterUPView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHATextView.h"
@class CharacterUPView;
@protocol CharacterUPViewDelegate <NSObject>
- (void)CharaUpBtnClick:(UIButton *)sender;
- (void)cancelClick:(UIButton *)sender;
@end

@interface CharacterUPView : UIView
@property (nonatomic,strong)PHATextView *BidCLIdearTextView;
@property (nonatomic,strong)PHATextView *ResultTextView;
@property (nonatomic,strong)PHATextView *ChangeWaysTextView;
- (instancetype)initWithFrame:(CGRect)frame AndMessageDic:(NSDictionary *)dic;
@property (nonatomic,weak)id <CharacterUPViewDelegate> delegate;
@end
