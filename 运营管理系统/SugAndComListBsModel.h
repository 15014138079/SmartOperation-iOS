//
//  SugAndComListBsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/21.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "SugAndComDataModel.h"
@interface SugAndComListBsModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)SugAndComDataModel *data;
@end
