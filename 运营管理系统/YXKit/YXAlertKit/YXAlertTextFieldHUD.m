//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertTextFieldHUD.h"

#define RateOfWidth     260//([UIScreen mainScreen].bounds.size.width * 262.5 / 375)// 宽
#define RateOfHeight    240//([UIScreen mainScreen].bounds.size.height * 232.5 / 667)// 高

@interface YXAlertTextFieldHUD ()

@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *submit;
@property (nonatomic, strong) UILabel *label;

@end

@implementation YXAlertTextFieldHUD

- (instancetype)init {
    if ([super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboradShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboradHide:) name:UIKeyboardWillHideNotification object:nil];
        [self initUI];
    }
    return self;
}

- (void)keyboradShow:(NSNotification *)aNotice {
    CGFloat height = [aNotice.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    CGFloat distance = CGRectGetHeight(self.frame)-height-_alertView.frame.origin.y-RateOfHeight;
    if (distance < 0) {
        [UIView animateWithDuration:0.3 animations:^{
            _alertView.center = CGPointMake(_alertView.center.x, _alertView.center.y+distance);
        }];
    }
}

- (void)keyboradHide:(NSNotification *)aNotice {
    [UIView animateWithDuration:0.3 animations:^{
        _alertView.center = self.center;
    }];
}

- (void)initUI {
    [self addSubview:self.alertView];
    [_alertView addSubview:self.submit];
    [_alertView addSubview:self.textField];
    [_alertView addSubview:self.label];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.alertView.bounds = CGRectMake(0, 0, RateOfWidth, RateOfHeight);
    self.alertView.center = self.center;
    
    self.submit.frame = CGRectMake(14, RateOfHeight-44, RateOfWidth-28, 30);
    
    self.textField.bounds = CGRectMake(0, 0, RateOfWidth-56, 30);
    self.textField.center = CGPointMake(RateOfWidth/2, RateOfHeight/2);
    
    self.label.frame = CGRectMake(25, 20, RateOfWidth-40, 50);
    
}

- (void)show {
    [super show];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    keyAnimation.values = @[@0.2,@1,@0.85,@1];
    keyAnimation.duration = 0.6;
    keyAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [_alertView.layer addAnimation:keyAnimation forKey:nil];
    _alertView.transform = CGAffineTransformMakeScale(1, 1);
}
- (void)dismiss {
    [super dismiss];
    [_textField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)submitAction {
    [self dismiss];
    if (self.textFieldTypeClickedButton) {
        self.textFieldTypeClickedButton(_textField.text);
    }
}

- (void)setPlaceHolder:(NSString *)placeHolder {
    if (placeHolder == nil || placeHolder.length < 1) return;
    _textField.placeholder = placeHolder;
}

- (void)setKeyBoardType:(UIKeyboardType)type {
    _textField.keyboardType = type;
}

- (void)setTitleMessage:(NSString *)message {
    if (message == nil || message.length < 1) return;
    _label.text = message;
}

- (void)setSubmitButtonTitle:(NSString *)title {
    if (title == nil || title.length < 1) return;
    [_submit setTitle:title forState:UIControlStateNormal];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    UITouch *touch = touches.anyObject;
    if (touch.view != _alertView) {
        [self dismiss];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setter&Getter
- (UIView *)alertView {
    if (!_alertView) {
        _alertView = [[UIView alloc] init];
        _alertView.layer.cornerRadius = 4;
        _alertView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.shadowColor = [UIColor grayColor].CGColor;
        _alertView.layer.shadowOpacity = 0.8;
        _alertView.layer.shadowOffset = CGSizeMake(5, 5);
    }
    return _alertView;
}
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.font = [UIFont systemFontOfSize:15];
        _textField.placeholder = @"Input";
        _textField.textAlignment = NSTextAlignmentCenter;
        CALayer *layer = [CALayer layer];
        layer.frame = CGRectMake(0, 29, RateOfWidth-56, 1);
        layer.backgroundColor = [UIColor colorWithRed:43/255.0 green:91/255.0 blue:225/255.0 alpha:1].CGColor;
        _textField.keyboardType = UIKeyboardTypeEmailAddress;
        [_textField.layer addSublayer:layer];
    }
    return _textField;
}
- (UIButton *)submit {
    if (!_submit) {
        _submit = [[UIButton alloc] init];
        _submit.backgroundColor = [UIColor colorWithRed:43/255.0 green:91/255.0 blue:225/255.0 alpha:1];
        _submit.titleLabel.font = [UIFont systemFontOfSize:15];
        _submit.titleLabel.textColor = [UIColor whiteColor];
        _submit.layer.cornerRadius = 4;
        [_submit setTitle:@"Submit" forState:UIControlStateNormal];
        [_submit addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submit;
}
- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor grayColor];
        _label.numberOfLines = 2;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = @"Please enter your account\nin the hospital.";
    }
    return _label;
}

@end
