//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertHUD.h"

@interface YXAlertTextFieldHUD : YXAlertHUD

- (void)setPlaceHolder:(NSString *)placeHolder;
- (void)setKeyBoardType:(UIKeyboardType)type;
- (void)setSubmitButtonTitle:(NSString *)title;
- (void)setTitleMessage:(NSString *)message;

@end
