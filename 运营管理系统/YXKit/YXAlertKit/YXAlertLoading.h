//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertHUD.h"

@interface YXAlertLoading : YXAlertHUD

- (void)setTitleMessage:(NSString *)message;

@end
