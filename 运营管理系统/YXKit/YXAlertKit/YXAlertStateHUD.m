//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertStateHUD.h"

#define RateOfWidth     260//([UIScreen mainScreen].bounds.size.width * 262.5 / 375)// 宽
#define RateOfHeight    240//([UIScreen mainScreen].bounds.size.height * 232.5 / 667)// 高

@interface YXAlertStateHUD ()

@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIColor *iconColor;

@end

@implementation YXAlertStateHUD

- (instancetype)init {
    if ([super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [self initUI];
    }
    return self;
}

- (void)initUI {
    [self addSubview:self.alertView];
    [_alertView addSubview:self.label];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.alertView.bounds = CGRectMake(0, 0, RateOfWidth, RateOfHeight);
    self.alertView.layer.cornerRadius = 20;
    self.alertView.center = self.center;
    
    self.label.frame = CGRectMake(25, RateOfHeight-45, RateOfWidth-50, 30);
    
}

- (void)show {
    [super show];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    keyAnimation.values = @[@0.2,@1,@0.85,@1];
    keyAnimation.duration = 0.6;
    keyAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    keyAnimation.delegate = self;
    [_alertView.layer addAnimation:keyAnimation forKey:nil];
    _alertView.transform = CGAffineTransformMakeScale(1, 1);
}
- (void)dismiss {
    [super dismiss];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)setTitleMessage:(NSString *)message {
    if (message == nil || message.length < 1) return;
    _label.text = message;
}

- (void)setIconColor:(UIColor *)iconColor {
    if (iconColor) _iconColor = iconColor;
    else if (_hudType == 0) _iconColor = [UIColor colorWithRed:43/255.0 green:91/255.0 blue:225/255.0 alpha:1];
    else if (_hudType == 1) _iconColor = [UIColor colorWithRed:175/255.0 green:175/255.0 blue:175/255.0 alpha:1];
    else _iconColor = [UIColor redColor];
}

- (void)setTextColor:(UIColor *)textColor {
    if (!textColor) return;
    _label.textColor = textColor;
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
    if ([anim isKindOfClass:CAKeyframeAnimation.class] && flag) {
        
        if (self.hudType == 0) {
            CAShapeLayer *shape = [CAShapeLayer layer];
            CGRect frame = CGRectZero;
            frame.size.height = frame.size.width = 60*4/3;
            frame.origin.x = RateOfWidth/2-40;
            frame.origin.y = RateOfHeight/2-60;
            shape.frame = frame;
            shape.lineCap = kCALineCapRound;
            shape.lineWidth = 5;
            shape.fillColor = [UIColor clearColor].CGColor;
            shape.strokeColor = _iconColor.CGColor;
            [_alertView.layer addSublayer:shape];
            
            UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:shape.bounds];
            [bezierPath moveToPoint:CGPointMake(12.5*4/3,30*4/3)];
            [bezierPath addLineToPoint:CGPointMake(27.5*4/3,45*4/3)];
            [bezierPath addLineToPoint:CGPointMake(47.5*4/3,20*4/3)];
            shape.path = bezierPath.CGPath;
            
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
            animation.fromValue = @0;
            animation.toValue = @1;
            animation.duration = 0.3;
            [shape addAnimation:animation forKey:nil];
            
            [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.3];
        }else {
            CAShapeLayer *shape = [CAShapeLayer layer];
            CGRect frame = CGRectZero;
            frame.size.height = frame.size.width = 60*4/3;
            frame.origin.x = RateOfWidth/2-40;
            frame.origin.y = RateOfHeight/2-60;
            shape.frame = frame;
            shape.lineCap = kCALineCapRound;
            shape.lineWidth = 5;
            shape.fillColor = [UIColor clearColor].CGColor;
            shape.strokeColor = _iconColor.CGColor;
            [_alertView.layer addSublayer:shape];
            
            UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:shape.bounds];
            [bezierPath moveToPoint:CGPointMake(15*4/3,15*4/3)];
            [bezierPath addLineToPoint:CGPointMake(45*4/3,45*4/3)];
            [bezierPath moveToPoint:CGPointMake(45*4/3, 15*4/3)];
            [bezierPath addLineToPoint:CGPointMake(15*4/3,45*4/3)];
            shape.path = bezierPath.CGPath;
            
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
            animation.fromValue = @0;
            animation.toValue = @1;
            animation.duration = 0.3;
            [shape addAnimation:animation forKey:nil];
            
            [self performSelector:@selector(dismiss) withObject:nil afterDelay:1.3];
        }
        
    }
}

#pragma mark - Setter&Getter
- (UIView *)alertView {
    if (!_alertView) {
        _alertView = [[UIView alloc] init];
        _alertView.layer.cornerRadius = 4;
        _alertView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.shadowColor = [UIColor grayColor].CGColor;
        _alertView.layer.shadowOpacity = 0.8;
        _alertView.layer.shadowOffset = CGSizeMake(5, 5);
    }
    return _alertView;
}
- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor grayColor];
        _label.textAlignment = NSTextAlignmentCenter;
    }
    return _label;
}
- (YXAlertHUDType)hudType {
    if (!_hudType) {
        _hudType = YXAlertHUDType_Success;
    }
    return _hudType;
}

@end
