//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertLoading.h"

#define RateOfWidth     260//([UIScreen mainScreen].bounds.size.width * 262.5 / 375)// 宽
#define RateOfHeight    240//([UIScreen mainScreen].bounds.size.height * 232.5 / 667)// 高

#define IMAGE_NAME(_NAME) [UIImage imageNamed:_NAME]

@interface YXAlertLoading ()

@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation YXAlertLoading

- (instancetype)init {
    if ([super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [self initUI];
    }
    return self;
}

- (void)initUI {
    [self addSubview:self.alertView];
    [_alertView addSubview:self.label];
    [_alertView addSubview:self.imageView];
    [_imageView startAnimating];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.alertView.bounds = CGRectMake(0, 0, RateOfWidth, RateOfHeight);
    self.alertView.center = self.center;
    
    self.label.frame = CGRectMake(25, RateOfHeight-45, RateOfWidth-50, 30);
    
    CGRect frame = CGRectZero;
    frame.size.height = frame.size.width = 34;
    frame.origin.x = RateOfWidth/2-17;
    frame.origin.y = RateOfHeight/2-27;
    self.imageView.frame = frame;
    
}

- (void)show {
    [super show];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    keyAnimation.values = @[@0.2,@1,@0.85,@1];
    keyAnimation.duration = 0.6;
    keyAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [_alertView.layer addAnimation:keyAnimation forKey:nil];
    _alertView.transform = CGAffineTransformMakeScale(1, 1);
}
- (void)dismiss {
    [super dismiss];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)setTitleMessage:(NSString *)message {
    if (message == nil || message.length < 1) return;
    _label.text = message;
}

#pragma mark - Setter&Getter
- (UIView *)alertView {
    if (!_alertView) {
        _alertView = [[UIView alloc] init];
        _alertView.layer.cornerRadius = 4;
        _alertView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        _alertView.backgroundColor = [UIColor whiteColor];
        _alertView.layer.shadowColor = [UIColor grayColor].CGColor;
        _alertView.layer.shadowOpacity = 0.8;
        _alertView.layer.shadowOffset = CGSizeMake(5, 5);
    }
    return _alertView;
}
- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor grayColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = @"Please wait for processing";
    }
    return _label;
}
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        
        NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle bundleForClass:[self class]] pathForResource:@"LoadingImageBundle" ofType:@"bundle"]];
        NSArray *pngs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:bundle.bundlePath error:nil];
        // 排序
        NSArray *arr = [pngs sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
            NSNumber *num1 = [NSNumber numberWithInt:[obj1 substringWithRange:NSMakeRange(0,obj1.length-4)].intValue];
            NSNumber *num2 = [NSNumber numberWithInt:[obj2 substringWithRange:NSMakeRange(0,obj2.length-4)].intValue];
            NSComparisonResult result = [num1 compare:num2];
            return result == NSOrderedDescending;
        }];
        NSMutableArray *animationImages = [NSMutableArray array];
        for (int i = 0;i < arr.count;++ i) {
            [animationImages addObject:[UIImage imageWithContentsOfFile:[bundle.bundlePath stringByAppendingPathComponent:arr[i]]]];
        }
        
        _imageView.animationImages = animationImages;
        _imageView.animationDuration = 1.5;
        _imageView.animationRepeatCount = INT_MAX;
    }
    return _imageView;
}

@end