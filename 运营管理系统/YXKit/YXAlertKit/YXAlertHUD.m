//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertHUD.h"
#import "YXAlertTextFieldHUD.h"
#import "YXAlertStateHUD.h"
#import "YXAlertLoading.h"

@interface YXAlertHUD ()

@end

@implementation YXAlertHUD

+ (id)textFieldHUDWithSubmitTitle:(NSString *)title message:(NSString *)msg placeholder:(NSString *)palceholder keyboard:(UIKeyboardType)type {
    YXAlertTextFieldHUD *hud = [[YXAlertTextFieldHUD alloc] init];
    [hud setPlaceHolder:palceholder];
    [hud setKeyBoardType:type];
    [hud setSubmitButtonTitle:title];
    [hud setTitleMessage:msg];
    return hud;
}

+ (id)stateHUDWithHUDTyep:(YXAlertHUDType)type message:(NSString *)msg textColor:(UIColor *)textColor iconColor:(UIColor *)iconColor {
    YXAlertStateHUD *hud = [[YXAlertStateHUD alloc] init];
    hud.hudType = type;
    [hud setTitleMessage:msg];
    [hud setIconColor:iconColor];
    [hud setTextColor:textColor];
    return hud;
}

+ (id)loadingHUDWithMessage:(NSString *)msg {
    YXAlertLoading *hud = [[YXAlertLoading alloc] init];
    [hud setTitleMessage:msg];
    return hud;
}

- (void)show {}
- (void)dismiss {}

@end
