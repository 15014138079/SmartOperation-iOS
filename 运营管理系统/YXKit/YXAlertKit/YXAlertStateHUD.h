//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import "YXAlertHUD.h"

@interface YXAlertStateHUD : YXAlertHUD

@property (nonatomic) YXAlertHUDType hudType;
- (void)setTitleMessage:(NSString *)message;
- (void)setIconColor:(UIColor *)iconColor;
- (void)setTextColor:(UIColor *)textColor;

@end
