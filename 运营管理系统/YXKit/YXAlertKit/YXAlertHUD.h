//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef enum
{
    YXAlertHUDType_Success = 0, // default
    YXAlertHUDType_Failed
}YXAlertHUDType;

@interface YXAlertHUD : UIView

/// 用于包含textField时点击按钮的回调
@property (nonatomic, strong) void (^textFieldTypeClickedButton) (NSString *text);

#pragma mark - 便利初始化方法
+ (id)textFieldHUDWithSubmitTitle:(NSString *)title
                          message:(NSString *)msg
                      placeholder:(NSString *)palceholder
                         keyboard:(UIKeyboardType)type;

+ (id)stateHUDWithHUDTyep:(YXAlertHUDType)type
                  message:(NSString *)msg
                textColor:(UIColor *)textColor
                iconColor:(UIColor *)iconColor;

+ (id)loadingHUDWithMessage:(NSString *)msg;

#pragma mark - 提供给子类实现，给外界调用以实现添加和消除
- (void)show;
- (void)dismiss;

@end
