//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef enum InfiScrollType {
    InfiScrollType_Ressort = 0,// 弹簧效果，触底反弹
    InfiScrollType_Loop// 无限循环
}InfiScrollType;

@protocol YXInfiniteScrollViewDelegate;

@interface YXInfiniteScrollView : UIView

/// 是否需要自动滚动，如果不等于0则表示不需要，否则会按照该时间间隔进行滚动，默认为0，不滚动。
@property (nonatomic, assign) NSTimeInterval timeInterval;

@property (nonatomic, strong, readonly) UICollectionView *collectionView;

+ (instancetype)infiScrViewWithFrame:(CGRect)frame
                                    delegate:(id <YXInfiniteScrollViewDelegate>)delegate
                collectionViewLayout:(UICollectionViewLayout *)layout
                          scrollType:(InfiScrollType)type;


@end

@protocol YXInfiniteScrollViewDelegate <NSObject>

@required
- (NSInteger)itemNumberInInfi:(YXInfiniteScrollView *)infi;
- (UICollectionViewCell *)itemForInfi:(YXInfiniteScrollView *)infi
                                         index:(NSInteger)index;
@optional
- (void)itemDidSelectedWithInfi:(YXInfiniteScrollView *)infi
                       forIndex:(NSInteger)index;

@end
