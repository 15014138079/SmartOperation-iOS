//
//  Copyright © 2016年 yxiang. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface ProximitySensor : NSObject

- (void)yx_listeningProSenWithState:(void (^) (BOOL proximityState))stateHandler;
- (void)yx_stopProSen;

@end
