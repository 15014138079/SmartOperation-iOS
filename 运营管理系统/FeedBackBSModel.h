//
//  FeedBackBSModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/9.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "FeedBackDataModel.h"
@interface FeedBackBSModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)FeedBackDataModel *data;
@end
