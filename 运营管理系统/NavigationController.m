//
//  NavigationController.m
//  OneOwnerPurchase
//
//  Created by Yumukim on 15/12/9.
//  Copyright © 2015年 Yumukim. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

+ (void)initialize
{
#pragma mark - 重写导航栏 Bar
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    // 不透明
    navigationBar.translucent = NO;
    // 设置导航栏bar背景颜色
    [navigationBar setBarTintColor:UIColorFromRGB(0x0093d5)];
    [navigationBar setTintColor:[UIColor whiteColor]];// iOS7的情况下,设置NavigationBarItem文字的颜色
    // 3.设置导航栏文字的主题
    [navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.0],NSForegroundColorAttributeName : [UIColor whiteColor]}];


    
}

//改变状态栏样式
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
