//
//  XJhisPsonDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "XJhisPsonListModel.h"
@interface XJhisPsonDataModel : JSONModel
@property (nonatomic,strong)NSArray <XJhisPsonListModel> *beanList;
@end
