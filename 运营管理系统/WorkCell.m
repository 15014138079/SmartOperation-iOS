//
//  WorkCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/21.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "WorkCell.h"

@implementation WorkCell
{
    __weak IBOutlet UIImageView *leftIcon;
    __weak IBOutlet UILabel *Countent;
    __weak IBOutlet UIImageView *rightIcon;
    __weak IBOutlet UILabel *messages;
    
}

- (void)fillCellWithModel:(workModel *)model
{
    leftIcon.image = [[UIImage imageNamed:model.leftIcon]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    Countent.text = model.countent;
    messages.text = model.message;
    messages.lineBreakMode = NSLineBreakByTruncatingMiddle;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
