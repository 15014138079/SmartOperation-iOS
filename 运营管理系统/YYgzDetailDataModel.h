//
//  YYgzDetailDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYgzDetailFaulModel.h"
@interface YYgzDetailDataModel : JSONModel
@property (nonatomic,strong)YYgzDetailFaulModel *FaultRecd;
@end
