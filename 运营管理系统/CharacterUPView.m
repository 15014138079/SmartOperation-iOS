//
//  CharacterUPView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "CharacterUPView.h"

@implementation CharacterUPView
- (instancetype)initWithFrame:(CGRect)frame AndMessageDic:(NSDictionary *)dic{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self initMessageView:dic];
    }
    return self;
}
- (void)initMessageView:(NSDictionary *)dictionary{
    
    UIView *back1 = [[UIView alloc]initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 55)];
    back1.backgroundColor = [UIColor whiteColor];
    [self addSubview:back1];
    UILabel *nameLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 5, kSCREEN_WIDTH-24, 21)];
    nameLab.text = [NSString stringWithFormat:@"填  报  人: %@",dictionary[@"userName"]];
    nameLab.textColor = UIColorFromRGB(0x4d4d4d);
    nameLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:nameLab];
    UILabel *PhoneLab = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(nameLab.frame)+5, kSCREEN_WIDTH-24, 21)];
    PhoneLab.text = [NSString stringWithFormat:@"联系方式: %@",dictionary[@"phone"]];
    PhoneLab.textColor = UIColorFromRGB(0x4d4d4d);
    PhoneLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:PhoneLab];
    
    UIView *back2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back1.frame)+10, kSCREEN_WIDTH, 106)];
    back2.backgroundColor = [UIColor whiteColor];
    [self addSubview:back2];
    UILabel *BidClIdearLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, kSCREEN_WIDTH-24, 20)];
    BidClIdearLab.textAlignment = NSTextAlignmentLeft;
    BidClIdearLab.text = @"对已发生不符合的处置方案:";
    BidClIdearLab.font = [UIFont systemFontOfSize:15.0];
    BidClIdearLab.textColor = UIColorFromRGB(0x4d4d4d);
    [back2 addSubview:BidClIdearLab];
    _BidCLIdearTextView = [[PHATextView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(BidClIdearLab.frame), kSCREEN_WIDTH-24, 80)];
    _BidCLIdearTextView.placeholder = @"输入处置方案";
    _BidCLIdearTextView.font = [UIFont systemFontOfSize:15.0];
    _BidCLIdearTextView.layer.borderColor = UIColorFromRGB(0xf0f0f0).CGColor;
    _BidCLIdearTextView.layer.borderWidth = 1.0f;
    _BidCLIdearTextView.layer.cornerRadius = 3;
    _BidCLIdearTextView.returnKeyType = UIReturnKeyDone;
    [back2 addSubview:_BidCLIdearTextView];
    
    UIView *back3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back2.frame)+10, kSCREEN_WIDTH, 106)];
    back3.backgroundColor = [UIColor whiteColor];
    [self addSubview:back3];
    UILabel *ResultLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, kSCREEN_WIDTH-24, 20)];
    ResultLab.textAlignment = NSTextAlignmentLeft;
    ResultLab.text = @"原因分析:";
    ResultLab.font = [UIFont systemFontOfSize:15.0];
    ResultLab.textColor = UIColorFromRGB(0x4d4d4d);
    [back3 addSubview:ResultLab];
    _ResultTextView = [[PHATextView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(ResultLab.frame), kSCREEN_WIDTH-24, 80)];
    _ResultTextView.placeholder = @"输入原因分析";
    _ResultTextView.font = [UIFont systemFontOfSize:15.0];
    _ResultTextView.layer.borderColor = UIColorFromRGB(0xf0f0f0).CGColor;
    _ResultTextView.layer.borderWidth = 1.0f;
    _ResultTextView.layer.cornerRadius = 3;
    _ResultTextView.returnKeyType = UIReturnKeyDone;
    [back3 addSubview:_ResultTextView];
    
    UIView *back4 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back3.frame)+10, kSCREEN_WIDTH, 106)];
    back4.backgroundColor = [UIColor whiteColor];
    [self addSubview:back4];
    UILabel *ChangeIdearLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, kSCREEN_WIDTH-24, 20)];
    ChangeIdearLab.textAlignment = NSTextAlignmentLeft;
    ChangeIdearLab.text = @"整改措施:";
    ChangeIdearLab.font = [UIFont systemFontOfSize:15.0];
    ChangeIdearLab.textColor = UIColorFromRGB(0x4d4d4d);
    [back4 addSubview:ChangeIdearLab];
    _ChangeWaysTextView = [[PHATextView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(ChangeIdearLab.frame), kSCREEN_WIDTH-24, 80)];
    _ChangeWaysTextView.placeholder = @"输入整改措施信息";
    _ChangeWaysTextView.font = [UIFont systemFontOfSize:15.0];
    _ChangeWaysTextView.layer.borderColor = UIColorFromRGB(0xf0f0f0).CGColor;
    _ChangeWaysTextView.layer.borderWidth = 1.0f;
    _ChangeWaysTextView.layer.cornerRadius = 3;
    _ChangeWaysTextView.returnKeyType = UIReturnKeyDone;
    [back4 addSubview:_ChangeWaysTextView];
    
    UIView *back5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back4.frame)+10, kSCREEN_WIDTH, 46)];
    back5.backgroundColor = [UIColor whiteColor];
    [self addSubview:back5];
    UIButton  *CharaUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [CharaUpBtn setTitle:@"提交" forState:UIControlStateNormal];
    CharaUpBtn.backgroundColor = UIColorFromRGB(0x2dd500);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    CharaUpBtn.frame = CGRectMake(kSCREEN_WIDTH/6-10, 3, kSCREEN_WIDTH/3, 40);
    [CharaUpBtn addTarget:self action:@selector(CharaUpBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back5 addSubview:CharaUpBtn];
    UIButton *CharaCancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [CharaCancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    CharaCancelBtn.backgroundColor = UIColorFromRGB(0xd50037);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    CharaCancelBtn.frame = CGRectMake(CGRectGetMaxX(CharaUpBtn.frame)+20, 3, kSCREEN_WIDTH/3, 40);
    [CharaCancelBtn addTarget:self action:@selector(CharaRevokeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back5 addSubview:CharaCancelBtn];

}
- (void)CharaUpBtnClick:(UIButton *)sender{
    [self.delegate CharaUpBtnClick:sender];
}
- (void)CharaRevokeBtnClick:(UIButton *)sender{
    [self.delegate cancelClick:sender];
}


@end
