//
//  AlarmTableViewCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/5.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "AlarmTableViewCell.h"
#import "Masonry.h"
@implementation AlarmTableViewCell
{
    UIImageView *_imageView;
    UILabel *_title;
    UILabel *_state;
    UILabel *_detail;
    UILabel *_time;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *heardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10)];
        heardView.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self.contentView addSubview:heardView];
        _imageView = [[UIImageView alloc]init];
        [self.contentView addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(heardView.mas_bottom).offset(0);
            make.left.equalTo(self.contentView).offset(0);
            make.size.mas_equalTo(CGSizeMake(48, 70));
        }];
        _title = [[UILabel alloc]init];
        //_title.font = [UIFont systemFontOfSize:21];
        UIColor *titleColor = UIColorFromRGB(0x4d4d4d);
        _title.textColor = titleColor;
        [self.contentView addSubview:_title];
        [_title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imageView.mas_right).offset(5);
            make.top.equalTo(heardView.mas_bottom).offset(5);
            make.size.mas_equalTo(CGSizeMake(200, 30));
        }];
        
        UIColor *deColor = UIColorFromRGB(0xb6b6b6);
        _state = [[UILabel alloc]init];
        _state.textAlignment = NSTextAlignmentRight;
        _state.font = [UIFont systemFontOfSize:15];
        _state.textColor = deColor;
        [self.contentView addSubview:_state];
        [_state mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.top.equalTo(heardView.mas_bottom).offset(5);
            make.size.mas_equalTo(CGSizeMake(80, 30));
        }];
        
        _detail = [[UILabel alloc]init];
        _detail.textColor = deColor;
        _detail.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_detail];
        [_detail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imageView.mas_right).offset(7);
            make.top.equalTo(_title.mas_bottom).offset(-3);
            make.size.mas_equalTo(CGSizeMake(170, 30));
        }];
        _time = [[UILabel alloc]init];
        _time.textAlignment = NSTextAlignmentRight;
        _time.font = [UIFont systemFontOfSize:15];
        _time.textColor = deColor;
        [self.contentView addSubview:_time];
        [_time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.top.equalTo(_state.mas_bottom).offset(-3);
            make.size.mas_equalTo(CGSizeMake(90, 30));
            
        }];
    }
    return self;
}

- (void)fillCellWithModel:(YYAlarmListModel *)model{

    if (model.state == 0 ) {
        _imageView.image = [UIImage imageNamed:@"c_red"];
        _state.text = @"未处理";
        _state.textColor = UIColorFromRGB(0xd50037);
    }else if(model.state == 1){
        _imageView.image = [UIImage imageNamed:@"c_yellow"];
        _state.text = @"处理中";
        _state.textColor = UIColorFromRGB(0xF1CA07);
    }else{
        _imageView.image = [UIImage imageNamed:@"c_green"];
        _state.text = @"已处理";
        _state.textColor = UIColorFromRGB(0x2dd500);
    }
    _title.text = model.name;
    _detail.text = model.discrib;
    NSString *time_str = [model.time substringToIndex:10];
    _time.text = time_str;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
