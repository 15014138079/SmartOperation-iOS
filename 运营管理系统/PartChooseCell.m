//
//  PartChooseCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "PartChooseCell.h"
#import "Masonry.h"
@implementation PartChooseCell
{
    UILabel *_Num;//编号
    UILabel *_AllPartNum;//配件总数量
    UILabel *_discreb;//描述
    UIButton *_btn1;//减
    UIButton *_addBtn;//加
    UITextField *_valueTF;//选择配件数量
    UIButton *_btn2;//确认
    NSInteger value;
    NSString *_unit;//单位
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *WLNumb = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 80, 21)];
        WLNumb.text = @"物料编码:";
        WLNumb.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:WLNumb];
        _Num = [[UILabel alloc]init];
        _Num.text = @"123456789";
        _Num.textColor = UIColorFromRGB(0x4d4d4d);
        _Num.font = [UIFont systemFontOfSize:15.0];
        _Num.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_Num];
        [_Num mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.left.equalTo(WLNumb.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake((kSCREEN_WIDTH-80)/2, 21));
        }];
        _AllPartNum = [[UILabel alloc]init];
        _AllPartNum.textColor = UIColorFromRGB(0x4d4d4d);
        _AllPartNum.font = [UIFont systemFontOfSize:15.0];
        _AllPartNum.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_AllPartNum];
        [_AllPartNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake((kSCREEN_WIDTH-80)/2, 21));
        }];
        _discreb = [[UILabel alloc]init];
        _discreb.text = @"我的弗兰克的风格可能分开更好看呢个很难开口就发货法国航空肌肤后来分开就好饭后就离开";
        _discreb.numberOfLines = 0;
        _discreb.textColor = UIColorFromRGB(0xbbbbbb);
        [self.contentView addSubview:_discreb];
        [_discreb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(WLNumb.mas_bottom).offset(10);
            make.left.equalTo(self.contentView).offset(5);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-10, 62));
        }];
        
        _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btn2 setTitle:@"确认" forState:UIControlStateNormal];
        _btn2.backgroundColor = UIColorFromRGB(0x2dd500);
        [_btn2 addTarget:self action:@selector(upValue:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btn2];
        [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_discreb.mas_bottom).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(-15);
            make.size.mas_equalTo(CGSizeMake(80, 40));
        }];

        _btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btn1 setTitle:@"－" forState:UIControlStateNormal];
        [_btn1 setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        [_btn1 addTarget:self action:@selector(subtractClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btn1];
        [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_btn2);
            make.left.equalTo(self.contentView).offset(10);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
        _valueTF = [[UITextField alloc]init];
        _valueTF.text = @"1";
        _valueTF.textAlignment = NSTextAlignmentCenter;
        _valueTF.textColor = UIColorFromRGB(0x0093d5);
        _valueTF.borderStyle = UITextBorderStyleNone;
        _valueTF.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
        _valueTF.layer.borderWidth = 1.0;
        _valueTF.enabled = NO;
        [self.contentView addSubview:_valueTF];
        [_valueTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_btn2);
            make.left.equalTo(_btn1.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(40, 21));
        }];
        
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setTitle:@"＋" forState:UIControlStateNormal];
        [_addBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addBtn];
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_valueTF);
            make.left.equalTo(_valueTF.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
    }
    return self;
}
- (void)fillCellWithModel:(PartMtsModel *)model{
    
    _Num.text = model.logId;
    _discreb.text = model.discrib;
    _unit = model.unit;
    _AllPartNum.text = [NSString stringWithFormat:@"%ld%c%@%c",model.numb,'(',model.unit,')'];

}
- (void)subtractClick{
    
    if ([_valueTF.text isEqualToString:@"1"]) {
        value = 1;
        _valueTF.text = [NSString stringWithFormat:@"%ld",value];
    }else{
        value = value-1;
        _valueTF.text = [NSString stringWithFormat:@"%ld",value];
    }
}
- (void)addClick{
    value++;
    _valueTF.text = [NSString stringWithFormat:@"%ld",value];
}
- (void)upValue:(UIButton *)sender{
    
    NSNotification *notice1 = [NSNotification notificationWithName:@"cellnotice1" object:nil userInfo:@{@"num":_Num.text,@"disc":_discreb.text,@"value":_valueTF.text,@"unit":_unit}];
    [[NSNotificationCenter defaultCenter]postNotification:notice1];
    
    NSNotification *notice2 = [NSNotification notificationWithName:@"cellnotice2" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter]postNotification:notice2];
    

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
