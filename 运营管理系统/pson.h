//
//  pson.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/25.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol pson <NSObject>

@end

@interface pson : JSONModel
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,assign)NSInteger stationNum;
@property (nonatomic,strong)NSString *opCenName;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *post;
@property (nonatomic,assign)float lnt;
@property (nonatomic,assign)float lat;
@end
/*
 "phone": "18229706587", 
 "stationNum": 6,
 "opCenName": "深圳运营中心",
 "name": "易铮",
 "lnt": "",
 "lat": ""
 */
