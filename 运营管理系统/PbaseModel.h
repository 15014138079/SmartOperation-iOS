//
//  PbaseModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "PDataModel.h"
@interface PbaseModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)PDataModel *data;
@end
