//
//  HideNameOrShowPopView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "HideNameOrShowPopView.h"
@interface HideNameOrShowPopView()
{
    __weak IBOutlet UIButton *ShowNameBtn;
    __weak IBOutlet UILabel *ShowNameLab;
    __weak IBOutlet UIButton *HideNameBtn;
    __weak IBOutlet UILabel *HideNameLab;
    NSInteger btntag;
}
@end

@implementation HideNameOrShowPopView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
        self.layer.cornerRadius = 3;
        self.layer.borderColor = UIColorFromRGB(0xeaeaea).CGColor;
        self.layer.borderWidth = 1.0f;
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowColor = UIColorFromRGB(0xb6b6b6).CGColor;
        self.layer.shadowOffset = CGSizeMake(0, 0);
        UIImage *check = [UIImage imageNamed:@"selected_button"];
        UIImage *uncheck = [UIImage imageNamed:@"unchecked_button"];
        [ShowNameBtn setBackgroundImage:uncheck forState:UIControlStateNormal];
        [HideNameBtn setBackgroundImage:uncheck forState:UIControlStateNormal];
        ShowNameBtn.tag = 0;
        HideNameBtn.tag = 1;
        //先初始化btntag
        btntag = ShowNameBtn.tag;
        [ShowNameBtn setBackgroundImage:check forState:UIControlStateSelected];
        [HideNameBtn setBackgroundImage:check forState:UIControlStateSelected];
        
        ShowNameBtn.selected = YES;
        [ShowNameBtn addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
        [HideNameBtn addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)selectedChange:(UIButton *)sender{
    
        btntag = sender.tag;
        //实现单选
        ShowNameBtn.selected = NO;
        HideNameBtn.selected = NO;
        sender.selected = YES;
}
- (IBAction)OKbtn:(id)sender {
    
    [self.delegate OKBtnClick:sender andTag:btntag];
    
}
- (IBAction)CancelBtnClick:(id)sender {
    
    [self.delegate CancelClick:sender andTag:btntag];
    
}
- (void)layoutSubviews{
    
    self.frame = CGRectMake((kSCREEN_WIDTH-245)/2, kSCREEN_HEIGHT, 245, 125);
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.frame = CGRectMake((kSCREEN_WIDTH-245)/2, kSCREEN_HEIGHT/2-64, 245, 125);
    }];
}

@end
