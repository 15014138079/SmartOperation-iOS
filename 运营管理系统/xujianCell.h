//
//  xujianCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "stationModel.h"
@interface xujianCell : UITableViewCell
- (void)fillCellWithModel:(stationModel *)model;
@end
