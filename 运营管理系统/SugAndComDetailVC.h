//
//  SugAndComDetailVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SugDetailView.h"
@interface SugAndComDetailVC : UIViewController<SugDetailViewDelegate>
@property(nonatomic,strong)NSDictionary *detailData;
@end
