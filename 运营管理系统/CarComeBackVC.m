//
//  CarComeBackVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/10.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarComeBackVC.h"
#import "CarBackListCell.h"
#import "CarComeBackDTVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "Carlist.h"//模型
@interface CarComeBackVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@end

@implementation CarComeBackVC
- (AFHTTPSessionManager *)manager{
    
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"车辆归还列表";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    _dataSource = [[NSMutableArray alloc]init];
    
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.userId = delegate.userId;
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.rowHeight = 60;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //去掉分割线
//    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    //去掉多余的cell
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 500)];
    v.backgroundColor = UIColorFromRGB(0xe6e6e6);
    [self.tableView setTableFooterView:v];
    [self.view addSubview:_tableView];
   
    
}
/*
- (void)load{
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_carBackList parameters:@{userid:[NSString stringWithFormat:@"%ld",self.userId]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];

        NSArray *list = dic[@"data"][@"carlist"];
        for (NSInteger i = 0; i < list.count; ++i) {
            Carlist *model = [[Carlist alloc]init];
            model.Brand = list[i][@"brand"];
            model.BuyTime = list[i][@"buyT"];
            model.plateNum = list[i][@"plateNum"];
            model.carID = [list[i][@"id"]integerValue];
            model.Cmodel = list[i][@"model"];
            model.repName = list[i][@"repName"];
            
            [_dataSource addObject:model];
        }
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarBackListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[CarBackListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    Carlist *model = _dataSource[indexPath.section];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Carlist *model = _dataSource[indexPath.section];
    CarComeBackDTVC *vc = [[CarComeBackDTVC alloc]init];
    vc.carId = model.carID;
    vc.band = model.Brand;
    vc.Cmodel = model.Cmodel;
    vc.repName = model.repName;
    vc.plateNum = model.plateNum;
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)back{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"您是否确定退出当前页面" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *OkAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:cancelAction];
    [alert addAction:OkAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
