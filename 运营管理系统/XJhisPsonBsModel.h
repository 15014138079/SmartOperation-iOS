//
//  XJhisPsonBsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "XJhisPsonDataModel.h"
@interface XJhisPsonBsModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)XJhisPsonDataModel *data;
@end
