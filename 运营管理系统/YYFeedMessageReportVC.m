//
//  YYFeedMessageReportVC.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "YYFeedMessageReportVC.h"

@interface YYFeedMessageReportVC ()
{
    UIScrollView *_scrollview;
    NSDictionary *_UsersDic;
    NSString *_upUrl;
    NSDictionary *_premater;
    NSInteger userID;
    UIButton *_BsPsonBtn;//报送人
    UIButton *_TePsonBtn;//技术支持签收人
    UIButton *_CharaPsonBtn;//品质签收人
    NSMutableArray *_BSmessageArr;
    CGFloat BSviewHeight;
    CGFloat TechHeight;
    NSString *_NowTime;
    
    FallLineView *_Fallview;
    JoinFaluterView *_JoinFalutView;
    ServerUnusualView *_ServerFView;
}
@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(nonatomic,strong)TechniqueUPView *techniqueView;
@property(nonatomic,strong)CharacterUPView *characterView;
@property(nonatomic,strong)UILabel *BSlab;
@property(nonatomic,strong)UILabel *TePsonLab;
@property(nonatomic,strong)UILabel *MassLab;

@property(nonatomic,strong)BsMessageDetailView *bsView;//报送信息
@property(nonatomic,strong)TechMessageView *techMsgView;//技术支持信息
@property(nonatomic,strong)MassMsgView *CLmsgView;//品质负责人处理信息
@property(nonatomic,strong)NSArray *imageArray;
@end

@implementation YYFeedMessageReportVC
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (NSString *)filePath{
    NSString *documentFilePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filetPath = [documentFilePath stringByAppendingPathComponent:@"Log.txt"];
    return filetPath;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"质量信息报告单";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemClick)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    _UsersDic = [NSDictionary dictionaryWithContentsOfFile:[self filePath]];
    NSDictionary *dic = @{@"userName":[_UsersDic objectForKey:@"username"],@"phone":[_UsersDic objectForKey:@"tel"]};
    userID = [[_UsersDic objectForKey:@"userid"]integerValue];

    _BSmessageArr = [[NSMutableArray alloc]init];
    
    _scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
    _scrollview.showsVerticalScrollIndicator = NO;
    _scrollview.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_scrollview];
    if (self.state == 3) {
        _techniqueView = [[TechniqueUPView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 257) AndMessageDic:dic];
        _techniqueView.delegate = self;
        [_scrollview addSubview:_techniqueView];
        _CharaPsonBtn.userInteractionEnabled = NO;
    }else{
        
        _characterView = [[CharacterUPView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 469) AndMessageDic:dic];
        _characterView.delegate = self;
        [_scrollview addSubview:_characterView];
        _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, 540);
    }
    
    self.BSlab = [[UILabel alloc]initWithFrame:CGRectMake(-2, kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3+2, 60)];
    self.BSlab.backgroundColor = [UIColor whiteColor];
    self.BSlab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.BSlab.layer.borderWidth = 1.0f;
    //BSlab.layer.cornerRadius = 5;
    self.BSlab.font = [UIFont systemFontOfSize:13.0];
    self.BSlab.textColor = UIColorFromRGB(0xbfbfc4);
    self.BSlab.numberOfLines = 0;
    self.BSlab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.BSlab];
    _BsPsonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _BsPsonBtn.frame = self.BSlab.frame;
    [_BsPsonBtn addTarget:self action:@selector(BsBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_BsPsonBtn];
    
    self.TePsonLab = [[UILabel alloc]initWithFrame:CGRectZero];
    self.TePsonLab.backgroundColor = [UIColor whiteColor];
    self.TePsonLab.font = [UIFont systemFontOfSize:13.0];
    self.TePsonLab.layer.borderWidth = 1.0f;
    self.TePsonLab.numberOfLines = 0;
    self.TePsonLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.TePsonLab];
    
    self.MassLab = [[UILabel alloc]initWithFrame:CGRectZero];
    self.MassLab.backgroundColor = [UIColor whiteColor];
    self.MassLab.layer.borderWidth = 1.0f;
    self.MassLab.font = [UIFont systemFontOfSize:13.0];
    self.MassLab.numberOfLines = 0;
    self.MassLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.MassLab];

    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    _NowTime = [dateFormatter stringFromDate:currentDate];
    
    if (self.state == 3) {
        self.TePsonLab.frame = CGRectMake(CGRectGetMaxX(self.BSlab.frame), kSCREEN_HEIGHT-60, kSCREEN_WIDTH/3, 63);
        self.TePsonLab.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
        self.TePsonLab.layer.cornerRadius = 5;
        self.TePsonLab.textColor = UIColorFromRGB(0x4d4d4d);
        self.MassLab.frame = CGRectMake(CGRectGetMaxX(self.TePsonLab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3+1, 60);
        self.MassLab.text = @"品质部签收人:\n日期:";
        self.MassLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
        self.MassLab.textColor = UIColorFromRGB(0xbfbfc4);
    }else{
        self.TePsonLab.frame = CGRectMake(CGRectGetMaxX(self.BSlab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3, 60);
        self.TePsonLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
        self.TePsonLab.font = [UIFont systemFontOfSize:13.0];
        self.TePsonLab.textColor = UIColorFromRGB(0xbfbfc4);
        self.MassLab.frame = CGRectMake(CGRectGetMaxX(self.TePsonLab.frame), kSCREEN_HEIGHT-60, kSCREEN_WIDTH/3+1, 63);
        self.MassLab.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
        self.MassLab.layer.cornerRadius = 5.0;
        self.MassLab.textColor = UIColorFromRGB(0x4d4d4d);
    }

    _TePsonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _TePsonBtn.frame = self.TePsonLab.frame;
    [_TePsonBtn addTarget:self action:@selector(TePsonBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_TePsonBtn];
    
    _CharaPsonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _CharaPsonBtn.frame = self.MassLab.frame;
    [_CharaPsonBtn addTarget:self action:@selector(CharaPsonBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_CharaPsonBtn];
    if (self.state == 4) {
        _CharaPsonBtn.userInteractionEnabled = YES;
    }else{
        _CharaPsonBtn.userInteractionEnabled = NO;
    }
    [self BsMsgNetWorking];
}
//报送信息view
- (void)BsMsgNetWorking{
    [_Fallview removeFromSuperview];
    [_ServerFView removeFromSuperview];
    [_JoinFalutView removeFromSuperview];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"加载中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:FeedBackDtURL parameters:@{@"qualityId":[NSString stringWithFormat:@"%ld",self.feedBackID]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        FeedBackBeanModel *model = [[FeedBackBeanModel alloc]initWithDictionary:dic[@"data"][@"bean"] error:nil];
        [_BSmessageArr addObject:model];
        NSString *BStime = [model.receiveTime substringToIndex:10];
        self.BSlab.text = [NSString stringWithFormat:@"报送人:%@\n日期:%@",model.userN,BStime];
        if (self.state == 3) {
            [self initBSmsgView];
            self.TePsonLab.text = [NSString stringWithFormat:@"技术支持签收人:\n%@\n日期:%@",[_UsersDic objectForKey:@"username"],_NowTime];
        }else if (self.state == 4){
            [self initBSmsgView];
            [self initTechMsgView];
            NSString *TeTime = [model.artisanTime substringToIndex:10];
            self.TePsonLab.text = [NSString stringWithFormat:@"技术支持签收人:\n%@\n日期:%@",model.artisanName,TeTime];
            self.MassLab.text = [NSString stringWithFormat:@"品质部签收人:\n%@\n日期:%@",[_UsersDic objectForKey:@"username"],_NowTime];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        self.BSlab.alpha = 0;
//        self.TePsonLab.alpha = 0;
//        self.MassLab.alpha = 0;
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        if (status >= 500) {
            _ServerFView = [[ServerUnusualView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) AndTitle:[error userInfo][@"NSLocalizedDescription"]];
            _ServerFView.delegate = self;
            [self.view addSubview:_ServerFView];
        }else if ([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"请求超时。"]) {
            _Fallview = [[FallLineView alloc]initWithFrame:CGRectMake(0,64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _Fallview.delegate = self;
            [self.view addSubview:_Fallview];
        }else if([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"似乎已断开与互联网的连接。"]){
            _JoinFalutView = [[JoinFaluterView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _JoinFalutView.delegate = self;
            [self.view addSubview:_JoinFalutView];
        }else{
            [self showAlert:@"温馨提示" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
    }];
}
/*
 失败页面代理方法
 */
- (void)ServerRefresh:(UIButton *)sender{
    [self BsMsgNetWorking];
}
- (void)FallLineRefresh:(UIButton *)sender{
    [self BsMsgNetWorking];
}
- (void)JoinRefersh:(UIButton *)sender{
    [self BsMsgNetWorking];
}
//实例化报送信息的view
- (void)initBSmsgView{
    
    FeedBackBeanModel *model = _BSmessageArr[0];
    NSDictionary *dic = @{@"pson":model.userN,@"tel":model.userTel,@"findtime":model.time,@"findaddress":model.stationAddress,@"custname":model.entName,@"equipNum":model.code,@"typename":model.typeName,@"equipname":model.equipName,@"equipmodel":model.equipModel,@"value":[NSString stringWithFormat:@"%ld台",model.num],@"mainGZ":model.faultDisc,@"discrb":model.discrb};
    
    
    self.imageArray = [model.images componentsSeparatedByString:@","];
    /*
     报送信息view
     */
    _bsView = [[BsMessageDetailView alloc]initWithFrame:CGRectMake(0,10, kSCREEN_WIDTH, 300) AndDataDictionray:dic MaxY:10 WithImageUrl:self.imageArray];
    _bsView.backgroundColor = [UIColor whiteColor];
    _bsView.delegate = self;
    _bsView.alpha = 0;
    [_scrollview addSubview:_bsView];
    
}
#pragma mark-->BsMessageDetailViewDelegate
- (void)didSelectedImageURL:(NSArray *)arr AndIndex:(NSInteger)index{
    AlbumVC *alVC = [[AlbumVC alloc]init];
    alVC.ImageUrl = arr;
    alVC.index = index;
    [self.navigationController pushViewController:alVC animated:YES];
}
- (void)returnBSMessageViewHeight:(CGFloat)height{
    
    BSviewHeight = height;
//    _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, height+100);
}
/*
 报送信息view的状态
 */
- (void)dismissBSmsgView{
    
    _bsView.alpha = 0;
}
- (void)showBSmsgView{
    
    _bsView.alpha = 1;
}

/*
 技术支持信息view
 */
- (void)initTechMsgView{
    
    FeedBackBeanModel *model = _BSmessageArr[0];
    NSDictionary *techDic = @{@"pson":model.artisanName,@"tel":@"12345678911",@"clmessage":model.suggestion,@"time":model.artisanTime};
    _techMsgView = [[TechMessageView alloc]initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 300) AndDiction:techDic MaxY:10];
    _techMsgView.delegate = self;
    _techMsgView.backgroundColor = [UIColor whiteColor];
    _techMsgView.alpha = 0;
    [_scrollview addSubview:_techMsgView];
    
}
/*
 技术支持信息view的状态
 */
- (void)dismissTechMsgView{
    
    _techMsgView.alpha = 0;
}
- (void)showTechMsgView{
    _techMsgView.alpha = 1;
}
- (void)returnTechMessageViewHeight:(CGFloat)height{
    TechHeight = height;
}
- (void)BsBtnClick:(UIButton *)sender{
    
    [self showBSmsgView];
    _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, BSviewHeight+100);
    if (self.state == 3) {
        _techniqueView.alpha = 0;
    }else if(self.state == 4){
        
        [self dismissTechMsgView];
        _characterView.alpha = 0;
    }
    
    self.BSlab.frame = CGRectMake(-2, kSCREEN_HEIGHT-60, kSCREEN_WIDTH/3+2, 63);
    self.BSlab.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    self.BSlab.layer.cornerRadius = 5;
    self.BSlab.textColor = UIColorFromRGB(0x4d4d4d);
    self.TePsonLab.frame = CGRectMake(CGRectGetMaxX(self.BSlab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3, 60);
    self.TePsonLab.backgroundColor = [UIColor whiteColor];
    self.TePsonLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.TePsonLab.layer.borderWidth = 1.0f;
    self.TePsonLab.font = [UIFont systemFontOfSize:13.0];
    self.TePsonLab.numberOfLines = 0;
    self.TePsonLab.textColor = UIColorFromRGB(0xbfbfc4);
    self.TePsonLab.layer.cornerRadius = 0;
   
    self.MassLab.frame = CGRectMake(CGRectGetMaxX(self.TePsonLab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3, 60);
    self.MassLab.backgroundColor = [UIColor whiteColor];
    self.MassLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.MassLab.textColor = UIColorFromRGB(0xbfbfc4);
    self.MassLab.layer.cornerRadius = 0;
}
- (void)TePsonBtnClick:(UIButton *)sender{
    
    _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, TechHeight+100);
    [self dismissBSmsgView];
    if (self.state == 3) {
       _techniqueView.alpha = 1;
    }else if (self.state == 4){
        _characterView.alpha = 0;
        [self showTechMsgView];
    }
    
    self.BSlab.frame = CGRectMake(-2, kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3+2, 60);
    self.BSlab.backgroundColor = [UIColor whiteColor];
    self.BSlab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.BSlab.layer.cornerRadius = 0;
    self.BSlab.textColor = UIColorFromRGB(0xbfbfc4);

    
    self.TePsonLab.frame = CGRectMake(CGRectGetMaxX(self.BSlab.frame), kSCREEN_HEIGHT-60, kSCREEN_WIDTH/3, 63);
    self.TePsonLab.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    self.TePsonLab.layer.cornerRadius = 5;
    self.TePsonLab.textColor = UIColorFromRGB(0x4d4d4d);
    
    self.MassLab.frame = CGRectMake(CGRectGetMaxX(self.TePsonLab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3, 60);
    self.MassLab.backgroundColor = [UIColor whiteColor];
    self.MassLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.MassLab.textColor = UIColorFromRGB(0xbfbfc4);
    self.MassLab.layer.cornerRadius = 0;
    
}
- (void)CharaPsonBtnClick:(UIButton *)sender{
    
    [self dismissTechMsgView];
    [self dismissBSmsgView];
    _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, 540);
    _characterView.alpha = 1;
    self.BSlab.frame = CGRectMake(-2, kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3+2, 60);
    self.BSlab.backgroundColor = [UIColor whiteColor];
    self.BSlab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.BSlab.layer.cornerRadius = 0;
    self.BSlab.textColor = UIColorFromRGB(0xbfbfc4);
    
    self.TePsonLab.frame = CGRectMake(CGRectGetMaxX(self.BSlab.frame), kSCREEN_HEIGHT-58, kSCREEN_WIDTH/3, 60);
    self.TePsonLab.backgroundColor = [UIColor whiteColor];
    self.TePsonLab.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.TePsonLab.textColor = UIColorFromRGB(0xbfbfc4);
    self.TePsonLab.layer.cornerRadius = 0;
    
    self.MassLab.frame = CGRectMake(CGRectGetMaxX(self.TePsonLab.frame), kSCREEN_HEIGHT-60, kSCREEN_WIDTH/3, 63);
    self.MassLab.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    self.MassLab.layer.cornerRadius = 5;
    self.MassLab.textColor = UIColorFromRGB(0x4d4d4d);
}
- (void)Networking{

    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *timeStr = [dateFormatter stringFromDate:currentDate];
    
    if (self.state == 3) {
        _upUrl = FeedBackTechniqueMsgURL;
        _premater = @{@"suggestion":_techniqueView.DiscribTextView.text,@"time":timeStr,@"id":[NSString stringWithFormat:@"%ld",self.feedBackID],@"userId":[NSString stringWithFormat:@"%ld",userID]};
        if (_techniqueView.DiscribTextView.text.length < 1) {
            [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"我知道了"];
        }else{
            [self upMessages:_premater];
        }
    }else{
        _upUrl = FeedBackCharactMsgURL;
        _premater = @{@"scheme":_characterView.BidCLIdearTextView.text,@"id":[NSString stringWithFormat:@"%ld",self.feedBackID],@"causes":_characterView.ChangeWaysTextView.text,@"measures":_characterView.ResultTextView.text,@"time":timeStr,@"userId":[NSString stringWithFormat:@"%ld",userID]};
        if (_characterView.BidCLIdearTextView.text.length < 1 || _characterView.ChangeWaysTextView.text.length < 1 || _characterView.ResultTextView.text.length < 1) {
            
            [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"我知道了"];
        }else{
            [self upMessages:_premater];
        }
    }
}
- (void)upMessages:(NSDictionary *)prameter{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"提交中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:_upUrl parameters:_premater progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"status"]integerValue]==1) {
            //延时操作
            [UIView animateWithDuration:0 delay:2.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                
                [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
                
            } completion:^(BOOL finished) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [self showAlert:@"提交失败" message:dic[@"message"] actionWithTitle:@"我知道了"];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        //        NSSLog(@"%ld",(long)responses.statusCode);
        if (status >=500 || status == 0) {
            [self showAlert:@"提交失败" message:@"服务器出错" actionWithTitle:@"我知道了"];
        }else{
            [self showAlert:@"提交失败" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
    }];

}
/**
    技术支持中的提交取消点击事件
 */
- (void)TechUpBtnClick:(UIButton *)sender{
    [self Networking];
}
- (void)TechRevokeClick:(UIButton *)sender{
    
    [self leftItemClick];
}
/**
    品质支持中的提交取消点击事件
 */

- (void)CharaUpBtnClick:(UIButton *)sender{
    [self Networking];
}
- (void)cancelClick:(UIButton *)sender{
    [self leftItemClick];
}

/*
    Item点击事件
 */
- (void)leftItemClick{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"您是否确定退出当前页面" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *OkAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:cancelAction];
    [alert addAction:OkAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark 封装提示框
- (void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (UILabel *)BSlab{
    
    if (!_BSlab) {
        _BSlab = [[UILabel alloc]init];
    }
    return _BSlab;
}
- (UILabel *)TePsonLab{
    if (!_TePsonLab) {
        _TePsonLab = [[UILabel alloc]init];
    }
    return _TePsonLab;
}
- (UILabel *)MassLab{
    if (!_MassLab) {
        _MassLab = [[UILabel alloc]init];
    }
    return _MassLab;
}
@end
