//
//  ShowTimeViewController.h
//  iOS 自定义时间轴
//
//  Created by Apple on 16/7/26.
//  Copyright © 2016年 zls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightSlidetableViewCell.h"
#import "TimeModel.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "GZshenpiVC.h"
#import "canshuModel.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "FallLineView.h"//连接超时
#import "JoinFaluterView.h"//连接失败
#import "ServerUnusualView.h"//服务器出错
#import "NothingDataView.h"
@interface ShowTimeViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,FallLineDelegate,JoinFalutDelegate,ServerUnsualDelegate,NothingViewDelegate>
@property(nonatomic,strong)NSString *userID;
@end
