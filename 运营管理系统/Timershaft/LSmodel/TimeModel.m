//
//  TimeModel.m
//  iOS 自定义时间轴
//
//  Created by Apple on 16/7/26.
//  Copyright © 2016年 zls. All rights reserved.
//

#import "TimeModel.h"

@implementation TimeModel
-(instancetype)initData:(NSDictionary*)dic
{
    if (self=[super init]) {
        self.timeStr=[dic objectForKey:@"timeStr"];
        self.titleStr=[dic objectForKey:@"titleStr"];
        self.averageScoreStr=[dic objectForKey:@"averageScoreStr"];
        self.state = [[dic objectForKey:@"state"]integerValue];
    }
    return self;
}
@end
