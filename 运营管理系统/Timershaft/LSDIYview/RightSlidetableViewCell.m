//
//  RightSlidetableViewCell.m
//  iOS 自定义时间轴
//
//  Created by Apple on 16/7/26.
//  Copyright © 2016年 zls. All rights reserved.
//

#import "RightSlidetableViewCell.h"

@implementation RightSlidetableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initView];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
//布局
-(void)initView
{
    //竖线
    self.verticalLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,0,2,62)];
    self.verticalLabel.backgroundColor=[UIColor lightGrayColor];
    [self.contentView addSubview:self.verticalLabel];
    
    //圆圈
    self.circleView=[[UIImageView alloc]initWithFrame:CGRectMake(53,24,16,16)];
    self.circleView.layer.cornerRadius=8;
    self.circleView.backgroundColor=[UIColor whiteColor];
    self.circleView.layer.borderWidth = 1.0;
    self.circleView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.contentView addSubview:self.circleView];
    
    
    CGFloat circleWidth=CGRectGetMaxX(self.circleView.frame);
    //时间
    self.timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(circleWidth-65,22,120,21)];
    self.timeLabel.text=@"9/18";
    //self.timeLabel.font=[UIFont systemFontOfSize:17.0];
    [self.contentView addSubview:self.timeLabel];
    
    CGFloat timeWidth=CGRectGetMaxX(self.timeLabel.frame);
    //分数
    self.scoreLabel=[[UILabel alloc]initWithFrame:CGRectMake(circleWidth+5,5,kSCREEN_WIDTH-timeWidth-10,35)];
    self.scoreLabel.text=@"故障名称";
    //self.scoreLabel.font=[UIFont systemFontOfSize:17.0];
    self.scoreLabel.textColor = UIColorFromRGB(0x4d4d4d);
    [self.contentView addSubview:self.scoreLabel];
    //右边logo
    self.rightIcon = [[UIImageView alloc]initWithFrame:CGRectMake(kSCREEN_WIDTH/12*11-10, 20, 23, 27)];
    [self.contentView addSubview:self.rightIcon];
    //标题
    self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(circleWidth+5,35,kSCREEN_WIDTH-circleWidth-10,30)];
    self.titleLabel.text=@"标题:关于zls是不是宇宙最帅的讨论";
    self.titleLabel.font=[UIFont systemFontOfSize:15];
    self.titleLabel.textColor = UIColorFromRGB(0xb6b6b6);
    self.titleLabel.numberOfLines=0;
    [self.contentView addSubview:self.titleLabel];
}
//赋值
-(void)setData:(TimeModel *)timeModel
{
    self.timeLabel.text=timeModel.timeStr;
    //故障标题
    self.scoreLabel.text=timeModel.titleStr;
    //故障内容
    self.titleLabel.text=[NSString stringWithFormat:@"%@",timeModel.averageScoreStr];
    if (timeModel.state == 0) {
        
        self.rightIcon.image = [[UIImage imageNamed:@"mail"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        //重新绘制大小
        self.rightIcon.frame = CGRectMake(kSCREEN_WIDTH/12*11-10, 20, 23, 17);
    }else if (timeModel.state == 1){
        self.rightIcon.image = [[UIImage imageNamed:@"mail_1"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
//    self.rightIcon.image = [UIImage imageNamed:timeModel.iconName];
//    NSDictionary *fontDic=@{NSFontAttributeName:[UIFont systemFontOfSize:18]};
//    CGSize size1=CGSizeMake(kSCREEN_WIDTH,0);
//    CGSize titleLabelSize=[timeModel.averageScoreStr boundingRectWithSize:size1 options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading   attributes:fontDic context:nil].size;
    


//    if (titleLabelSize.height<40) {
//    }else{ //重新绘制高度
//
//        //最终的高度
//        CGFloat finalHeight=titleLabelSize.height+20;
//        //绘制线
//        self.verticalLabel.frame=CGRectMake(80,0,2,finalHeight);
//        self.circleView.frame=CGRectMake(73,5,16,16);
//    
//        //标题重新设置
//        self.titleLabel.frame=CGRectMake(CGRectGetMaxX(self.circleView.frame)+20,35,kSCREEN_WIDTH-35, titleLabelSize.height);
//    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
