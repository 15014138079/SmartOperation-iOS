//
//  LeftSlidertableViewCell.m
//  iOS 自定义时间轴
//
//  Created by Apple on 16/7/26.
//  Copyright © 2016年 zls. All rights reserved.
//

#import "LeftSlidertableViewCell.h"

@implementation LeftSlidertableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initView];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
//布局
-(void)initView
{
    //竖线
    self.verticalLabel=[[UILabel alloc]initWithFrame:CGRectMake(kSCREEN_WIDTH/2-1,0,2,80)];
    self.verticalLabel.backgroundColor=[UIColor lightGrayColor];
    [self.contentView addSubview:self.verticalLabel];
    
    //圆圈
    self.circleView=[[UIImageView alloc]initWithFrame:CGRectMake(kSCREEN_HEIGHT/2-5,35,10,10)];
    self.circleView.layer.cornerRadius=5;
    self.circleView.backgroundColor=[UIColor orangeColor];
    [self.contentView addSubview:self.circleView];
    
    
//    CGFloat circleWidth=CGRectGetMaxX(self.circleView.frame);
    //时间
    self.timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(kSCREEN_WIDTH/8*3,25,kSCREEN_WIDTH/4,30)];
    self.timeLabel.text=@"2016-07-26";
    self.timeLabel.font=[UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.timeLabel];
    
//    CGFloat timeWidth=CGRectGetMaxX(self.timeLabel.frame);
    //分数
    self.scoreLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,5,kSCREEN_WIDTH/2,30)];
    self.scoreLabel.text=@"分数:100";
    self.scoreLabel.textColor = UIColorFromRGB(0x4d4d4d);
    self.timeLabel.font=[UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.scoreLabel];
    
    //标题
    self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,45,kSCREEN_WIDTH/2,30)];
    self.titleLabel.text=@"标题:关于zls是不是宇宙最帅的讨论";
    self.titleLabel.font=[UIFont systemFontOfSize:16];
    self.titleLabel.textColor = UIColorFromRGB(0xb6b6b6);
    [self.contentView addSubview:self.titleLabel];
}
//赋值
-(void)setData:(TimeModel *)timeModel
{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
