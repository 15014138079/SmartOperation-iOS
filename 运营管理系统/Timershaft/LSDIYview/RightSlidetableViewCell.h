//
//  RightSlidetableViewCell.h
//  iOS 自定义时间轴
//
//  Created by Apple on 16/7/26.
//  Copyright © 2016年 zls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeModel.h"
@interface RightSlidetableViewCell : UITableViewCell

@property(strong,nonatomic) UILabel *verticalLabel ;//竖线
@property(strong,nonatomic) UIImageView *circleView; //圈
@property(strong,nonatomic) UILabel *timeLabel; //时间
@property(strong,nonatomic) UILabel *titleLabel; //标题
@property(strong,nonatomic) UILabel *scoreLabel; //分数
@property(strong,nonatomic) UIImageView *rightIcon;//图标
//赋值
-(void)setData:(TimeModel*)timeModel;

@end
