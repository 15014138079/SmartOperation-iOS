//
//  PDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "PartMtsModel.h"
@interface PDataModel : JSONModel
@property (nonatomic,strong)NSArray <PartMtsModel> *mtstock;
@end
