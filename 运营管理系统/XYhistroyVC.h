//
//  XYhistroyVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYhistroyVC : UIViewController
@property(nonatomic,assign)NSInteger userID;
@property (nonatomic,assign)NSInteger stationId;
@property (nonatomic,strong)NSMutableArray *stationMessageArray;
@end
