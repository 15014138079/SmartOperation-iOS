//
//  MassMsgView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MassMsgView;
@protocol MassMsgViewDelegate <NSObject>

- (void)returnMassMsgViewHeight:(CGFloat)height;

@end
@interface MassMsgView : UIView
- (instancetype)initWithFrame:(CGRect)frame AndDiction:(NSDictionary *)dic WithMaxY:(CGFloat)Y;
@property(nonatomic,weak)id <MassMsgViewDelegate> delegate;
@end
