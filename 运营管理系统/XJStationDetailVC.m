//
//  XJStationDetailVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJStationDetailVC.h"
#import "XJhisStationListModel.h"
@interface XJStationDetailVC ()
{
    XJhisStationListModel *model;
}
@property (nonatomic,strong)UIView *StationMessageView;
@property (nonatomic,strong)UIView *CompanyMessageView;
@end

@implementation XJStationDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);

    NSMutableArray *arr1 = [[NSMutableArray alloc]init];
    [arr1 addObject:[self.MessageArray firstObject]];
    //NSArray *arr = [self.MessageArray firstObject];
 
    //NSSLog(@"%@",[self.MessageArray firstObject]);
    model = [[XJhisStationListModel alloc]init];
    model = [arr1 firstObject];
    
    [self initSroler];
    

}
- (void)initSroler{
    UIScrollView *scroler = [[UIScrollView alloc]init];
    scroler.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64);
    scroler.backgroundColor = UIColorFromRGB(0xf0f0f0);
    // scroler.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT);
    [self.view addSubview:scroler];
    
    //站点信息
    self.StationMessageView.backgroundColor = [UIColor whiteColor];
    [scroler addSubview:self.StationMessageView];
    UIImageView *StationTitleImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"d_icon01"]];
    StationTitleImage.frame = CGRectMake(5, 5, 20, 20);
    [self.StationMessageView addSubview:StationTitleImage];
    UILabel *StationTitle = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, kSCREEN_WIDTH-30, 21)];
    StationTitle.text = @"站点信息";
    StationTitle.textColor = UIColorFromRGB(0x0093d5);
    [self.StationMessageView addSubview:StationTitle];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(StationTitle.frame)+5, kSCREEN_WIDTH, 1)];
    line.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.StationMessageView addSubview:line];
    UILabel *StationName = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(line.frame)+5, 90, 17)];
    StationName.text = @"站 点 名 称";
    StationName.textColor = UIColorFromRGB(0x4d4d4d);
    StationName.font = [UIFont systemFontOfSize:15.0];
    [self.StationMessageView addSubview:StationName];
    UILabel *StationNContent = [[UILabel alloc]initWithFrame:CGRectZero];
    StationNContent.textColor = UIColorFromRGB(0x4d4d4d);
    StationNContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    StationNContent.text = model.stationName;
    StationNContent.font = [UIFont systemFontOfSize:15.0];
    StationNContent.numberOfLines=0;
    CGRect SNRect = [StationNContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-115, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:StationNContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat SNHeight = SNRect.size.height;
    StationNContent.frame = CGRectMake(110, CGRectGetMaxY(line.frame)+5, kSCREEN_WIDTH-115, SNHeight);
    [self.StationMessageView addSubview:StationNContent];
    
    UILabel *STaddress = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(StationNContent.frame)+5, 90, 17)];
    STaddress.text = @"站 点 地 址";
    STaddress.textColor = UIColorFromRGB(0x4d4d4d);
    STaddress.font = [UIFont systemFontOfSize:15.0];
    [self.StationMessageView addSubview:STaddress];
    UILabel *STaddressContent = [[UILabel alloc]initWithFrame:CGRectZero];
    STaddressContent.numberOfLines = 0;
    STaddressContent.textColor = UIColorFromRGB(0x4d4d4d);
    STaddressContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    STaddressContent.font = [UIFont systemFontOfSize:15.0];
    STaddressContent.text = model.stationAddress;
    CGRect STAddressRect = [STaddressContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-115, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:STaddressContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat STaddressHeight = STAddressRect.size.height;
    STaddressContent.frame = CGRectMake(110, CGRectGetMaxY(StationNContent.frame)+5, kSCREEN_WIDTH-115, STaddressHeight);
    [self.StationMessageView addSubview:STaddressContent];
    NSArray *arr = @[model.stationType,model.principalName,model.principalPhone,[NSString stringWithFormat:@"%.2f元",model.monthCost]];
    NSArray *titArr = @[@"检 测 类 型",@"负  责  人",@"负责人电话",@"月 运 营 费"];
    for (NSInteger i = 0; i < titArr.count; i++) {
        UILabel *titlab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(STaddressContent.frame)+5+i*(17+5), 90, 17)];
        titlab.text = titArr[i];
        titlab.textColor = UIColorFromRGB(0x4d4d4d);
        titlab.font = [UIFont systemFontOfSize:15.0];
        [self.StationMessageView addSubview:titlab];
        UILabel *rightContLab = [[UILabel alloc]initWithFrame:CGRectMake(110,CGRectGetMaxY(STaddressContent.frame)+5+i*(17+5) , kSCREEN_WIDTH-115, 17)];
        rightContLab.textColor = UIColorFromRGB(0x4d4d4d);
        rightContLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
        rightContLab.font = [UIFont systemFontOfSize:15.0];
        rightContLab.text = arr[i];
        [self.StationMessageView addSubview:rightContLab];
        self.StationMessageView.frame = CGRectMake(0, 5, kSCREEN_WIDTH, CGRectGetMaxY(rightContLab.frame)+5);
    }
    //公司信息
    self.CompanyMessageView.backgroundColor = [UIColor whiteColor];
    [scroler addSubview:self.CompanyMessageView];
    UIImageView *CompTitImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"d_icon01"]];
    CompTitImage.frame = CGRectMake(5, 5, 20, 20);
    [self.CompanyMessageView addSubview:CompTitImage];
    UILabel *CompanyMessageTitle = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, kSCREEN_WIDTH-30, 21)];
    CompanyMessageTitle.text = @"公司信息";
    CompanyMessageTitle.textColor = UIColorFromRGB(0x0093d5);
    [self.CompanyMessageView addSubview:CompanyMessageTitle];
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(CompanyMessageTitle.frame)+5, kSCREEN_WIDTH, 1)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.CompanyMessageView addSubview:line1];
    
    UILabel *companyName = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(line1.frame)+5, 90, 17)];
    companyName.text = @"公 司 名 称";
    companyName.textColor = UIColorFromRGB(0x4d4d4d);
    companyName.font = [UIFont systemFontOfSize:15.0];
    [self.CompanyMessageView addSubview:companyName];
    UILabel *CompanyNcontent = [[UILabel alloc]initWithFrame:CGRectZero];
    CompanyNcontent.textColor = UIColorFromRGB(0x4d4d4d);
    CompanyNcontent.text = model.companyName;
    CompanyNcontent.font = [UIFont systemFontOfSize:15.0];
    CompanyNcontent.numberOfLines = 0;
    CompanyNcontent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect ComNcontRect = [CompanyNcontent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-115, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes: [NSDictionary dictionaryWithObjectsAndKeys:CompanyNcontent.font,NSFontAttributeName, nil]  context:nil];
    CGFloat ComNHeight = ComNcontRect.size.height;
    CompanyNcontent.frame = CGRectMake(110, CGRectGetMaxY(line1.frame)+5, kSCREEN_WIDTH-115, ComNHeight);
    [self.CompanyMessageView addSubview:CompanyNcontent];
    
    UILabel *CompanyAddressTit = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(CompanyNcontent.frame)+5, 90, 17)];
    CompanyAddressTit.text = @"公 司 地 址";
    CompanyAddressTit.textColor = UIColorFromRGB(0x4d4d4d);
    CompanyAddressTit.font = [UIFont systemFontOfSize:15.0];
    [self.CompanyMessageView addSubview:CompanyAddressTit];
    UILabel *ComAddressContent = [[UILabel alloc]initWithFrame:CGRectZero];
    ComAddressContent.textColor = UIColorFromRGB(0x4d4d4d);
    ComAddressContent.text = model.companyAddress;
    ComAddressContent.numberOfLines = 0;
    ComAddressContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    ComAddressContent.font = [UIFont systemFontOfSize:15.0];
    CGRect ComAdressRect = [ComAddressContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-115, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:ComAddressContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat ComAdresHeight = ComAdressRect.size.height;
    ComAddressContent.frame = CGRectMake(110, CGRectGetMaxY(CompanyNcontent.frame)+5, kSCREEN_WIDTH-115, ComAdresHeight);
    [self.CompanyMessageView addSubview:ComAddressContent];
    NSArray *comOtherArr = @[model.companyPrincipalName,model.companyPrincipalPhone,model.contractCode,[NSString stringWithFormat:@"%.2f万元",model.contractMoney],model.contractStart,model.contractEnd];
    NSArray *OtherTitle = @[@"公司负责人",@"负责人电话",@"合 同 编 码",@"合 同 费 用",@"合同开始时间",@"合同结束时间"];
    for (NSInteger i = 0; i < OtherTitle.count; i++) {
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(ComAddressContent.frame)+5+i*(17+5), 90, 17)];
        title.text = OtherTitle[i];
        title.adjustsFontSizeToFitWidth = YES;
        title.font = [UIFont systemFontOfSize:15.0];
        title.textColor = UIColorFromRGB(0x4d4d4d);
        [self.CompanyMessageView addSubview:title];
        UILabel *rightContLab = [[UILabel alloc]initWithFrame:CGRectMake(110,CGRectGetMaxY(ComAddressContent.frame)+5+i*(17+5) , kSCREEN_WIDTH-115, 17)];
        rightContLab.textColor = UIColorFromRGB(0x4d4d4d);
        rightContLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
        rightContLab.font = [UIFont systemFontOfSize:15.0];
        rightContLab.text = comOtherArr[i];
        [self.CompanyMessageView addSubview:rightContLab];
        self.CompanyMessageView.frame = CGRectMake(0, CGRectGetMaxY(self.StationMessageView.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(rightContLab.frame)+5);

    }
    scroler.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(self.CompanyMessageView.frame));
    
}
- (UIView *)StationMessageView{
    if (!_StationMessageView) {
        _StationMessageView = [[UIView alloc]init];
    }
    return _StationMessageView;
}

- (UIView *)CompanyMessageView{
    if (!_CompanyMessageView) {
        _CompanyMessageView = [[UIView alloc]init];
    }
    return _CompanyMessageView;
}

@end
