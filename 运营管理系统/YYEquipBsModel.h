//
//  YYEquipBsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/11.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYEquipDataModel.h"
@interface YYEquipBsModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)YYEquipDataModel *data;
@end
