//
//  EquipModelCell.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/11.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "EquipModelCell.h"
#import "Masonry.h"
@implementation EquipModelCell
{
    UILabel *_companyN;
    UILabel *_Numb;
    UILabel *_proportion;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _companyN = [[UILabel alloc]init];
        _companyN.text = @"广东分公司";
        _companyN.numberOfLines = 0;
        _companyN.font = [UIFont systemFontOfSize:14.0];
        _companyN.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:_companyN];
        [_companyN mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(10);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-185, 35));
        }];
        _Numb = [[UILabel alloc]init];
        _Numb.text = @"24";
        _Numb.textColor = UIColorFromRGB(0x4d4d4d);
        _Numb.font = [UIFont systemFontOfSize:14.0];
        _Numb.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_Numb];
        [_Numb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(_companyN.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        
        _proportion = [[UILabel alloc]init];
        _proportion.text = @"11";
        _proportion.font = [UIFont systemFontOfSize:14.0];
        _proportion.textColor = UIColorFromRGB(0x4d4d4d);
        _proportion.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_proportion];
        [_proportion mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.size.mas_equalTo(CGSizeMake(80, 21));
            make.centerY.equalTo(_companyN);
        }];
    }
    return self;
}
- (void)fillCellWithModel:(YYEquipListModel *)model{
    
    _companyN.text = model.equipmodel;
    _Numb.text = [NSString stringWithFormat:@"%ld",model.value];
    _proportion.text = [NSString stringWithFormat:@"%.2f%c",model.percent*100,'%'];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
