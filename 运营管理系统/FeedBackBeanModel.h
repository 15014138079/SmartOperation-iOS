//
//  FeedBackBeanModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/9.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@interface FeedBackBeanModel : JSONModel
@property (nonatomic,assign)NSInteger state;
@property (nonatomic,assign)NSInteger equipId;
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,assign)NSInteger qualityId;
@property (nonatomic,assign)NSInteger num;

@property (nonatomic,strong)NSString *equipName;
@property (nonatomic,strong)NSString *artisanTime;
@property (nonatomic,strong)NSString *discrb;
@property (nonatomic,strong)NSString *sqeTime;
@property (nonatomic,strong)NSString *time;

@property (nonatomic,assign)NSInteger artisanId;
@property (nonatomic,strong)NSString *scheme;
@property (nonatomic,strong)NSString *faultDisc;
@property (nonatomic,assign)NSInteger sqeId;
@property (nonatomic,strong)NSString *sqeName;

@property (nonatomic,strong)NSString *entName;
@property (nonatomic,strong)NSString *userN;
@property (nonatomic,strong)NSString *code;
@property (nonatomic,strong)NSString *equipModel;
@property (nonatomic,strong)NSString *typeName;

@property (nonatomic,strong)NSString *artisanName;
@property (nonatomic,strong)NSString *userTel;
@property (nonatomic,strong)NSString *receiveTime;
@property (nonatomic,strong)NSString *images;
@property (nonatomic,strong)NSString *suggestion;

@property (nonatomic,strong)NSString *measures;
@property (nonatomic,strong)NSString *causes;
@property (nonatomic,strong)NSString *stationAddress;

@end
/*
 "equipName": "宇星科技,YX-CODcr-Ⅱ(CODcr)",
 "state": 0,
 "artisanTime": "",
 "disc": "哦哦哦我已经离开咯莫努力咯额",
 "sqeTime": "",
 
 "equipId": 8932,
 "time": "2017-02-08 16:47:41",
 "artisanId": "",
 "scheme": "",
 "faultDisc": "咯哦五咯莫过哦哦哦快来看看武力解决",
 
 "sqeId": "",
 "sqeName": "",
 "userId": 1100,
 "entName": "古浪县新洁城区生活污水处理有限公司",
 "userName": "唐河尧",
 
 "code": "",
 "equipModel": "YX-CODcr-Ⅱ",
 "typeName": "废水监测",
 "qualityId": 5,
 "artisanName": "",
 
 "num": 1,
 "userTel": "18693525945",
 "receiveTime": "2017-02-08 16:48:06",
 "images": "",
 "suggestion": "",
 
 "measures": "",
 "causes": "",
 "stationAddress": "古浪县城北侧王庄、陈庄段"
 
 */
