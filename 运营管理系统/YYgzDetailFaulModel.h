//
//  YYgzDetailFaulModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYgzDetailAppList.h"
#import "YYgzDeatailConList.h"
#import "YYgzDetailFixList.h"


@interface YYgzDetailFaulModel : JSONModel
@property (nonatomic,strong)NSString *equipBrand;
@property (nonatomic,strong)NSString *findPsonName;
@property (nonatomic,strong)NSString *equipName;
@property (nonatomic,strong)NSString *toUpTime;
@property (nonatomic,strong)NSArray <YYgzDetailAppList> *approveList;
@property (nonatomic,strong)NSString *faultimage;
@property (nonatomic,strong)NSString *equipmodel;
@property (nonatomic,strong)NSArray <YYgzDeatailConList> *confirmList;
@property (nonatomic,strong)NSString *faultDiscrib;
@property (nonatomic,strong)NSString *stationName;
@property (nonatomic,strong)NSString *faultType;
@property (nonatomic,strong)NSArray <YYgzDetailFixList> *fixList;

@end
/*
 "equipBrand": "宇星科技",
 "findPsonName": "路剑涛",
 "equipName": "宇星科技,YX-CODcr-Ⅱ(CODcr,)",
 "toUpTime": "2017-01-13 18:31:36",
 "approveList": [
 {
 "approveRemark": "",
 "approveTime": "",
 "approvePsonName": "路剑涛"
 }
 ],
 "faultimage": "",
 "equipmodel": "YX-CODcr-Ⅱ",
 "confirmList": [ ],
 "faultDiscrib": "是因为",
 "stationName": "辛店污水处理厂进水口",
 "faultType": "分析仪供电模块、开关电源",
 "fixList": [ ]
 */
