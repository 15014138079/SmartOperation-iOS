//
//  MapView.m
//
//
//  Created by Jian-Ye on 12-10-16.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "MapView.h"
#import "CallOutAnnotationView.h"
#import "CalloutMapAnnotation.h"
#import "BasicMapAnnotation.h"


@interface MapView ()<MKMapViewDelegate,CallOutAnnotationViewDelegate>

@property (nonatomic,weak)id<MapViewDelegate> delegate;

@property (nonatomic,strong)CalloutMapAnnotation *calloutAnnotation;
@end

@implementation MapView

@synthesize mapView = _mapView;
@synthesize delegate = _delegate;

- (id)init
{
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        
//        MKMapView *mapView = [[MKMapView alloc] initWithFrame:self.bounds];
//         mapView.delegate = self;
//        [self addSubview:mapView];
        //self.mapView =  mapView;
        self.mapView = [[MKMapView alloc]initWithFrame:self.bounds];
        self.mapView.delegate = self;
        [self addSubview:self.mapView];
        self.span = 10000;
//        MKCoordinateSpan span = MKCoordinateSpanMake(1, 1);

    }
    return self;
}

- (id)initWithDelegate:(id<MapViewDelegate>)delegate
{
    if (self = [self init]) {
        self.delegate = delegate;
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    self.mapView.frame = frame;
    [super setFrame:frame];
}

- (void)beginLoad
{
//    NSUserDefaults *userDefualts = [NSUserDefaults standardUserDefaults];
//    CGFloat lat = [userDefualts floatForKey:@"lat"];
//    CGFloat lnt = [userDefualts floatForKey:@"lnt"];

    for (int i = 0; i < [_delegate numbersWithCalloutViewForMapView]; i++) {

        CLLocationCoordinate2D location = [_delegate coordinateForMapViewWithIndex:i];

        BasicMapAnnotation *  annotation=[[BasicMapAnnotation alloc] initWithLatitude:location.latitude andLongitude:location.longitude tag:i];
        [self.mapView   addAnnotation:annotation];
    }
    
//    if (lat == 0 && lnt == 0) {
//        
//        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(28.0803/*纬度*/, 113.023/*经度*/);
//        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center,_span ,_span );
//        MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
//        [_mapView setRegion:adjustedRegion animated:YES];
//
//    }else{
//    
//        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(lat/*纬度*/, lnt/*经度*/);
//        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center,_span ,_span );
//        MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
//        [_mapView setRegion:adjustedRegion animated:YES];
//    }


}

//大头针没点击时显示泡泡 实例化下面的详细信息view
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
	if ([view.annotation isKindOfClass:[BasicMapAnnotation class]]) {
        
        BasicMapAnnotation *annotation = (BasicMapAnnotation *)view.annotation;
        
        if (_calloutAnnotation.coordinate.latitude == annotation.latitude&&
            _calloutAnnotation.coordinate.longitude == annotation.longitude)
        {
            return;
        }
        if (_calloutAnnotation) {
            [mapView removeAnnotation:_calloutAnnotation];
            self.calloutAnnotation = nil;
        }
        self.calloutAnnotation = [[CalloutMapAnnotation alloc]
                              initWithLatitude:annotation.latitude
                              andLongitude:annotation.longitude
                                  tag:annotation.tag];
        [mapView addAnnotation:_calloutAnnotation];
        
        [mapView setCenterCoordinate:_calloutAnnotation.coordinate animated:YES];
        [self.delegate AnnotationViewShow:annotation.tag];
	}
}

- (void)didSelectAnnotationView:(CallOutAnnotationView *)view
{
    CalloutMapAnnotation *annotation = (CalloutMapAnnotation *)view.annotation;
    if([self.delegate respondsToSelector:@selector(calloutViewDidSelectedWithIndex:)])
    {
        [self.delegate calloutViewDidSelectedWithIndex:annotation.tag];
    }

    [self mapView:self.mapView didDeselectAnnotationView:view];
}
//点击其他地方 泡泡和详细信息view隐藏
- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{

    
    if (_calloutAnnotation)
    {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude)
        {
            [self.delegate AnnotationViewHiden];
            [mapView removeAnnotation:_calloutAnnotation];
            self.calloutAnnotation = nil;
        }
        
        
        
        
    }
    
    
    
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{

	if ([annotation isKindOfClass:[CalloutMapAnnotation class]])
    {
        
        CalloutMapAnnotation *calloutAnnotation = (CalloutMapAnnotation *)annotation;
        
        CallOutAnnotationView *annotationView = (CallOutAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutView"];
        if (!annotationView)
        {
            annotationView = [[CallOutAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutView" delegate:self];
        }
        for (UIView *view in  annotationView.contentView.subviews) {
            [view removeFromSuperview];
        }
        [annotationView.contentView addSubview:[self.delegate mapViewCalloutContentViewWithIndex:calloutAnnotation.tag]];
        return annotationView;
	} else if ([annotation isKindOfClass:[BasicMapAnnotation class]])
    {
       
        BasicMapAnnotation *basicMapAnnotation = (BasicMapAnnotation *)annotation;
        MKAnnotationView *annotationView =[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"CustomAnnotation"];
        annotationView.canShowCallout = NO;
        annotationView.image = [self.delegate baseMKAnnotationViewImageWithIndex:basicMapAnnotation.tag];
        if (!annotationView)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:@"CustomAnnotation"];
            annotationView.canShowCallout = NO;
            annotationView.image = [self.delegate baseMKAnnotationViewImageWithIndex:basicMapAnnotation.tag];
        }
		
		return annotationView;
    }
	return nil;
}

@end
