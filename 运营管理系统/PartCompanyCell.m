//
//  PartCompanyCell.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "PartCompanyCell.h"

@implementation PartCompanyCell
{
    
    __weak IBOutlet UILabel *companyName;
}
-(void)fillCellWithModel:(YYbeanListModel *)model{
    companyName.text = model.branchName;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
