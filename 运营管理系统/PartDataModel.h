//
//  PartDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "MslistModel.h"
@interface PartDataModel : JSONModel
@property (nonatomic,strong)NSArray <MslistModel> *mslist;
@end
