//
//  GZtypeBaseModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/30.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "TypeDataModel.h"
@interface GZtypeBaseModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)TypeDataModel *data;
@end
/*
 "message": "获取故障类型信息成功",
 "status": 1,
 "data": {}
 */

