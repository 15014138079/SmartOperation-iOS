//
//  StationView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/3.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StationView;
@protocol StationDataSource <NSObject>
- (void)StationArr:(NSArray *)dataSource ID:(NSInteger)userID;
- (void)tableviewHid:(UIButton *)sender;
- (void)didSelectedCell:(NSInteger)userID StationID:(NSInteger)stationid andDataArray:(NSArray *)StationArray;
@end
@interface StationView : UIView
@property (nonatomic,strong)id <StationDataSource> dataSource;
- (void)StationArr:(NSArray *)dataSource ID:(NSInteger)userID;
- (void)reloadTableView;
//- (void)tableviewHid;
- (void)didSelectedCell:(NSInteger)userID StationID:(NSInteger)stationid andDataArray:(NSArray *)StationArray;
@end
