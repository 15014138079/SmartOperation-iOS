//
//  SugAndComDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/21.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "SugAndComBeanListModel.h"
@interface SugAndComDataModel : JSONModel
@property (nonatomic,strong)NSArray <SugAndComBeanListModel> *beanList;
@end
