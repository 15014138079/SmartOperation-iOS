//
//  YYFgsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/15.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol YYFgsModel <NSObject>

@end
@interface YYFgsModel : JSONModel
@property (nonatomic,assign)float percent;
@property (nonatomic,strong)NSString *fgs;
@property (nonatomic,assign)NSInteger value;
@end
