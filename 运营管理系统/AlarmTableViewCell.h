//
//  AlarmTableViewCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/5.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYAlarmListModel.h"
@interface AlarmTableViewCell : UITableViewCell
- (void)fillCellWithModel:(YYAlarmListModel *)model;
@end
