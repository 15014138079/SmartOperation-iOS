//
//  SearchHeardView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/23.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "SearchHeardView.h"

@implementation SearchHeardView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initView];
    }
    return self;
}
- (void)initView{
    
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 7, 25, 21)];
    timeLab.text = @"📅";
    timeLab.font = [UIFont systemFontOfSize:15.0];
    timeLab.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:timeLab];
    self.startimeTF = [[UITextField alloc]initWithFrame:CGRectMake(37, 7, (kSCREEN_WIDTH-52)/2, 21)];
    self.startimeTF.userInteractionEnabled = NO;
    self.startimeTF.font = [UIFont systemFontOfSize:15.0];
    self.startimeTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    self.startimeTF.placeholder = @"选择时间";
    self.startimeTF.layer.cornerRadius = 2;
    self.startimeTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.startimeTF];
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startBtn.frame = self.startimeTF.frame;
    [startBtn addTarget:self action:@selector(chooseTimeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    startBtn.tag = 1001;
    [self addSubview:startBtn];
    UIView *horizontalLine = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.startimeTF.frame), 17, 10, 1)];
    horizontalLine.backgroundColor = UIColorFromRGB(0xcccccc);
    [self addSubview:horizontalLine];
    self.endtimeTF = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(horizontalLine.frame), 7, (kSCREEN_WIDTH-52)/2, 21)];
    self.endtimeTF.userInteractionEnabled = NO;
    self.endtimeTF.font = [UIFont systemFontOfSize:15.0];
    self.endtimeTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    self.endtimeTF.placeholder = @"选择时间";
    self.endtimeTF.layer.cornerRadius = 2;
    self.endtimeTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.endtimeTF];
    UIButton *endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    endBtn.frame = self.endtimeTF.frame;
    [endBtn addTarget:self action:@selector(chooseTimeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    endBtn.tag = 1002;
    [self addSubview:endBtn];
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(timeLab.frame)+3, kSCREEN_WIDTH, 1)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:line1];
    
    
    self.questionTypeTF = [[UITextField alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(line1.frame)+3, (kSCREEN_WIDTH-30)/2, 21)];
    self.questionTypeTF.userInteractionEnabled = NO;
    self.questionTypeTF.placeholder = @"选择问题类型";
    self.questionTypeTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    self.questionTypeTF.font = [UIFont systemFontOfSize:15.0];
    self.questionTypeTF.layer.cornerRadius = 2;
    self.questionTypeTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.questionTypeTF];
    UIButton *questionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    questionBtn.tag = 1003;
    questionBtn.frame = self.questionTypeTF.frame;
    [questionBtn addTarget:self action:@selector(chooseTypeOrCompany:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:questionBtn];
    
    
    self.branchCompanyTF = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.questionTypeTF.frame)+10, CGRectGetMaxY(line1.frame)+3, (kSCREEN_WIDTH-30)/2, 21)];
    self.branchCompanyTF.userInteractionEnabled = NO;
    self.branchCompanyTF.placeholder = @"选择分公司";
    self.branchCompanyTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    self.branchCompanyTF.font = [UIFont systemFontOfSize:15.0];
    self.branchCompanyTF.layer.cornerRadius = 2;
    self.branchCompanyTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.branchCompanyTF];
    UIButton *branchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    branchBtn.tag = 1004;
    branchBtn.frame = self.branchCompanyTF.frame;
    [branchBtn addTarget:self action:@selector(chooseTypeOrCompany:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:branchBtn];
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.questionTypeTF.frame)+3, kSCREEN_WIDTH, 1)];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:line2];
    
    self.searchTF = [[UITextField alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(line2.frame)+3, kSCREEN_WIDTH-80, 30)];
    self.searchTF.placeholder = @"🔍输入搜索关键字";
    self.searchTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    self.searchTF.layer.cornerRadius = 4;
    self.searchTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.searchTF];
    
    UIButton *SearchBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    SearchBtn.titleLabel.font = [UIFont systemFontOfSize:19.0];
    SearchBtn.frame = CGRectMake(CGRectGetMaxX(self.searchTF.frame), CGRectGetMaxY(line2.frame)+3, 80, 30);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    SearchBtn.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
    [SearchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:SearchBtn];
    
    //响应者  responder
    //第一响应者  first responder 用户正在操作的控件
    //如果文本框称为第一响应者,则系统会自动弹出键盘
    //如果想要隐藏键盘,只需要放弃第一响应者即可
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    
        [tap addTarget:self action:@selector(hideKeyboard)];
    
        [self addGestureRecognizer:tap];
}
- (void)hideKeyboard{
    
    [self.searchTF resignFirstResponder];
}
- (void)chooseTimeBtnClick:(UIButton *)sender{
    
    [self.searchTF resignFirstResponder];
    //NSSLog(@"%ld",sender.tag);
    [self.delegate chooseTime:sender ClickTag:sender.tag];
}
- (void)chooseTypeOrCompany:(UIButton *)sender{
    //NSSLog(@"%ld",sender.tag);
    [self.searchTF resignFirstResponder];
    [self.delegate chooseTypeOrCompany:sender ClickTag:sender.tag];
}
- (void)searchBtnClick:(UIButton *)sender{
    
    [self.delegate HeardViewSearchClick:sender];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
