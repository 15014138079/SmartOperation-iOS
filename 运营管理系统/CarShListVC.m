//
//  CarShListVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/11.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarShListVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "RightSlidetableViewCell.h"
#import "TimeModel.h"
#import "canshuModel.h"
#import "CarShPVC.h"
@interface CarShListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property(strong,nonatomic) UITableView *listTableview;
@property(strong,nonatomic) NSMutableArray *dataList;
@property(nonatomic,strong) NSMutableArray *dataSource;
@end

@implementation CarShListVC
- (AFHTTPSessionManager *)manager{

    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"车辆申用列表";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.userId = delegate.userId;
    //[self load];
    [self initView];
}
/*
- (void)load{
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_carExtendList parameters:@{userid:[NSString stringWithFormat:@"%ld",self.userId]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dicdate = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSDictionary *data = dicdate[@"data"];
        NSArray *arr = data[@"valist"];
        
        self.dataList=[NSMutableArray arrayWithCapacity:0];
        self.dataSource = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i<arr.count; ++i) {
            
            NSString *strTime = [NSString stringWithFormat:@"%@",arr[i][@"time"]];
            
            NSString *Month_str = [strTime substringWithRange:NSMakeRange(5, 2)];
            NSString *Day_str = [strTime substringWithRange:NSMakeRange(8, 2)];
            NSString *Time = [NSString stringWithFormat:@"%@/%@",Month_str,Day_str];
            //averageScoreStr 标题 titleStr 内容
            NSDictionary *dic=@{@"timeStr":Time,@"titleStr":[NSString stringWithFormat:@"品牌:%@      型号: %@",arr[i][@"brand"],arr[i][@"model"]],@"averageScoreStr":arr[i][@"title"],@"state":arr[i][@"state"]};
            TimeModel *model=[[TimeModel alloc]initData:dic];
            [self.dataList addObject:model];
            canshuModel *model1 = [[canshuModel alloc]init];
            model1.Parid = [arr[i][@"id"]integerValue];
            [self.dataSource addObject:model1];
        }
        [self.listTableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];


}*/
//设置UI视图
-(void)initView
{
    self.listTableview=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT-55) style:UITableViewStylePlain];
    self.listTableview.delegate=self;
    self.listTableview.dataSource=self;
    [self.view addSubview:self.listTableview];
}

#pragma mark-----------------------------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId=@"testTime";
    RightSlidetableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell=[[RightSlidetableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    TimeModel *model=self.dataList[indexPath.row];
    [cell setData:model];
    //self.listTableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    TimeModel *model=self.dataList[indexPath.row];
//    NSDictionary *fontDic=@{NSFontAttributeName:[UIFont systemFontOfSize:18]};
//    CGSize size1=CGSizeMake(kSCREEN_WIDTH,0);
//    CGSize titleLabelSize=[model.titleStr boundingRectWithSize:size1 options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading   attributes:fontDic context:nil].size;
//    
//    return titleLabelSize.height+40;
    return 62.0;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    canshuModel *model = _dataSource[indexPath.row];
    
    CarShPVC *vc = [[CarShPVC alloc]init];
    vc.ID = model.Parid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
