//
//  YYAlarmDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYAlarmListModel.h"
@interface YYAlarmDataModel : JSONModel
@property (nonatomic,strong)NSArray <YYAlarmListModel> *beanList;
@end
