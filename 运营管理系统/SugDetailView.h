//
//  SugDetailView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SugDetailView;
@protocol SugDetailViewDelegate <NSObject>
- (void)returnSugDetailViewHeight:(NSInteger)height;
@end
@interface SugDetailView : UIView
- (instancetype)initWithFrame:(CGRect)frame AndDetailData:(NSDictionary *)detailDic;
@property(nonatomic,weak)id <SugDetailViewDelegate> delegate;
@end
