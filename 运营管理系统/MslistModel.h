//
//  MslistModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol MslistModel <NSObject>

@end
@interface MslistModel : JSONModel
@property (nonatomic,assign)NSInteger brId;
@property (nonatomic,strong)NSString *brName;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSString *elecBench;
@property (nonatomic,strong)NSString *logId;

@property (nonatomic,assign)NSInteger numb;
@property (nonatomic,strong)NSString *tm;
@property (nonatomic,strong)NSString *unit;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *perChg;
@property (nonatomic,strong)NSString *storeCode;
@property (nonatomic,strong)NSString *storeName;
@end


/*
 "brId": 24,
 "brName": "甘肃分公司",
 "discrib": "电磁阀(高压阀)/24V DC(耐压=0.8MPa)",
 "elecBench": "",
 "id": 576,
 "logId": "19501006000020",
 "name": "",
 "numb": 1372,
 "perChg": "",
 "storeCode": "SYXY123",
 "storeName": "甘肃（临夏）仓",
 "tm": "2016-12-03 15:07:58",
 "unit": "个"
 */
