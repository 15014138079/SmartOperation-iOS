//
//  FaultPhotoView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FaultPhotoViewDelegate <NSObject>
-(void)didSelectPhotoURL:(NSArray *)FautImageURL andIndex:(NSInteger)index;
@end
@interface FaultPhotoView : UIView
@property (nonatomic,weak)id <FaultPhotoViewDelegate> delegate;
-(instancetype)initWithFrame:(CGRect)frame imageurl:(NSArray *)ImageURL;
@end
