//
//  YearsVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/13.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "GZcountYearsCell.h"
#import "YYyearsDataModel.h"
#import "YearsModel.h"
#import "DVBarChartView.h"
#import "AppDelegate.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface YearsVC : UIViewController<UITableViewDataSource,UITableViewDelegate,DVBarChartViewDelegate,JoinFalutDelegate,FallLineDelegate,ServerUnsualDelegate,NothingViewDelegate>
@property (nonatomic,assign)NSInteger userID;
@end
