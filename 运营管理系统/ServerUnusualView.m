//
//  ServerUnusualView.m
//  XJHistory
//
//  Created by 杨毅 on 17/1/9.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "ServerUnusualView.h"
#import "Masonry.h"
@implementation ServerUnusualView
-(instancetype)initWithFrame:(CGRect)frame AndTitle:(NSString *)errorTitle{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIImageView *backImageView = [[UIImageView alloc]init];
        backImageView.image = [UIImage imageNamed:@"img03"];
        [self addSubview:backImageView];
        [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(150, 204));
        }];
        UILabel *YCreasion = [[UILabel alloc]init];
        YCreasion.text = [NSString stringWithFormat:@"不好...%@",errorTitle];
        YCreasion.textAlignment = NSTextAlignmentCenter;
        YCreasion.textColor = UIColorFromRGB(0x4d4d4d);
        [self addSubview:YCreasion];
        [YCreasion mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backImageView.mas_bottom).offset(5);
            make.left.equalTo(self).offset((kSCREEN_WIDTH-145)/2);
            make.size.mas_equalTo(CGSizeMake(145, 21));
        }];
        UIImageView *leftImageView = [[UIImageView alloc]init];
        leftImageView.image = [UIImage imageNamed:@"abnormal_icon"];
        [self addSubview:leftImageView];
        [leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(YCreasion);
            make.right.equalTo(YCreasion.mas_left).offset(0);
            make.top.equalTo(backImageView.mas_bottom).offset(5);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
        UIView *refrashView = [[UIView alloc]init];
        refrashView.backgroundColor = UIColorFromRGB(0x0093d5);
        refrashView.layer.cornerRadius = 2;
        [self addSubview:refrashView];
        [refrashView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset((kSCREEN_WIDTH-80)/2);
            make.top.equalTo(YCreasion.mas_bottom).offset(10);
            make.size.mas_equalTo(CGSizeMake(80, 20));
        }];
        UIImageView *refreshImage = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, 15, 15)];
        refreshImage.image = [UIImage imageNamed:@"refresh_icon"];
        [refrashView addSubview:refreshImage];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"刷新页面" forState:UIControlStateNormal];
        btn.frame = CGRectMake(17, 0, 63, 20);
        btn.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [btn addTarget:self action:@selector(refresh3:) forControlEvents:UIControlEventTouchUpInside];
        [refrashView addSubview:btn];
    }
    return self;
}
- (void)refresh3:(UIButton *)sender{
    
    [self.delegate ServerRefresh:sender];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
