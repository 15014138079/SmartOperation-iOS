//
//  YYMapStationBSmodel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/5.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYmapStationDataModel.h"
@interface YYMapStationBSmodel : JSONModel
@property(nonatomic,strong)NSString *message;
@property(nonatomic,assign)NSInteger status;
@property(nonatomic,strong)YYmapStationDataModel *data;
@end
