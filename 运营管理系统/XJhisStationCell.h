//
//  XJhisStationCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XJhisStationListModel.h"
@interface XJhisStationCell : UITableViewCell
-(void)fillCellWithModel:(XJhisStationListModel *)model;
@end
