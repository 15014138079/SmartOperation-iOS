//
//  workModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/21.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface workModel : NSObject
@property (nonatomic,strong)NSString *leftIcon;
@property (nonatomic,strong)NSString *countent;
@property (nonatomic,assign)NSInteger LevelTag;
@property (nonatomic,strong)NSString *message;
+ (NSArray *)WorkModule:(NSArray *)MenuIDs;//工作模块
+ (NSArray *)SearchModule:(NSArray *)MenuIDs;//查询模块
@end
