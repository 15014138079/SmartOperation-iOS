//
//  MassagerVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "MassagerVC.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Masonry.h"
#import "MBProgressHUD.h"
#import "YYgzDetailDataModel.h"
#import "AlbumVC.h"
#import "FaultPhotoView.h"
@interface MassagerVC ()<FaultPhotoViewDelegate>
{
    NSArray *_arr1;//基础信息
    NSArray *_arr2;//上报信息
    UIScrollView *_scrollView;
    UILabel *_content1;
    UILabel *_content2;
    UIView *_IntervalView1;
    UIView *_IntervalView2;
    UILabel *_titleNameSH;//审核
    UILabel *_UPcontent1;//上报内容其他
    UILabel *_UPcontent2;//上报内容备注信息
    UILabel *_SHcontent1;//审核内容其他
    UILabel *_SHcontent2;//审核内容备注信息
    
    NSArray *_faultImageUrlArr;
    NSArray *_fixImageUrlArr;
    NSMutableArray *_allFaultImageArr;
//    FallLineView *_Fallview;
//    JoinFaluterView *_JoinFalutView;
//    ServerUnusualView *_ServerFView;
    FaultPhotoView *_faultImageView1;
    UIView *_QRmessageView;
    UIView *_messageView;
    
}

@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)UIView *backView1;
@property (nonatomic,strong)UIView *backView2;
@property (nonatomic,strong)UIView *backView3;
@property (nonatomic,strong)UIView *backView4;
@property (nonatomic,strong)UIView *backView5;
@property (nonatomic,strong)UIView *back1;
@property (nonatomic,strong)UIView *back2;
@property (nonatomic,strong)UIView *back3;
@property (nonatomic,strong)UIView *back4;
@property (nonatomic,strong)UIView *back5;
@property (nonatomic,strong)UIView *bordview1;
@property (nonatomic,strong)UIView *bordview2;
@property (nonatomic,strong)UIView *bordview3;
@property (nonatomic,strong)UIView *bordview4;
@property (nonatomic,strong)UIView *bordview5;
@property (nonatomic,strong)NSMutableArray *shArr;
@property (nonatomic,strong)NSMutableArray *chuliArr;
@property (nonatomic,strong)NSMutableArray *QRenArr;
@end

@implementation MassagerVC
- (AFHTTPSessionManager *)manager{

    if (!_manager) {
        
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.fauId = delegate.GZid;
    _shArr = [[NSMutableArray alloc]init];
    _chuliArr = [[NSMutableArray alloc]init];
    _QRenArr = [[NSMutableArray alloc]init];
    _allFaultImageArr = [[NSMutableArray alloc]init];
    [self starLoad];

}
- (void)initScorllView{
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT);
    _scrollView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    
    [self.view addSubview:_scrollView];
    // 隐藏水平滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    // self.backView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 15, kSCREEN_WIDTH, 200)];
    self.backView1.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:self.backView1];
    self.bordview1.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.backView1 addSubview:self.bordview1];
    self.back1.backgroundColor = [UIColor whiteColor];
    [self.bordview1 addSubview:self.back1];
    
    UILabel *bsMass = [[UILabel alloc]init];
    bsMass.text = @"基本信息";
    bsMass.adjustsFontSizeToFitWidth = YES;
    bsMass.textColor = UIColorFromRGB(0x0093d5);
    bsMass.font = [UIFont systemFontOfSize:20.0];
    bsMass.backgroundColor = [UIColor whiteColor];
    [self.back1 addSubview:bsMass];
    [bsMass mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.back1).offset(-10);
        make.centerX.equalTo(self.back1);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    NSArray *leftTitle_arr = @[@"故障基站:",@"设备品牌:",@"设备名称:",@"设备型号:"];
    
    for (NSInteger j = 0; j < _arr1.count; ++j) {
        
        UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 40 + j * (21+15), 70, 21)];
        titleName.adjustsFontSizeToFitWidth = YES;
        titleName.font = [UIFont systemFontOfSize:15.0];
        titleName.textColor = UIColorFromRGB(0x4d4d4d);
        titleName.text = leftTitle_arr[j];
        [self.back1 addSubview:titleName];
        UILabel *content = [[UILabel alloc]initWithFrame:CGRectZero];
        content.adjustsFontSizeToFitWidth = YES;
        content.text = _arr1[j];
        content.numberOfLines = 0;
        content.font = [UIFont systemFontOfSize:15.0];
        content.textColor = UIColorFromRGB(0x4d4d4d);
        CGRect rect = [content.text boundingRectWithSize:CGSizeMake((kSCREEN_WIDTH-50)/4*3, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:content.font,NSFontAttributeName, nil] context:nil];
        CGFloat height = rect.size.height; //获取CGSize的高
        content.frame = CGRectMake(90, 40 + j * (21+15), (kSCREEN_WIDTH-50)/4*3, height);
        self.back1.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(content.frame)+5);
        self.bordview1.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back1.frame)+1);
        self.backView1.frame = CGRectMake(0, 15, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview1.frame)+5);
        [self.back1 addSubview:content];
    }
    //上报信息
    
    self.backView2.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:self.backView2];
    
    self.bordview2.backgroundColor = UIColorFromRGB(0xb6b6b6);
    [self.backView2 addSubview:self.bordview2];
    
    self.back2.backgroundColor = [UIColor whiteColor];
    [self.bordview2 addSubview:self.back2];
    
    
    UILabel *sbMass = [[UILabel alloc]init];
    sbMass.text = @"上报信息";
    sbMass.adjustsFontSizeToFitWidth = YES;
    sbMass.textColor = UIColorFromRGB(0x0093d5);
    sbMass.font = [UIFont systemFontOfSize:20.0];
    sbMass.backgroundColor = [UIColor whiteColor];
    [self.back2 addSubview:sbMass];
    [sbMass mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.back2).offset(-10);
        make.centerX.equalTo(self.back2);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    NSArray *leftTitle_arr1 = @[@"上报人员:",@"上报时间:",@"故障类型",@"故障描述:"];
    
    for (NSInteger j = 0; j < leftTitle_arr1.count; ++j) {
        
        
        
        if (j != 3) {
            _UPcontent1 = [[UILabel alloc]initWithFrame:CGRectZero];
            _UPcontent1.text = _arr2[j];
            _UPcontent1.adjustsFontSizeToFitWidth = YES;
            _UPcontent1.numberOfLines = 0;
            _UPcontent1.font = [UIFont systemFontOfSize:15.0];
            _UPcontent1.textColor = UIColorFromRGB(0x4d4d4d);
            CGRect rect = [_UPcontent1.text boundingRectWithSize:CGSizeMake((kSCREEN_WIDTH-20)/3*2, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_UPcontent1.font,NSFontAttributeName, nil] context:nil];
            CGFloat height = rect.size.height; //获取CGSize的高
            _UPcontent1.frame = CGRectMake(90, 40 + j * (21+10), (kSCREEN_WIDTH-50)/4*3, height);
            [self.back2 addSubview:_UPcontent1];
            UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 40 + j * (21+10), 70, 21)];
            titleName.adjustsFontSizeToFitWidth = YES;
            titleName.font = [UIFont systemFontOfSize:15.0];
            titleName.textColor = UIColorFromRGB(0x4d4d4d);
            titleName.text = leftTitle_arr1[j];
            [self.back2 addSubview:titleName];
        }else{
            _UPcontent2 = [[UILabel alloc]initWithFrame:CGRectZero];
            _UPcontent2.text = _arr2[j];
            _UPcontent2.adjustsFontSizeToFitWidth = YES;
            _UPcontent2.numberOfLines = 0;
            _UPcontent2.font = [UIFont systemFontOfSize:15.0];
            _UPcontent2.textColor = UIColorFromRGB(0x4d4d4d);
            CGRect rect = [_UPcontent2.text boundingRectWithSize:CGSizeMake((kSCREEN_WIDTH-20)/3*2, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_UPcontent2.font,NSFontAttributeName, nil] context:nil];
            CGFloat height = rect.size.height; //获取CGSize的高
            
            _UPcontent2.frame = CGRectMake(90, CGRectGetMaxY(_UPcontent1.frame)+10, (kSCREEN_WIDTH-50)/4*3, height);
            [self.back2 addSubview:_UPcontent2];
            UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_UPcontent1.frame)+10, 70, 21)];
            titleName.adjustsFontSizeToFitWidth = YES;
            titleName.font = [UIFont systemFontOfSize:15.0];
            titleName.textColor = UIColorFromRGB(0x4d4d4d);
            titleName.text = leftTitle_arr1[j];
            [self.back2 addSubview:titleName];
            
            
            
            
        }
    }
    if (_faultImageUrlArr.count == 1 && [_faultImageUrlArr[0]length]==0) {
        
        self.back2.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(_UPcontent2.frame)+5);
        self.bordview2.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back2.frame)+1);
        self.backView2.frame = CGRectMake(0, CGRectGetMaxY(self.backView1.frame)+10, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview2.frame)+5);
    }else{
        //图片展示的view
        FaultPhotoView *faultImageView = [[FaultPhotoView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_UPcontent2.frame), kSCREEN_WIDTH-20, 100) imageurl:_faultImageUrlArr];
        faultImageView.delegate = self;
        [self.back2 addSubview:faultImageView];
        self.back2.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(faultImageView.frame)+5);
        self.bordview2.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back2.frame)+1);
        self.backView2.frame = CGRectMake(0, CGRectGetMaxY(self.backView1.frame)+10, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview2.frame)+5);
    }
    
    if (_shArr.count == 0) {
        
        self.backView3.alpha = 0;
        self.backView3.frame = CGRectMake(0, CGRectGetMaxY(self.backView2.frame), kSCREEN_WIDTH, 5);
        _IntervalView1.frame = CGRectMake(0, CGRectGetMaxY(self.backView2.frame), kSCREEN_WIDTH, 5);
    }else{
        //审核信息
        self.backView3.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:self.backView3];
        
        self.bordview3.backgroundColor = UIColorFromRGB(0xb6b6b6);
        [self.backView3 addSubview:self.bordview3];
        
        self.back3.backgroundColor = [UIColor whiteColor];
        [self.bordview3 addSubview:self.back3];
        
        UILabel *shMass = [[UILabel alloc]init];
        shMass.text = @"审核信息";
        shMass.adjustsFontSizeToFitWidth = YES;
        shMass.textColor = UIColorFromRGB(0x0093d5);
        shMass.font = [UIFont systemFontOfSize:20.0];
        shMass.backgroundColor = [UIColor whiteColor];
        [self.back3 addSubview:shMass];
        [shMass mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.back3).offset(-10);
            make.centerX.equalTo(self.back3);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        
        
        NSArray *leftTitle_arr3 = @[@"",@"审  批  人:",@"审批备注:"];
        for (NSInteger i = 0 ; i < _shArr.count; i++) {
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            YYgzDetailAppList *model = _shArr[i];
            if (model.approveTime.length < 1) {
                [arr addObject:@"待审批"];
                [arr addObject:model.approvePsonName];
                [arr addObject:@"暂未审批"];
            }else{
                [arr addObject:model.approveTime];
                [arr addObject:model.approvePsonName];
                [arr addObject:model.approveRemark];
            }
            
            for (NSInteger j = 0 ;j < leftTitle_arr3.count;j++) {
                
                _titleNameSH.adjustsFontSizeToFitWidth = YES;
                if (j%3 != 0) {
                    if (j%2 == 0 && model.approveRemark.length<1) {
                        
                    }else{
                        _SHcontent1 = [[UILabel alloc]initWithFrame:CGRectZero];
                        _SHcontent1.adjustsFontSizeToFitWidth = YES;
                        _SHcontent1.text = arr[j];
                        _SHcontent1.font = [UIFont systemFontOfSize:15.0];
                        _SHcontent1.numberOfLines = 0;
                        _SHcontent1.textColor = UIColorFromRGB(0x4d4d4d);
                        
                        CGRect rect = [_SHcontent1.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_SHcontent1.font,NSFontAttributeName, nil] context:nil];
                        
                        //CGFloat width = rect.size.width;//获取CGSize的宽
                        CGFloat height = rect.size.height; //获取CGSize的高
                        _SHcontent1.frame = CGRectMake(90, 100*i+(40 + j * (21+10)), kSCREEN_WIDTH/4*3-30, height);
                        [self.back3 addSubview:_SHcontent1];
                        
                        _titleNameSH = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(_SHcontent1.frame), 70, 21)];
                        _titleNameSH.font = [UIFont systemFontOfSize:15.0];
                        _titleNameSH.textColor = UIColorFromRGB(0x4d4d4d);
                        _titleNameSH.text = leftTitle_arr3[j];
                        [self.back3 addSubview:_titleNameSH];
                    }
                    
                }else if (j%3 == 0 && j > 0){
                    
                    _SHcontent2 = [[UILabel alloc]initWithFrame:CGRectZero];
                    _SHcontent2.adjustsFontSizeToFitWidth = YES;
                    _SHcontent2.text = arr[j];
                    _SHcontent2.font = [UIFont systemFontOfSize:15.0];
                    _SHcontent2.numberOfLines = 0;
                    _SHcontent2.textColor = UIColorFromRGB(0x4d4d4d);
                    
                    CGRect rect = [_SHcontent2.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_SHcontent2.font,NSFontAttributeName, nil] context:nil];
                    
                    //CGFloat width = rect.size.width;//获取CGSize的宽
                    CGFloat height = rect.size.height; //获取CGSize的高
                    _SHcontent2.frame = CGRectMake(30, CGRectGetMaxY(_SHcontent1.frame)+10, kSCREEN_WIDTH/4*3-30, height);
                    [self.back3 addSubview:_SHcontent2];
                    _titleNameSH = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(_SHcontent2.frame)+5, 10, 10)];
                    _titleNameSH.font = [UIFont systemFontOfSize:15.0];
                    _titleNameSH.backgroundColor = UIColorFromRGB(0x0093d5);
                    
                    [self.back3 addSubview:_titleNameSH];
                    
                }else{
                    
                    _SHcontent2 = [[UILabel alloc]initWithFrame:CGRectZero];
                    _SHcontent2.adjustsFontSizeToFitWidth = YES;
                    _SHcontent2.text = arr[j];
                    _SHcontent2.font = [UIFont systemFontOfSize:15.0];
                    _SHcontent2.numberOfLines = 0;
                    if ([_SHcontent2.text isEqualToString:@"待审批"]) {
                        _SHcontent2.textColor = UIColorFromRGB(0xd50037);
                    }else{
                        _SHcontent2.textColor = UIColorFromRGB(0x4d4d4d);
                    }
                    CGRect rect = [_SHcontent2.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_SHcontent2.font,NSFontAttributeName, nil] context:nil];
                    
                    //CGFloat width = rect.size.width;//获取CGSize的宽
                    CGFloat height = rect.size.height; //获取CGSize的高
                    _SHcontent2.frame = CGRectMake(30, 100*i+(40 + j * (21+10)), kSCREEN_WIDTH/4*3-30, height);
                    [self.back3 addSubview:_SHcontent2];
                    _titleNameSH = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(_SHcontent2.frame)+5, 10, 10)];
                    _titleNameSH.font = [UIFont systemFontOfSize:15.0];
                    _titleNameSH.backgroundColor = UIColorFromRGB(0x0093d5);
                    //titleName.text = leftTitle_arr3[j];
                    [self.back3 addSubview:_titleNameSH];
                    
                }
                self.back3.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(_SHcontent1.frame)+5);
                self.bordview3.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back3.frame)+1);
                self.backView3.frame = CGRectMake(0, CGRectGetMaxY(self.backView2.frame)+10, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview3.frame)+5);
            }
        }
        //间隔
        _IntervalView1 = [[UIView alloc]init];
        _IntervalView1.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self.backView3 addSubview:_IntervalView1];
        [_IntervalView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.back3.mas_bottom).offset(5);
            make.left.equalTo(self.backView3).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 10));
        }];
    }
    
    if (_chuliArr.count == 0) {
        self.backView4.alpha = 0;
        self.backView4.frame = CGRectMake(0, CGRectGetMaxY(self.backView3.frame), kSCREEN_WIDTH, 5);
        _IntervalView2.frame = CGRectMake(0, CGRectGetMaxY(self.backView3.frame), kSCREEN_WIDTH, 5);
    }else{
        //处理信息
        //高度根据最后label的高度计算
        
        self.backView4.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:self.backView4];
        self.bordview4.backgroundColor = UIColorFromRGB(0xb6b6b6);
        [self.backView4 addSubview:self.bordview4];
        
        //高度根据最后label的高度计算
        self.back4.backgroundColor = [UIColor whiteColor];
        [self.bordview4 addSubview:self.back4];
        
        
        UILabel *chuliMass = [[UILabel alloc]init];
        chuliMass.text = @"处理信息";
        chuliMass.adjustsFontSizeToFitWidth = YES;
        chuliMass.textColor = UIColorFromRGB(0x0093d5);
        chuliMass.font = [UIFont systemFontOfSize:20.0];
        chuliMass.backgroundColor = [UIColor whiteColor];
        [self.back4 addSubview:chuliMass];
        [chuliMass mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.back4).offset(-10);
            make.centerX.equalTo(self.back4);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        
        //NSArray *leftTitle_arr2 = @[@"",@"处理人员:",@"处理状态:",@"处理结果:"];
        for (NSInteger i = 0; i < _chuliArr.count; i++) {
            YYgzDetailFixList *model = _chuliArr[i];
            //  0驳回 1确认 2待确认 －1000待处理
            _fixImageUrlArr = [model.fixImage componentsSeparatedByString:@","];
            if (model.fixState==0 || model.fixState == 1 || model.fixState == 2) {
                _messageView = [[UIView alloc]initWithFrame:CGRectZero];
                _messageView.backgroundColor = [UIColor whiteColor];
                [self.back4 addSubview:_messageView];
                UIView *leftview = [[UIView alloc]initWithFrame:CGRectMake(15, 5, 10, 10)];
                leftview.backgroundColor = UIColorFromRGB(0x0093d5);
                [_messageView addSubview:leftview];
                UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftview.frame)+2, 0, kSCREEN_WIDTH/4*3-30, 20)];
                timeLab.text = model.fixTime;
                timeLab.font = [UIFont systemFontOfSize:15.0];
                timeLab.textColor = UIColorFromRGB(0x4d4d4d);
                [_messageView addSubview:timeLab];
                UILabel *PsonLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(timeLab.frame)+5, kSCREEN_WIDTH-50, 20)];
                PsonLab.textColor = UIColorFromRGB(0x4d4d4d);
                PsonLab.text = [NSString stringWithFormat:@"处理人员: %@",model.fixPsonName];
                PsonLab.font = [UIFont systemFontOfSize:15.0];
                [_messageView addSubview:PsonLab];
                UILabel *StataLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(PsonLab.frame)+5, 70, 20)];
                StataLab.text = @"处理状态:";
                StataLab.textColor = UIColorFromRGB(0x4d4d4d);
                StataLab.font = [UIFont systemFontOfSize:15.0];
                [_messageView addSubview:StataLab];
                UILabel *StataLabContent = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(StataLab.frame),CGRectGetMaxY(PsonLab.frame)+5, 80, 20)];
                StataLabContent.font = [UIFont systemFontOfSize:15.0];
                if (model.fixState == 1) {
                    StataLabContent.text = @"确认";
                    StataLabContent.textColor = UIColorFromRGB(0x2dd500);
                }else if (model.fixState == 0){
                    StataLabContent.text = @"驳回";
                    StataLabContent.textColor = UIColorFromRGB(0xd50037);
                }else if (model.fixState == 2){
                    StataLabContent.text = @"待确认";
                    StataLabContent.textColor = UIColorFromRGB(0xd50037);
                }else{
                    StataLabContent.text = @"待处理";
                    StataLabContent.textColor = UIColorFromRGB(0xd50037);
                }
                [_messageView addSubview:StataLabContent];
                UILabel *ResultLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(StataLab.frame)+5, 70, 20)];
                ResultLab.text = @"处理结果:";
                ResultLab.textColor = UIColorFromRGB(0x4d4d4d);
                ResultLab.font = [UIFont systemFontOfSize:15.0];
                [_messageView addSubview:ResultLab];
                UILabel *ResultLabContent = [[UILabel alloc]initWithFrame:CGRectZero];
                ResultLabContent.textColor = UIColorFromRGB(0x4d4d4d);
                ResultLabContent.text = model.fixRemark;
                ResultLabContent.font = [UIFont systemFontOfSize:15.0];
                ResultLabContent.numberOfLines = 0;
                CGRect rect = [ResultLabContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:ResultLabContent.font,NSFontAttributeName, nil] context:nil];
                
                //CGFloat width = rect.size.width;//获取CGSize的宽
                CGFloat height = rect.size.height; //获取CGSize的高
                ResultLabContent.frame = CGRectMake(CGRectGetMaxX(ResultLab.frame),CGRectGetMaxY(StataLab.frame)+5, kSCREEN_WIDTH/4*3-20, height);
                [_messageView addSubview:ResultLabContent];
                if (_fixImageUrlArr.count == 1 && [_fixImageUrlArr[0]length]==0) {
                    _messageView.frame = CGRectMake(0, i*CGRectGetMaxY(ResultLabContent.frame)+40, kSCREEN_WIDTH-20,CGRectGetMaxY(ResultLabContent.frame));
                }else{
                    _faultImageView1 = [[FaultPhotoView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(ResultLabContent.frame), kSCREEN_WIDTH-20, 100) imageurl:_fixImageUrlArr];
                    _faultImageView1.delegate = self;
                    [_messageView addSubview:_faultImageView1];
                    _messageView.frame = CGRectMake(0, i*CGRectGetMaxY(_faultImageView1.frame)+40, kSCREEN_WIDTH-20,CGRectGetMaxY(_faultImageView1.frame));
                }
                
                self.back4.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(_messageView.frame)+5);
                self.bordview4.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back4.frame)+1);
                self.backView4.frame = CGRectMake(0, CGRectGetMaxY(self.backView3.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview4.frame)+5);
            }else if (model.fixState == -1000){
                
                UIView *messageview1 = [[UIView alloc]initWithFrame:CGRectMake(0, i*CGRectGetMaxY(_messageView.frame)+10, kSCREEN_WIDTH-20, 50)];
                messageview1.backgroundColor = [UIColor whiteColor];
                [self.back4 addSubview:messageview1];
                UIView *leftview1 = [[UIView alloc]initWithFrame:CGRectMake(15, 5, 10, 10)];
                leftview1.backgroundColor = UIColorFromRGB(0x0093d5);
                [messageview1 addSubview:leftview1];
                UILabel *contentLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftview1.frame)+2,0, kSCREEN_WIDTH/4*3-30, 20)];
                contentLab.text = @"待处理";
                contentLab.textColor = UIColorFromRGB(0xd50037);
                contentLab.font = [UIFont systemFontOfSize:15.0];
                [messageview1 addSubview:contentLab];
                UILabel *PsonLab1 = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(contentLab.frame)+5,kSCREEN_WIDTH-50 , 20)];
                PsonLab1.text = [NSString stringWithFormat:@"处理人员: %@",model.fixPsonName];
                PsonLab1.textColor = UIColorFromRGB(0x4d4d4d);
                PsonLab1.font = [UIFont systemFontOfSize:15.0];
                [messageview1 addSubview:PsonLab1];
                self.back4.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(messageview1.frame)+5);
                self.bordview4.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back4.frame)+1);
                self.backView4.frame = CGRectMake(0, CGRectGetMaxY(self.backView3.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview4.frame)+5);
                
            }
            
            
        }
        
        //间隔
        _IntervalView2 = [[UIView alloc]init];
        _IntervalView2.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self.backView4 addSubview:_IntervalView2];
        [_IntervalView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.back4.mas_bottom).offset(5);
            make.left.equalTo(self.backView4).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 10));
        }];
    }
    
    
    if (_QRenArr.count == 0 ) {
        
        self.backView5.alpha = 0;
        self.backView5.frame = CGRectMake(0, CGRectGetMaxY(self.backView4.frame), kSCREEN_WIDTH, 0);
    }else{
        //确认信息
        self.backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:self.backView5];
        
        self.bordview5.backgroundColor = UIColorFromRGB(0xb6b6b6);
        [self.backView5 addSubview:self.bordview5];
        //高度根据最后label的高度计算
        self.back5.backgroundColor = [UIColor whiteColor];
        
        [self.bordview5 addSubview:self.back5];
        
        
        UILabel *QRMass = [[UILabel alloc]init];
        QRMass.text = @"确认信息";
        QRMass.adjustsFontSizeToFitWidth = YES;
        QRMass.textColor = UIColorFromRGB(0x0093d5);
        QRMass.font = [UIFont systemFontOfSize:20.0];
        QRMass.backgroundColor = [UIColor whiteColor];
        [self.back5 addSubview:QRMass];
        [QRMass mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.back5).offset(-10);
            make.centerX.equalTo(self.back5);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        //NSArray *leftTitle_arr4 = @[@"",@"确  认  人:",@"确认结果:",@"备注信息:"];
        for (NSInteger i = 0; i < _QRenArr.count; i++) {
            YYgzDeatailConList *model = _QRenArr[i];
            if (model.confirmState==0 || model.confirmState == 1 ) {
                _QRmessageView = [[UIView alloc]initWithFrame:CGRectZero];
                _QRmessageView.backgroundColor = [UIColor whiteColor];
                [self.back5 addSubview:_QRmessageView];
                UIView *QRleftview = [[UIView alloc]initWithFrame:CGRectMake(15, 5, 10, 10)];
                QRleftview.backgroundColor = UIColorFromRGB(0x0093d5);
                [_QRmessageView addSubview:QRleftview];
                UILabel *QRtimeLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(QRleftview.frame)+2, 0, kSCREEN_WIDTH/4*3-30, 20)];
                QRtimeLab.text = model.confirmTime;
                QRtimeLab.font = [UIFont systemFontOfSize:15.0];
                QRtimeLab.textColor = UIColorFromRGB(0x4d4d4d);
                [_QRmessageView addSubview:QRtimeLab];
                UILabel *QRPsonLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(QRtimeLab.frame)+5, kSCREEN_WIDTH-50, 20)];
                QRPsonLab.textColor = UIColorFromRGB(0x4d4d4d);
                QRPsonLab.text = [NSString stringWithFormat:@"确  认  人: %@",model.confirmPsonName];
                QRPsonLab.font = [UIFont systemFontOfSize:15.0];
                [_QRmessageView addSubview:QRPsonLab];
                UILabel *QRStataLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(QRPsonLab.frame)+5, 70, 20)];
                QRStataLab.text = @"确认结果:";
                QRStataLab.textColor = UIColorFromRGB(0x4d4d4d);
                QRStataLab.font = [UIFont systemFontOfSize:15.0];
                [_QRmessageView addSubview:QRStataLab];
                UILabel *QRStataLabContent = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(QRStataLab.frame),CGRectGetMaxY(QRPsonLab.frame)+5, 80, 20)];
                QRStataLabContent.font = [UIFont systemFontOfSize:15.0];
                if (model.confirmState == 0) {
                    QRStataLabContent.text = @"驳回";
                    QRStataLabContent.textColor = UIColorFromRGB(0xd50037);
                }else if (model.confirmState == 1){
                    QRStataLabContent.text = @"已确认";
                    QRStataLabContent.textColor = UIColorFromRGB(0x2dd500);
                }else{
                    QRStataLabContent.text = @"待确认";
                    QRStataLabContent.textColor = UIColorFromRGB(0xd50037);
                }
                [_QRmessageView addSubview:QRStataLabContent];
                UILabel *QRResultLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(QRStataLab.frame)+5, 70, 20)];
                QRResultLab.text = @"备注信息:";
                QRResultLab.textColor = UIColorFromRGB(0x4d4d4d);
                QRResultLab.font = [UIFont systemFontOfSize:15.0];
                [_QRmessageView addSubview:QRResultLab];
                UILabel *QRResultLabContent = [[UILabel alloc]initWithFrame:CGRectZero];
                QRResultLabContent.textColor = UIColorFromRGB(0x4d4d4d);
                QRResultLabContent.text = model.confirmRemark;
                QRResultLabContent.font = [UIFont systemFontOfSize:15.0];
                QRResultLabContent.numberOfLines = 0;
                CGRect rect = [QRResultLabContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:QRResultLabContent.font,NSFontAttributeName, nil] context:nil];
                
                //CGFloat width = rect.size.width;//获取CGSize的宽
                CGFloat height = rect.size.height; //获取CGSize的高
                QRResultLabContent.frame = CGRectMake(CGRectGetMaxX(QRResultLab.frame),CGRectGetMaxY(QRStataLab.frame)+5, kSCREEN_WIDTH/4*3-20, height);
                [_QRmessageView addSubview:QRResultLabContent];
                _QRmessageView.frame = CGRectMake(0, i*CGRectGetMaxY(QRResultLabContent.frame)+40, kSCREEN_WIDTH-20, CGRectGetMaxY(QRResultLabContent.frame));
                self.back5.frame = CGRectMake(1, 1, kSCREEN_WIDTH-20, CGRectGetMaxY(_QRmessageView.frame)+5);
                self.bordview5.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back5.frame)+1);
                self.backView5.frame = CGRectMake(0, CGRectGetMaxY(self.backView4.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview5.frame)+5);
                
            }else{
                UIView *QRmessageview1 = [[UIView alloc]initWithFrame:CGRectMake(0, i*CGRectGetMaxY(_QRmessageView.frame)+40, kSCREEN_WIDTH-20, 50)];
                QRmessageview1.backgroundColor = [UIColor whiteColor];
                [self.back5 addSubview:QRmessageview1];
                UIView *QRleftview1 = [[UIView alloc]initWithFrame:CGRectMake(15, 5, 10, 10)];
                QRleftview1.backgroundColor = UIColorFromRGB(0x0093d5);
                [QRmessageview1 addSubview:QRleftview1];
                UILabel *QRcontentLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(QRleftview1.frame)+2,0, kSCREEN_WIDTH/4*3-30, 20)];
                QRcontentLab.text = @"待确认";
                QRcontentLab.textColor = UIColorFromRGB(0xd50037);
                QRcontentLab.font = [UIFont systemFontOfSize:15.0];
                [QRmessageview1 addSubview:QRcontentLab];
                UILabel *QRPsonLab1 = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(QRcontentLab.frame)+5,kSCREEN_WIDTH-50 , 20)];
                QRPsonLab1.text = [NSString stringWithFormat:@"确  认  人: %@",model.confirmPsonName];
                QRPsonLab1.textColor = UIColorFromRGB(0x4d4d4d);
                QRPsonLab1.font = [UIFont systemFontOfSize:15.0];
                [QRmessageview1 addSubview:QRPsonLab1];
                self.back5.frame = CGRectMake(10, 10, kSCREEN_WIDTH-20, CGRectGetMaxY(QRmessageview1.frame)+5);
                self.bordview5.frame = CGRectMake(9, 14, kSCREEN_WIDTH-18, CGRectGetMaxY(self.back5.frame)+1);
                self.backView5.frame = CGRectMake(0, CGRectGetMaxY(self.backView4.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(self.bordview5.frame)+5);
                
            }
            
        }
    }
    if (_shArr.count == 0 && _chuliArr.count == 0 && _QRenArr.count == 0) {
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT);
    }else{
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH,CGRectGetMaxY(_backView5.frame) + 100);
    }

}
- (void)starLoad{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"加载中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:K_AlarmDetail parameters:@{gzid:[NSString stringWithFormat:@"%ld",self.fauId]} progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dicresponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        YYgzDetailDataModel *datamodel = [[YYgzDetailDataModel alloc]initWithDictionary:dicresponse[@"data"] error:nil];
        YYgzDetailFaulModel *model = datamodel.FaultRecd;
        _arr1 = @[model.stationName,model.equipBrand,model.equipName,model.equipmodel];
        _arr2 = @[model.findPsonName,model.toUpTime,model.faultType,model.faultDiscrib];
        _faultImageUrlArr = [model.faultimage componentsSeparatedByString:@","];
        for (YYgzDetailAppList *Smodel in model.approveList) {
            
            [_shArr addObject:Smodel];
        }
        for (YYgzDeatailConList *JxModel in model.confirmList) {
            
            [_QRenArr addObject:JxModel];
        }
        for (YYgzDetailFixList *qrModel in model.fixList) {
            
            [_chuliArr addObject:qrModel];
            
        }
        

        
        [self initScorllView];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
//点击查看照片 FaultPhotoView 代理方法
- (void)didSelectPhotoURL:(NSArray *)FautImageURL andIndex:(NSInteger)index
{
    AlbumVC *alVC = [[AlbumVC alloc]init];
    alVC.ImageUrl = FautImageURL;
    alVC.index = index;
    [self.navigationController pushViewController:alVC animated:YES];
}

- (UIView *)bordview1{
    if (!_bordview1) {
        _bordview1 = [[UIView alloc]init];
    }
    return _bordview1;
}
- (UIView *)bordview2{
    if (!_bordview2) {
        _bordview2 = [[UIView alloc]init];
    }
    return _bordview2;
}
- (UIView *)bordview3{
    if (!_bordview3) {
        _bordview3 = [[UIView alloc]init];
    }
    return _bordview3;
}
- (UIView *)bordview4{
    if (!_bordview4) {
        _bordview4 = [[UIView alloc]init];
    }
    return _bordview4;
}
- (UIView *)bordview5{
    if (!_bordview5) {
        _bordview5 = [[UIView alloc]init];
    }
    return _bordview5;
}

- (UIView *)backView1{
    if (!_backView1) {
        _backView1 = [[UIView alloc]init];
    }
    return _backView1;
}
- (UIView *)back1
{
    if (!_back1) {
        
        _back1 = [[UIView alloc]init];
        
    }
    return _back1;
}
- (UIView *)backView2{
    if (!_backView2) {
        _backView2 = [[UIView alloc]init];
    }
    return _backView2;
}
- (UIView *)back2
{
    if (!_back2) {
        
        _back2 = [[UIView alloc]init];
        
    }
    return _back2;
}

- (UIView *)backView3
{
    if (!_backView3) {
        _backView3 = [[UIView alloc]init];
    }
    return _backView3;
}
- (UIView *)back3{
    
    if (!_back3) {
        
        _back3 = [[UIView alloc]init];
        
    }
    return _back3;
    
}
- (UIView *)backView4{
    if (!_backView4) {
        _backView4 = [[UIView alloc]init];
    }
    return _backView4;
}
- (UIView *)back4
{
    if (!_back4) {
        
        _back4 = [[UIView alloc]init];
        
    }
    return _back4;
}
- (UIView *)backView5{
    if (!_backView5) {
        _backView5 = [[UIView alloc]init];
    }
    return _backView5;
}
- (UIView *)back5
{
    if (!_back5) {
        
        _back5 = [[UIView alloc]init];
        
    }
    return _back5;
}

@end
