//
//  YYgzDetailAppList.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYgzDetailAppList <NSObject>

@end

@interface YYgzDetailAppList : JSONModel
@property (nonatomic,strong)NSString *approveRemark;
@property (nonatomic,strong)NSString *approveTime;
@property (nonatomic,strong)NSString *approvePsonName;
@end
