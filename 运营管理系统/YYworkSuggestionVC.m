//
//  YYworkSuggestionVC.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "YYworkSuggestionVC.h"

@interface YYworkSuggestionVC ()
{
    SuggestionChiledVC *_suggestion;
    ComplainChildVC *_complain;
}
@end

@implementation YYworkSuggestionVC
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"seach_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemClick)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:@[@"建议",@"投诉"]];
    segment.frame = CGRectMake((kSCREEN_WIDTH-240)/2, 10, 120, 30);
    segment.tintColor = [UIColor whiteColor];
    segment.selectedSegmentIndex = 0;
    [segment addTarget:self action:@selector(handelSegementControlAction:)forControlEvents:(UIControlEventValueChanged)];
    self.navigationItem.titleView = segment;
    _suggestion = [[SuggestionChiledVC alloc]init];
    [self.view addSubview:_suggestion.view];
    [self addChildViewController:_suggestion];
    _complain = [[ComplainChildVC alloc]init];

}
- (void)handelSegementControlAction:(UISegmentedControl *)segment{
    
    switch (segment.selectedSegmentIndex) {
            
        case 0:
            [self.view addSubview:_suggestion.view];
            [self addChildViewController:_suggestion];

            break;
        case 1:
            [self.view addSubview:_complain.view];
            [self addChildViewController:_complain];

            break;
        default:
            break;
    }

}
- (void)rightItemClick{
    
    SugAndComSearchVC *searchVC = [[SugAndComSearchVC alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}
- (void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
