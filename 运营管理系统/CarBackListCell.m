//
//  CarBackListCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/11.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarBackListCell.h"
#import "Masonry.h"

@implementation CarBackListCell
{
    UILabel *_carName;
    UILabel *_time;
    UILabel *_carPai;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *leftIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 43, 43)];
        leftIcon.image = [[UIImage imageNamed:@"a_carback"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self.contentView addSubview:leftIcon];
        
        _carName = [[UILabel alloc]init];
        _carName.text = @"奥迪A4";
        _carName.textColor = UIColorFromRGB(0x4d4d4d);
        _carName.font = [UIFont systemFontOfSize:19.0];
        [self.contentView addSubview:_carName];
        [_carName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(5);
            make.left.equalTo(leftIcon.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(120, 21));
        }];
        UILabel *syTime = [[UILabel alloc]init];
        syTime.text = @"购买日期:";
        syTime.font = [UIFont systemFontOfSize:15.0];
        syTime.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:syTime];
        [syTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_carName.mas_bottom).offset(5);
            make.left.equalTo(leftIcon.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(70, 21));
        }];
        _time = [[UILabel alloc]init];
        _time.textColor = UIColorFromRGB(0x4d4d4d);
        _time.text = @"2016-09-02";
        _time.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:_time];
        [_time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_carName.mas_bottom).offset(5);
            make.left.equalTo(syTime.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(90,21));
            
        }];
        UILabel *carP = [[UILabel alloc]init];
        carP.text = @"车牌号:";
        carP.textColor = UIColorFromRGB(0x4d4d4d);
        carP.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:carP];
        [carP mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_carName.mas_bottom).offset(5);
            make.left.equalTo(_time.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(50, 21));
            
        }];
        _carPai = [[UILabel alloc]init];
        _carPai.text = @"湘A 668868";
        _carPai.textColor = UIColorFromRGB(0x4d4d4d);
        _carPai.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:_carPai];
        [_carPai mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_carName.mas_bottom).offset(5);
            make.left.equalTo(carP.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(100, 21));
        }];

    }
    return self;

}
- (void)fillCellWithModel:(Carlist *)model{
    
    if ([model.Brand isEqualToString:@""]) {
        
        _carName.text = @"无";
        
    }else{
        
       _carName.text = model.Brand;
    }
    
    _time.text = [model.BuyTime substringWithRange:NSMakeRange(0, 10)];
    _carPai.text = model.plateNum;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
