//
//  YYMassFeedListCell.m
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/18.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "YYMassFeedListCell.h"

@implementation YYMassFeedListCell
{
    __weak IBOutlet UIImageView *LeftIconImageView;
    __weak IBOutlet UILabel *TitleLab;//公司名称
    __weak IBOutlet UILabel *TimeLab;
    __weak IBOutlet UILabel *DiscrLab;
    __weak IBOutlet UIImageView *StateImageView;
    __weak IBOutlet UILabel *StateMessageLab;//处理流程状态信息
}

- (void)fillCellWithModel:(YYFeedListBeanlistModel *)model{


        switch (model.state) {
            case 0:
                LeftIconImageView.image = [UIImage imageNamed:@"feedback_y"];
                StateMessageLab.text = @"运营管理部技术支持中";
                StateImageView.image = [UIImage imageNamed:@"feedblock_y"];
                break;
            case 1:
                LeftIconImageView.image = [UIImage imageNamed:@"feedback_y"];
                StateMessageLab.text = @"品质部客诉负责人处理中";
                StateImageView.image = [UIImage imageNamed:@"feedblock_y"];
                break;
            case 2:
                LeftIconImageView.image = [UIImage imageNamed:@"feedback_g"];
                StateMessageLab.text = @"已完成";
                StateImageView.image = [UIImage imageNamed:@"feedblock_g"];
                break;
                
            default:
                break;
        }

    TitleLab.text = model.entName;
    TitleLab.lineBreakMode = NSLineBreakByTruncatingMiddle;
    TimeLab.text = [model.time substringToIndex:10];
    DiscrLab.text = model.faultDisc;
    DiscrLab.lineBreakMode = NSLineBreakByTruncatingMiddle;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    DiscrLab.frame = CGRectMake(56, 43, kSCREEN_WIDTH-(66+StateMessageLab.frame.size.width), 21);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
