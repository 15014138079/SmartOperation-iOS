//
//  MapVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapDataDelegate.h"
#import <CoreLocation/CoreLocation.h>
@interface MapVC : UIViewController<MapDataDelegate,CLLocationManagerDelegate>
@property (nonatomic,assign)double lat;
@property (nonatomic,assign)double longt;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,assign)NSInteger userID;
@end
