//
//  FeedBackHisAndListCell.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYFeedListBeanlistModel.h"
@interface FeedBackHisAndListCell : UITableViewCell
- (void)fillCellWithModel:(YYFeedListBeanlistModel *)model;
@end
