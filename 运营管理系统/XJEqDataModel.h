//
//  XJEqDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/3.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "XJEqListModel.h"
@interface XJEqDataModel : JSONModel
@property (nonatomic,strong)NSArray <XJEqListModel> *Equiplist;
@end
