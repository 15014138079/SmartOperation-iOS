//
//  AlarmViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/1.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlarmTableViewCell.h"
#import "AFNetworking.h"
#import "TroubleDetailVC.h"
#import "SearchViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "YYAlarmDataModel.h"
#import "GZupdataViewController.h"
#import "GZshenpiVC.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface AlarmViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate>

@property (nonatomic,assign)NSInteger userId;
@end
