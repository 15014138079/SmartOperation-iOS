//
//  UserButton.h
//  用户页面
//
//  Created by 黄兴满 on 15/11/18.
//  Copyright © 2015年 黄兴满. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeButton : UIButton

+ (instancetype)homeButton;

@property (weak, nonatomic) IBOutlet UIImageView *photo;



@end
