//
//  SuggesAspectModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/21.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol SuggesAspectModel <NSObject>

@end
@interface SuggesAspectModel : JSONModel
@property (nonatomic,strong)NSString *aspectName;
@property (nonatomic,assign)NSInteger aspectId;
@end
/*
 {
 "aspectName": "技术问题",
 "aspectId": 1
 },
 */
