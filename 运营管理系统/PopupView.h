//
//  PopupView.h
//  LewPopupViewController
//
//  Created by deng on 15/3/5.
//  Copyright (c) 2015年 pljhonglu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PopupView;
@protocol PopupViewDelegate <NSObject>

@optional
- (void)chooseType:(UIButton *)sender;
- (void)ChooseSB:(UIButton *)sender;
- (void)choosePson:(UIButton *)sender;
@end
@interface PopupView : UIView
@property (nonatomic, strong)IBOutlet UIView *innerView;
@property (nonatomic, weak)UIViewController *parentVC;
@property (nonatomic, strong)NSString *username;
@property (nonatomic, strong)UIButton *btn;
@property (nonatomic, strong)UIButton *BSpsonBtn;
@property (nonatomic, strong)UILabel *SbChooseLab;
@property (nonatomic, weak) id<PopupViewDelegate> delegate;
+ (instancetype)defaultPopupView;
- (instancetype)initWithBounds:(CGRect)bounds titleMenus:(NSArray *)titles;

@end
