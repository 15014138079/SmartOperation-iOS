//
//  XJhisPsonListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol XJhisPsonListModel <NSObject>

@end
@interface XJhisPsonListModel : JSONModel
@property (nonatomic,strong)NSString *opCenName;
@property (nonatomic,strong)NSString *postName;
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)NSString *userN;
@property (nonatomic,assign)NSInteger opCenId;
@end
/*
 "opCenName": "武威运营中心",
 "postName": "运营工程师",
 "userId": 1101,
 "userName": "郑博",
 "opCenId": 92
 */
