//
//  MbaseModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "Mdata.h"
@interface MbaseModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)Mdata *data;
@end
