//
//  YYgzDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/4.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "typeModel.h"
@interface YYgzDataModel : JSONModel
@property (nonatomic,strong)NSArray <typeModel> *Type;
@end
