//
//  YYXJdataModel.h
//  XJHistory
//
//  Created by 杨毅 on 16/12/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYXJhisListModel.h"
@interface YYXJdataModel : JSONModel
@property (nonatomic,strong)NSArray <YYXJhisListModel> *beanList;
@end
