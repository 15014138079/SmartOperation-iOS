//
//  YYMassFeedDetailVC.h
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+XTYWExtension.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "FeedBackDataModel.h"
#import "AlbumVC.h"
#import "BsMessageDetailView.h"
#import "TechMessageView.h"
#import "MassMsgView.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"

@interface YYMassFeedDetailVC : UIViewController<BsMessageDetailViewDelegate,TechMessageViewDelegate,MassMsgViewDelegate,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate>
@property (nonatomic,assign)NSInteger qualityID;//质量反馈id
@property (nonatomic,assign)NSInteger state;//流程进度状态
@end
