//
//  ExpenseCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpenseModel.h"

@class ExpenseCell;
/**
 *  代理
 */
@protocol closeDelegate <NSObject>

-(void)Refresh:(NSInteger)index;

@end

@interface ExpenseCell : UITableViewCell
- (void)fillCellWithModel:(ExpenseModel *)model Index:(NSInteger)index;
@property (weak, nonatomic) id<closeDelegate> delegate;
@end
