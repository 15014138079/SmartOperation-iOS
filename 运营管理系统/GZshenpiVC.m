//
//  GZshenpiVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/24.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "GZshenpiVC.h"

static NSString *collectionViewCellIdentf = @"collectionViewCell1";
static CGFloat imageSize = 80;

@interface GZshenpiVC ()
{
    UIScrollView *_scrollView;
    
    UIView *_backView3;
    
    UIView *_backView5;
    UIView *_backView6;
    UIView *_backView7;
    UILabel *_content1;
    UIView *_backView8;
    UIView *_backView9;
    ZJSwitch *_swi;
    
    NSArray *_content_arr;
    
    PHATextView *_bztextView;//审核备注信息
    
    NSInteger psonid;//指派人id
    NSInteger state;//上报状态
    NSDictionary *_parameter;
    NSString *_time;
    UIButton *_updata;
    
    UILabel *_chosupPsonLab;
    NSInteger ParamteUpPsonId;
    NSMutableArray *_UpPsonNameArr;//上级人员
    NSMutableArray *_UpPsonId;
    FallLineView *_Fallview;
    JoinFaluterView *_JoinFalutView;
    ServerUnusualView *_ServerFView;

}

@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(nonatomic,strong)NSMutableArray *psonName_arr;//指派人员名单
@property(nonatomic,strong)NSMutableArray *psonid_arr;//指派人员id
@property(nonatomic,strong)UIView  *backView2;
@property(nonatomic,strong)UIView  *backView4;
@property(nonatomic,strong)NSArray *imageUrlArr;
@property(nonatomic,strong)NSMutableArray *allImageURL;
@property (nonatomic,strong)UIButton *chosUpPsonBtn;
@property (nonatomic,strong)UIButton *PsonBtn;//指派人btn

@end

@implementation GZshenpiVC
- (AFHTTPSessionManager *)manager{

    if (!_manager) {
        
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:NO];
    //NSSLog(@"%ld  %@",self.faultApprovalId,self.Pid);
    NSString *doucumentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    //2.拼接上文件路径
    NSString *filtPath = [doucumentsPath stringByAppendingPathComponent:@"Log.txt"];
    //3.字典从文件中读取数据
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:filtPath];
    if (dic != nil) {
        
        if (![[dic objectForKey:@"username"] isKindOfClass:[NSNull class]] && ![[dic objectForKey:@"password"] isKindOfClass:[NSNull class]]) {
            self.userId = [[dic objectForKey:@"userid"]integerValue];
            self.upper = [[dic objectForKey:@"upper"]integerValue];
        }else{
            //读取保存在中间对象中的数据
            UIApplication *app = [UIApplication sharedApplication];
            
            AppDelegate *delegate = app.delegate;
            self.userId = delegate.userId;
            self.upper = delegate.upper;
        }
    }
    [self PsonLoad];
    [self shangjiPsonLoad];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"故障审批审核";
    self.view.backgroundColor = [UIColor whiteColor];
    _UpPsonNameArr = [[NSMutableArray alloc]init];
    _UpPsonId = [[NSMutableArray alloc]init];
    _psonName_arr = [[NSMutableArray alloc]init];
    _psonid_arr = [[NSMutableArray alloc]init];
    /*
    //读取保存在中间对象中的数据
    UIApplication *app = [UIApplication sharedApplication];
    
    AppDelegate *delegate = app.delegate;
    self.userId = delegate.userId;
    self.upper = delegate.upper;
    NSLog(@"%ld",self.userId);*/
    state = 1;//0是不上报 1是上报

    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    [self starLoad];
    
    //处理多个网络请求并发
//    dispatch_group_t group = dispatch_group_create();
//    dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
//        [self starLoad];
//        
//    });
//    dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
//        [self PsonLoad];
//    });
/*
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    //设置最大线程数
    queue.maxConcurrentOperationCount = 2;
    //创建第一个操作
    NSBlockOperation *opreationA = [NSBlockOperation blockOperationWithBlock:^{
        [self starLoad];
    }];
    NSBlockOperation *operationB = [NSBlockOperation blockOperationWithBlock:^{
        [self PsonLoad];
    }];
    //分别加入到队列中
    [queue addOperation:opreationA];
    [queue addOperation:operationB];
*/
    
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        
//        [self starLoad];
//        
//        
//    });
//    [self PsonLoad];
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        
//        [self PsonLoad];
//        
//    });
    _allImageURL = [[NSMutableArray alloc]init];
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    _time = [dateFormatter stringFromDate:currentDate];
}
//上级人员列表
- (void)shangjiPsonLoad{
    
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_UpPsonURL parameters:@{@"userId":[NSString stringWithFormat:@"%ld",self.userId]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [_UpPsonId removeAllObjects];
        [_UpPsonNameArr removeAllObjects];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSSLog(@"%@",dic);
        YYUpPsonDataModel *datamodel = [[YYUpPsonDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (YYUpPsonBeanList *model in datamodel.beanList) {
            NSString *str = [NSString stringWithFormat:@"%@%c%@%c",model.userN,'(',model.postName,')'];
            [_UpPsonNameArr addObject:str];
            [_UpPsonId addObject:[NSString stringWithFormat:@"%ld",model.userId]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

- (void)starLoad{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"加载中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:K_GZbsMassage parameters:@{gzid:self.Pid} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dic);
        NSDictionary *fau = dic[@"data"][@"FaultRecd"];
        _content_arr = @[fau[@"station"],fau[@"equip"],fau[@"brand"],fau[@"model"],fau[@"ApPerson"],_time,fau[@"discrib"]];
        _imageUrlArr = [fau[@"images"] componentsSeparatedByString:@","];
        for (NSInteger i = 0; i < _imageUrlArr.count; i++) {
            PhotoModel *model = [[PhotoModel alloc]init];
            model.imageName = _imageUrlArr[i];
            [_allImageURL addObject:model];
        }
        
        [self initScorlleview];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        if (status >= 500) {
            _ServerFView = [[ServerUnusualView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) AndTitle:[error userInfo][@"NSLocalizedDescription"]];
            _ServerFView.delegate = self;
            [self.view addSubview:_ServerFView];
        }else if ([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"请求超时。"]) {
            _Fallview = [[FallLineView alloc]initWithFrame:CGRectMake(0,64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _Fallview.delegate = self;
            [self.view addSubview:_Fallview];
        }else if([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"似乎已断开与互联网的连接。"]){
            _JoinFalutView = [[JoinFaluterView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _JoinFalutView.delegate = self;
            [self.view addSubview:_JoinFalutView];
        }else{
            [self showAlert:@"温馨提示" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
  
    }];
}
- (void)FallLineRefresh:(UIButton *)sender{
    
    [_Fallview removeFromSuperview];
    [self starLoad];
}
- (void)ServerRefresh:(UIButton *)sender{
    [_ServerFView removeFromSuperview];
    [self starLoad];
}
- (void)JoinRefersh:(UIButton *)sender{
    [_JoinFalutView removeFromSuperview];
    [self starLoad];
}

- (void)initScorlleview{


    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64);
    _scrollView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+100);
    [self.view addSubview:_scrollView];
    // 隐藏水平滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;

    //故障基本信息背景
    UIView *backView1 = [[UIView alloc]init];
    backView1.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView1];
    [backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_scrollView).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 50));
    }];
    //故障基本信息图标 d_icon01   编辑图标d_icon04 大小30*30
    UIImageView *gzMimageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    gzMimageView.image = [[UIImage imageNamed:@"d_icon01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [backView1 addSubview:gzMimageView];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(45, 15, 150, 21)];
    title.text = @"故障基本信息";
    title.font = [UIFont systemFontOfSize:21.0];
    title.textColor = UIColorFromRGB(0x0093d5);
    [backView1 addSubview:title];
    
    //基本信息详情背景
    //_backView2 = [[UIView alloc]init];
    self.backView2.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:self.backView2];

    
    NSArray *leftTitle_arr = @[@"故障基站",@"设备名称",@"设备品牌",@"设备型号",@"上报人员",@"上报时间",@"故障描述"];


    for (NSInteger i = 0; i < leftTitle_arr.count; ++i) {
        

        UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 5 + i * (21+1), 60, 21)];
        titleName.adjustsFontSizeToFitWidth = YES;
        titleName.font = [UIFont systemFontOfSize:15.0];
        titleName.textColor = UIColorFromRGB(0x4d4d4d);
        titleName.text = leftTitle_arr[i];
        [self.backView2 addSubview:titleName];
        
        if (i < 6) {
            
            UILabel *content = [[UILabel alloc]initWithFrame:CGRectMake(80, 5 + i * (21+1), kSCREEN_WIDTH-85, 21)];
            content.adjustsFontSizeToFitWidth = YES;
            content.text = _content_arr[i];
            content.backgroundColor = UIColorFromRGB(0xf0f0f0);
            content.font = [UIFont systemFontOfSize:15.0];
            content.textColor = UIColorFromRGB(0x4d4d4d);
            [self.backView2 addSubview:content];
            
        }else{
            
            _content1 = [[UILabel alloc]initWithFrame:CGRectZero];
            _content1.font = [UIFont systemFontOfSize:15.0];
            _content1.adjustsFontSizeToFitWidth = YES;
            _content1.numberOfLines = 0;
            _content1.backgroundColor = UIColorFromRGB(0xf0f0f0);
            _content1.text = _content_arr[i];
            _content1.textColor = UIColorFromRGB(0x4d4d4d);
            CGRect rect = [_content1.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-85, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_content1.font,NSFontAttributeName, nil] context:nil];
            CGFloat height = rect.size.height;
            _content1.frame = CGRectMake(80, 5 + i * (21+1), kSCREEN_WIDTH-85, height+5);
            [self.backView2 addSubview:_content1];
        }
        
    }
    
    if (_imageUrlArr.count == 1 && [_imageUrlArr[0] length] == 0){
        self.backView2.frame = CGRectMake(0, 52, kSCREEN_WIDTH, CGRectGetMaxY(_content1.frame)+5);
    }else{
    //图片展示
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(imageSize, imageSize);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    //    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_content1.frame)+2, kSCREEN_WIDTH, imageSize + 20) collectionViewLayout:layout];
    
    [collectionView registerClass:[PhotoCollectionCell class] forCellWithReuseIdentifier:collectionViewCellIdentf];
    UINib *nib = [UINib nibWithNibName:@"PhotoCollectionCell" bundle:nil];
    [collectionView registerNib:nib forCellWithReuseIdentifier:@"imageCell"];
    collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    [self.backView2 addSubview:collectionView];
    self.backView2.frame = CGRectMake(0, 52, kSCREEN_WIDTH, CGRectGetMaxY(collectionView.frame)+5);
    }
    
    //故障审批审核
    _backView3 = [[UIView alloc]init];
    _backView3.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView3];
    [_backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView2.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 50));
    }];
    UIImageView *editImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    editImageView.image = [[UIImage imageNamed:@"d_icon04"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_backView3 addSubview:editImageView];
    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(45, 15, 150, 21)];
    title2.adjustsFontSizeToFitWidth = YES;
    title2.text = @"故障审核审批";
    title2.font = [UIFont systemFontOfSize:21.0];
    title2.textColor = UIColorFromRGB(0x0093d5);
    [_backView3 addSubview:title2];
    
    
    //审核审批具体信息背景
    self.backView4 = [[UIView alloc]init];
    self.backView4.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:self.backView4];

    NSArray *leftT = @[@"是否上报",@"审核时间"];
    for (NSInteger i = 0; i < leftT.count; ++i) {
        UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 10 + i * (21+15), 60, 21)];
        titleName.font = [UIFont systemFontOfSize:15.0];
        titleName.textColor = UIColorFromRGB(0x4d4d4d);
        titleName.adjustsFontSizeToFitWidth = YES;
        titleName.text = leftT[i];
        [self.backView4 addSubview:titleName];
        if (i == 0) {
            
            _swi = [[ZJSwitch alloc]initWithFrame:CGRectMake(80, 7, 60, 31)];
            _swi.transform = CGAffineTransformMakeScale(0.75, 0.75);
            _swi.tintColor = [UIColor lightGrayColor];
            _swi.onText = @"是";
            _swi.offText = @"否";
            _swi.on = YES;
            [_swi addTarget:self action:@selector(handleSwitch:) forControlEvents:UIControlEventValueChanged];
            [self.backView4 addSubview:_swi];
        }else{
            
            NSDate *currentDate = [NSDate date];//获取当前时间，日期
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
            UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(80, 5 + i * (31+5), kSCREEN_WIDTH/2, 30)];
            timeLab.adjustsFontSizeToFitWidth = YES;
            timeLab.text = [dateFormatter stringFromDate:currentDate];
            timeLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
            timeLab.font = [UIFont systemFontOfSize:13.0];
            timeLab.textColor = UIColorFromRGB(0x4d4d4d);
            [self.backView4 addSubview:timeLab];
        }
    }
    if (self.upper == 1) {
        _swi.on = NO;
        _swi.userInteractionEnabled = NO;
        [self.backView4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView3.mas_bottom).offset(2);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 77));
        }];

    }else{
        _chosupPsonLab = [[UILabel alloc]initWithFrame:CGRectMake(15, 76, 65, 30)];
        _chosupPsonLab.text = @"报  送  人";
        _chosupPsonLab.textColor = UIColorFromRGB(0x4d4d4d);
        _chosupPsonLab.font = [UIFont systemFontOfSize:15.0];
        [self.backView4 addSubview:_chosupPsonLab];
        _chosUpPsonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _chosUpPsonBtn.frame = CGRectMake(80, 76, kSCREEN_WIDTH-90, 30);
        _chosUpPsonBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _chosUpPsonBtn.layer.cornerRadius = 5;
        _chosUpPsonBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [_chosUpPsonBtn setTitle:@"选择报送人名称" forState:UIControlStateNormal];
        [_chosUpPsonBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
        [_chosUpPsonBtn addTarget:self action:@selector(chosupPsonBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.backView4 addSubview:_chosUpPsonBtn];
        [self.backView4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView3.mas_bottom).offset(2);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 114));
        }];
        //默认没有指派人员 先显示备注栏
        _backView6 = [[UIView alloc]init];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];
        [_backView6 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backView4.mas_bottom).offset(0);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 85));
        }];
        UILabel *shTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, 21)];
        shTitle.adjustsFontSizeToFitWidth = YES;
        shTitle.textColor = UIColorFromRGB(0x4d4d4d);
        shTitle.text = @"审核备注";
        shTitle.font = [UIFont systemFontOfSize:15.0];
        [_backView6 addSubview:shTitle];
        _bztextView = [[PHATextView alloc]initWithFrame:CGRectMake(80, 0, kSCREEN_WIDTH-85, 80)];
        _bztextView.font = [UIFont systemFontOfSize:15.0];
        _bztextView.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _bztextView.placeholder = @"不超过100个字符";
        //_bztextView.textColor = UIColorFromRGB(0x4d4d4d);
        _bztextView.delegate = self;
        _bztextView.backgroundColor = [UIColor whiteColor];
        _bztextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _bztextView.layer.borderWidth = 0.5;
        _bztextView.layer.cornerRadius = 3;
        [_backView6 addSubview:_bztextView];
        
        _backView7 = [[UIView alloc]init];
        _backView7.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView7];
        [_backView7 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6.mas_bottom).offset(10);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
        }];
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView7 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView7).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView7 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView7).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
    }
 

}
- (void)handleSwitch:(ZJSwitch *)sender{
    //upper 0 不是最高等级可以继续上报 1是最高权限 不能上报
    if (sender.isOn) {

        state = 1;
        //移除 _backView5,_backView8,_backView9 添加_backView6,_backView7
        _chosUpPsonBtn.alpha = 1;
        _chosupPsonLab.alpha = 1;
        [self.backView4 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView3.mas_bottom).offset(2);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 114));
        }];
        [_backView5 removeFromSuperview];
        [_backView8 removeFromSuperview];
        [_backView9 removeFromSuperview];
        
        //默认没有指派人员 先显示备注栏
        _backView6 = [[UIView alloc]init];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];
        [_backView6 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backView4.mas_bottom).offset(0);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 85));
        }];
        UILabel *shTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, 21)];
        shTitle.adjustsFontSizeToFitWidth = YES;
        shTitle.textColor = UIColorFromRGB(0x4d4d4d);
        shTitle.text = @"审核备注";
        shTitle.font = [UIFont systemFontOfSize:15.0];
        [_backView6 addSubview:shTitle];
        _bztextView = [[PHATextView alloc]initWithFrame:CGRectMake(80, 0, kSCREEN_WIDTH-85, 80)];
        _bztextView.font = [UIFont systemFontOfSize:15.0];
        _bztextView.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _bztextView.placeholder = @"不超过100个字符";
        //_bztextView.textColor = UIColorFromRGB(0x4d4d4d);
        _bztextView.delegate = self;
        _bztextView.backgroundColor = [UIColor whiteColor];
        _bztextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _bztextView.layer.borderWidth = 0.5;
        _bztextView.layer.cornerRadius = 3;
        [_backView6 addSubview:_bztextView];
        
        _backView7 = [[UIView alloc]init];
        _backView7.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView7];
        [_backView7 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6.mas_bottom).offset(10);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
        }];
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView7 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView7).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView7 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView7).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];

        
    }else{
        //是   移除_backView6,_backView7  增加_backView5,_backView8,_backView9
        state = 0;
        _chosUpPsonBtn.alpha = 0;
        _chosupPsonLab.alpha = 0;
        [_chosUpPsonBtn setTitle:@"选择报送人名称" forState:UIControlStateNormal];
        [_chosUpPsonBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
        ParamteUpPsonId = 0;
        [self.backView4 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView3.mas_bottom).offset(2);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 77));
        }];
        [_backView6 removeFromSuperview];
        [_backView7 removeFromSuperview];
        _backView5 = [[UIView alloc]init];
        _backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView5];
        [_backView5 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backView4.mas_bottom).offset(0);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 35));
        }];
        UILabel *title3 = [[UILabel alloc]initWithFrame:CGRectMake(15, 4, 60, 21)];
        title3.adjustsFontSizeToFitWidth = YES;
        title3.textColor = UIColorFromRGB(0x4d4d4d);
        title3.text = @"指派人员";
        title3.font = [UIFont systemFontOfSize:15.0];
        [_backView5 addSubview:title3];
        _PsonBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _PsonBtn.frame = CGRectMake(80, 0, kSCREEN_WIDTH/2, 30);
        [_PsonBtn setTitle:@"输入指派人员姓名" forState:UIControlStateNormal];
        [_PsonBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
        _PsonBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
        //_PsonBtn.titleLabel.font = [UIFont systemFontOfSize:19.0];
        //button 标题对齐方式
        _PsonBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //设置标题距离左边边距10的点
        _PsonBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [_PsonBtn addTarget:self action:@selector(choosePson:) forControlEvents:UIControlEventTouchUpInside];
        [_backView5 addSubview:_PsonBtn];
        
        _backView8 = [[UIView alloc]init];
        _backView8.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView8];
        [_backView8 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView5.mas_bottom).offset(0);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 85));
        }];
        UILabel *shTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, 21)];
        shTitle.adjustsFontSizeToFitWidth = YES;
        shTitle.textColor = UIColorFromRGB(0x4d4d4d);
        shTitle.text = @"审核备注";
        shTitle.font = [UIFont systemFontOfSize:15.0];
        [_backView8 addSubview:shTitle];
        _bztextView = [[PHATextView alloc]initWithFrame:CGRectMake(80, 0, kSCREEN_WIDTH-85, 80)];
        _bztextView.font = [UIFont systemFontOfSize:15.0];
        _bztextView.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _bztextView.placeholder = @"不超过100个字符";
        //_bztextView.textColor = UIColorFromRGB(0x4d4d4d);
        _bztextView.delegate = self;
        _bztextView.backgroundColor = [UIColor whiteColor];
        _bztextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _bztextView.layer.borderWidth = 0.5;
        _bztextView.layer.cornerRadius = 3;
        [_backView8 addSubview:_bztextView];
        
        _backView9 = [[UIView alloc]init];
        _backView9.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView9];
        [_backView9 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView8.mas_bottom).offset(10);
            make.left.equalTo(_scrollView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
        }];
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView9 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView9).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView9 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView9).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
    }
}
- (void)chosupPsonBtnClick:(UIButton *)sender{
    
    
    MHActionSheet *actionSheet = [[MHActionSheet alloc] initSheetWithTitle:nil style:MHSheetStyleDefault itemTitles:_UpPsonNameArr];
    __weak typeof(self) weakSelf = self;
    [actionSheet didFinishSelectIndex:^(NSInteger index, NSString *title) {
        [weakSelf.chosUpPsonBtn setTitle:title forState:UIControlStateNormal];
        [weakSelf.chosUpPsonBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        ParamteUpPsonId = [_UpPsonId[index]integerValue];
        //equipID = [_sbID[index]integerValue];
        
        
    }];
}
- (void)PsonLoad{

        
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [_manager POST:K_lowerPson parameters:@{userid:[NSString stringWithFormat:@"%ld",self.userId]} progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [_psonName_arr removeAllObjects];
            [_psonid_arr removeAllObjects];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            YYUpPsonDataModel *datamodel = [[YYUpPsonDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
            for (YYUpPsonBeanList *model in datamodel.beanList) {
                NSString *str = [NSString stringWithFormat:@"%@%c%@%c",model.userN,'(',model.postName,')'];
                [_psonName_arr addObject:str];
                [_psonid_arr addObject:[NSString stringWithFormat:@"%ld",model.userId]];
            }
            
           
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        

}
- (void)choosePson:(UIButton *)sender{

    
    
    MHActionSheet *actionSheet = [[MHActionSheet alloc] initSheetWithTitle:nil style:MHSheetStyleDefault itemTitles:_psonName_arr];
    __weak typeof(self) weakSelf = self;
    [actionSheet didFinishSelectIndex:^(NSInteger index, NSString *title) {
        [weakSelf.PsonBtn setTitle:title forState:UIControlStateNormal];
        [weakSelf.PsonBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        psonid = [_psonid_arr[index]integerValue];

    }];

}
#pragma mark-->collectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

        return _allImageURL.count;

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    PhotoModel *model = _allImageURL[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AlbumVC *alVC = [[AlbumVC alloc]init];
    alVC.ImageUrl = _imageUrlArr;
    alVC.index = indexPath.row;
    [self.navigationController pushViewController:alVC animated:YES];
}
- (void)up{
    
    [UIView animateWithDuration:0 delay:1.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _updata.userInteractionEnabled = NO;
    } completion:^(BOOL finished) {
        _updata.userInteractionEnabled = YES;
    }];
    if (state == 1 && self.faultApprovalId != 0) {
    
            _parameter = @{faultid:self.Pid,sbState:[NSString stringWithFormat:@"%ld",state],updataTime:_content_arr[5],apID:[NSString stringWithFormat:@"%ld",self.userId],disc:_bztextView.text,@"faultApprovalId":[NSString stringWithFormat:@"%ld",self.faultApprovalId],@"HeadPsonId":[NSString stringWithFormat:@"%ld",ParamteUpPsonId]};
            if (_bztextView.text.length < 1 || [_chosUpPsonBtn.titleLabel.text isEqualToString:@"选择报送人名称"]) {
                [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"确定"];
            }else if (_bztextView.text.length > 100){
                [self showAlert:@"温馨提示" message:@"备注信息超过限定字数请重新填写" actionWithTitle:@"确定"];
            }else{
                [self networkUP];
            }
        
    }else if (state == 1 && self.faultApprovalId == 0){
        _parameter = @{faultid:self.Pid,sbState:[NSString stringWithFormat:@"%ld",state],updataTime:_content_arr[5],apID:[NSString stringWithFormat:@"%ld",self.userId],disc:_bztextView.text,@"HeadPsonId":[NSString stringWithFormat:@"%ld",ParamteUpPsonId]};
        if (_bztextView.text.length < 1 || [_chosUpPsonBtn.titleLabel.text isEqualToString:@"选择报送人名称"]) {
            [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"确定"];
        }else if (_bztextView.text.length > 100){
            [self showAlert:@"温馨提示" message:@"备注信息超过限定字数请重新填写" actionWithTitle:@"确定"];
        }else{
            [self networkUP];
        }

    }else if (state == 0 && self.faultApprovalId != 0) {
        _parameter = @{faultid:self.Pid,sbState:[NSString stringWithFormat:@"%ld",state],updataTime:_content_arr[5],apID:[NSString stringWithFormat:@"%ld",self.userId],disc:_bztextView.text,hdId:[NSString stringWithFormat:@"%ld",psonid],@"faultApprovalId":[NSString stringWithFormat:@"%ld",self.faultApprovalId]};
        if (_bztextView.text.length < 1 || [_PsonBtn.titleLabel.text isEqualToString:@"输入指派人员姓名"]) {
             [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"确定"];
        }else if (_bztextView.text.length > 100){
            [self showAlert:@"温馨提示" message:@"备注信息超过限定字数请重新填写" actionWithTitle:@"确定"];
        }else{
            [self networkUP];
        }
    }else if (state == 0 && self.faultApprovalId == 0){
        
        _parameter = @{faultid:self.Pid,sbState:[NSString stringWithFormat:@"%ld",state],updataTime:_content_arr[5],apID:[NSString stringWithFormat:@"%ld",self.userId],disc:_bztextView.text,hdId:[NSString stringWithFormat:@"%ld",psonid]};
        if (_bztextView.text.length < 1 || [_PsonBtn.titleLabel.text isEqualToString:@"输入指派人员姓名"]) {
            [self showAlert:@"温馨提示" message:@"请完善提交信息" actionWithTitle:@"确定"];
        }else if (_bztextView.text.length > 100){
            [self showAlert:@"温馨提示" message:@"备注信息超过限定字数请重新填写" actionWithTitle:@"确定"];
        }else{
            [self networkUP];
        }
    }else{
    
    }

    

}
- (void)networkUP{
    NSSLog(@"%@",_parameter);
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:K_spUpdata parameters:_parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSInteger status = [dic[@"status"]integerValue];
        NSLog(@"%ld",status);
        if (status == 1) {
            
            //延时操作
            [UIView animateWithDuration:0 delay:2.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                
                [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
                
            } completion:^(BOOL finished) {
                
                /*if (self.faultApprovalId != 0) {
                    [self.navigationController popViewControllerAnimated:YES];
                }else{*/
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    /*
                //将控制器压入栈，然后去栈中查找
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[WorkViewController class]]) {
                        
                        WorkViewController *work = (WorkViewController *)controller;
                        [self.navigationController popToViewController:work animated:YES];
                    }
                }
                }*/
                }];
            
            
        }else{
            
        [self showAlert:@"提交失败" message:dic[@"message"] actionWithTitle:@"我知道了"];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
//        NSSLog(@"%ld",(long)responses.statusCode);
        if (status >=500) {
            [self showAlert:@"提交失败" message:@"服务器出错" actionWithTitle:@"我知道了"];
        }else{
            [self showAlert:@"提交失败" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
    }];

}
- (UIView *)backView2{
    if (!_backView2) {
        _backView2 = [[UIView alloc]init];
    }
    return _backView2;
}
#pragma mark 封装提示框
-(void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)back{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"您是否确定退出当前页面" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *OkAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:cancelAction];
    [alert addAction:OkAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
@end
