//
//  TechMessageView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TechMessageView;
@protocol TechMessageViewDelegate <NSObject>
- (void)returnTechMessageViewHeight:(CGFloat)height;
@end
@interface TechMessageView : UIView
- (instancetype)initWithFrame:(CGRect)frame AndDiction:(NSDictionary *)dic MaxY:(CGFloat)Y;
@property(nonatomic,weak)id <TechMessageViewDelegate> delegate;
@end
