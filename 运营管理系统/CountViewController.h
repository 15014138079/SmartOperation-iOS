//
//  CountViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/1.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "equeipsModel.h"
#import "MBProgressHUD.h"

#import "XJhisBranchCell.h"

#import "XJBranchDataModel.h"
#import "XJhisUserVC.h"
#import "XJhisPsonDataModel.h"
#import "XJhisPsonCell.h"
#import "XJhisStationDataModel.h"

#import "ZJAlertListView.h"
#import "JoinFaluterView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
#import "ServerUnusualView.h"
#import "MyData.h"
#import "XJparameter.h"//站点ID模型
#import "MainViewController.h"
#import "xujianCell.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "XJLRViewController.h"
#import "Besicmodel.h"
#import <CoreLocation/CoreLocation.h>
@interface CountViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,ZJAlertListViewDelegate,ZJAlertListViewDatasource,JoinFalutDelegate,FallLineDelegate,ServerUnsualDelegate,NothingViewDelegate>
@property (nonatomic,assign)NSInteger userId;

@end
