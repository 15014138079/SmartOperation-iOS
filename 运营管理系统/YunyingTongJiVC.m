//
//  YunyingTongJiVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/13.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "YunyingTongJiVC.h"
#import "CompanyVC.h"//分公司
#import "BrandVC.h"//品牌
#import "ProjectTypeVC.h"//项目类型
#import "YearsVC.h"//安装年限
#import "ControlVC.h"//控制类型
#import "YunWeiPsonVC.h"//运维人员
#import "SGTopTitleView.h"
//#import "EquipmentVC.h"


@interface YunyingTongJiVC ()<SGTopTitleViewDelegate, UIScrollViewDelegate>
{
    UIButton *_rightBtn;
 
}
@property (nonatomic, strong) SGTopTitleView *topTitleView;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) NSArray *titles;
@end

@implementation YunyingTongJiVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"运营统计";
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setBackgroundImage:[[UIImage imageNamed:@"histogram_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    [rightBtn setBackgroundImage:[[UIImage imageNamed:@"table_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateSelected];
    
    rightBtn.frame = CGRectMake(0, 0, 22, 16);
    [rightBtn addTarget:self action:@selector(rightItemClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    _rightBtn = rightBtn;

    // 1.添加所有子控制器
    [self setupChildViewController];
    
    
    self.titles = @[@"分公司", @"品牌", @"项目类型", @"安装年限",@"控制类型",@"运维人员"];

    self.topTitleView = [SGTopTitleView topTitleViewWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 44)];
    _topTitleView.layer.borderWidth = 1;
    _topTitleView.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    
    _topTitleView.scrollTitleArr = [NSArray arrayWithArray:_titles];
    
    _topTitleView.delegate_SG = self;
    [self.view addSubview:_topTitleView];
    
    // 创建底部滚动视图
    self.mainScrollView = [[UIScrollView alloc] init];
    _mainScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    _mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width * _titles.count, 0);
    _mainScrollView.backgroundColor = [UIColor clearColor];
    // 开启分页
    _mainScrollView.pagingEnabled = YES;
    // 没有弹簧效果
    _mainScrollView.bounces = NO;
    // 隐藏水平滚动条
    _mainScrollView.showsHorizontalScrollIndicator = NO;
    // 设置代理
    _mainScrollView.delegate = self;
    [self.view addSubview:_mainScrollView];
    
    CompanyVC *oneVC = [[CompanyVC alloc] init];
    [self.mainScrollView addSubview:oneVC.view];
    [self addChildViewController:oneVC];
    [self.view insertSubview:_mainScrollView belowSubview:_topTitleView];
    

}
#pragma mark - - - SGTopScrollMenu代理方法
- (void)SGTopTitleView:(SGTopTitleView *)topTitleView didSelectTitleAtIndex:(NSInteger)index{
    
    // 1 计算滚动的位置
    CGFloat offsetX = index * self.view.frame.size.width;
    self.mainScrollView.contentOffset = CGPointMake(offsetX, 0);
    
    // 2.给对应位置添加对应子控制器
    [self showVc:index];
    _rightBtn.selected = NO;
}

// 添加所有子控制器
- (void)setupChildViewController {
    // 分公司
    CompanyVC *oneVC = [[CompanyVC alloc] init];
    [self addChildViewController:oneVC];
    
    // 品牌
    BrandVC *twoVC = [[BrandVC alloc] init];
    [self addChildViewController:twoVC];
    
    // 项目类型
    ProjectTypeVC *threeVC = [[ProjectTypeVC alloc] init];
    [self addChildViewController:threeVC];
    
    // 安装年限
    YearsVC *fourVC = [[YearsVC alloc] init];
    [self addChildViewController:fourVC];
    //控制类型
    ControlVC *fiveVC = [[ControlVC alloc]init];
    [self addChildViewController:fiveVC];
    //运维人员
    YunWeiPsonVC *sixVC = [[YunWeiPsonVC alloc]init];
    [self addChildViewController:sixVC];
    //设备型号
   // EquipmentVC *equipVC = [[EquipmentVC alloc]init];
    //[self addChildViewController:equipVC];
}

// 显示控制器的view
- (void)showVc:(NSInteger)index {
    
    CGFloat offsetX = index * self.view.frame.size.width;
    
    UIViewController *vc = self.childViewControllers[index];
    
    // 判断控制器的view有没有加载过,如果已经加载过,就不需要加载
    if (vc.isViewLoaded) return;
    
    [self.mainScrollView addSubview:vc.view];
    vc.view.frame = CGRectMake(offsetX, 0, self.view.frame.size.width, self.view.frame.size.height);
    _rightBtn.selected = NO;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    // 计算滚动到哪一页
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    // 1.添加子控制器view
    [self showVc:index];
    
    // 2.把对应的标题选中
    UILabel *selLabel = self.topTitleView.allTitleLabel[index];
    
    // 3.滚动时，改变标题选中
    [self.topTitleView scrollTitleLabelSelecteded:selLabel];
    // 3.让选中的标题居中
    [self.topTitleView scrollTitleLabelSelectededCenter:selLabel];
    _rightBtn.selected = NO;
    
}

- (void)rightItemClick:(UIButton *)sender{

    //    实现多选
     BOOL isSelected = sender.isSelected;
     if (isSelected)
     {
     //将当前状态设置为normal
         sender.selected = NO;

        NSNotification *notice = [NSNotification notificationWithName:@"123" object:nil];
         [[NSNotificationCenter defaultCenter]postNotification:notice];
         

     }
     else
     {
         sender.selected = YES;

         NSNotification *notice = [NSNotification notificationWithName:@"456" object:nil];
         [[NSNotificationCenter defaultCenter]postNotification:notice];

     }
    


}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
