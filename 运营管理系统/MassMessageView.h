//
//  MassMessageView.h
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+XTYWExtension.h"
#import "PHATextView.h"
//图片上传
#import "KKUploadPhotoCollectionViewCell.h"
#import "KKPhotoPickerManager.h"
@class MassMessageView;
@protocol MassMessageViewDelegate <NSObject>
//发生时间选择代理方法
- (void)FinderTimeChoose:(UIButton *)sender;
//产品选择代理方法
- (void)ProductChoose:(UIButton *)sender;
//图片代理
- (void)MasViewAddAndSetSubViews:(KKUploadPhotoCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath;
- (void)MasViewCollectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
//提交 取消代理
- (void)upBtnClick:(UIButton *)sender;
- (void)RevokeClick:(UIButton *)sender;
@end

@interface MassMessageView : UIView <UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)UITextField *NumberTF;
@property(nonatomic,strong)UIButton *FinderTimeBtn;//发生时间
@property(nonatomic,strong)UIButton *ChooseProductBTN;
@property(nonatomic,strong)UILabel *EquipeNum;//设备编号
@property(nonatomic,strong)UILabel *MonitorType;//监测类型
@property(nonatomic,strong)UILabel *ProductModel;//产品型号
@property(nonatomic,strong)UITextField *GuzhangTF;
@property(nonatomic,strong)PHATextView *DiscribTextView;
@property(nonatomic, strong) UICollectionView *collectionView; //添加图片,每个cell内有一个imageView
@property(nonatomic, strong) NSMutableArray *imageArray;
@property(nonatomic,strong)UIButton *UpBtn;
@property(nonatomic,weak)id <MassMessageViewDelegate> delegate;
- (instancetype)initWithFrame:(CGRect)frame AndDataDictionary:(NSDictionary *)dataDic;
@end
