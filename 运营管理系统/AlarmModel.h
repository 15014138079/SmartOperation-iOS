//
//  AlarmModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/5.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlarmModel : NSObject

@property (nonatomic, strong)NSString *title;//故障标题
@property (nonatomic, assign)NSInteger state;//故障状态
@property (nonatomic, strong)NSString *discrib;//故障描述
@property (nonatomic, strong)NSString *occurT;//发现时间
@property (nonatomic, assign)NSInteger parameters;//参数
@end
