//
//  YYFeedListBeanlistModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/8.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYFeedListBeanlistModel <NSObject>

@end

@interface YYFeedListBeanlistModel : JSONModel
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *time;
@property (nonatomic,strong)NSString *faultDisc;
@property (nonatomic,strong)NSString *entName;
@property (nonatomic,assign)NSInteger state;

@property (nonatomic,assign)NSInteger psonState;//质量反馈确认才有这个字段
@end
/*
 "id": 2,
 "time": "2017-02-08 15:05:46",
 "faultDisc": "测试",
 "entName": "古浪县新洁城区生活污水处理有限公司",
 "state": 0
 */
