//
//  AppDelegate.h
//  运营管理系统
//
//  Created by 杨毅 on 16/8/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic)NSInteger userId;
@property (strong, nonatomic)NSString *username;
@property (strong, nonatomic)NSString *loginName;
@property (strong, nonatomic)NSString *tel;
@property (assign, nonatomic)NSInteger GZid;
@property (assign, nonatomic)NSInteger level;
@property (strong, nonatomic)NSString *levelName;
@property (strong, nonatomic)NSString *email;
@property (strong, nonatomic)NSArray *List;
//故障处理模块中 故障开始时间
@property (strong, nonatomic)NSString *StarTime;
@property (strong, nonatomic)NSString *iconUrl;
//审核中判断当前用户是否为最高等级权限
@property (assign, nonatomic)NSInteger upper;


@end

