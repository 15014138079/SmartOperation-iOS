//
//  SectionTowFaultModel.h
//  WSDropMenuView
//
//  Created by 杨毅 on 16/11/29.
//  Copyright © 2016年 Senro Wong. All rights reserved.
//

#import "JsonModel.h"
#import "SectionThreeFaultModel.h"
@protocol SectionTowFaultModel <NSObject>

@end

@interface SectionTowFaultModel : JSONModel
@property (nonatomic,strong)NSString *code;
@property (nonatomic,strong)NSArray <SectionThreeFaultModel> *faultTypes;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *remark;
@property (nonatomic,assign)NSInteger type;
@end
