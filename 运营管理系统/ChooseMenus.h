//
//  ChooseMenus.h
//  多级菜单
//
//  Created by 杨毅 on 16/12/1.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#define WSNoFound (-1)

typedef void(^ReturnBlock)(NSString * block , NSInteger ID);

@interface WSIndexPath : NSObject

@property (nonatomic,assign) NSInteger row; //左边第一级的行
@property (nonatomic,assign) NSInteger item; //左边第二级的行
@property (nonatomic,assign) NSInteger rank; //左边第三级的行

+ (instancetype)twIndexPathWithRow:(NSInteger )row
                              item:(NSInteger )item
                              rank:(NSInteger )rank;

@end


@class ChooseMenus;
@protocol ChooseMenusDataSource <NSObject>

- (void)typeIDOne:(NSArray *)typeIDOne;//第一级
- (void)typeIDTow:(NSArray *)typeIDTow;//第二级
- (void)typeIDThree:(NSArray *)typeIDThree;//第三级

- (NSInteger )dropMenuView:(ChooseMenus *)dropMenuView numberWithIndexPath:(WSIndexPath *)indexPath;

- (NSString *)dropMenuView:(ChooseMenus *)dropMenuView titleWithIndexPath:(WSIndexPath *)indexPath;

- (void)CancelChooseView:(UIButton *)sender;
@end

@interface ChooseMenus : UIView
@property(nonatomic,weak) id <ChooseMenusDataSource> dataSource;

@property(nonatomic,strong)UIButton *CancelBtn;//确定按钮
@property(nonatomic,copy)ReturnBlock block;
- (void)TitleBlock:(ReturnBlock)block;
- (void)reloadLeftTableView;
- (void)typeIDOne:(NSArray *)typeIDOne;
- (void)typeIDTow:(NSArray *)typeIDTow;
- (void)typeIDThree:(NSArray *)typeIDThree;

@end
