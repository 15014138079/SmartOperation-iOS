//
//  FaultPhotoView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "FaultPhotoView.h"
#import "PhotoCollectionCell.h"
#import "PhotoModel.h"
static NSString *collectionViewCellIdentf = @"collectionViewCell1";
static CGFloat imageSize = 80;
@interface FaultPhotoView()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *_ImageUrlArr;
    NSArray *_allImageArr;
}
@end
@implementation FaultPhotoView
-(instancetype)initWithFrame:(CGRect)frame imageurl:(NSArray *)ImageURL{
    self = [super initWithFrame:frame];
    if (self) {
        _ImageUrlArr = [[NSMutableArray alloc]init];
        _allImageArr = ImageURL;
        //图片展示
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, imageSize + 20) collectionViewLayout:layout];
        
        [collectionView registerClass:[PhotoCollectionCell class] forCellWithReuseIdentifier:collectionViewCellIdentf];
        UINib *nib = [UINib nibWithNibName:@"PhotoCollectionCell" bundle:nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:@"imageCell"];
        collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:collectionView];
        for (NSInteger i = 0; i < ImageURL.count; i++) {
            PhotoModel *model = [[PhotoModel alloc]init];
            model.imageName = ImageURL[i];
            [_ImageUrlArr addObject:model];
        }

    }
    return self;
}
#pragma mark-->collectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return _ImageUrlArr.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    PhotoModel *model = _ImageUrlArr[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    [self.delegate didSelectPhotoURL:_allImageArr andIndex:indexPath.row];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
