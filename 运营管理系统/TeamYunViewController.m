//
//  TeamYunViewController.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/6.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#define INTERVALSIZE (kSCREEN_WIDTH - 90*3)/4
#define BTN_WIDTH  90
#define BTN_HEIGHT 90


#import "TeamYunViewController.h"
#import "HomeButton.h"
#import "RunStateViewController.h"
#import "MapVC.h"
#import "WorkVC.h"
#import "PartVC.h"
@interface TeamYunViewController ()

@end

@implementation TeamYunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSArray *arr = @[@"b_icon01",@"b_icon02",@"b_icon03",@"b_icon04",@"b_icon05",@"b_icon06"];
    NSArray *title_arr = @[@"设备信息",@"定位查询",@"人员管理",@"工作管理",@"车辆管理",@"备件耗材"];
    for (NSInteger i = 0; i < 6; i++) {
        CGFloat a = INTERVALSIZE +i%3*(BTN_WIDTH+INTERVALSIZE);
        CGFloat b = 80 +i/3*(BTN_WIDTH+INTERVALSIZE + 25);
        
        CGFloat c = (kSCREEN_WIDTH - 80*3)/4 + i%3*(80 + (kSCREEN_WIDTH - 80*3)/4);
        HomeButton *btn = [HomeButton homeButton];
        btn.frame = CGRectMake(a,b, BTN_WIDTH, BTN_HEIGHT);
        [btn setImage:[UIImage imageNamed:arr[i]] forState:UIControlStateNormal];
        btn.tag = 2000 + i;
        [btn addTarget:self action:@selector(nextVC:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(c, b+90, 80, 30)];
        title.textAlignment = NSTextAlignmentCenter;
        title.text = title_arr[i];
        [self.view addSubview:title];
    }

}
- (void)nextVC:(HomeButton *)sender{
    
    if (sender.tag == 2000 || sender.tag == 2002 || sender.tag == 2004) {
        
        RunStateViewController *run = [[RunStateViewController alloc]init];
        [self.navigationController pushViewController:run animated:NO];
    }else if (sender.tag == 2001){
    
        MapVC *map = [[MapVC alloc]init];
        [self.navigationController pushViewController:map animated:NO];
    }else if(sender.tag == 2003){
        
        WorkVC *work = [[WorkVC alloc]init];
        [self.navigationController pushViewController:work animated:NO];
        
    }else{
        
        PartVC *part = [[PartVC alloc]init];
        [self.navigationController pushViewController:part animated:NO];
    }
}

@end
