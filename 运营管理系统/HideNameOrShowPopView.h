//
//  HideNameOrShowPopView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HideNameOrShowPopView;
@protocol HideNameOrShowPopViewDelegate <NSObject>
- (void)OKBtnClick:(UIButton *)sender andTag:(NSInteger)tag;
- (void)CancelClick:(UIButton *)sender andTag:(NSInteger)tag;
- (void)dismissHideNameView;
@end
@interface HideNameOrShowPopView : UIView
@property(nonatomic,weak)id <HideNameOrShowPopViewDelegate> delegate;

@end
