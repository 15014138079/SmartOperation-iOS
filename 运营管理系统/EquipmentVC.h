//
//  EquipmentVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/11.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Masonry.h"
#import "EquipModelCell.h"
#import "YYEquipDataModel.h"
#import "DVBarChartView.h"
#import "AppDelegate.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface EquipmentVC : UIViewController<UITableViewDataSource,UITableViewDelegate,DVBarChartViewDelegate,JoinFalutDelegate,FallLineDelegate,ServerUnsualDelegate,NothingViewDelegate>
@property (nonatomic,assign)NSInteger userID;
@end
