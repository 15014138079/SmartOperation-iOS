//
//  OhlistModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/31.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol OhlistModel <NSObject>

@end
@interface OhlistModel : JSONModel
@property (nonatomic,strong)NSString *equip;
@property (nonatomic,strong)NSString *equipBrand;
@property (nonatomic,strong)NSString *fixName;
@property (nonatomic,strong)NSString *fixMtPcs;

@property (nonatomic,strong)NSString *station;
@property (nonatomic,strong)NSString *faultTime;
@property (nonatomic,assign)NSInteger state;
@property (nonatomic,strong)NSString *faultLevel;

@property (nonatomic,assign)NSInteger fixTime;
//@property (nonatomic,strong)NSString *fixResult;
@property (nonatomic,strong)NSString *equipModel;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *title;

@property (nonatomic,strong)NSString *time;
@property (nonatomic,strong)NSString *fixResult;
@property (nonatomic,strong)NSString *images;
@property (nonatomic,strong)NSString *fixType;

@end
/*
 "equip": "宇星COD在线自动监测仪",
 "equipBrand": "宇星",
 "fixName": "易铮",
 "fixMtPcs": "是",
 "station": "佳创电子排口",
 "faultTime": "2016-10-28 19:06:01",
 "state": 0,
 "faultLevel": "选择级别",
 "fixTime": 123,
 "equipModel": "YX-CODCR-Ⅱ",
 "id": 43,
 "title": "你有病",
 "time": "2016-09-12 12:15:01",
 "fixResult": "输入分析结果",
 "fixType": "选择类型"
 */