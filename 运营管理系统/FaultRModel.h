//
//  FaultRModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "shModel.h"
#import "Jxmodel.h"
#import "QRmodel.h"
@interface FaultRModel : JSONModel

@property (nonatomic,strong)NSString *sbDiscrib;
@property (nonatomic,strong)NSString *equipBrand;
@property (nonatomic,strong)NSString *sbTime;
@property (nonatomic,strong)NSString *equipName;

@property (nonatomic,strong)NSArray <shModel> *sh;
@property (nonatomic,strong)NSString *stationName;
@property (nonatomic,strong)NSArray <Jxmodel> *jx;
@property (nonatomic,strong)NSString *sbPson;
@property (nonatomic,strong)NSString *equipModel;
@property (nonatomic,strong)NSArray <QRmodel> *qr;
@end
/*
 "jx": [],
 "sbDiscrib": "恶魔",
 "equipBrand": "宇星科技",
 "sbTime": "2016-11-08 15:57:10",
 "equipName": "废水在线监测设备",
 "sh": [],
 "stationName": "兰考马目造纸有限公司总排口",
 "sbPson": "周金磊",
 "equipModel": "YX-CODcr-Ⅱ"
 */
