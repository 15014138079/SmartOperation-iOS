//
//  UnusualBaseView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/24.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "UnusualBaseView.h"
#import "Masonry.h"
@implementation UnusualBaseView
{
    UIImageView *UnusualImageView;
    UILabel *UnusualLab;
    
    
}
- (instancetype)initWithFrame:(CGRect)frame UnusualState:(NSInteger)UnusualStatus AndUnusualTitle:(NSString *)Title{


    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = UIColorFromRGB(0xffffff);
        UnusualImageView = [[UIImageView alloc]init];
        [self addSubview:UnusualImageView];
        [UnusualImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(150, 150));
        }];
        UnusualLab = [[UILabel alloc]init];
        UnusualLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:UnusualLab];
        [UnusualLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(0);
            make.top.equalTo(UnusualImageView.mas_bottom).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 21));
        }];
        UIButton *refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [refreshBtn setTitle:@"点击刷新" forState:UIControlStateNormal];
        refreshBtn.backgroundColor = UIColorFromRGB(0x0093d5);
        refreshBtn.layer.cornerRadius = 2;
        [refreshBtn addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:refreshBtn];
        [refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(UnusualLab.mas_bottom).offset(5);
            make.left.equalTo(self).offset((kSCREEN_WIDTH-80)/2);
            make.size.mas_equalTo(CGSizeMake(80, 25));
        }];
        switch (UnusualStatus) {
            case NotingDataStatus:
                UnusualImageView.image = [UIImage imageNamed:@"ic_smile_gray"];
                UnusualLab.text = Title;
                break;
            case ObtainDataFalStatus:
                UnusualImageView.image = [UIImage imageNamed:@"ic_smile_gray"];
                UnusualLab.text = Title;
                break;
            case OtherStatus:
                UnusualImageView.image = [UIImage imageNamed:@"img01"];
                UnusualLab.text = Title;
                break;
            case 0:
                //连接超时
                UnusualImageView.image = [UIImage imageNamed:@"img02"];
                UnusualLab.text = Title;
                break;
            case ServerQuestionStatus:
                UnusualImageView.image = [UIImage imageNamed:@"img03"];
                UnusualLab.text = Title;
                break;
            case 404:
                UnusualImageView.image = [UIImage imageNamed:@"img01"];
                UnusualLab.text = Title;
                break;
            default:
                break;
        }
    }
    return self;
}

- (void)refresh:(UIButton *)sender{
    
    [self.delegate RefreshBtnClick:sender];
}

@end
