//
//  MassHistoryListVC.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "MassHistoryListVC.h"

@interface MassHistoryListVC ()
{
    NSInteger pageNumber;
    NSDictionary *_prameter;
    FallLineView *_Fallview;
    JoinFaluterView *_JoinFalutView;
    ServerUnusualView *_ServerFView;
    NothingDataView *_nothing;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation MassHistoryListVC
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.tableview.mj_header beginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"历史质量反馈处理列表";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *letfItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]style:UIBarButtonItemStylePlain target:self action:@selector(leftItemClick)];
    self.navigationItem.leftBarButtonItem = letfItem;
    
    _dataSource = [[NSMutableArray alloc]init];
    _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) style:UITableViewStylePlain];
    _tableview.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.rowHeight = 65;
    _tableview.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableview];
    [self.tableview registerNib:[UINib nibWithNibName:@"YYMassFeedListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    _tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageNumber = 0;
        _prameter = @{@"userId":[NSString stringWithFormat:@"%ld",self.userID],@"pageNum":[NSString stringWithFormat:@"%ld",pageNumber],@"state":@0};
        [self networking];
    }];
    _tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNumber = pageNumber + 1;
        _prameter = @{@"userId":[NSString stringWithFormat:@"%ld",self.userID],@"pageNum":[NSString stringWithFormat:@"%ld",pageNumber],@"state":@0};
        [self networking];
    }];

}
- (void)EndRefresh{
    [self.tableview.mj_header endRefreshing];
    [self.tableview.mj_footer endRefreshing];
}
- (void)networking{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"加载中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:FeedBackConfirmURL parameters:_prameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self EndRefresh];
        [_dataSource removeAllObjects];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        YYFeedListDataModel *dataModel = [[YYFeedListDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (YYFeedListBeanlistModel *ListModel in dataModel.beanList) {
            [_dataSource addObject:ListModel];
        }
        if (_dataSource.count == 0) {
            _nothing = [[NothingDataView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) title:@"没有更多数据了^o^"];
            _nothing.delegate = self;
            [self.view addSubview:_nothing];
        }

        [self.tableview reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self EndRefresh];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        if (status >= 500) {
            _ServerFView = [[ServerUnusualView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) AndTitle:[error userInfo][@"NSLocalizedDescription"]];
            _ServerFView.delegate = self;
            [self.view addSubview:_ServerFView];
        }else if ([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"请求超时。"]) {
            _Fallview = [[FallLineView alloc]initWithFrame:CGRectMake(0,64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _Fallview.delegate = self;
            [self.view addSubview:_Fallview];
        }else if([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"似乎已断开与互联网的连接。"]){
            _JoinFalutView = [[JoinFaluterView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _JoinFalutView.delegate = self;
            [self.view addSubview:_JoinFalutView];
        }else{
            [self showAlert:@"温馨提示" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }

    }];

}
/*失败页面代理方法*/
- (void)NothingRefresh:(UIButton *)sender{
    [_nothing removeFromSuperview];
    [self.tableview.mj_header beginRefreshing];
}
- (void)FallLineRefresh:(UIButton *)sender{
    [_Fallview removeFromSuperview];
    [self.tableview.mj_header beginRefreshing];
}
- (void)JoinRefersh:(UIButton *)sender{
    [_JoinFalutView removeFromSuperview];
    [self.tableview.mj_header beginRefreshing];
}
- (void)ServerRefresh:(UIButton *)sender{
    [_ServerFView removeFromSuperview];
    [self.tableview.mj_header beginRefreshing];
}
#pragma mark-->tableviewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YYMassFeedListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    YYFeedListBeanlistModel *model = _dataSource[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YYFeedListBeanlistModel *model = _dataSource[indexPath.row];
    YYMassFeedDetailVC *detailVC = [[YYMassFeedDetailVC alloc]init];
    detailVC.qualityID = model.ID;
    detailVC.state = model.state;
    [self.navigationController pushViewController:detailVC animated:YES];
    
}

#pragma mark 封装提示框
- (void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)leftItemClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
