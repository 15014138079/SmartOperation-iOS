//
//  brandModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol brandModel <NSObject>

@end
@interface brandModel : JSONModel
@property (nonatomic,assign)float percent;
@property (nonatomic,strong)NSString *brandN;
@property (nonatomic,assign)NSInteger value;
@end
