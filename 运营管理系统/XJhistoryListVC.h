//
//  XJhistoryListVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "YYXJdataModel.h"
#import "YYXJhistoryListCell.h"
#import "XJRecordVC.h"
#import "MJRefresh.h"
#import "AppDelegate.h"
#import "CJCalendarViewController.h"
#import "NothingDataView.h"
#import "ServerUnusualView.h"
#import "JoinFaluterView.h"
#import "FallLineView.h"
#import "MBProgressHUD.h"
@interface XJhistoryListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,CalendarViewControllerDelegate,JoinFalutDelegate,FallLineDelegate,NothingViewDelegate,ServerUnsualDelegate>
@property(nonatomic,assign)NSInteger userID;
@property (nonatomic,assign)NSInteger stationId;
@end
