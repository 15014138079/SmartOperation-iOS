//
//  UIView+XTYWExtension.h
//  Dear宝贝
//
//  Created by dearbaobei on 16/3/7.
//  Copyright © 2016年 dearbaobei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (XTYWExtension)
@property (nonatomic,assign)CGSize size;
@property (nonatomic,assign)CGFloat width;
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,assign)CGFloat x;
@property (nonatomic,assign)CGFloat y;
@property (nonatomic,assign)CGFloat centerX;
@property (nonatomic,assign)CGFloat centerY;


/**在分类中声明@property ，只会声称方法的声明。不会生成方法的实现和带有_下划线的成员变量*/
@end
