//
//  XJhisUserVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJhisUserVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "XJhisPsonCell.h"
#import "XJhisPsonDataModel.h"
#import "XJhisStationVC.h"
@interface XJhisUserVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *_parameter;
    NSMutableArray *_dataSoure;
}
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation XJhisUserVC
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"人员列表";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *dele = app.delegate;
    _parameter = @{userid:[NSString stringWithFormat:@"%ld",dele.userId],@"branchId":[NSString stringWithFormat:@"%ld",self.branchID]};
    _dataSoure = [[NSMutableArray alloc]init];
    _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableview.backgroundColor = [UIColor clearColor];
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableview];
    [_tableview registerNib:[UINib nibWithNibName:@"XJhisPsonCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self netWorking];
}
- (void)netWorking{
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:XJhisPsonURL parameters:_parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        XJhisPsonDataModel *dataModel = [[XJhisPsonDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (XJhisPsonListModel *model in dataModel.beanList) {
            [_dataSoure addObject:model];
        }
        [self.tableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSoure.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    XJhisPsonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[XJhisPsonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    XJhisPsonListModel *model = _dataSoure[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XJhisPsonListModel *model = _dataSoure[indexPath.row];
    XJhisStationVC *stationVC = [[XJhisStationVC alloc]init];
    stationVC.userID = model.userId;
    [self.navigationController pushViewController:stationVC animated:YES];
}
- (void)backVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
