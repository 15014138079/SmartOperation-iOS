//
//  YYFeedListVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/5.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "YYMassFeedListCell.h"
#import "YYMassFeedDetailVC.h"
#import "YYmassAddVC.h"
#import "YYFeedListDataModel.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "YYFeedVC.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface YYFeedListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,NothingViewDelegate,FallLineDelegate,JoinFalutDelegate,ServerUnsualDelegate>
@property(nonatomic,assign)NSInteger stationId;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSString *custName;
@end
