//
//  PartChooseVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/24.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDelegate.h"
#import "Masonry.h"
#import "AFNetworking.h"
#import "PartChooseCell.h"
#import "PDataModel.h"
#import "PartMtsModel.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "AppDelegate.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface PartChooseVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate>
@property (nonatomic, weak) id<DataDelegate> delegate;
@property (nonatomic,assign)NSInteger userID;
@end
