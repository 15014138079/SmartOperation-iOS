//
//  PhotoCollectionCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/9.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoModel.h"
@interface PhotoCollectionCell : UICollectionViewCell
- (void)fillCellWithModel:(PhotoModel *)model;
@end
