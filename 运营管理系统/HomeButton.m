//
//  UserButton.m
//  用户页面
//
//  Created by 黄兴满 on 15/11/18.
//  Copyright © 2015年 黄兴满. All rights reserved.
//

#import "HomeButton.h"

@implementation HomeButton

+ (instancetype)homeButton
{

    return [[[NSBundle mainBundle] loadNibNamed:@"HomeButton" owner:nil options:nil] lastObject];


}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

@end
