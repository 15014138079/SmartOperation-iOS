//
//  YearsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/18.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol YearsModel <NSObject>

@end
@interface YearsModel : JSONModel
@property (nonatomic,assign)float percent;
@property (nonatomic,strong)NSString *time;
@property (nonatomic,assign)NSInteger value;
@end
