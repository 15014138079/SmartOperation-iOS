//
//  SugAndComDetailVC.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "SugAndComDetailVC.h"

@interface SugAndComDetailVC ()
{
    UIScrollView *_scroller;
}
@end

@implementation SugAndComDetailVC
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"详情";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    _scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
    [self.view addSubview:_scroller];
    SugDetailView *detailview = [[SugDetailView alloc]initWithFrame:CGRectMake(10, 10, kSCREEN_WIDTH-20, 220) AndDetailData:self.detailData];
    detailview.delegate = self;
    [_scroller addSubview:detailview];
}
- (void)returnSugDetailViewHeight:(NSInteger)height{
    
    _scroller.contentSize = CGSizeMake(kSCREEN_WIDTH, height+50);
}
- (void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
