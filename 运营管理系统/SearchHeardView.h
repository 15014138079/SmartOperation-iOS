//
//  SearchHeardView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/23.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SearchHeardView;
@protocol SearchHeardViewDelegate <NSObject>
- (void)chooseTime:(UIButton *)sender ClickTag:(NSInteger)tag;
- (void)chooseTypeOrCompany:(UIButton *)sender ClickTag:(NSInteger)tag;
- (void)HeardViewSearchClick:(UIButton *)sender;
@end

@interface SearchHeardView : UIView
@property (nonatomic,strong)UITextField *startimeTF;
@property (nonatomic,strong)UITextField *endtimeTF;
@property (nonatomic,strong)UITextField *questionTypeTF;
@property (nonatomic,strong)UITextField *branchCompanyTF;
@property (nonatomic,strong)UITextField *searchTF;

@property(nonatomic,weak)id <SearchHeardViewDelegate> delegate;
@end
