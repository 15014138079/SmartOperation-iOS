//
//  Mdata.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "MapStation.h"
#import "BranchOfcList.h"
#import "pson.h"
@interface Mdata : JSONModel
@property(nonatomic,strong)NSArray <MapStation> *stationList;
@property(nonatomic,strong)NSArray <BranchOfcList> *BranchOfcList;
@property(nonatomic,strong)NSArray <pson> *psonList;
@end
