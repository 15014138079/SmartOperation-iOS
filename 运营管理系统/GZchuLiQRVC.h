//
//  GZchuLiQRVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightSlidetableViewCell.h"
#import "TimeModel.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "canshuModel.h"
#import "GZchuliQRmassagerVC.h"
#import "MBProgressHUD.h"
#import "GZdataModel.h"
#import "OhlistModel.h"
#import "MJRefresh.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface GZchuLiQRVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate>
@property (nonatomic,assign)NSInteger userId;
@end
