//
//  StationView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/3.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "StationView.h"
#import "XJhisStationDataModel.h"
#import "XJhisStationCell.h"
@interface StationView()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_StationDataSource;
    NSInteger ID;
}
@property (nonatomic,strong)UITableView *tableView;
@end
@implementation StationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.8f;
        self.layer.shadowRadius = 4.f;
        self.layer.shadowOffset = CGSizeMake(0,0);
        UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, kSCREEN_WIDTH-20, 20)];
        titleLab.text = @"站点列表";
        titleLab.textColor = UIColorFromRGB(0x0093d5);
        titleLab.textAlignment = NSTextAlignmentCenter;
        titleLab.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:titleLab];
        UIButton *dismiss = [UIButton buttonWithType:UIButtonTypeCustom];
        dismiss.frame = CGRectMake(kSCREEN_WIDTH-50, 2, 25, 25);
        [dismiss setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
        [dismiss addTarget:self action:@selector(tableviewHid:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:dismiss];
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 30,kSCREEN_WIDTH-20, frame.size.height-30) style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.rowHeight = 50.0f;
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.tableFooterView = [[UIView alloc]init];
        [self addSubview:self.tableView];
        
        [self.tableView registerNib:[UINib nibWithNibName:@"XJhisStationCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        
    }
    return self;
}
- (void)StationArr:(NSArray *)dataSource ID:(NSInteger)userID
{
    _StationDataSource = dataSource;
    ID = userID;
}
- (void)reloadTableView{
    
    [self.tableView reloadData];
}
- (void)tableviewHid:(UIButton *)sender{

    [self.dataSource tableviewHid:sender];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _StationDataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XJhisStationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[XJhisStationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    XJhisStationListModel *model = _StationDataSource[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    XJhisStationListModel *model = _StationDataSource[indexPath.row];
    NSMutableArray *ModelArr = [[NSMutableArray alloc]init];
    [ModelArr addObject:model];
    [self.dataSource didSelectedCell:ID StationID:model.stationId andDataArray:ModelArr];
}
@end
