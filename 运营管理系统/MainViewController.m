//
//  MainViewController.m
//  运营管理系统
//
//  Created by 杨毅 on 16/8/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//
#define ImageviewWidth 18
#define Frame_Width    _left.frame.size.width//200



#import "MainViewController.h"
#import "YunYinViewController.h"
#import "TeamYunViewController.h"


@interface MainViewController ()
{
    YunYinViewController *_yunyin;
    TeamYunViewController *_team;
    UIButton *_btn1;
    UIButton *_btn2;
}


@end

@implementation MainViewController
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置tabbarItem
    
    UIImageView *backView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    backView.image = [[UIImage imageNamed:@"bgwhit"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    backView.userInteractionEnabled = YES;
    self.navigationItem.titleView = backView;
    self.view.backgroundColor = [UIColor whiteColor];

    _btn1 = [UIButton buttonWithType:UIButtonTypeCustom /*自定义按钮,会去除按钮原有的样式*/];
    _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *check1 = [[UIImage imageNamed:@"i01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *uncheck1 = [[UIImage imageNamed:@"i02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *check2 = [[UIImage imageNamed:@"b02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *uncheck2 = [[UIImage imageNamed:@"b01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //背景图片
    [_btn1 setBackgroundImage:uncheck1 forState:UIControlStateNormal];
    [_btn2 setBackgroundImage:uncheck2 forState:UIControlStateNormal];
    
    //实现选择
    //1.设置UIControlStateSelected状态
    //UIControlStateSelected 指的是按钮被选中
    [_btn1 setBackgroundImage:check1 forState:UIControlStateSelected];
    [_btn2 setBackgroundImage:check2 forState:UIControlStateSelected];
    //2.给按钮添加事件处理,在处理函数中改变selected状态
    [_btn1 addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
    [_btn2 addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _btn1.frame = CGRectMake(0, 0, 60, 30);
    _btn2.frame = CGRectMake(60, 0, 60, 30);
    _btn1.tag = 1000;
    _btn2.tag = 1001;
    _btn1.selected = YES;
    [backView addSubview:_btn1];
    [backView addSubview:_btn2];
    _yunyin = [[YunYinViewController alloc]init];
    _team = [[TeamYunViewController alloc]init];
    [self.view addSubview:_yunyin.view];
    [self addChildViewController:_yunyin];
}
- (void)selectedChange:(UIButton *)sender{
   
    _btn1.selected = NO;
    _btn2.selected = NO;
    sender.selected = YES;
    switch (sender.tag) {
        case 1000:
            [self.view addSubview:_yunyin.view];
            [self addChildViewController:_yunyin];
            [_team.view removeFromSuperview];

            break;
        case 1001:
            [self.view addSubview:_team.view];
            [self addChildViewController:_team];
            [_yunyin.view removeFromSuperview];

            break;

        default:
            break;
    }
}
@end
