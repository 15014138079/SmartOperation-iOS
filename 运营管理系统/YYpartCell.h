//
//  YYpartCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/14.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MslistModel.h"
@interface YYpartCell : UITableViewCell
- (void)fillCellWithModel:(MslistModel *)model;
@end
