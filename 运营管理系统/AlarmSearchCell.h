//
//  AlarmSearchCell.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYAlarmSearchList.h"
@interface AlarmSearchCell : UITableViewCell
- (void)fillCellWithModel:(YYAlarmSearchList *)model;
@end
