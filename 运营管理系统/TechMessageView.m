//
//  TechMessageView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "TechMessageView.h"
#import "Masonry.h"
@interface TechMessageView()
{
    CGFloat MaxY;
    UILabel *_TechniqueTitle;
    UILabel *_SuggestionContent;
    UIView *_line2;
}
@end
@implementation TechMessageView

- (instancetype)initWithFrame:(CGRect)frame AndDiction:(NSDictionary *)dic MaxY:(CGFloat)Y{
    self = [super initWithFrame:frame];
    if (self) {
        MaxY = Y;
        NSArray *titleArray = @[@"填  报  人",@"联系方式",@"处理时间"];
        NSArray *conteneArray = @[dic[@"pson"],dic[@"tel"],dic[@"time"]];
        for (NSInteger i = 0; i < titleArray.count; i++) {
           _TechniqueTitle = [[UILabel alloc]initWithFrame:CGRectMake(5, i*(5+18)+2, 90, 18)];
            _TechniqueTitle.textColor = UIColorFromRGB(0x4d4d4d);
            _TechniqueTitle.text = titleArray[i];
            _TechniqueTitle.textAlignment = NSTextAlignmentCenter;
            _TechniqueTitle.font = [UIFont systemFontOfSize:15.0];
            [self addSubview:_TechniqueTitle];
           UILabel *TechniqueContentLab = [[UILabel alloc]initWithFrame:CGRectMake(100, i*(5+18)+2, kSCREEN_WIDTH-105, 18)];
            TechniqueContentLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
            TechniqueContentLab.textColor = UIColorFromRGB(0x4d4d4d);
            TechniqueContentLab.text = conteneArray[i];
            TechniqueContentLab.font = [UIFont systemFontOfSize:15.0];
            [self addSubview:TechniqueContentLab];
            UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(TechniqueContentLab.frame)+2, kSCREEN_WIDTH, 1)];
            line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [self addSubview:line1];
        }
        
        _SuggestionContent = [[UILabel alloc]init];
        _SuggestionContent.text = dic[@"clmessage"];
        _SuggestionContent.numberOfLines = 0;
        _SuggestionContent.textColor = UIColorFromRGB(0x4d4d4d);
        _SuggestionContent.font = [UIFont systemFontOfSize:15.0];
        _SuggestionContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
        CGRect rect = [_SuggestionContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_SuggestionContent.font, NSFontAttributeName,nil] context:nil];
        _SuggestionContent.frame = CGRectMake(100, CGRectGetMaxY(_TechniqueTitle.frame)+5, kSCREEN_WIDTH-105, rect.size.height);
        [self addSubview:_SuggestionContent];
        UILabel *SuggestionLab = [[UILabel alloc]init];
        SuggestionLab.textAlignment = NSTextAlignmentCenter;
        SuggestionLab.text = @"处理意见";
        SuggestionLab.textColor = UIColorFromRGB(0x4d4d4d);
        SuggestionLab.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:SuggestionLab];
        [SuggestionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_SuggestionContent);
            make.left.equalTo(self).offset(5);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
        _line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_SuggestionContent.frame)+2, kSCREEN_WIDTH, 1)];
        _line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:_line2];
    }
    return self;
}
- (void)layoutSubviews{
    
    self.frame = CGRectMake(0, MaxY, kSCREEN_WIDTH, CGRectGetMaxY(_line2.frame)+2);
    [self.delegate returnTechMessageViewHeight:CGRectGetMaxY(_line2.frame)+2];
}
@end
