//
//  TechniqueUPView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHATextView.h"
@class TechniqueUPView;
@protocol TechniqueUPViewDelegate <NSObject>
- (void)TechUpBtnClick:(UIButton *)sender;
- (void)TechRevokeClick:(UIButton *)sender;
@end
@interface TechniqueUPView : UIView
@property(nonatomic,strong)PHATextView *DiscribTextView;
- (instancetype)initWithFrame:(CGRect)frame AndMessageDic:(NSDictionary *)dic;
@property (nonatomic,weak)id <TechniqueUPViewDelegate> delegate;
@end
