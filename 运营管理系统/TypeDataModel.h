//
//  TypeDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/30.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "SectionOneFaultModel.h"

@interface TypeDataModel : JSONModel
@property (nonatomic,strong)NSArray <SectionOneFaultModel> *FaultType;
@end
/*
 "FaultType": []
 */
