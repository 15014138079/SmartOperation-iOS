//
//  YunYinViewController.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/6.
//  Copyright © 2016年 杨毅. All rights reserved.
//




#import "YunYinViewController.h"
#import "HomeButton.h"
#import "YunyingTongJiVC.h"
#import "SheBeiTongJiVC.h"
#import "GZtongJiVC.h"
#import "MapVC.h"
#import "PartVC.h"
#import "PeopleVC.h"
#import "YYXJhistoryVC.h"
#import "workModel.h"
#import "AppDelegate.h"
@interface YunYinViewController ()
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)NSMutableArray *dataSource1;
@end

@implementation YunYinViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;


    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"查询";
    self.view.backgroundColor = [UIColor whiteColor];
    _dataSource = [[NSMutableArray alloc]init];

    UIApplication *app1 = [UIApplication sharedApplication];
    AppDelegate *dele = app1.delegate;
    for (NSInteger i = 0; i < dele.List.count; i++) {
        if ([dele.List[i]integerValue] > 5) {
            
            [_dataSource addObject:dele.List[i]];
        }
        
    }
    _dataSource1 = [[NSMutableArray alloc]initWithArray:[workModel SearchModule:_dataSource]];
    

    for (NSInteger i = 0; i < _dataSource1.count; i++) {
        workModel *model = _dataSource1[i];
        CGFloat a = INTERVALSIZE +i%3*(BTN_WIDTH+INTERVALSIZE);
        CGFloat b = 80 +i/3*(BTN_WIDTH+INTERVALSIZE);
        
       // CGFloat c = (kSCREEN_WIDTH - 80*3)/4 + i%3*(83 + (kSCREEN_WIDTH - 80*3)/4);
        HomeButton *btn = [HomeButton homeButton];
        btn.frame = CGRectMake(a,b, BTN_WIDTH, BTN_HEIGHT);
        [btn setImage:[UIImage imageNamed:model.leftIcon] forState:UIControlStateNormal];
        btn.tag = model.LevelTag;
        [btn addTarget:self action:@selector(nextVC:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(btn.frame)-10, b+60, 80, 30)];
        title.font = [UIFont systemFontOfSize:15.0];
        title.textAlignment = NSTextAlignmentCenter;
        title.text = model.countent;
        [self.view addSubview:title];
    }

}
- (void)nextVC:(HomeButton *)sender{

    YunyingTongJiVC *yunyin = [[YunyingTongJiVC alloc]init];
    //SheBeiTongJiVC *shebei = [[SheBeiTongJiVC alloc]init];
    GZtongJiVC *gzTJ = [[GZtongJiVC alloc]init];
    MapVC *map = [[MapVC alloc]init];
    PartVC *part = [[PartVC alloc]init];
    YYXJhistoryVC *XJhis = [[YYXJhistoryVC alloc]init];
    //PeopleVC *people = [[PeopleVC alloc]init];

        switch (sender.tag) {
            case 6:
                [self.navigationController pushViewController:yunyin animated:YES];
                break;
            case 7:
                [self.navigationController pushViewController:gzTJ animated:YES];
                break;
            case 8:
                [self.navigationController pushViewController:part animated:YES];
                break;
            case 9:
                [self.navigationController pushViewController:map animated:YES];
                break;
            case 10:
                [self.navigationController pushViewController:XJhis animated:YES];
                break;
            default:
                break;
        }
    
}



@end
