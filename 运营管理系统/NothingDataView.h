//
//  NothingDataView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NothingDataView;
@protocol NothingViewDelegate <NSObject>

-(void)NothingRefresh:(UIButton *)sender;

@end
@interface NothingDataView : UIView
@property (nonatomic,weak)id <NothingViewDelegate> delegate;
-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;
@end
