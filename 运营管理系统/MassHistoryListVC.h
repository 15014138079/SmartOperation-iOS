//
//  MassHistoryListVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "YYMassFeedListCell.h"
#import "YYFeedListDataModel.h"
#import "YYMassFeedDetailVC.h"
#import "MJRefresh.h"
#import "NothingDataView.h"
#import "FallLineView.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
@interface MassHistoryListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate>
@property (nonatomic,assign)NSInteger userID;
@end
