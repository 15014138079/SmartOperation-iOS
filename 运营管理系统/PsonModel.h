//
//  PsonModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol PsonModel <NSObject>

@end
@interface PsonModel : JSONModel
@property (nonatomic,assign)float percent;
@property (nonatomic,strong)NSString *pson;
@property (nonatomic,assign)NSInteger value;
@end
/*
 "percent": 0.06,
 "pson": "熊志超",
 "value": 3
 */