//
//  NothingDataView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "NothingDataView.h"
#import "Masonry.h"
@implementation NothingDataView
-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake((kSCREEN_WIDTH-150)/2, 70, 150, 150)];
        backImageView.image = [UIImage imageNamed:@"ic_smile_gray"];
        [self addSubview:backImageView];

        UILabel *YCreasion = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(backImageView.frame)+20, kSCREEN_WIDTH, 21)];
        YCreasion.adjustsFontSizeToFitWidth = YES;
        YCreasion.text = title;
        YCreasion.textAlignment = NSTextAlignmentCenter;
        YCreasion.textColor = UIColorFromRGB(0x4d4d4d);
        [self addSubview:YCreasion];

        UIView *refrashView = [[UIView alloc]init];
        refrashView.backgroundColor = UIColorFromRGB(0x0093d5);
        refrashView.layer.cornerRadius = 2;
        [self addSubview:refrashView];
        [refrashView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset((kSCREEN_WIDTH-80)/2);
            make.top.equalTo(YCreasion.mas_bottom).offset(20);
            make.size.mas_equalTo(CGSizeMake(80, 20));
        }];
        UIImageView *refreshImage = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, 15, 15)];
        refreshImage.image = [UIImage imageNamed:@"refresh_icon"];
        [refrashView addSubview:refreshImage];
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn1 setTitle:@"刷新页面" forState:UIControlStateNormal];
        btn1.frame = CGRectMake(17, 0, 63, 20);
        btn1.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [btn1 addTarget:self action:@selector(refresh1:) forControlEvents:UIControlEventTouchUpInside];
        [refrashView addSubview:btn1];
    }
    return self;
}
- (void)refresh1:(UIButton *)sender{
    
    [self.delegate NothingRefresh:sender];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
