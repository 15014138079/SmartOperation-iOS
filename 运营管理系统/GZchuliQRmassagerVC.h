//
//  GZchuliQRmassagerVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/13.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GZchuliQRmassagerVC : UIViewController
@property(nonatomic,assign)NSInteger orID;
@property(nonatomic,strong)NSString *username;
@property(nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)NSArray *section1;
@property (nonatomic,strong)NSArray *section2;
@property (nonatomic,strong)NSArray *imageArr;
@end
