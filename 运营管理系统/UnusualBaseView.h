//
//  UnusualBaseView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/24.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UnusualBaseView;
@protocol UnusualBaseViewDelegate <NSObject>
- (void)RefreshBtnClick:(UIButton *)sender;
@end
@interface UnusualBaseView : UIView
- (instancetype)initWithFrame:(CGRect)frame UnusualState:(NSInteger)UnusualStatus AndUnusualTitle:(NSString *)Title;
@property(nonatomic,weak)id <UnusualBaseViewDelegate> delegate;
@end
