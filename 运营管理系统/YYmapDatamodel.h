//
//  YYmapDatamodel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/5.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYmapPsonListModel.h"
@interface YYmapDatamodel : JSONModel
@property (nonatomic,strong)NSArray <YYmapPsonListModel> *beanList;
@end
