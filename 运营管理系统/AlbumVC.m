//
//  AlbumVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/11/9.
//  Copyright © 2016年 杨毅. All rights reserved.
//
//相册
#import "AlbumVC.h"
#import "UIImageView+WebCache.h"
@interface AlbumVC ()
{
    NSInteger _index;
    UIImageView *_imageView;
    CGFloat NewWidth;
    CGFloat NewHiedth;
}
@end

@implementation AlbumVC
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, kSCREEN_WIDTH, kSCREEN_HEIGHT-20)];
    backView.backgroundColor = [UIColor blackColor];
    //backView.alpha = 0.75;
    [self.view addSubview:backView];
    //NSLog(@"%@",self.ImageUrl);
    _index = self.index;
    _imageView = [[UIImageView alloc] initWithFrame:backView.frame];

//    [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"yunyin_icon4"]];
//    [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"yunyin_icon4"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        _imageView.contentMode = UIViewContentModeScaleToFill;
//    }];
    
    [[SDWebImageManager sharedManager]downloadImageWithURL:self.ImageUrl[_index] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //completedCount ++ ;
        if (error) {
            //DDLogDebug(@"error is %@",error);
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_fail"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
                //_imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            [backView addSubview:_imageView];
        }
        if (image) {
            //图片下载完成  在这里进行相关操作，如加到数组里 或者显示在imageView上
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
                //_imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            [backView addSubview:_imageView];
        }
    
    }];
    
   /*[_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGSize size = image.size;
        CGFloat w = size.width;
        CGFloat H = size.height;

        NewWidth = kSCREEN_WIDTH/w;
        NewHiedth = kSCREEN_HEIGHT/H;
        if ( w==0 || H == 0) {
            
            _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
            
        }else{
            if (NewWidth>NewHiedth) {
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
            }else{
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
            }
        }
        //_imageView.contentMode = UIViewContentModeScaleAspectFill;
        
    }];*/
    [backView addSubview:_imageView];

    //    UITapGestureRecognizer    点击
    //    UIPanGestureRecognizer    滑动
    //    UISwipeGestureRecognizer  方向
    //    UILongPressGestureRecognizer 长按
    //    UIRotationGestureRecognizer 旋转
    //    UIPinchGestureRecognizer    捏合
    //方向手势
    UISwipeGestureRecognizer *right = [[UISwipeGestureRecognizer alloc] init];
    //设置方向
    right.direction = UISwipeGestureRecognizerDirectionRight;
    [right addTarget:self action:@selector(prevImage)];
    
    UISwipeGestureRecognizer *left = [[UISwipeGestureRecognizer alloc] init];
    
    left.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [left addTarget:self action:@selector(nextImage)];
    
    //点击手势 返回vc
    UITapGestureRecognizer *back = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(popVC)];
    back.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:back];

    
    [self.view addGestureRecognizer:right];
    [self.view addGestureRecognizer:left];

}
- (void)nextImage
{
    //通过变量来记录当前是第几张图片
    _index ++;
    if (_index == self.ImageUrl.count) //已经到了最后一张
    {
        _index = 0; //重新设置为第一张图片
    }
    //拼接图片的名字,得到当前要显示图片的名字
    //NSString *name = [NSString stringWithFormat:@"%ld.jpg", _index];
    //修改当前imageView要显示的图片
   // _imageView.image = [UIImage imageNamed:name];

    
    //2、//需要用到第三方库SDWebImage
//    UIImageView *v1 = [[UIImageView alloc]init];
//    [self.view addSubview:v1];
//    [_imageView sd_setImageWithURL:[NSURL URLWithString:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"default1"] options:SDWebImageRetryFailedcompleted:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageUrl){
//        CGSize size = image.size;
//        CGFloat w = size.width;
//        CGFloat H = size.height;
//    }];
    
//    [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"yunyin_icon4"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        _imageView.contentMode = UIViewContentModeCenter;
//    }];
    
   /* [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGSize size = image.size;
        CGFloat w = size.width;
        CGFloat H = size.height;
        NewWidth = kSCREEN_WIDTH/w;
        NewHiedth = kSCREEN_HEIGHT/H;
        if ( w==0 || H == 0) {
            
            _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
            
        }else{
            if (NewWidth>NewHiedth) {
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
            }else{
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
            }
        }
        //_imageView.contentMode = UIViewContentModeScaleAspectFill;
    }];*/
    [[SDWebImageManager sharedManager]downloadImageWithURL:self.ImageUrl[_index] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //completedCount ++ ;
        if (error) {
            //DDLogDebug(@"error is %@",error);
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_fail"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
               // _imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            
        }
        if (image) {
            //图片下载完成  在这里进行相关操作，如加到数组里 或者显示在imageView上
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
                //_imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            
        }
        
    }];


}

- (void)prevImage
{
    
    if (_index == 0)
    {
        _index = 0;
        
    }else{
        
        _index --;
    }

    //NSString *name = [NSString stringWithFormat:@"%ld.jpg", _index];
    //_imageView.image = [UIImage imageNamed:name];
    /*[_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGSize size = image.size;
        CGFloat w = size.width;
        CGFloat H = size.height;

        NewWidth = kSCREEN_WIDTH/w;
        NewHiedth = kSCREEN_HEIGHT/H;
        if ( w==0 || H == 0) {
            
            _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
            
        }else{
            if (NewWidth>NewHiedth) {
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
            }else{
                _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
            }
        }
       // _imageView.contentMode = UIViewContentModeScaleAspectFill;
        
    }];*/
    
    [[SDWebImageManager sharedManager]downloadImageWithURL:self.ImageUrl[_index] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //completedCount ++ ;
        if (error) {
            //DDLogDebug(@"error is %@",error);
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_fail"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
                _imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            
        }
        if (image) {
            //图片下载完成  在这里进行相关操作，如加到数组里 或者显示在imageView上
            [_imageView sd_setImageWithURL:self.ImageUrl[_index] placeholderImage:[UIImage imageNamed:@"image_loading"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                CGSize size = image.size;
                CGFloat w = size.width;
                CGFloat H = size.height;
                
                NewWidth = kSCREEN_WIDTH/w;
                NewHiedth = kSCREEN_HEIGHT/H;
                if ( w==0 || H == 0) {
                    
                    _imageView.frame = CGRectMake((kSCREEN_WIDTH-120)/2, (kSCREEN_HEIGHT-120)/2, 120,120);
                    
                }else{
                    if (NewWidth>NewHiedth) {
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewHiedth*w)/2, (kSCREEN_HEIGHT-NewHiedth*H)/2, NewHiedth*w,NewHiedth*H);
                    }else{
                        _imageView.frame = CGRectMake((kSCREEN_WIDTH-NewWidth*w)/2, (kSCREEN_HEIGHT-NewWidth*H)/2, NewWidth*w,NewWidth*H);
                    }
                }
                //_imageView.contentMode = UIViewContentModeScaleAspectFill;
                
            }];
            
        }
        
    }];

    
}
- (void)popVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
