//
//  UpListVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/3.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "XJhisBranchCell.h"
#import "XJBranchDataModel.h"
#import "XJhisUserVC.h"
#import "XJhisPsonDataModel.h"
#import "XJhisPsonCell.h"
#import "XJhisStationDataModel.h"
#import "GZupdataViewController.h"
#import "ZJAlertListView.h"
#import "MBProgressHUD.h"
#import "FallLineView.h"//连接超时
#import "JoinFaluterView.h"//连接失败
#import "ServerUnusualView.h"//服务器出错
#import "NothingDataView.h"
@interface UpListVC : UIViewController <UITableViewDelegate,UITableViewDataSource,ZJAlertListViewDatasource,ZJAlertListViewDelegate,JoinFalutDelegate,FallLineDelegate,ServerUnsualDelegate,NothingViewDelegate>

@end
