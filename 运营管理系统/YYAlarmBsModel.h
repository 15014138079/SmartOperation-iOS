//
//  YYAlarmBsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYAlarmDataModel.h"
@interface YYAlarmBsModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)YYAlarmDataModel *data;
@end
/*
 "message": "获取故障信息成功",
 "status": 1,
 "data": {
 "guzhanglist": []
 }
 */
