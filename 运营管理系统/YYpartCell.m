//
//  YYpartCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/14.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "YYpartCell.h"
#import "Masonry.h"
@implementation YYpartCell
{
    UILabel *logId;
    UILabel *tm;
    UILabel *discrib;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}
- (void)initView{
    UIView *heardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10)];
    heardView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.contentView addSubview:heardView];
    UIImageView *leftLogo = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 43, 60)];
    leftLogo.image = [[UIImage imageNamed:@"iconpartLeft"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.contentView addSubview:leftLogo];
    logId = [[UILabel alloc]init];
    logId.adjustsFontSizeToFitWidth = YES;
    logId.textAlignment = NSTextAlignmentLeft;
    logId.font = [UIFont systemFontOfSize:14.0];
    logId.textColor = UIColorFromRGB(0x4d4d4d);
    [self.contentView addSubview:logId];
    [logId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(heardView.mas_bottom).offset(2);
        make.left.equalTo(leftLogo.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(120, 21));
    }];
    //库存
    tm = [[UILabel alloc]init];
    tm.adjustsFontSizeToFitWidth = YES;
    tm.textAlignment = NSTextAlignmentRight;
    tm.font = [UIFont systemFontOfSize:12.0];
    tm.textColor = UIColorFromRGB(0x0093d5);
    [self.contentView addSubview:tm];
    [tm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(logId);
        make.right.equalTo(self.contentView).offset(0);
        make.size.mas_equalTo(CGSizeMake(136, 21));
    }];
    discrib = [[UILabel alloc]initWithFrame:CGRectMake(43, 38, kSCREEN_WIDTH-43, 30)];
    discrib.textColor = UIColorFromRGB(0x4d4d4d);
    discrib.font = [UIFont systemFontOfSize:12.0];
    discrib.numberOfLines = 2;
    //discrib.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:discrib];

}
- (void)fillCellWithModel:(MslistModel *)model
{
    logId.text = model.logId;
    
    tm.text = [NSString stringWithFormat:@"%ld%c%@%c",model.numb,'(',model.unit,')'];
    discrib.text = model.discrib;
//    NSDictionary *fontDic=@{NSFontAttributeName:[UIFont systemFontOfSize:15.0]};
//    CGSize size1=CGSizeMake(kSCREEN_WIDTH-43,0);
//    CGSize titleLabelSize=[model.discrib boundingRectWithSize:size1 options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading   attributes:fontDic context:nil].size;
//    
//    
//    
//    if (titleLabelSize.height < 40) {
//        
//        
//    }else{ //重新绘制高度
//        
//        
//        //标题重新设置
//        discrib.frame=CGRectMake(43,38,kSCREEN_WIDTH-43, titleLabelSize.height);
//    }

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
