//
//  XJhisPsonCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJhisPsonCell.h"

@implementation XJhisPsonCell
{
    __weak IBOutlet UILabel *username;
    
    __weak IBOutlet UILabel *opcenter;
    __weak IBOutlet UILabel *postName;
}
- (void)fillCellWithModel:(XJhisPsonListModel *)model
{
    
    username.text = model.userN;
    if (model.opCenName.length > 1) {
        opcenter.text = [NSString stringWithFormat:@"%c%@%c",'(',model.opCenName,')'];
    }else{
    
    }
    postName.text = [NSString stringWithFormat:@"%@",model.postName];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
