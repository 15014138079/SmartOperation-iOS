//
//  chuliListVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightSlidetableViewCell.h"
#import "TimeModel.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "GZshenpiVC.h"
#import "canshuModel.h"
#import "GZchuliVC.h"
#import "MBProgressHUD.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface chuliListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate>
@property(nonatomic,strong)NSString *userID;
@end
