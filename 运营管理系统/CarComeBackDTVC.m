//
//  CarComeBackDTVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/11.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarComeBackDTVC.h"
#import "Masonry.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "YXKit.h"
@interface CarComeBackDTVC ()
{
    UIScrollView *_scrollView;
    UITextView *_textView;
    NSString *_time;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@end

@implementation CarComeBackDTVC
- (AFHTTPSessionManager *)manager{
    
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"车辆归还";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.userId = delegate.userId;
    
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    _time = [dateFormatter stringFromDate:currentDate];
    
    NSLog(@"%ld",self.carId);
    
    [self initScrollView];
}
- (void)initScrollView{

    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT);
    _scrollView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+100);
    [self.view addSubview:_scrollView];
    // 隐藏水平滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    UIView *backView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 100)];
    backView1.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView1];
    UIImageView *leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 80)];
    leftImageView.image = [[UIImage imageNamed:@"big_car"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [backView1 addSubview:leftImageView];
    NSArray *title = @[@"车辆品牌:",@"车辆型号:"];
    NSArray *content = @[self.band,self.Cmodel];
    for (NSInteger i = 0; i < title.count; ++i) {
        UILabel *t = [[UILabel alloc]initWithFrame:CGRectMake(100, 25 + i * (21 + 10), 80, 21)];
        t.text = title[i];
        t.textColor = UIColorFromRGB(0x4d4d4d);
        [backView1 addSubview:t];
        UILabel *c = [[UILabel alloc]initWithFrame:CGRectMake(190, 25 + i * (21+10), 100, 21)];
        c.textColor = UIColorFromRGB(0x4d4d4d);
        c.text = content[i];
        [backView1 addSubview:c];
    }
    UIView *backView2 = [[UIView alloc]init];
    backView2.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView1.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 100));
    }];
    NSString *time_str = [_time substringWithRange:NSMakeRange(0, 10)];
    NSArray *leftT = @[@"申请人:",@"部   门:",@"日   期:"];
    NSArray *rightC = @[self.repName,@"运营管理部",time_str];
    for (NSInteger i = 0; i <leftT.count; ++i) {
        UILabel *t = [[UILabel alloc]initWithFrame:CGRectMake(15, 10 + i * (21 +5), 80, 21)];
        t.text = leftT[i];
        t.textColor = UIColorFromRGB(0x4d4d4d);
        [backView2 addSubview:t];
        if (i == 2) {
            UILabel *c = [[UILabel alloc]initWithFrame:CGRectMake(100, 10 + i * (21 + 5), 120, 21)];
            c.text = rightC[i];
            c.textColor = UIColorFromRGB(0x4d4d4d);
            c.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [backView2 addSubview:c];
        }else{
            UILabel *c = [[UILabel alloc]initWithFrame:CGRectMake(100, 10 + i * (21 + 5), 120, 21)];
            c.text = rightC[i];
            c.textColor = UIColorFromRGB(0x4d4d4d);
            [backView2 addSubview:c];
        }
    }
    UIView *backView3 = [[UIView alloc]init];
    backView3.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView3];
    [backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView2.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 40));
    }];
    UILabel *T = [[UILabel alloc]init];
    T.text = @"车牌号码:";
    T.textColor = UIColorFromRGB(0x4d4d4d);
    [backView3 addSubview:T];
    [T mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView3).offset(10);
        make.left.equalTo(backView3).offset(15);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    UILabel *plateN = [[UILabel alloc]init];
    plateN.text = self.plateNum;
    plateN.textColor = UIColorFromRGB(0x4d4d4d);
    [backView3 addSubview:plateN];
    [plateN mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView3).offset(10);
        make.left.equalTo(T.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(100, 21));
    }];
    UIView *backView4 = [[UIView alloc]init];
    backView4.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView4];
    [backView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView3.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 40));
    }];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"车辆保养信息" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [btn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [backView4 addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView4).offset(5);
        make.left.equalTo(backView4).offset(15);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-15, 30));
    }];
    UIImageView *arow = [[UIImageView alloc]init];
    arow.image = [UIImage imageNamed:@"next"];
    [btn addSubview:arow];
    [arow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btn).offset(5);
        make.left.equalTo(btn.mas_right).offset(-30);
        make.size.mas_equalTo(CGSizeMake(15, 20));
    }];
    UIView *backView5 = [[UIView alloc]init];
    backView5.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView5];
    [backView5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView4.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 100));
    }];
    UILabel *BZtitle = [[UILabel alloc]init];
    BZtitle.text = @"备注信息:";
    BZtitle.textColor = UIColorFromRGB(0x4d4d4d);
    [backView5 addSubview:BZtitle];
    [BZtitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView5).offset(10);
        make.left.equalTo(backView5).offset(15);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    _textView = [[UITextView alloc]init];
    _textView.font = [UIFont systemFontOfSize:17.0];
    _textView.textColor = UIColorFromRGB(0xb6b6b6);
    _textView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _textView.text = @"输入备注信息";
    [backView5 addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView5).offset(5);
        make.left.equalTo(BZtitle.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3*2-10, 80));
    }];
    
    UIView *backView6 = [[UIView alloc]init];
    backView6.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView6];
    [backView6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView5.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
    }];
    //提交取消bttuon
    UIButton *updata = [UIButton buttonWithType:UIButtonTypeSystem];
    [updata setTitle:@"提交" forState:UIControlStateNormal];
    [updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    updata.backgroundColor = UIColorFromRGB(0x2dd500);
    [updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
    [backView6 addSubview:updata];
    [updata mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView6).offset(10);
        make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
    
    UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancerl setTitle:@"取消" forState:UIControlStateNormal];
    [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    cancerl.backgroundColor = UIColorFromRGB(0xd50037);
    [backView6 addSubview:cancerl];
    [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView6).offset(10);
        make.left.equalTo(updata.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];

}
/*
- (void)up{
    

    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_carBackUp parameters:@{carid:[NSString stringWithFormat:@"%ld",self.carId],userid:[NSString stringWithFormat:@"%ld",self.userId],Remark:_textView.text,updataTime:_time} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSInteger status = [dic[@"status"]integerValue];
        NSLog(@"%ld",status);
        if (status == 1) {
            
            [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
        }else{
            
            [[YXAlertHUD stateHUDWithHUDTyep:1 message:@"提交失败" textColor:[UIColor redColor] iconColor:[UIColor redColor]] show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}*/
- (void)next{
    NSLog(@"车辆保养");
}
- (void)back{

    [self.navigationController popViewControllerAnimated:YES];
}
@end
