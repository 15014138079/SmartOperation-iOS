//
//  GZcountTypeCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/18.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "typeModel.h"
@interface GZcountTypeCell : UITableViewCell
- (void)fillCellWithModel:(typeModel *)model;
@end
