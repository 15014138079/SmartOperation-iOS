//
//  YYFeedBackConfirmVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/9.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "YYFeedListDataModel.h"
#import "FeedBackHisAndListCell.h"
#import "YYFeedMessageReportVC.h"
#import "MassHistoryListVC.h"
#import "MJRefresh.h"
#import "FallLineView.h"//连接超时
#import "JoinFaluterView.h"//连接失败
#import "ServerUnusualView.h"//服务器出错
#import "NothingDataView.h"
@interface YYFeedBackConfirmVC : UIViewController<UITableViewDelegate,UITableViewDataSource,ServerUnsualDelegate,JoinFalutDelegate,FallLineDelegate,NothingViewDelegate>

@end
