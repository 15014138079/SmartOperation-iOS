//
//  PartDetailVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "PartDetailVC.h"
#import "Masonry.h"
@interface PartDetailVC ()
@property (nonatomic,strong)UIScrollView *scrollview;
@property (nonatomic,strong)UIView *section2;
@end

@implementation PartDetailVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"配件信息详情";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    [self initScorll];

}
- (void)initScorll{
    
    _scrollview = [[UIScrollView alloc]init];
    _scrollview.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT);
    _scrollview.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, 0);
    [self.view addSubview:_scrollview];
    UIView *secion1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 80)];
    secion1.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:secion1];
    UIImageView *icon1 = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
    icon1.image = [[UIImage imageNamed:@"icon_0101_b"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [secion1 addSubview:icon1];
    //编码标题
    UILabel *numT = [[UILabel alloc]init];
    numT.adjustsFontSizeToFitWidth = YES;
    numT.text = [NSString stringWithFormat:@"编码: %@",self.logId];
    numT.textColor = UIColorFromRGB(0x4d4d4d);
    [secion1 addSubview:numT];
    [numT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(secion1).offset(18);
        make.left.equalTo(icon1.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-75, 21));
    }];

    //数量标题
    UILabel *valueT = [[UILabel alloc]init];
    valueT.adjustsFontSizeToFitWidth = YES;
    valueT.textColor = UIColorFromRGB(0x4d4d4d);
    valueT.text = [NSString stringWithFormat:@"数量: %ld%c%@%c",self.number,'(',self.unit,')'];
    [secion1 addSubview:valueT];
    [valueT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon1.mas_right).offset(5);
        make.top.equalTo(numT.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-75, 21));
    }];

    

    self.section2.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:self.section2];
    
    UIImageView *icon2 = [[UIImageView alloc]init];
    icon2.image = [[UIImage imageNamed:@"inf_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.section2 addSubview:icon2];
    [icon2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.section2);
        make.left.equalTo(self.section2).offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    //配件描述
    UILabel *discribL= [[UILabel alloc]initWithFrame:CGRectZero];
    discribL.textColor = UIColorFromRGB(0x4d4d4d);
    discribL.text = [NSString stringWithFormat:@"描述: %@",self.discrib];
    discribL.font = [UIFont systemFontOfSize:17.0];
    discribL.numberOfLines = 0;
    CGRect rect = [discribL.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-50, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:discribL.font,NSFontAttributeName, nil] context:nil];
    CGFloat height = rect.size.height;
    discribL.frame = CGRectMake(50, 5, kSCREEN_WIDTH-50, height);
    self.section2.frame = CGRectMake(0, CGRectGetMaxY(secion1.frame)+10, kSCREEN_WIDTH, height+10);
    [self.section2 addSubview:discribL];
//仓库编码
    UIView *section3 = [[UIView alloc]init];
    section3.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:section3];
    [section3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.section2.mas_bottom).offset(10);
        make.left.equalTo(_scrollview).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 35));
    }];
    UIImageView *icon3 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 2,30, 30)];
    icon3.image = [[UIImage imageNamed:@"n"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [section3 addSubview:icon3];
    UILabel *CKname = [[UILabel alloc]init];
    CKname.text = [NSString stringWithFormat:@"仓库名称: %@",self.storeCode];
    CKname.textColor = UIColorFromRGB(0x4d4d4d);
    [section3 addSubview:CKname];
    [CKname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(icon3);
        make.left.equalTo(icon3.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-50, 21));
    }];
//仓库名称
    UIView *section4 = [[UIView alloc]init];
    section4.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:section4];
    [section4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(section3.mas_bottom).offset(10);
        make.left.equalTo(_scrollview).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 35));
    }];
    UIImageView *icon4 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 2,30, 30)];
    icon4.image = [[UIImage imageNamed:@"dw_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [section4 addSubview:icon4];
    
    UILabel *center = [[UILabel alloc]init];
    center.text = [NSString stringWithFormat:@"仓库名称: %@",self.storeName];
    center.textColor = UIColorFromRGB(0x4d4d4d);
    [section4 addSubview:center];
    [center mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(icon4);
        make.left.equalTo(icon4.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-50, 21));
    }];


    //更新时间
    UIView *section5 = [[UIView alloc]init];
    section5.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:section5];
    [section5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(section4.mas_bottom).offset(10);
        make.left.equalTo(_scrollview).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 35));
    }];
    UIImageView *icon5 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 2,30, 30)];
    icon5.image = [[UIImage imageNamed:@"Partime"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [section5 addSubview:icon5];
    
    UILabel *timeLab = [[UILabel alloc]init];
    timeLab.text = [NSString stringWithFormat:@"更新时间: %@",self.time];
    timeLab.textColor = UIColorFromRGB(0x4d4d4d);
    [section5 addSubview:timeLab];
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(icon5);
        make.left.equalTo(icon5.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-50, 21));
    }];
#if 0
    //运营中心
    UIView *section4 = [[UIView alloc]init];
    section4.backgroundColor = [UIColor whiteColor];
    [_scrollview addSubview:section4];
    [section4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(section3.mas_bottom).offset(10);
        make.left.equalTo(_scrollview).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 35));
    }];
    UIImageView *icon4 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 2,30, 30)];
    icon4.image = [[UIImage imageNamed:@"dw_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [section4 addSubview:icon4];
    
    UILabel *center = [[UILabel alloc]init];
    center.text = [NSString stringWithFormat:@"运营中心: %@",self.brName];
    center.textColor = UIColorFromRGB(0x4d4d4d);
    [section4 addSubview:center];
    [center mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(icon4);
        make.left.equalTo(icon4.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-50, 21));
    }];
#endif
}
- (UIView *)section2{
    if (!_section2) {
        _section2 = [[UIView alloc]init];
    }
    return _section2;
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
