//
//  XJhisStationVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJhisStationVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "XJhisStationDataModel.h"
#import "XJhisStationCell.h"
#import "XYhistroyVC.h"
@interface XJhisStationVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *_parameter;
    NSMutableArray *_dataSoure;
}
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)AFHTTPSessionManager *manager;

@end

@implementation XJhisStationVC
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"站点列表";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = leftItem;

    _parameter = @{userid:[NSString stringWithFormat:@"%ld",self.userID]};
    _dataSoure = [[NSMutableArray alloc]init];
    _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableview.backgroundColor = [UIColor clearColor];
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.rowHeight = 50.0f;
    _tableview.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableview];
    [_tableview registerNib:[UINib nibWithNibName:@"XJhisStationCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self netWorking];
}
- (void)netWorking{
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:XJhisStationURL parameters:_parameter progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        XJhisStationDataModel *dataModel = [[XJhisStationDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (XJhisStationListModel *model in dataModel.beanList) {
            [_dataSoure addObject:model];
        }
        [self.tableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma mark-->tableviewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSoure.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    XJhisStationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[XJhisStationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    XJhisStationListModel *model = _dataSoure[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    XJhisStationListModel *model = _dataSoure[indexPath.row];
    XYhistroyVC *vc = [[XYhistroyVC alloc]init];
    vc.userID = self.userID;
    vc.stationId = model.stationId;
    //vc.stationMessageArray = [[NSMutableArray alloc]init];
   // [vc.stationMessageArray addObject:model];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)backVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
