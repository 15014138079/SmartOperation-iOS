//
//  SuggesOrComplainCell.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/21.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SugAndComBeanListModel.h"
@class SuggesOrComplainCell;
@protocol SuggesOrComplainCellDelegate <NSObject>
- (void)sugBtnClick:(UIButton *)sender RowData:(NSDictionary *)rowDataDictionary;
@end
@interface SuggesOrComplainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ZanNumber;

@property (nonatomic,weak)id <SuggesOrComplainCellDelegate> delegate;
- (void)fillCellWithModel:(SugAndComBeanListModel *)model WithRow:(NSInteger)index;
@end
