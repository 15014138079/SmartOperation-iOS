//
//  GZshenpiVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/24.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "AFNetworking.h"
#import "BSMassageModel.h"
#import "ZJSwitch.h"
#import "AppDelegate.h"
#import "YXKit.h"
#import "MBProgressHUD.h"
#import "PhotoCollectionCell.h"
#import "AlbumVC.h"
#import "WorkViewController.h"
#import "PHATextView.h"
#import "MHActionSheet.h"
#import "YYUpPsonDataModel.h"
#import "FallLineView.h"//连接超时
#import "JoinFaluterView.h"//连接失败
#import "ServerUnusualView.h"//服务器出错

@interface GZshenpiVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,FallLineDelegate,JoinFalutDelegate,ServerUnsualDelegate>
@property(nonatomic,strong)NSString *Pid;//参数
@property(nonatomic,assign)NSInteger userId;
@property(assign ,nonatomic)NSInteger upper;//权限等级
@property(nonatomic,assign)NSInteger faultApprovalId;//是否是编辑状态的标记
@end
