//
//  SettingsViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/20.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
@interface SettingsViewController : UIViewController
@property(nonatomic,strong)NSString *loginAcc;
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)NSString *telNum;
@property (nonatomic,assign)NSInteger userID;
@property (nonatomic,strong)NSString *leveN;
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,strong)NSString *iconUrl;
@property (nonatomic,strong)NSString *userEmail;
@end
