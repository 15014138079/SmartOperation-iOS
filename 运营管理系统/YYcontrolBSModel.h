//
//  YYcontrolBSModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/15.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYctrolDataModel.h"
@interface YYcontrolBSModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)YYctrolDataModel *data;
@end
