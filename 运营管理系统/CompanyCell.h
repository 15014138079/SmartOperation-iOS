//
//  companyCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/25.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BranchOfcList.h"
@interface companyCell : UITableViewCell
- (void)fillCellWithModel:(BranchOfcList *)model;
@end
