//
//  YYgzDetailFixList.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYgzDetailFixList <NSObject>


@end

@interface YYgzDetailFixList : JSONModel
@property(nonatomic,strong)NSString *fixPsonName;
@property(nonatomic,strong)NSString *fixRemark;
@property(nonatomic,assign)NSInteger fixState;
@property(nonatomic,strong)NSString *fixTime;
@property(nonatomic,strong)NSString *fixImage;
@end
