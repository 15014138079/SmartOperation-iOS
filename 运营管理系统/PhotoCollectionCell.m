//
//  PhotoCollectionCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/11/9.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "PhotoCollectionCell.h"
#import "UIImageView+WebCache.h"
@implementation PhotoCollectionCell
{
    __weak IBOutlet UIImageView *imageView;
    
}
- (void)fillCellWithModel:(PhotoModel *)model
{
    [[SDWebImageManager sharedManager]downloadImageWithURL:[NSURL URLWithString:model.imageName] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        //completedCount ++ ;
        if (error) {
            [imageView sd_setImageWithURL:[NSURL URLWithString:model.imageName] placeholderImage:[UIImage imageNamed:@"image_fail"]];
            
        }
        if (image) {
            //图片下载完成  在这里进行相关操作，如加到数组里 或者显示在imageView上
            [imageView sd_setImageWithURL:[NSURL URLWithString:model.imageName] placeholderImage:[UIImage imageNamed:@"image_loading"]];
            
        }
        
    }];


}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
