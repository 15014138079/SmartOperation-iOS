//
//  equeipsModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol equeipsModel <NSObject>

@end
@interface equeipsModel : JSONModel
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *installT;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *lastRoute;
@property (nonatomic,strong)NSString *lastState;
@property (nonatomic,strong)NSString *pic;
@property (nonatomic,assign)NSInteger routCycle;
@property (nonatomic,assign)NSInteger state;
@end
/*
 "discrib": "",
 "id": 8706,
 "installT": "2014-09-05 00:00:00",
 "lastRoute": "",
 "lastState": "",
 "name": "宇星科技,YX-CAMS(CO)",
 "pic": "",
 "routCycle": 0,
 "state": 0
 */
