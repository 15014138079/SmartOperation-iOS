//
//  XJRecordVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJRecordVC.h"
#import "YYXJhisListModel.h"
#import "UIImage+GIF.h"
#import "PhotoCollectionCell.h"
#import "PhotoModel.h"
#import "AlbumVC.h"
static NSString *collectionViewCellIdentf = @"collectionViewCell1";
static CGFloat imageSize = 80;
@interface XJRecordVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UILabel *_leftTitleLab;
    UILabel *_rightContentLab;
    NSArray *_contentArray;
    UILabel *_leftT;//异常状态标题
    UIView  *_YCview;
    NSArray *_imageArray;
    YYXJhisListModel *_model;
}
@property (nonatomic,strong)NSMutableArray *AllImageUrl;
@property (nonatomic,strong)UIView *StationView;//站点信息view
@property (nonatomic,strong)UIView *detailView;//巡检信息view
@end

@implementation XJRecordVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:NO];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"巡检详细信息";
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = leftItem;
    _model = [self.DetailArray firstObject];
    _contentArray = @[_model.startTime,_model.stopTime,[NSString stringWithFormat:@"%ld",_model.resultStatus]];
    _imageArray = [_model.photos componentsSeparatedByString:@","];
    _AllImageUrl = [[NSMutableArray alloc]init];
    for (NSInteger i = 0; i < _imageArray.count; i++) {
        PhotoModel *model = [[PhotoModel alloc]init];
        model.imageName = _imageArray[i];
        [_AllImageUrl addObject:model];
    }
    [self initScroler];
}
- (void)initScroler{
    UIScrollView *scroler = [[UIScrollView alloc]init];
    scroler.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64);
    scroler.backgroundColor = UIColorFromRGB(0xf0f0f0);
   // scroler.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT);
    [self.view addSubview:scroler];

    self.StationView.backgroundColor = [UIColor whiteColor];
    [scroler addSubview:self.StationView];
    UIImageView *StaionTitleImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 15, 20)];
    StaionTitleImage.image = [UIImage imageNamed:@"f_icon03"];
    [self.StationView addSubview:StaionTitleImage];
    UILabel *StaionTit = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(StaionTitleImage.frame)+5, 5, kSCREEN_WIDTH-30, 21)];
    StaionTit.text = @"站点信息";
    StaionTit.textColor = UIColorFromRGB(0x0093d5);
    [self.StationView addSubview:StaionTit];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(StaionTit.frame)+3, kSCREEN_WIDTH, 1)];
    line.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.StationView addSubview:line];
    UILabel *StationName = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(line.frame)+5, 70, 17)];
    StationName.text = @"站点名称";
    StationName.textColor = UIColorFromRGB(0x4d4d4d);
    StationName.font = [UIFont systemFontOfSize:15.0];
    [self.StationView addSubview:StationName];
    UILabel *stationNContent = [[UILabel alloc]initWithFrame:CGRectZero];
    stationNContent.text = _model.stationName;
    stationNContent.textColor = UIColorFromRGB(0x4d4d4d);
    stationNContent.font = [UIFont systemFontOfSize:15.0];
    stationNContent.numberOfLines = 0;
    stationNContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect rect = [stationNContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-90, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:stationNContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat height = rect.size.height;
    stationNContent.frame = CGRectMake(CGRectGetMaxX(StationName.frame)+5, CGRectGetMaxY(line.frame)+5, kSCREEN_WIDTH-90, height);
    [self.StationView addSubview:stationNContent];
    UILabel *StationAddressContent = [[UILabel alloc]initWithFrame:CGRectZero];
    StationAddressContent.text = _model.stationLocation;
    StationAddressContent.textColor = UIColorFromRGB(0x4d4d4d);
    StationAddressContent.font = [UIFont systemFontOfSize:15.0];
    StationAddressContent.numberOfLines = 0;
    StationAddressContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect AddressRect = [StationAddressContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-90, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:StationAddressContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat AddressHeight = AddressRect.size.height;
    StationAddressContent.frame = CGRectMake(90, CGRectGetMaxY(stationNContent.frame)+5, kSCREEN_WIDTH-90, AddressHeight);
    [self.StationView addSubview:StationAddressContent];
    UILabel *StationAddress = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(StationAddressContent.frame),70, 21)];
    StationAddress.textColor = UIColorFromRGB(0x4d4d4d);
    StationAddress.text = @"站点地址";
    StationAddress.font = [UIFont systemFontOfSize:15.0];
    [self.StationView addSubview:StationAddress];
    self.StationView.frame = CGRectMake(0, 5, kSCREEN_WIDTH, CGRectGetMaxY(StationAddressContent.frame)+5);
    
    self.detailView.backgroundColor = [UIColor whiteColor];
    [scroler addSubview:self.detailView];
    
    UIImageView *XJmessageImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    XJmessageImage.image = [UIImage imageNamed:@"d_icon01"];
    [self.detailView addSubview:XJmessageImage];
    UILabel *XJmessageT = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(XJmessageImage.frame)+5, 5, kSCREEN_WIDTH-30, 21)];
    XJmessageT.text = @"巡检信息";
    XJmessageT.textColor = UIColorFromRGB(0x0093d5);
    [self.detailView addSubview:XJmessageT];
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(XJmessageT.frame)+3, kSCREEN_WIDTH, 1)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.detailView addSubview:line1];
    

    NSArray *titleName = @[@"开始时间",@"结束时间",@"异常状态"];
    for (NSInteger i = 0; i < titleName.count; i++) {

        if(i<2){
            _rightContentLab = [[UILabel alloc]initWithFrame:CGRectZero];
            _rightContentLab.textColor = UIColorFromRGB(0x4d4d4d);
            _rightContentLab.font=[UIFont systemFontOfSize:15.0];
            _rightContentLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
            _rightContentLab.text = _contentArray[i];
            _rightContentLab.numberOfLines = 0;
            CGRect rect = [_rightContentLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-90, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_rightContentLab.font,NSFontAttributeName, nil] context:nil];
            CGFloat height = rect.size.height;
            _rightContentLab.frame = CGRectMake(90,CGRectGetMaxY(line1.frame)+5+i*(height + 5), kSCREEN_WIDTH-90, height);
            [self.detailView addSubview:_rightContentLab];
            _leftTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(_rightContentLab.frame), 70, 18)];
            _leftTitleLab.text = titleName[i];
            _leftTitleLab.font = [UIFont systemFontOfSize:15.0];
            _leftTitleLab.textColor = UIColorFromRGB(0x4d4d4d);
            [self.detailView addSubview:_leftTitleLab];

        }else if (i == 2) {
            _YCview = [[UIView alloc]initWithFrame:CGRectMake(90, CGRectGetMaxY(_rightContentLab.frame)+5, kSCREEN_WIDTH-90, 17)];
            _YCview.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [self.detailView addSubview:_YCview];
            UIImageView *stataView = [[UIImageView alloc]initWithFrame:CGRectMake(2, 0, 17, 17)];
            [_YCview addSubview:stataView];
            UILabel *stataLab = [[UILabel alloc]initWithFrame:CGRectMake(21, 0, 80, 17)];
            stataLab.font = [UIFont systemFontOfSize:15.0];
            stataLab.textAlignment = NSTextAlignmentLeft;
            stataLab.textColor = UIColorFromRGB(0x4d4d4d);
            [_YCview addSubview:stataLab];
            if ([_contentArray[i] integerValue] == 0) {
                UIImage *image = [UIImage sd_animatedGIFNamed:@"off_line"];
                stataView.image = image;
                stataLab.text = @"异常";
            }else if ([_contentArray[i] integerValue] == 1){
                UIImage *image = [UIImage sd_animatedGIFNamed:@"on_line"];
                stataView.image = image;
                stataLab.text = @"正常";
            }else{
            
            }
            _leftT = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMinY(_YCview.frame), 70, 17)];
            _leftT.text = titleName[i];
            _leftT.font = [UIFont systemFontOfSize:15.0];
            _leftT.textColor = UIColorFromRGB(0x4d4d4d);
            [self.detailView addSubview:_leftT];
            
        }

    }
    UILabel *upAddress = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_leftT.frame)+5, 70, 17)];
    upAddress.text = @"提交地址";
    upAddress.textColor = UIColorFromRGB(0x4d4d4d);
    upAddress.font = [UIFont systemFontOfSize:15.0];
    [self.detailView addSubview:upAddress];
    UILabel *upAddressContent = [[UILabel alloc]initWithFrame:CGRectZero];
    upAddressContent.text = _model.commitLocation;
    upAddressContent.numberOfLines = 0;
    upAddressContent.textColor = UIColorFromRGB(0x4d4d4d);
    upAddressContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
    upAddressContent.font = [UIFont systemFontOfSize:15.0];
    CGRect upaddressRect = [upAddressContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-90, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:upAddressContent.font,NSFontAttributeName, nil] context:nil];
    CGFloat upAddressHeiht = upaddressRect.size.height;
    upAddressContent.frame = CGRectMake(90, CGRectGetMaxY(_YCview.frame)+5, kSCREEN_WIDTH-90, upAddressHeiht);
    [self.detailView addSubview:upAddressContent];
    
    UILabel *BZmessageT = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(upAddressContent.frame)+5, 70, 17)];
    BZmessageT.text = @"备注信息";
    BZmessageT.font = [UIFont systemFontOfSize:15.0];
    BZmessageT.textColor = UIColorFromRGB(0x4d4d4d);
    [self.detailView addSubview:BZmessageT];
    UILabel *messages = [[UILabel alloc]initWithFrame:CGRectZero];
    messages.text = _model.resultDesc;
    messages.numberOfLines = 0;
    messages.font = [UIFont systemFontOfSize:15.0];
    messages.textColor = UIColorFromRGB(0x4d4d4d);
    messages.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect rect1 = [messages.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-90, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:messages.font,NSFontAttributeName, nil] context:nil];
    CGFloat height1 = rect1.size.height;
    messages.frame = CGRectMake(90, CGRectGetMaxY(upAddressContent.frame)+5, kSCREEN_WIDTH-90, height1);
    [self.detailView addSubview:messages];
    if (_imageArray.count == 1 && [_imageArray[0] length] == 0) {
    self.detailView.frame = CGRectMake(0, CGRectGetMaxY(self.StationView.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(messages.frame)+5);
    }else{
        UILabel *ImageT = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(BZmessageT.frame)+5, 70, 17)];
        ImageT.text = @"图片信息";
        ImageT.textColor = UIColorFromRGB(0x4d4d4d);
        ImageT.font = [UIFont systemFontOfSize:15.0];
        [_detailView addSubview:ImageT];
        //图片展示
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(ImageT.frame)+5, kSCREEN_WIDTH, imageSize + 20) collectionViewLayout:layout];
        
        [collectionView registerClass:[PhotoCollectionCell class] forCellWithReuseIdentifier:collectionViewCellIdentf];
        UINib *nib = [UINib nibWithNibName:@"PhotoCollectionCell" bundle:nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:@"imageCell"];
        collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = [UIColor whiteColor];
        [self.detailView addSubview:collectionView];
    self.detailView.frame = CGRectMake(0, CGRectGetMaxY(self.StationView.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(collectionView.frame)+5);
    }
    scroler.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(self.detailView.frame));

}
#pragma mark-->collectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return _AllImageUrl.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    PhotoModel *model = _AllImageUrl[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    AlbumVC *alVC = [[AlbumVC alloc]init];
    alVC.ImageUrl = _imageArray;
    alVC.index = indexPath.row;
    [self.navigationController pushViewController:alVC animated:YES];
}
- (UIView *)StationView{
    if (!_StationView) {
        _StationView = [[UIView alloc]init];
    }
    return _StationView;
}
-(UIView *)detailView{
    if (!_detailView) {
        
        _detailView = [[UIView alloc]init];
    }
    return _detailView;
}
- (void)backVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
