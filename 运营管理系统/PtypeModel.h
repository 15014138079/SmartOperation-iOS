//
//  PtypeModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol PtypeModel <NSObject>

@end
@interface PtypeModel : JSONModel
@property (nonatomic,strong)NSString *ptype;
@property (nonatomic,assign)float percent;
@property (nonatomic,assign)NSInteger value;
@end
/*
 {
 "ptype": "水质污染源",
 "percent": 0.84,
 "value": 42
 }
 */