//
//  YMTabBarViewController.h
//  OneOwnerPurchase
//
//  Created by Yumukim on 15/11/16.
//  Copyright © 2015年 Yumukim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlarmViewController.h"
#import "WorkViewController.h"
#import "YunYinViewController.h"
#import "SettingsViewController.h"

@interface TabBarViewController : UITabBarController

@property (nonatomic,strong) UIViewController * FirstController;

@property (nonatomic,strong) UIViewController * SecondController;

@property (nonatomic,strong) UIViewController * ThirdController;

@property (nonatomic,strong) UIViewController * ForthController;



@end
