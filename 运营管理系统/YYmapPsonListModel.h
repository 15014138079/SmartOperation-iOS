//
//  YYmapPsonListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/5.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYmapPsonListModel <NSObject>

@end
@interface YYmapPsonListModel : JSONModel
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,strong)NSString *opCenName;
@property (nonatomic,strong)NSString *post;
@property (nonatomic,assign)NSInteger stationNum;
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)NSString *username;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,strong)NSString *locationTime;
@property (nonatomic,assign)float lnt;
@property (nonatomic,assign)float lat;
@end
/*
 "phone": "13692321549",
 "opCenName": "湛江运营中心",
 "post": "运营工程师",
 "stationNum": 10,
 "userId": 1107,
 "userName": "徐博慧",
 "locationTime":"湖南省长沙市雨花区环保路188号国际企业中心1栋C座2",
 "lnt": 0,
 "lat": 0
 */
