//
//  YYUpPsonDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYUpPsonBeanList.h"
@interface YYUpPsonDataModel : JSONModel
@property (nonatomic,strong)NSArray <YYUpPsonBeanList> *beanList;
@end
