//
//  ExpenseModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpenseModel : NSObject
@property (nonatomic,strong)NSString *djnum;
@property (nonatomic,strong)NSString *PJnum;
@property (nonatomic,strong)NSString *aliph;
@property (nonatomic,strong)NSString *PJbianhao;
@property (nonatomic,assign)NSInteger value;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSString *unit;
@end
/*
 djnum
 PJnum
 aliph
 PJbianhao
 value
 disc
 */