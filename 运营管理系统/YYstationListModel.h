//
//  YYstationListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/5.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol YYstationListModel <NSObject>
@end
@interface YYstationListModel : JSONModel
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,strong)NSString *opCenName;
@property (nonatomic,strong)NSString *principalName;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,assign)NSInteger stationId;
@property (nonatomic,strong)NSString *stationName;
@property (nonatomic,assign)float lnt;
@property (nonatomic,assign)float lat;
@end
/*
 "phone": "18038739113",
 "opCenName": "湛江运营中心",
 "principalName": "陈政全",
 "address": "厂区内",
 "stationId": 2335,
 "stationName": "徐闻县污水处理厂（排水口）",
 "lnt": 0,
 "lat": 0
 */
