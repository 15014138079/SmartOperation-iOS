//
//  GZywPsonVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/14.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "GZcountPsonCell.h"
#import "AFNetworking.h"
#import "YYpsonDataModel.h"
#import "MBProgressHUD.h"
#import "CircleView.h"
#import "CJCalendarViewController.h"
#import "AppDelegate.h"
#import "JoinFaluterView.h"
#import "FallLineView.h"
#import "ServerUnusualView.h"
#import "NothingDataView.h"
#import "MJRefresh.h"
@interface GZywPsonVC : UIViewController<UITableViewDelegate,UITableViewDataSource,CircleViewDataSource,CalendarViewControllerDelegate,JoinFalutDelegate,FallLineDelegate,ServerUnsualDelegate,NothingViewDelegate>

@end
