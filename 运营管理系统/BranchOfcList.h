//
//  BranchOfcList.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/25.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol BranchOfcList <NSObject>

@end

@interface BranchOfcList : JSONModel
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,strong)NSString *pson;
@property (nonatomic,strong)NSString *branchOfcName;
@property (nonatomic,assign)float lnt;
@property (nonatomic,assign)float lat;
@end
/*
 "phone": "",
 "address": "",
 "pson": "",
 "branchOfcName": "湖南分公司",
 "lnt": "",
 "lat": ""
 */
