//
//  LeftMenuView.m
//  污染源在线监测
//
//  Created by 杨毅 on 16/7/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//
#define ImageviewWidth 18
#define Frame_Width    self.frame.size.width//200


#import "LeftMenuView.h"
#import "Masonry.h"
#import "MenuModel.h"
#import "MainViewController.h"
//#import "NextViewController.h"
//#import "MenuModel.h"
@interface LeftMenuView()<UITableViewDelegate,UITableViewDataSource>
{
    UIButton *_monitor_btn;
    UIButton *_team_btn;
    UIView *_headerView1;
    UIButton *_backBtn;
}
//@property(nonatomic,strong)UITableView *contentTableView;
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation LeftMenuView

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return  self;
}

- (void)initView{
   

    //添加头部
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,Frame_Width , 60)];
    UIColor *color = UIColorFromRGB(0x039ade);
    headerView.backgroundColor = color;
    [self addSubview:headerView];
    UILabel *title = [[UILabel alloc]init];
    title.text = @"主菜单";
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont systemFontOfSize:21];
    [title setTextColor:[UIColor whiteColor]];
    [headerView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView).offset(self.frame.size.width/3);
        make.top.equalTo(headerView).offset(30);
        make.size.mas_offset(CGSizeMake(80, 30));
    }];
    //监测设备运营管理
    _monitor_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    _monitor_btn.tag = 1001;
    UIImage *nor = [[UIImage imageNamed:@"b_icon_01_1"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *press = [[UIImage imageNamed:@"b_icon_01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_monitor_btn setImage:nor forState:UIControlStateNormal];
    [_monitor_btn setImage:press forState:UIControlStateSelected];
    [_monitor_btn addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_monitor_btn];
    [_monitor_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView).offset((self.frame.size.height - 30)/4);
        make.left.equalTo(self).offset(self.frame.size.width/3);
        make.size.mas_offset(CGSizeMake(self.frame.size.width/3, 60));
    }];
    UILabel *monitor_title = [[UILabel alloc]init];
    monitor_title.text = @"监测设备运营管理";
    [self addSubview:monitor_title];
    [monitor_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_monitor_btn.mas_bottom).offset(5);
        make.left.equalTo(self).offset(self.frame.size.width/4-10);
        make.size.mas_offset(CGSizeMake(self.frame.size.width/2+20, 30));
    }];
    
    
    //运营团队云管理
    _team_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    _team_btn.tag = 1002;
    UIImage *nor2 = [[UIImage imageNamed:@"b_icon_02_1"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *press2 = [[UIImage imageNamed:@"b_icon_02"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_team_btn setImage:nor2 forState:UIControlStateNormal];
    [_team_btn setImage:press2 forState:UIControlStateSelected];
    [_team_btn addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_team_btn];
    [_team_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(monitor_title.mas_bottom).offset((self.frame.size.height - 30)/4);
        make.left.equalTo(self).offset(self.frame.size.width/3);
        make.size.mas_offset(CGSizeMake(self.frame.size.width/3, 50));
    }];
    UILabel *team_title = [[UILabel alloc]init];
    team_title.text = @"运营团队云管理";
    [self addSubview:team_title];
    [team_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_team_btn.mas_bottom).offset(5);
        make.left.equalTo(self).offset(self.frame.size.width/4);
        make.size.mas_offset(CGSizeMake(self.frame.size.width/2, 30));
    }];


    
}


- (void)selectedChange:(UIButton *)sender{
    
    _monitor_btn.selected = NO;
    _team_btn.selected = NO;
    sender.selected = YES;
    if (sender.tag == 1001) {
        //添加头部
        _headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0,Frame_Width , 60)];
        UIColor *color = UIColorFromRGB(0x039ade);
        _headerView1.backgroundColor = color;
        [self addSubview:_headerView1];
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"return"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(removeView) forControlEvents:UIControlEventTouchUpInside];
        [_headerView1 addSubview:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headerView1).offset(15);
            make.top.equalTo(_headerView1).offset(23);
            make.size.mas_offset(CGSizeMake(25, 25));
        }];
        UILabel *title = [[UILabel alloc]init];
        title.text = @"监测设备运营管理";
        title.textAlignment = NSTextAlignmentCenter;
        title.font = [UIFont systemFontOfSize:18];
        [title setTextColor:[UIColor whiteColor]];
        [_headerView1 addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backBtn.mas_right).offset(10);
            make.top.equalTo(_headerView1).offset(20);
            make.size.mas_offset(CGSizeMake(150, 30));
        }];
        _dataSource = [[NSMutableArray alloc]init];
        NSArray *subtitle = @[@"统计分析",@"设备运行状态",@"故障及报警查询",@"故障检测结果查询",@"故障录入",@"巡检录入",@"车辆信息查询"];
        NSArray *images = @[@"k_icon_01",@"k_icon_02",@"k_icon_03",@"k_icon_04",@"k_icon_05",@"k_icon_06",@"k_icon_07"];
        
        for (NSInteger i = 0; i < subtitle.count; ++i) {
            MenuModel *model = [[MenuModel alloc]init];
            model.title = subtitle[i];
            model.icon = images[i];
            [_dataSource addObject:model];
        }
        //NSLog(@"data%@",_dataSource);
        
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 60, self.frame.size.width, self.frame.size.height)];
        //禁止tableview滚动
        _tableview.scrollEnabled = NO;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        //去掉多余的cell和分割线
        _tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:_tableview];
    }else if (sender.tag == 1002){
    
        _headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0,Frame_Width , 60)];
        UIColor *color = UIColorFromRGB(0x039ade);
        _headerView1.backgroundColor = color;
        [self addSubview:_headerView1];
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"return"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(removeView) forControlEvents:UIControlEventTouchUpInside];
        [_headerView1 addSubview:_backBtn];
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headerView1).offset(15);
            make.top.equalTo(_headerView1).offset(23);
            make.size.mas_offset(CGSizeMake(25, 25));
        }];
        UILabel *title = [[UILabel alloc]init];
        title.text = @"运营团队云管理";
        title.textAlignment = NSTextAlignmentCenter;
        title.font = [UIFont systemFontOfSize:18];
        [title setTextColor:[UIColor whiteColor]];
        [_headerView1 addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_backBtn.mas_right).offset(10);
            make.top.equalTo(_headerView1).offset(20);
            make.size.mas_offset(CGSizeMake(150, 30));
        }];
        _dataSource = [[NSMutableArray alloc]init];
        NSArray *subtitle = @[@"站点设备信息查询",@"定位查询",@"人员管理及信息查询",@"工作管理及纪录查询",@"车辆管理及纪录查询",@"备件耗材信息管理"];
        NSArray *images = @[@"d_icon_01",@"d_icon_02",@"d_icon_03",@"d_icon_04",@"d_icon_05",@"d_icon_06"];
        
        for (NSInteger i = 0; i < subtitle.count; ++i) {
            MenuModel *model = [[MenuModel alloc]init];
            model.title = subtitle[i];
            model.icon = images[i];
            [_dataSource addObject:model];
        }
        //NSLog(@"data%@",_dataSource);
        
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 60, self.frame.size.width,self.frame.size.height)];
        //禁止tableview滚动
        _tableview.scrollEnabled = NO;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        //去掉多余的cell和分割线
        _tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:_tableview];
        
        
    

    }
    if ([self.customDelegate respondsToSelector:@selector(LeftMenuViewBtnClick:)]) {
        [self.customDelegate LeftMenuViewBtnClick:sender.tag];
    }
    
    
    
}

#pragma Mark-->uitableView delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    MenuModel *model = [[MenuModel alloc]init];
    model = _dataSource[indexPath.row];
    cell.textLabel.text = model.title;
    cell.imageView.image = [UIImage imageNamed:model.icon];
    //右边箭头
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //去掉分割线
    //_tableview.separatorStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if ([self.customDelegate respondsToSelector:@selector(MenuDTView:didClickMenuIndex:)]) {
        [self.customDelegate MenuDTView:self didClickMenuIndex:indexPath.row];
    }
    
}


- (void)removeView{
    
    [UIView animateWithDuration:0.25 animations:^{
        _backBtn.alpha = 0.6;
    } completion:^(BOOL finished) {
        _backBtn.alpha = 1;
    }];
    [_tableview removeFromSuperview];
    [_headerView1 removeFromSuperview];

}


@end
