//
//  Besicmodel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "dataModel.h"
@interface Besicmodel : JSONModel
@property (nonatomic,strong)NSString *massage;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)dataModel *data;
@end
