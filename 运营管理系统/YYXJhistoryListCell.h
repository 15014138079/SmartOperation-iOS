//
//  YYXJhistoryListCell.h
//  XJHistory
//
//  Created by 杨毅 on 16/12/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYXJhisListModel.h"
@interface YYXJhistoryListCell : UITableViewCell
- (void)fillCellWithModel:(YYXJhisListModel *)model;
@end
