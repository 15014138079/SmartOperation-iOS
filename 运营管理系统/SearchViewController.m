//
//  SearchViewController.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/23.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()
{
    UITextField *_searchText;
    UIButton *_starTimeTF;
    UIButton *_stopTimeTF;
    NSInteger tag;
    UIButton *_btn1;
    UIButton *_btn2;
    UIButton *_btn3;
    NSInteger fttpid;
    NSInteger sta0;
    NSInteger sta1;
    NSInteger sta2;
    NSDictionary *_paramet;
    FallLineView *_Fallview;
    JoinFaluterView *_JoinFalutView;
    ServerUnusualView *_ServerFView;
    NothingDataView *_nothing;
    NSMutableArray *_GZtype;//故障类型
    NSMutableArray *_typeID;//类型id数组
    ChooseMenus *_MenusView;
    BOOL isSelect;
}
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)NSMutableArray *GZtype;
@property (nonatomic,strong)NSMutableArray *typeId;
@property (nonatomic,strong)UIButton *GZtypeBtn;
@property (nonatomic,strong)NSMutableArray *dataSource0;
@property (nonatomic,strong)NSMutableArray *dataSource1;
@property (nonatomic,strong)NSMutableArray *dataSource2;
@property (nonatomic,strong)NSMutableArray *dataSource3;
@property (nonatomic,strong)NSMutableArray *typeIDArr1;
@property (nonatomic,strong)NSMutableArray *typeIDArr2;
@property (nonatomic,strong)NSMutableArray *typeIDArr3;
@end

@implementation SearchViewController
- (AFHTTPSessionManager *)manager{

    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"故障检索";
    self.view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;

    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.userId = delegate.userId;
    _dataSource = [[NSMutableArray alloc]init];
    _dataSource0 = [[NSMutableArray alloc]init];
    _dataSource1 = [[NSMutableArray alloc]init];
    _dataSource2 = [[NSMutableArray alloc]init];
    _dataSource3 = [[NSMutableArray alloc]init];
    _typeIDArr1  = [[NSMutableArray alloc]init];
    _typeIDArr2  = [[NSMutableArray alloc]init];
    _typeIDArr3  = [[NSMutableArray alloc]init];
    
    self.tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT) style:UITableViewStylePlain];
    self.tableview.rowHeight = 80;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.tableview.backgroundColor = UIColorFromRGB(0xf0f0f0);
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
   
    [self.tableview setTableFooterView:v];
    UIView *heardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 200)];
    heardView.backgroundColor = [UIColor whiteColor];
    [_tableview setTableHeaderView:heardView];
    [self.view addSubview:_tableview];
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 15, 75, 21)];
    timeLab.text = @"起止时间:";
    timeLab.textColor = UIColorFromRGB(0x4d4d4d);
    [heardView addSubview:timeLab];
    NSInteger btnWidth = (kSCREEN_WIDTH-101)/2;
    _starTimeTF = [UIButton buttonWithType:UIButtonTypeSystem];
    [_starTimeTF setTitle:@"2016-09-19" forState:UIControlStateNormal];
    [_starTimeTF setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _starTimeTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _starTimeTF.titleLabel.font = [UIFont systemFontOfSize:19.0];
    _starTimeTF.layer.cornerRadius = 5;
    //btn标题对齐方式
    _starTimeTF.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_starTimeTF addTarget:self action:@selector(chooseTime:) forControlEvents:UIControlEventTouchUpInside];
    _starTimeTF.tag = 1001;
    [heardView addSubview:_starTimeTF];
    [_starTimeTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(heardView).offset(10);
            make.left.equalTo(timeLab.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(btnWidth, 30));
        }];
    UILabel *line = [[UILabel alloc]init];
    line.text = @"--";
    line.textColor = UIColorFromRGB(0xb6b6b6);
    [heardView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(heardView).offset(15);
        make.left.equalTo(_starTimeTF.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(16, 21));
    }];
    
    _stopTimeTF = [UIButton buttonWithType:UIButtonTypeSystem];
    [_stopTimeTF setTitle:@"2016-09-21" forState:UIControlStateNormal];
    [_stopTimeTF setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _stopTimeTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _stopTimeTF.titleLabel.font = [UIFont systemFontOfSize:19.0];
    _stopTimeTF.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _stopTimeTF.layer.cornerRadius = 5;
    [_stopTimeTF addTarget:self action:@selector(chooseTime:) forControlEvents:UIControlEventTouchUpInside];
    _stopTimeTF.tag = 1002;
    [heardView addSubview:_stopTimeTF];
    [_stopTimeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(heardView).offset(10);
        make.left.equalTo(line.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(btnWidth, 30));
    }];

    UIView *line1 = [[UILabel alloc]init];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [heardView addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_stopTimeTF.mas_bottom).offset(10);
        make.left.equalTo(heardView).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-10, 1));
    }];
    _GZtypeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_GZtypeBtn setTitle:@"选择故障类型(可选)" forState:UIControlStateNormal];
    [_GZtypeBtn setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _GZtypeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _GZtypeBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _GZtypeBtn.layer.cornerRadius = 5;
    isSelect = NO;
    [_GZtypeBtn addTarget:self action:@selector(chooseType:) forControlEvents:UIControlEventTouchUpInside];
    [heardView addSubview:_GZtypeBtn];
    [_GZtypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line1.mas_bottom).offset(10);
        make.left.equalTo(heardView).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-40, 30));
    }];
    UIView *line2 = [[UILabel alloc]init];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [heardView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_GZtypeBtn.mas_bottom).offset(10);
        make.left.equalTo(heardView).offset(5);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-10, 1));
    }];
    _searchText = [[UITextField alloc]init];
    _searchText.placeholder = @"🔍输入搜索关键字(可填)";
    _searchText.borderStyle = UITextBorderStyleNone;
    _searchText.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _searchText.textColor = UIColorFromRGB(0x4d4d4d);
    _searchText.layer.cornerRadius = 5;
    [heardView addSubview:_searchText];
    [_searchText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line2.mas_bottom).offset(10);
        make.left.equalTo(heardView).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-90, 30));
    }];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.titleLabel.font = [UIFont systemFontOfSize:19.0];
    [btn setTitle:@"搜索" forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [heardView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line2.mas_bottom).offset(10);
        make.left.equalTo(_searchText.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    UIImage *check = [UIImage imageNamed:@"check"];
    UIImage *uncheck = [UIImage imageNamed:@"uncheck"];
    _btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn1 setImage:uncheck forState:UIControlStateNormal];
    [_btn1 setImage:check forState:UIControlStateSelected];
    [_btn1 addTarget:self action:@selector(selectedChange1) forControlEvents:UIControlEventTouchUpInside];

    [heardView addSubview:_btn1];
    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(20);
        make.left.equalTo(heardView).offset(30);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    UILabel *not = [[UILabel alloc]init];
    not.text = @"未处理";
    not.textAlignment = NSTextAlignmentLeft;
    not.textColor = UIColorFromRGB(0x4d4d4d);
    [heardView addSubview:not];
    [not mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(18);
        make.left.equalTo(_btn1.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake((kSCREEN_WIDTH-114)/3, 21));
    }];
    _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn2 setImage:uncheck forState:UIControlStateNormal];
    [_btn2 setImage:check forState:UIControlStateSelected];
    [_btn2 addTarget:self action:@selector(selectedChange2) forControlEvents:UIControlEventTouchUpInside];

    [heardView addSubview:_btn2];
    [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(20);
        make.left.equalTo(not.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    UILabel *center = [[UILabel alloc]init];
    center.text = @"处理中";
    center.textAlignment = NSTextAlignmentLeft;
    center.textColor = UIColorFromRGB(0x4d4d4d);
    [heardView addSubview:center];
    [center mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(18);
        make.left.equalTo(_btn2.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake((kSCREEN_WIDTH-114)/3, 21));
    }];

    _btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn3 setImage:uncheck forState:UIControlStateNormal];
    [_btn3 setImage:check forState:UIControlStateSelected];
    [_btn3 addTarget:self action:@selector(selectedChange3) forControlEvents:UIControlEventTouchUpInside];

    [heardView addSubview:_btn3];
    [_btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(20);
        make.left.equalTo(center.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    UILabel *already = [[UILabel alloc]init];
    already.text = @"已处理";
    already.textAlignment = NSTextAlignmentLeft;
    already.textColor = UIColorFromRGB(0x4d4d4d);
    [heardView addSubview:already];
    [already mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_searchText.mas_bottom).offset(18);
        make.left.equalTo(_btn3.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake((kSCREEN_WIDTH-114)/3, 21));
    }];
    
}
- (void)initView{
    
    _MenusView = [[ChooseMenus alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/3*2, self.view.frame.size.width, self.view.frame.size.height/3)];
    _MenusView.backgroundColor = [UIColor whiteColor];
    _MenusView.dataSource = self;
    [_MenusView typeIDOne:_typeIDArr1];
    [_MenusView typeIDTow:_typeIDArr2];
    [_MenusView typeIDThree:_typeIDArr3];
    [self.view addSubview:_MenusView];
    [_MenusView TitleBlock:^(NSString *block , NSInteger ID) {
        [_GZtypeBtn setTitle:block forState:UIControlStateNormal];
        [_GZtypeBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        fttpid = ID;
    }];
    
}

- (void)selectedChange1{

    BOOL isSelected = _btn1.isSelected;
    if (isSelected)
    {
        //将当前状态设置为normal
        _btn1.selected = NO;
        sta0 = 0;
    }
    else
    {
        _btn1.selected = YES;
        sta0 = 1;
    }
}
- (void)selectedChange2{
    
    BOOL isSelected = _btn2.isSelected;
    if (isSelected)
    {
        //将当前状态设置为normal
        _btn2.selected = NO;
        sta1 = 0;
    }
    else
    {
        _btn2.selected = YES;
        sta1 = 1;
    }
}
- (void)selectedChange3{
    
    BOOL isSelected = _btn3.isSelected;
    if (isSelected)
    {
        //将当前状态设置为normal
        _btn3.selected = NO;
        sta2 = 0;
    }
    else
    {
        _btn3.selected = YES;
        sta2 = 1;
    }
}
- (void)load{
    
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_GZtype parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        TypeDataModel *model = [[TypeDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (SectionOneFaultModel *OneModel in model.FaultType) {
            
            [_dataSource0 addObject:OneModel];
            [_typeIDArr1 addObject:[NSString stringWithFormat:@"%ld",OneModel.ID]];
            //第二层数组模型 2层
            NSMutableArray *arr1 = [[NSMutableArray alloc]init];
            //第二层数组 ID 2层
            NSMutableArray *IDArr1 = [[NSMutableArray alloc]init];
            //第三层数组ID   3层
            NSMutableArray *IDArr3 = [[NSMutableArray alloc]init];
            //第三层数组模型  3层
            NSMutableArray *arr3 = [[NSMutableArray alloc]init];
            for (SectionTowFaultModel *TowModel in OneModel.faultTypes) {
                [arr1 addObject:TowModel];
                [IDArr1 addObject:[NSString stringWithFormat:@"%ld",TowModel.ID]];
                NSMutableArray *arr2 = [[NSMutableArray alloc]init];
                NSMutableArray *IDArr2 = [[NSMutableArray alloc]init];
                for (SectionThreeFaultModel *ThreeModel in TowModel.faultTypes) {
                    [arr2 addObject:ThreeModel];
                    [IDArr2 addObject:[NSString stringWithFormat:@"%ld",ThreeModel.ID]];
                }
                [IDArr3 addObject:IDArr2];
                [arr3 addObject:arr2];
            }
            [_typeIDArr2 addObject:IDArr1];
            [_dataSource1 addObject:arr1];
            [_typeIDArr3 addObject:IDArr3];
            [_dataSource2 addObject:arr3];
            
        }
        
        [self initView];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)chooseType:(UIButton *)sender{
    
    if (isSelect) {
        //isSelect = NO;btn再次点击移除选择菜单
        isSelect = !isSelect;
        [_dataSource0 removeAllObjects];
        [_dataSource1 removeAllObjects];
        [_dataSource2 removeAllObjects];
        [_typeIDArr1 removeAllObjects];
        [_typeIDArr2 removeAllObjects];
        [_typeIDArr3 removeAllObjects];
        [_MenusView removeFromSuperview];
    }else{
        
        [self load];
      
        isSelect = YES;
    }

    
}
/*故障类型选择确定键*/
- (void)CancelChooseView:(UIButton *)sender{
    
    isSelect = !isSelect;
    [_dataSource0 removeAllObjects];
    [_dataSource1 removeAllObjects];
    [_dataSource2 removeAllObjects];
    [_typeIDArr1 removeAllObjects];
    [_typeIDArr2 removeAllObjects];
    [_typeIDArr3 removeAllObjects];
    //_MenusView.alpha = 0;
    [_MenusView removeFromSuperview];
    
}
#pragma mark - WSDropMenuView DataSource -
- (NSInteger)dropMenuView:(ChooseMenus *)dropMenuView numberWithIndexPath:(WSIndexPath *)indexPath{
    
    //WSIndexPath 类里面有注释
    
    if (indexPath.row == WSNoFound) {
        
        return _dataSource0.count;
    }
    if (indexPath.row != WSNoFound && indexPath.item == WSNoFound) {
        NSArray *arr = _dataSource1[indexPath.row];
        return arr.count;
    }
    
    if (indexPath.row != WSNoFound && indexPath.item != WSNoFound && indexPath.rank == WSNoFound) {
        NSArray *arr = _dataSource2[indexPath.row][indexPath.item];
        return arr.count;
    }
    
    
    
    return 0;
}

- (NSString *)dropMenuView:(ChooseMenus *)dropMenuView titleWithIndexPath:(WSIndexPath *)indexPath{
    
    //return [NSString stringWithFormat:@"%ld",indexPath.row];
    
    //左边 第一级
    if (indexPath.row != WSNoFound && indexPath.item == WSNoFound) {
        SectionOneFaultModel *model = _dataSource0[indexPath.row];
        
        return model.name;
    }
    
    if (indexPath.row != WSNoFound && indexPath.item != WSNoFound && indexPath.rank == WSNoFound) {
        
        SectionTowFaultModel *model = _dataSource1[indexPath.row][indexPath.item];
        
        
        
        return model.name;
    }
    
    if (indexPath.row != WSNoFound && indexPath.item != WSNoFound && indexPath.rank != WSNoFound) {
        SectionThreeFaultModel *model = _dataSource2[indexPath.row][indexPath.item][indexPath.rank];
        
        return model.name;
    }
    
    
    return @"";
    
}

- (void)searchBtnClick{


    if ((!_btn1.isSelected && !_btn2.isSelected && !_btn3.isSelected) && ([_starTimeTF.titleLabel.text isEqualToString:@"2016-09-19"] || [_stopTimeTF.titleLabel.text isEqualToString:@"2016-09-21"] )) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请完善搜索条件,处理状态至少选一个" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *OkAction = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:OkAction];
        
        [self presentViewController:alert animated:YES completion:nil];

    }else{
        if ([_GZtypeBtn.titleLabel.text isEqualToString:@"选择故障类型"]) {
        NSString *str = @"0";
        _paramet = @{star:_starTimeTF.titleLabel.text,endtime:_stopTimeTF.titleLabel.text,kword:_searchText.text,userid:[NSString stringWithFormat:@"%ld",self.userId],ftypeid:str,state0:[NSString stringWithFormat:@"%ld",sta0],state1:[NSString stringWithFormat:@"%ld",sta1],state2:[NSString stringWithFormat:@"%ld",sta2],@"userId":[NSString stringWithFormat:@"%ld",self.userId]};
        }else {
    
        _paramet = @{star:_starTimeTF.titleLabel.text,endtime:_stopTimeTF.titleLabel.text,kword:_searchText.text,userid:[NSString stringWithFormat:@"%ld",self.userId],ftypeid:[NSString stringWithFormat:@"%ld",fttpid],state0:[NSString stringWithFormat:@"%ld",sta0],state1:[NSString stringWithFormat:@"%ld",sta1],state2:[NSString stringWithFormat:@"%ld",sta2],@"userId":[NSString stringWithFormat:@"%ld",self.userId]};
        }
   
        [self networking];
    }
    
}
- (void)networking{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"加载中...";
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:K_SearchList parameters:_paramet progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [_dataSource removeAllObjects];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        YYAlarmDataModel *datamodel = [[YYAlarmDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (YYAlarmListModel *model in datamodel.beanList) {
            [_dataSource addObject:model];
        }

        if (_dataSource.count == 0) {
            self.tableview.scrollEnabled = NO;
            _nothing = [[NothingDataView alloc]initWithFrame:CGRectMake(0, 250, kSCREEN_WIDTH, kSCREEN_HEIGHT-200) title:@"没有您要的数据"];
            _nothing.delegate = self;
            [self.view addSubview:_nothing];
        }else{
            self.tableview.scrollEnabled = YES;
            [_nothing removeFromSuperview];
        }
        
        [self.tableview reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        if (status >= 500) {
            _ServerFView = [[ServerUnusualView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) AndTitle:[error userInfo][@"NSLocalizedDescription"]];
            _ServerFView.delegate = self;
            [self.view addSubview:_ServerFView];
        }else if ([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"请求超时。"]) {
            _Fallview = [[FallLineView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _Fallview.delegate = self;
            [self.view addSubview:_Fallview];
        }else if([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"似乎已断开与互联网的连接。"]){
            _JoinFalutView = [[JoinFaluterView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
            _JoinFalutView.delegate = self;
            [self.view addSubview:_JoinFalutView];
        }else{
            [self showAlert:@"温馨提示" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
        
    }];
}
- (void)NothingRefresh:(UIButton *)sender{
    [_nothing removeFromSuperview];
    [self networking];
}
-(void)JoinRefersh:(UIButton *)sender{
    [_JoinFalutView removeFromSuperview];
    [self networking];
}
-(void)FallLineRefresh:(UIButton *)sender{
    [_Fallview removeFromSuperview];
    [self networking];
}
-(void)ServerRefresh:(UIButton *)sender{
    [_ServerFView removeFromSuperview];
    [self networking];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlarmTableViewCell *cell = nil;
    static NSString *reuse=@"cell";
    
    if (cell==nil) {
        cell = [[AlarmTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuse];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        
        while ([cell.contentView.subviews lastObject] != nil) {
            [(UIView*)[cell.contentView.subviews lastObject] removeFromSuperview];  //删除并进行重新分配
        }
    }
    //cell 的序号
    UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    
    number.textColor = [UIColor whiteColor];
    number.textAlignment = NSTextAlignmentCenter;
    
    number.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    if ([number.text integerValue] > 100) {
        
        number.font = [UIFont systemFontOfSize:20];
    }else{
        number.font = [UIFont systemFontOfSize:40];
    }
    [cell addSubview:number];
    
    
    YYAlarmListModel *model = _dataSource[indexPath.row];
    [cell fillCellWithModel:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    TroubleDetailVC *vc = [[TroubleDetailVC alloc]init];
    YYAlarmListModel *model = _dataSource[indexPath.row];
    vc.parameters = model.gzID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)chooseTime:(UIButton *)sender{
    
    tag = sender.tag;
    
    CJCalendarViewController *calendarController = [[CJCalendarViewController alloc] init];
    calendarController.view.frame = self.view.frame;
    
    calendarController.delegate = self;
    NSArray *arr = [sender.titleLabel.text componentsSeparatedByString:@"-"];
    
    if (arr.count > 1) {
        [calendarController setYear:arr[0] month:arr[1] day:arr[2]];
    }
    
    [self presentViewController:calendarController animated:YES completion:nil];

}

-(void)CalendarViewController:(CJCalendarViewController *)controller didSelectActionYear:(NSString *)year month:(NSString *)month day:(NSString *)day{
    
    if (tag == 1001) {
        
        if (day.length == 1 && month.length != 1) {
            
            [_starTimeTF setTitle:[NSString stringWithFormat:@"%@-%@-0%@", year, month,day] forState:UIControlStateNormal];
        }else if (month.length == 1 && day.length != 1){
            
            [_starTimeTF setTitle:[NSString stringWithFormat:@"%@-0%@-%@", year, month, day] forState:UIControlStateNormal];
        }else if (month.length == 1 && day.length ==1 ){
            [_starTimeTF setTitle:[NSString stringWithFormat:@"%@-0%@-0%@", year, month, day] forState:UIControlStateNormal];
        } else{
            
            [_starTimeTF setTitle:[NSString stringWithFormat:@"%@-%@-%@", year, month, day] forState:UIControlStateNormal];
        }
    [_starTimeTF setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
 
    }else{
    
        if (day.length == 1 && month.length != 1) {
            
            [_stopTimeTF setTitle:[NSString stringWithFormat:@"%@-%@-0%@", year, month,day] forState:UIControlStateNormal];
        }else if (month.length == 1 && day.length != 1){
            
            [_stopTimeTF setTitle:[NSString stringWithFormat:@"%@-0%@-%@", year, month, day] forState:UIControlStateNormal];
        }else if (month.length == 1 && day.length ==1 ){
            [_stopTimeTF setTitle:[NSString stringWithFormat:@"%@-0%@-0%@", year, month, day] forState:UIControlStateNormal];
        } else{
            
            [_stopTimeTF setTitle:[NSString stringWithFormat:@"%@-%@-%@", year, month, day] forState:UIControlStateNormal];
        }
            [_stopTimeTF setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    }


}

#pragma mark 封装提示框
-(void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)back{

    [self.navigationController popViewControllerAnimated:YES];
}





@end


