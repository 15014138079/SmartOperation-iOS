//
//  workModel.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/21.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "workModel.h"

@implementation workModel
+ (NSArray *)WorkModule:(NSArray *)MenuIDs{

    NSMutableArray *Menus = [[NSMutableArray alloc]init];
    workModel *MODEL1 = [[workModel alloc]init];
    MODEL1.LevelTag = 1;
    MODEL1.leftIcon = @"fault_report";
    MODEL1.countent = @"故障上报";
    MODEL1.message = @"随时随地,及时提交故障";
    
    workModel *MODEL2 = [[workModel alloc]init];
    MODEL2.LevelTag = 2;
    MODEL2.leftIcon = @"fault_approval";
    MODEL2.countent = @"故障审批";
    MODEL2.message = @"继续上报故障,或指派人员处理";
    workModel *MODEL3 = [[workModel alloc]init];
    MODEL3.LevelTag = 3;
    MODEL3.leftIcon = @"fault_handling";
    MODEL3.countent = @"故障处理";
    MODEL3.message = @"待处理的故障，上传照片更直观";
    workModel *MODEL4 = [[workModel alloc]init];
    MODEL4.LevelTag = 4;
    MODEL4.leftIcon = @"fault_handling_confirmation";
    MODEL4.countent = @"故障处理确认";
    MODEL4.message = @"自己上报并处理的故障无需确认噢";
    workModel *MODEL5 = [[workModel alloc]init];
    MODEL5.LevelTag = 5;
    MODEL5.leftIcon = @"daily_inspection";
    MODEL5.countent = @"日常巡检";
    MODEL5.message = @"日常巡检也能上报故障~";
   
    workModel *MODEL6 = [[workModel alloc]init];
    MODEL6.LevelTag = 11;
    MODEL6.leftIcon = @"quality_feedback";
    MODEL6.countent = @"质量反馈";
    MODEL6.message = @"跟进、创建质量反馈流程";
    
    workModel *MODEL7 = [[workModel alloc]init];
    MODEL7.LevelTag = 12;
    MODEL7.leftIcon = @"quality_feedbac_processing";
    MODEL7.countent = @"质量反馈处理";
    MODEL7.message = @"质量反馈流程处理、查看历史处理信息";
    
    workModel *MODEL8 = [[workModel alloc]init];
    MODEL8.LevelTag = 13;
    MODEL8.leftIcon = @"work_proposal";
    MODEL8.countent = @"工作建议";
    MODEL8.message = @"浏览、创建工作建议";
    
    [Menus addObject:MODEL1];
    [Menus addObject:MODEL2];
    [Menus addObject:MODEL3];
    [Menus addObject:MODEL4];
    [Menus addObject:MODEL5];
    
    [Menus addObject:MODEL6];
    [Menus addObject:MODEL7];
    [Menus addObject:MODEL8];

    
    NSMutableArray *resultArr = [[NSMutableArray alloc]init];
    for (workModel *menu in Menus) {
        for (NSNumber *menuID in MenuIDs) {
            if (menuID.integerValue == menu.LevelTag) {
                [resultArr addObject:menu];
            }
        }
    }
  
    return resultArr;
    
}
+ (NSArray *)SearchModule:(NSArray *)MenuIDs{
    
    NSMutableArray *Menus = [[NSMutableArray alloc]init];
    workModel *MODEL1 = [[workModel alloc]init];
    MODEL1.LevelTag = 6;
    MODEL1.leftIcon = @"a_icon01";
    MODEL1.countent = @"运营统计";
    
    workModel *MODEL2 = [[workModel alloc]init];
    MODEL2.LevelTag = 7;
    MODEL2.leftIcon = @"fault-statistics";
    MODEL2.countent = @"故障统计";
    workModel *MODEL3 = [[workModel alloc]init];
    MODEL3.LevelTag = 8;
    MODEL3.leftIcon = @"b_icon06";
    MODEL3.countent = @"库存查询";
    
    workModel *MODEL4 = [[workModel alloc]init];
    MODEL4.LevelTag = 9;
    MODEL4.leftIcon = @"b_icon02";
    MODEL4.countent = @"定位查询";

    workModel *MODEL5 = [[workModel alloc]init];
    MODEL5.LevelTag = 10;
    MODEL5.leftIcon = @"history-Search";
    MODEL5.countent = @"巡检查询";
    
    [Menus addObject:MODEL1];
    [Menus addObject:MODEL2];
    [Menus addObject:MODEL3];
    [Menus addObject:MODEL4];
    [Menus addObject:MODEL5];
    
    NSMutableArray *resultArr = [[NSMutableArray alloc]init];
    for (workModel *menu in Menus) {
        for (NSNumber *menuID in MenuIDs) {
            if (menuID.integerValue == menu.LevelTag) {
                [resultArr addObject:menu];
            }
        }
    }
    
    NSLog(@"%@",resultArr);
    return resultArr;
}
@end
