//
//  CarfenPei.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PopupView;
@protocol CarfenPeiDelegate <NSObject>

@optional
- (void)choose:(UIButton *)sender;

@end
@interface CarfenPei : UIView
@property (strong, nonatomic) IBOutlet UIView *innerView;
@property (nonatomic, weak)UIViewController *parentVC;
@property (nonatomic, strong)UIButton *btn1;
@property (nonatomic, strong)UIButton *btn2;
@property (nonatomic, strong)UIButton *btn3;
@property (nonatomic, strong)UITextField *textTF;
@property (nonatomic, weak) id<CarfenPeiDelegate> delegate;
+ (instancetype)defaultPopupView;
- (instancetype)initWithBounds:(CGRect)bounds titleMenus:(NSArray *)titles;
@end
