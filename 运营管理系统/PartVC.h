//
//  PartVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "PartSearchVC.h"
#import "MJRefresh.h"
#import "AppDelegate.h"
#import "YYpartDataModel.h"
#import "YYFgsPartVC.h"//分公司配件
#import "PartCompanyCell.h"
#import "MBProgressHUD.h"
#import "JoinFaluterView.h"
#import "ServerUnusualView.h"
#import "FallLineView.h"
#import "NothingDataView.h"
@interface PartVC : UIViewController<UITableViewDelegate,UITableViewDataSource,JoinFalutDelegate,NothingViewDelegate,FallLineDelegate,ServerUnsualDelegate>

@end
