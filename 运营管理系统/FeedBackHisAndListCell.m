//
//  FeedBackHisAndListCell.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "FeedBackHisAndListCell.h"

@implementation FeedBackHisAndListCell
{
    __weak IBOutlet UIImageView *LeftIcon;
    __weak IBOutlet UILabel *CompanyTitle;
    __weak IBOutlet UILabel *TimeLab;
    __weak IBOutlet UILabel *DiscrLab;
    
}
- (void)fillCellWithModel:(YYFeedListBeanlistModel *)model{

    LeftIcon.image = [UIImage imageNamed:@"feedback_b"];
    CompanyTitle.text = model.entName;
    CompanyTitle.lineBreakMode = NSLineBreakByTruncatingMiddle;
    TimeLab.text = [model.time substringToIndex:10];
    DiscrLab.text = model.faultDisc;
    DiscrLab.lineBreakMode = NSLineBreakByTruncatingMiddle;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
