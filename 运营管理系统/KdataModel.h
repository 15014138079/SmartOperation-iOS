//
//  KdataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "KmtPcsModel.h"
@interface KdataModel : JSONModel
@property (nonatomic,strong)NSArray <KmtPcsModel> *MtPcsDetail;
@end
