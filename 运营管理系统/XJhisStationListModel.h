//
//  XJhisStationListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol XJhisStationListModel <NSObject>

@end
@interface XJhisStationListModel : JSONModel

@property (nonatomic,strong)NSString *companyAddress;//公司地址
@property (nonatomic,strong)NSString *principalName;
@property (nonatomic,assign)float monthCost;//
@property (nonatomic,strong)NSString *contractStart;//合同开始时间
@property (nonatomic,assign)float contractMoney;//
@property (nonatomic,assign)NSInteger stationId;
@property (nonatomic,strong)NSString *companyName;
@property (nonatomic,strong)NSString *companyPrincipalName;//公司负责人
@property (nonatomic,strong)NSString *principalPhone;//站点负责人电话
@property (nonatomic,strong)NSString *stationName;
@property (nonatomic,strong)NSString *companyPrincipalPhone;//公司负责人电话
@property (nonatomic,strong)NSString *stationType;//站点检测类型
@property (nonatomic,strong)NSString *contractCode;
@property (nonatomic,strong)NSString *stationAddress;
@property (nonatomic,strong)NSString *contractEnd;
@end
/*
 "companyAddress": "武威市凉州区金羊镇郭家寨村6组",
 "principalName": "郑博",
 "monthCost": "",
 "contractStart": "2016-04-18 00:00:00",
 "contractMoney": 4,
 "stationId": 2380,
 "companyName": "武威市供排水公司污水处理厂",
 "companyPrincipalName": "董厂",
 "principalPhone": "15209356324",
 "stationName": "武威市供排水公司污水处理厂进水口",
 "companyPrincipalPhone": "13884536666",
 "stationType": "废水监测",
 "stationAddress": "武威市凉州区金羊镇郭家寨村6组",
 "contractCode": "YX-甘肃－2016－010",
 "contractEnd": "2017-04-17 00:00:00"
 */
