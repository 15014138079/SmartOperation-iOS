//
//  MyData.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/5.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "equeipsModel.h"

@interface MyData : NSObject
@property (nonatomic, strong) NSArray<equeipsModel> *equeipsModels;
@property (nonatomic, assign, getter=isExpend) BOOL expand;
@end
