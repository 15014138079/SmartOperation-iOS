//
//  PopupView.m
//  LewPopupViewController
//
//  Created by deng on 15/3/5.
//  Copyright (c) 2015年 pljhonglu. All rights reserved.
//

#import "PopupView.h"
#import "UIViewController+LewPopupViewController.h"
#import "LewPopupViewAnimationFade.h"
#import "LewPopupViewAnimationSlide.h"
#import "LewPopupViewAnimationSpring.h"
#import "LewPopupViewAnimationDrop.h"
#import "Masonry.h"
#import "YXKit.h"
#import "PHATextView.h"
@interface PopupView ()

@property (nonatomic, strong) NSArray *titleMenus;

@end
@implementation PopupView
{
    UITextField *_titelTF;
    PHATextView *_Discr;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithBounds:(CGRect)bounds titleMenus:(NSArray *)titles
{
    self = [super initWithFrame:bounds];
    if (self) {
        // Initialization code
        [[NSBundle mainBundle] loadNibNamed:[[self class] description] owner:self options:nil];
        _innerView.frame = bounds;
        [self addSubview:_innerView];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[[UIImage imageNamed:@"close"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];

        btn.frame = CGRectMake(_innerView.frame.size.width-30,20, 20, 20);
        [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        UIImageView *LeftIcon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 50, 30, 30)];
        LeftIcon.image = [[UIImage imageNamed:@"icon00001"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self addSubview:LeftIcon];
        UILabel *title = [[UILabel alloc]init];
        title.text = @"故障上报";
        title.font = [UIFont systemFontOfSize:21.0];
        title.textColor = UIColorFromRGB(0x4d4d4d);
        [self addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(55);
            make.left.equalTo(LeftIcon.mas_right).offset(10);
            make.size.mas_equalTo(CGSizeMake(100, 21));
        }];
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(LeftIcon.mas_bottom).offset(5);
            make.left.equalTo(self).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 1));
        }];
        NSArray *leftTitle = @[@"故障设备",@"发现人",@"故障时间"];
        self.titleMenus = titles;
        for (NSInteger i = 0; i < leftTitle.count; ++i) {
            UILabel *leftT = [[UILabel alloc]initWithFrame:CGRectMake(20,90+i*(21 + 15) , 80, 21)];
            leftT.text = leftTitle[i];
            leftT.textColor = UIColorFromRGB(0x4d4d4d);
            leftT.font = [UIFont systemFontOfSize:19.0];
            [self addSubview:leftT];
            if (i == 0) {
                _SbChooseLab = [[UILabel alloc]initWithFrame:CGRectMake(123,90+i*(21 + 15) , self.frame.size.width/5*3, 21)];
                _SbChooseLab.lineBreakMode =NSLineBreakByTruncatingMiddle;
                _SbChooseLab.text = @"选择设备";
                _SbChooseLab.textColor = UIColorFromRGB(0xb6b6b6);
                [self addSubview:_SbChooseLab];
                UIButton *eqChooseBtn = [UIButton buttonWithType:UIButtonTypeSystem];
                eqChooseBtn.frame = CGRectMake(123,90+i*(21 + 15) , self.frame.size.width/5*3, 21);
                eqChooseBtn.backgroundColor = [UIColor clearColor];
                [eqChooseBtn addTarget:self action:@selector(ChooseSB:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:eqChooseBtn];

            }else if (i == 2){
                UILabel *rightC = [[UILabel alloc]initWithFrame:CGRectMake(123,90+i*(21 + 15) ,self.frame.size.width/5*3, 21)];
                rightC.text = self.titleMenus[i];
                rightC.textColor = UIColorFromRGB(0xb6b6b6);
                [self addSubview:rightC];
            }else{
                UILabel *rightC = [[UILabel alloc]initWithFrame:CGRectMake(123,90+i*(21 + 15) ,self.frame.size.width/5*3, 21)];
                rightC.text = self.titleMenus[i];
                rightC.textColor = UIColorFromRGB(0xb6b6b6);
                //rightC.font = [UIFont systemFontOfSize:19.0];
                [self addSubview:rightC];
            }

            UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(123, 111+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
            line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [self addSubview:line1];
            
        }
        UILabel *GZtypeT = [[UILabel alloc]initWithFrame:CGRectMake(20, 198, 80, 21)];
        GZtypeT.textColor = UIColorFromRGB(0x4d4d4d);
        GZtypeT.text = @"故障类型";
        GZtypeT.font = [UIFont systemFontOfSize:19.0];
        [self addSubview:GZtypeT];
        _btn = [UIButton buttonWithType:UIButtonTypeSystem];
 
        [_btn setTitle:@"选择故障类型" forState:UIControlStateNormal];
        [_btn setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _btn.titleLabel.font = [UIFont systemFontOfSize:17.0];
        [_btn addTarget:self action:@selector(chooseType:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn];
        [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(198);
            make.left.equalTo(GZtypeT.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 21));
        }];
        UIView *line2 = [[UIView alloc]init];
        line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line2];
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(GZtypeT.mas_right).offset(20);
            make.top.equalTo(_btn.mas_bottom).offset(1);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 1));
        }];
        UILabel *xing = [[UILabel alloc]init];
        xing.text = @"*";
        xing.textColor = [UIColor redColor];
        [self addSubview:xing];
        [xing mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(198);
            make.right.equalTo(_btn.mas_right).offset(3);
            make.size.mas_equalTo(CGSizeMake(10, 10));
        }];

        UILabel *GZtitle = [[UILabel alloc]init];
        GZtitle.text = @"故障标题";
        GZtitle.textColor = UIColorFromRGB(0x4d4d4d);
        GZtitle.font = [UIFont systemFontOfSize:19.0];
        [self addSubview:GZtitle];
        [GZtitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(GZtypeT.mas_bottom).offset(15);
            make.left.equalTo(self).offset(20);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        _titelTF = [[UITextField alloc]init];
        _titelTF.borderStyle = UITextBorderStyleNone;
        _titelTF.placeholder = @"输入标题";
        _titelTF.textColor = UIColorFromRGB(0xbfbfc4);
        //_titelTF.font = [UIFont systemFontOfSize:19.0];
        [self addSubview:_titelTF];
        [_titelTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line2.mas_bottom).offset(14);
            make.left.equalTo(GZtitle.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 21));
        }];
        UIView *line3 = [[UIView alloc]init];
        line3.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line3];
        [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(GZtitle.mas_right).offset(20);
            make.top.equalTo(_titelTF.mas_bottom).offset(1);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 1));
        }];
        UILabel *xing1 = [[UILabel alloc]init];
        xing1.text = @"*";
        xing1.textColor = [UIColor redColor];
        [self addSubview:xing1];
        [xing1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line2.mas_bottom).offset(15);
            make.right.equalTo(_titelTF.mas_right).offset(3);
            make.size.mas_equalTo(CGSizeMake(10, 10));
        }];
        UILabel *baosongPsonLab = [[UILabel alloc]init];
        baosongPsonLab.text = @"报送人";
        baosongPsonLab.textColor = UIColorFromRGB(0x4d4d4d);
        baosongPsonLab.font = [UIFont systemFontOfSize:19.0];
        [self addSubview:baosongPsonLab];
        [baosongPsonLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(GZtitle.mas_bottom).offset(15);
            make.left.equalTo(self).offset(20);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
        _BSpsonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_BSpsonBtn setTitle:@"选择报送人名称" forState:UIControlStateNormal];
        [_BSpsonBtn setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
//        _BSpsonBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
//        _BSpsonBtn.layer.cornerRadius = 2;
        _BSpsonBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _BSpsonBtn.titleLabel.font = [UIFont systemFontOfSize:17.0];
        [_BSpsonBtn addTarget:self action:@selector(choosePson:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_BSpsonBtn];
        [_BSpsonBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(GZtitle.mas_bottom).offset(15);
            make.left.equalTo(baosongPsonLab.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 21));
        }];
        UIView *line4 = [[UIView alloc]init];
        line4.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line4];
        [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(baosongPsonLab.mas_right).offset(20);
            make.top.equalTo(_BSpsonBtn.mas_bottom).offset(1);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/5*3-30, 1));
        }];

        
        UILabel *GZDiscT = [[UILabel alloc]init];
        GZDiscT.text = @"故障描述";
        GZDiscT.textColor = UIColorFromRGB(0x4d4d4d);
        GZDiscT.font = [UIFont systemFontOfSize:19.0];
        [self addSubview:GZDiscT];
        [GZDiscT mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(baosongPsonLab.mas_bottom).offset(15);
            make.left.equalTo(self).offset(20);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];

        _Discr = [[PHATextView alloc]init];
        _Discr.font = [UIFont systemFontOfSize:15.0];
        _Discr.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _Discr.placeholder = @"不超过100个字符";
        _Discr.returnKeyType = UIReturnKeyDone;
        //_bztextView.textColor = UIColorFromRGB(0x4d4d4d);
        _Discr.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _Discr.layer.borderWidth = 0.5;
        _Discr.layer.cornerRadius = 3;
        [self addSubview:_Discr];
        [_Discr mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(GZDiscT.mas_bottom).offset(10);
            make.left.equalTo(self).offset(35);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width-70, 160));
        }];
        UILabel *xing2 = [[UILabel alloc]init];
        xing2.text = @"*";
        xing2.textColor = [UIColor redColor];
        [self addSubview:xing2];
        [xing2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(GZDiscT.mas_bottom).offset(20);
            make.left.equalTo(_Discr.mas_right).offset(3);
            make.size.mas_equalTo(CGSizeMake(10, 10));
        }];
        /*保存*/
        UIButton *preserve = [UIButton buttonWithType:UIButtonTypeSystem];
        [preserve setTitle:@"提交" forState:UIControlStateNormal];
        [preserve setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        preserve.backgroundColor = UIColorFromRGB(0x2dd500);
        preserve.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [preserve addTarget:self action:@selector(updata) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:preserve];
        [preserve mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_Discr.mas_bottom).offset(10);
            make.left.equalTo(self).offset(self.frame.size.width/6);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width/3*2, 40));
        }];
        
    }
    return self;
}


+ (instancetype)defaultPopupView{

    return [[self alloc]init];
}
- (void)ChooseSB:(UIButton *)sender{
    
    [self.delegate ChooseSB:sender];
}
- (void)chooseType:(UIButton *)sender{
    
    [self.delegate chooseType:sender];
    
}
- (void)choosePson:(UIButton *)sender{
    
    [self.delegate choosePson:sender];
}

- (void)updata{
    
    NSString *GZequipName = self.titleMenus[0];
    NSString *Fpson = self.titleMenus[1];
    NSString *GZtype = _btn.titleLabel.text;
    NSString *GZtitle = _titelTF.text;
    NSString *GZdis = _Discr.text;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:GZequipName forKey:@"equipName"];
    [user setObject:Fpson forKey:@"founkPson"];
    [user setObject:GZtype forKey:@"type"];
    [user setObject:GZtitle forKey:@"title"];
    [user setObject:GZdis forKey:@"discrib"];
    if ([_SbChooseLab.text isEqualToString:@"选择设备"] || [_btn.titleLabel.text isEqualToString:@"选择故障类型"] ||[ _titelTF.text isEqualToString:@"输入标题"] || _Discr.text.length < 1 || [_BSpsonBtn.titleLabel.text isEqualToString:@"选择报送人名称"]) {
        [[YXAlertHUD stateHUDWithHUDTyep:1 message:@"请完善提交信息" textColor:[UIColor blackColor] iconColor:[UIColor redColor]] show];
    }else if (_Discr.text.length > 100){
        [[YXAlertHUD stateHUDWithHUDTyep:1 message:@"故障描述信息超过限定字数" textColor:[UIColor blackColor] iconColor:[UIColor redColor]] show];
    }else{
    [UIView animateWithDuration:0 delay:2.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
            [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交完成" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
    } completion:^(BOOL finished) {
            [self dismiss];
    }];
    }

    
//    LewPopupViewAnimationSlide *animation = [[LewPopupViewAnimationSlide alloc]init];
//    animation.type = LewPopupViewAnimationSlideTypeTopBottom;
//    [_parentVC lew_dismissPopupViewWithanimation:animation];
    
}
- (void)dismiss{
    
    LewPopupViewAnimationSlide *animation = [[LewPopupViewAnimationSlide alloc]init];
    animation.type = LewPopupViewAnimationSlideTypeTopBottom;
    [_parentVC lew_dismissPopupViewWithanimation:animation];
}

//- (IBAction)dismissViewSpringAction:(id)sender{
//    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationSpring new]];
//}
//
//- (IBAction)dismissViewDropAction:(id)sender{
//    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationDrop new]];
//}

@end
