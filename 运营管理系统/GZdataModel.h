//
//  GZdataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/31.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "OhlistModel.h"
@interface GZdataModel : JSONModel

@property (nonatomic,strong)NSArray <OhlistModel> *ohrstlist;
@end
