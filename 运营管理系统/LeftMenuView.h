//
//  LeftMenuView.h
//  污染源在线监测
//
//  Created by 杨毅 on 16/7/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LeftMenuView;
@protocol HomeMenuViewDelegate <NSObject>

- (void)LeftMenuViewBtnClick:(NSInteger)tag;
- (void)MenuDTView:(LeftMenuView *)pView didClickMenuIndex:(NSInteger)index;
@end


@interface LeftMenuView : UIView

@property (nonatomic,weak)id <HomeMenuViewDelegate> customDelegate;

@end
