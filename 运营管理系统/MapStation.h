//
//  MapStation.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/25.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol MapStation <NSObject>

@end
@interface MapStation : JSONModel
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,strong)NSString *opCenName;
@property (nonatomic,strong)NSString *pson;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,strong)NSString *name;//站点名称

@property (nonatomic,assign)float lnt;
@property (nonatomic,assign)float lat;
@end
/*
 "phone": "13510928111",
 "opCenName": "",
 "pson": "胡峰",
 "address": "深圳市南山区朗山二路8",
 "name": "清溢光电排口",
 "lnt": "113.95073",
 "lat": "22.565195"
 */
