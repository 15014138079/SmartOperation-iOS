//
//  companyCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/25.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "companyCell.h"
#import "Masonry.h"
@implementation companyCell
{
    UILabel *_leftTitle;
    UILabel *_rightContent;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _leftTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, kSCREEN_WIDTH, 21)];
        _leftTitle.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:_leftTitle];
        _rightContent = [[UILabel alloc]init];
        _rightContent.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:_rightContent];
        [_rightContent mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(80, 21));
        }];
    }
    return self;
}
- (void)fillCellWithModel:(BranchOfcList *)model
{
    _leftTitle.text = model.branchOfcName;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
