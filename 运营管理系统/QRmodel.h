//
//  QRmodel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/10.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol QRmodel <NSObject>

@end
@interface QRmodel : JSONModel
@property (nonatomic,strong)NSString *qrTime;
@property (nonatomic,strong)NSString *qrPson;
@property (nonatomic,strong)NSString *qrDiscrib;
@property (nonatomic,assign)NSInteger state;
@end
/*

 {
 "qrTime": "2016-11-08 16:41:18",
 "qrPson": "周金磊",
 "qrDiscrib": "测试"
 }

 */