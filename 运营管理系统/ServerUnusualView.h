//
//  ServerUnusualView.h
//  XJHistory
//
//  Created by 杨毅 on 17/1/9.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ServerUnusualView;
@protocol ServerUnsualDelegate <NSObject>
- (void)ServerRefresh:(UIButton *)sender;
@end
@interface ServerUnusualView : UIView
@property (nonatomic,weak)id <ServerUnsualDelegate> delegate;
- (instancetype)initWithFrame:(CGRect)frame AndTitle:(NSString *)errorTitle;
@end
