//
//  YYBrandDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/15.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "brandModel.h"
@interface YYBrandDataModel : JSONModel
@property (nonatomic ,strong)NSArray <brandModel> *Brand;
@end
