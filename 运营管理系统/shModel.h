//
//  shModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol shModel <NSObject>

@end

@interface shModel : JSONModel
@property (nonatomic,strong)NSString *shPson;
@property (nonatomic,strong)NSString *shDiscrib;
@property (nonatomic,strong)NSString *shTime;
@end
/*
 "shPson": "admIns",
 "shDiscrib": "",
 "shTime": "2016-09-22 10:03:50"
 
 */