//
//  GZbaseModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/31.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "GZdataModel.h"
@interface GZbaseModel : JSONModel
@property (nonatomic,strong)GZdataModel *data;
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@end
/*
 data
 message = "\U68c0\U4fee\U5217\U8868\U6210\U529f";
 status = 1;
 */