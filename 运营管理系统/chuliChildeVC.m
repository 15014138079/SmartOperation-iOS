//
//  chuliChildeVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "chuliChildeVC.h"
#import "Masonry.h"
#import "ZJSwitch.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "CustomPopOverView.h"
#import "YXKit.h"
//#import "MHDatePicker.h"
#import "KKUploadPhotoCollectionViewCell.h"
#import "KKPhotoPickerManager.h"
//出库单号模型
#import "KbaseModel.h"
#import "KdataModel.h"
#import "KmtPcsModel.h"
#import "ExpenseCell.h"
#import "ExpenseModel.h"
#import "PartChooseVC.h"

#import "WorkViewController.h"
//时间选择
#import "FDAlertView.h"
#import "RBCustomDatePickerView.h"
#import "PHATextView.h"
static NSString *collectionViewCellId = @"collectionViewCellId";
static CGFloat imageSize = 80;

@interface chuliChildeVC ()<CustomPopOverViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,closeDelegate,sendTheValueDelegate,UITextViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_backView3;
    UIView *_backView4;
    UIView *_backView5;
    UIView *_backView6;
    UIView *_backView7;
    UIButton *_jbBtn;
    UIButton *_jxTypeBtn;
    NSInteger tag;
    
    //UIButton  *_stopTime;
    UITextField *_lastTimeFd;
    ZJSwitch *_swit;
    PHATextView *_result;
    UITextField *_NumberTF;//库单号
    NSInteger stat;//搜索请求状态 1成功
    NSMutableArray *_mtpcsArry;//存储存在书库单的数据
    NSMutableArray *_alldata;//tableview 数据来源
    NSMutableArray *_listarr;//上传参数 partList
    NSDictionary *_parame;//参数
    UIButton *_updata;
    NSInteger TimeTag;

}
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)NSMutableArray *gzjb_arr;
@property (nonatomic,strong)NSMutableArray *jxtype_arr;
//@property (strong, nonatomic) MHDatePicker *selectTimePicker;
@property(nonatomic, strong) UICollectionView *collectionView; //添加图片,每个cell内有一个imageView
@property(nonatomic, strong) NSMutableArray *imageArray;
@property(nonatomic ,strong)UITableView *tableview;
@property (nonatomic,strong)UIButton *starTime;
@property(nonatomic,strong)UIButton  *stopTime;
@property(nonatomic,strong)UILabel *stopTimeLab;

@end

@implementation chuliChildeVC
- (AFHTTPSessionManager *)manager{
    
    if (!_manager) {
        
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.imageArray = [NSMutableArray array];
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.GZid = delegate.GZid;
    self.username = delegate.username;
    self.userId = delegate.userId;
    self.Stime = delegate.StarTime;
    NSSLog(@"%ld",self.GZid);
    [self load];
    //获取通知中心单例对象
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
    [center addObserver:self selector:@selector(notice:) name:@"cellnotice1" object:nil];
    stat = 2;
}
- (void)load{
    
  dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    for (NSInteger i=0; i<2; ++i) {
        if (i == 0) {
            dispatch_async(globalQueue, ^{
                
                self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                [_manager POST:K_GZjb parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
                    
                } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    NSArray *gzjb = dic[@"data"][@"gz"];
                    _gzjb_arr = [[NSMutableArray alloc]init];
                    for (NSInteger i = 0; i < gzjb.count; ++i) {
                        
                        [_gzjb_arr addObject:gzjb[i][@"name"]];
                    }
                    [self initScorller];
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                }];
                
            });
        }else{
        
            dispatch_async(globalQueue, ^{
                
                self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                [_manager POST:K_JXtype parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
                    
                } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    NSArray *jxtype = dic[@"data"][@"model"];
                    _jxtype_arr = [[NSMutableArray alloc]init];
                    for (NSInteger i = 0; i < jxtype.count; ++i) {
                        
                        [_jxtype_arr addObject:jxtype[i][@"name"]];
                    }
                    [self initScorller];
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                }];
                
            });
        }

    }
}
- (void)initScorller{

    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64);
    _scrollView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    //_scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+400);
    [self.view addSubview:_scrollView];
    // 隐藏水平滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    UIView *backView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 5, kSCREEN_WIDTH, 60)];
    backView1.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView1];

    UILabel *ManTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, kSCREEN_WIDTH, 21)];
    ManTitle.adjustsFontSizeToFitWidth = YES;
    ManTitle.text = [NSString stringWithFormat:@"检修责任人: %@",self.username];
    [backView1 addSubview:ManTitle];

    UIView *backView2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(backView1.frame)+5, kSCREEN_WIDTH, 320)];
    backView2.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView2];

    //故障级别
    UILabel *GZjbTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 80, 21)];
    GZjbTitle.adjustsFontSizeToFitWidth = YES;
    GZjbTitle.text = @"故障级别:";
    [backView2 addSubview:GZjbTitle];
    _jbBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _jbBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [_jbBtn setTitle:@"选择级别" forState:UIControlStateNormal];
    [_jbBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    [_jbBtn addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    _jbBtn.tag = 1001;
    //button 标题对齐方式
    _jbBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //设置标题距离左边边距10的点
    _jbBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [backView2 addSubview:_jbBtn];
    [_jbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView2).offset(15);
        make.left.equalTo(GZjbTitle.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    //检修类型
    UILabel *JXtypeTitle = [[UILabel alloc]init];
    JXtypeTitle.text = @"检修类型:";
    JXtypeTitle.adjustsFontSizeToFitWidth = YES;
    [backView2 addSubview:JXtypeTitle];
    [JXtypeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(GZjbTitle.mas_bottom).offset(20);
        make.left.equalTo(backView2).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    _jxTypeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [_jxTypeBtn setTitle:@"选择类型" forState:UIControlStateNormal];
    [_jxTypeBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    [_jxTypeBtn addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    _jxTypeBtn.tag = 1002;
    //button 标题对齐方式
    _jxTypeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //设置标题距离左边边距10的点
    _jxTypeBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    _jxTypeBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [backView2 addSubview:_jxTypeBtn];
    [_jxTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_jbBtn.mas_bottom).offset(12);
        make.left.equalTo(JXtypeTitle.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    //开始时间
    UILabel *staTimeT = [[UILabel alloc]init];
    staTimeT.text = @"开始时间:";
    staTimeT.adjustsFontSizeToFitWidth = YES;
    [backView2 addSubview:staTimeT];
    [staTimeT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(JXtypeTitle.mas_bottom).offset(20);
        make.left.equalTo(backView2).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    //_stopTimeLab = [[UILabel alloc]init];
    //_stopTimeLab.adjustsFontSizeToFitWidth = YES;
    _starTime = [UIButton buttonWithType:UIButtonTypeSystem];
    [_starTime setTitle:@"选择时间" forState:UIControlStateNormal];
    [_starTime setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    _starTime.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [_starTime addTarget:self action:@selector(chooseTime:) forControlEvents:UIControlEventTouchUpInside];
    _starTime.tag = 1003;
    [backView2 addSubview:_starTime];
    [_starTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_jxTypeBtn.mas_bottom).offset(12);
        make.left.equalTo(staTimeT.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(180, 30));
    }];
    
    //结束时间
    UILabel *stopTimeT = [[UILabel alloc]init];
    stopTimeT.text = @"结束时间:";
    stopTimeT.adjustsFontSizeToFitWidth = YES;
    [backView2 addSubview:stopTimeT];
    [stopTimeT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(staTimeT.mas_bottom).offset(20);
        make.left.equalTo(backView2).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    _stopTimeLab = [[UILabel alloc]init];
    _stopTimeLab.adjustsFontSizeToFitWidth = YES;
    _stopTimeLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _stopTimeLab.text = @"选择时间";
    _stopTimeLab.textColor = UIColorFromRGB(0xb6b6b6);
    _stopTimeLab.font = [UIFont systemFontOfSize:15.0];
    _stopTimeLab.textAlignment = NSTextAlignmentCenter;
    [backView2 addSubview:_stopTimeLab];
    [_stopTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_starTime.mas_bottom).offset(12);
        make.left.equalTo(stopTimeT.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(180, 30));
    }];
    _stopTime = [UIButton buttonWithType:UIButtonTypeSystem];
    //[_stopTime setTitle:@"2016-09-12 12:15:01" forState:UIControlStateNormal];
    //[_stopTime setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _stopTime.tag = 1004;
    _stopTime.backgroundColor = [UIColor clearColor];
    [_stopTime addTarget:self action:@selector(chooseTime:) forControlEvents:UIControlEventTouchUpInside];
   
    [backView2 addSubview:_stopTime];
    [_stopTime mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_starTime.mas_bottom).offset(12);
//        make.left.equalTo(stopTimeT.mas_right).offset(0);
        make.center.equalTo(_stopTimeLab);
        make.size.mas_equalTo(CGSizeMake(180, 30));
    }];
    
    //持续时间
    UILabel *lastTimeT = [[UILabel alloc]init];
    lastTimeT.text = @"持续时长:";
    lastTimeT.adjustsFontSizeToFitWidth = YES;
    [backView2 addSubview:lastTimeT];
    [lastTimeT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(stopTimeT.mas_bottom).offset(20);
        make.left.equalTo(backView2).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    _lastTimeFd = [[UITextField alloc]init];
    _lastTimeFd.placeholder = @"输入时长";
    _lastTimeFd.textColor = UIColorFromRGB(0x4d4d4d);
    _lastTimeFd.borderStyle = UITextBorderStyleNone;
    _lastTimeFd.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [backView2 addSubview:_lastTimeFd];
    [_lastTimeFd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_stopTime.mas_bottom).offset(12);
        make.left.equalTo(lastTimeT.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    UILabel *Unit = [[UILabel alloc]init];
    Unit.text = @"小时";
    Unit.textColor = UIColorFromRGB(0x4d4d4d);
    Unit.adjustsFontSizeToFitWidth = YES;
    [backView2 addSubview:Unit];
    [Unit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_lastTimeFd);
        make.left.equalTo(_lastTimeFd.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(40, 21));
    }];
    //结果分析
    UILabel *resultT = [[UILabel alloc]init];
    resultT.adjustsFontSizeToFitWidth = YES;
    resultT.text = @"处理结果:";
    [backView2 addSubview:resultT];
    [resultT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastTimeT.mas_bottom).offset(20);
        make.left.equalTo(backView2).offset(10);
        make.size.mas_offset(CGSizeMake(80, 21));
    }];
    _result = [[PHATextView alloc]init];
    _result.placeholder = @"不超过100个字符";
    _result.font = [UIFont systemFontOfSize:17.0];
    _result.delegate = self;
    _result.backgroundColor = [UIColor whiteColor];
    _result.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _result.layer.borderWidth = 0.5;
    _result.layer.cornerRadius = 3;
    _result.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [backView2 addSubview:_result];
    [_result mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_lastTimeFd.mas_bottom).offset(12);
        make.left.equalTo(resultT.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-120, 80));
    }];
    
    _backView3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(backView2.frame)+5, kSCREEN_WIDTH, 40)];
    _backView3.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView3];
    
    //更换配件
    UILabel *changePart = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 120, 21)];
    changePart.adjustsFontSizeToFitWidth = YES;
    changePart.text = @"是否更换配件";
    [_backView3 addSubview:changePart];
    _swit = [[ZJSwitch alloc] initWithFrame:CGRectMake(130, 5, 60, 31)];
    _swit.backgroundColor = [UIColor clearColor];
    _swit.tintColor = [UIColor lightGrayColor];
    _swit.onText = @"是";
    _swit.offText = @"否";
    _swit.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [_swit addTarget:self action:@selector(handleSwitchEvent:) forControlEvents:UIControlEventValueChanged];
    [_backView3 addSubview:_swit];
//照片上传
    _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView3.frame)+5, kSCREEN_WIDTH, 100)];
    _backView5.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView5];

    UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
    PhotoUp.adjustsFontSizeToFitWidth = YES;
    PhotoUp.text = @"照片上传:";
    [_backView5 addSubview:PhotoUp];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(imageSize, imageSize);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    //    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
    self.collectionView = collectionView;
    [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [_backView5 addSubview:collectionView];

    
//提交取消
    _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
    _backView6.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView6];
    
    //提交取消bttuon
    _updata = [UIButton buttonWithType:UIButtonTypeSystem];
    [_updata setTitle:@"提交" forState:UIControlStateNormal];
    [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
    _updata.backgroundColor = UIColorFromRGB(0x2dd500);
    [_backView6 addSubview:_updata];
    [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView6).offset(10);
        make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
    
    UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancerl setTitle:@"取消" forState:UIControlStateNormal];
    [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    cancerl.backgroundColor = UIColorFromRGB(0xd50037);
    [_backView6 addSubview:cancerl];
    [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView6).offset(10);
        make.left.equalTo(_updata.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
    _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);
}
#pragma mark  UICollectionView数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.imageArray.count < 5) {
        return self.imageArray.count + 1;
    }
    return self.imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    KKUploadPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellId forIndexPath:indexPath];
    //添加子控件,设置布局与控件图片
    [self addAndSetSubViews:cell indexPath:indexPath];
    return cell;
}

- (void)addAndSetSubViews:(KKUploadPhotoCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    //清空子控件,解决重用问题
    NSArray *subviews = [[NSArray alloc] initWithArray:cell.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
    UIImageView *imageView = [[UIImageView alloc]init];
    [cell.contentView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    cell.tag = 11; //根据tag值设定是否可点击,11可点击,初始全部可点击
    cell.imageView = imageView;
    cell.backgroundColor = [UIColor whiteColor];
    if(indexPath.row == self.imageArray.count){
        imageView.image = [UIImage imageNamed:@"add"];
    }else{
        imageView.image = nil;
    }
    
    UIButton *cancleBtn = [[UIButton alloc]init];
    cell.cancleBtn = cancleBtn;
    [cell.contentView addSubview: cancleBtn];
    [cancleBtn setImage:[UIImage imageNamed:@"circle_delete"] forState:UIControlStateNormal];
    cancleBtn.hidden = YES;//初始将删除按钮隐藏
    cell.cancleBtn.tag = indexPath.row;
    [cell.cancleBtn addTarget:self action:@selector(cancleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.imageView.frame = CGRectMake(0, 0, imageSize, imageSize);
    cell.cancleBtn.frame = CGRectMake(0, 0, 20, 20);
    
    if (self.imageArray.count > indexPath.row) {
        if ([self.imageArray[indexPath.row] isKindOfClass:[UIImage class]]) {
            cell.imageView.image = nil;
            cell.imageView.image = self.imageArray[indexPath.row];
            cell.cancleBtn.hidden = NO;
            cell.tag = 10; //初始设置tag为11,当为10时,表示已经添加图片
        }
    }
    
}

#pragma mark  collectionView代理方法,添加照片
//点击collectionView跳转到相册
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView cellForItemAtIndexPath:indexPath].tag == 11) {
        [[KKPhotoPickerManager shareInstace] showActionSheetInView:self.view fromController:self completionBlock:^(NSMutableArray *imageArray) {
            for (int i = 0; i<imageArray.count; i++) {
                if (self.imageArray.count < 5) {
                    UIImage *image = imageArray[i];
                    [self.imageArray addObject:image]; //上传图片保存到数组
                }
            }
            [self.collectionView reloadData];
        }];
    }
    
}

#pragma mark  删除图片
- (void)cancleBtnClick:(UIButton *)sender{
    if (sender.tag < self.imageArray.count) {
        [self.imageArray removeObjectAtIndex:sender.tag];
        sender.hidden = YES;
        [self.collectionView reloadData];
    }
}

- (void)choose:(UIButton *)sender{

    tag = sender.tag;
    if (tag == 1001) {
        
        if (_gzjb_arr.count == 0) {
            
        }else{
        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, 100, 44*3) titleMenus:_gzjb_arr];
        view.containerBackgroudColor = [UIColor clearColor];
        view.layer.shadowColor = [UIColor grayColor].CGColor;
        view.layer.shadowOpacity = 1.0f;
        view.layer.shadowOffset = CGSizeMake(0,0);
        view.delegate = self;
        [view showFrom:sender alignStyle:CPAlignStyleLeft];
        }
    }else if(tag== 1002){
        
        if (_jxtype_arr.count == 0) {
            
        }else{
            
        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, 100, 44*3) titleMenus:_jxtype_arr];
        view.containerBackgroudColor = [UIColor clearColor];
        view.delegate = self;
        view.layer.shadowColor = [UIColor grayColor].CGColor;
        view.layer.shadowOpacity = 1.0f;
        view.layer.shadowOffset = CGSizeMake(0,0);
            [view showFrom:sender alignStyle:CPAlignStyleCenter];
        }
    }

}
- (void)chooseTime:(UIButton *)sender{
/*
   MHDatePicker *selectTimePicker = [[MHDatePicker alloc] init];
        selectTimePicker.isBeforeTime = YES;
        __weak typeof(self) weakSelf = self;
        [selectTimePicker didFinishSelectedDate:^(NSDate *selectedDate) {
            
            weakSelf.stopTimeLab.text = [weakSelf dateStringWithDate:selectedDate DateFormat:@"yyyy-MM-dd HH:mm:ss"];  //setTitle:[weakSelf dateStringWithDate:selectedDate DateFormat:@"yyyy-MM-dd HH:mm:ss"] forState:UIControlStateNormal];
            weakSelf.stopTimeLab.textColor = UIColorFromRGB(0x4d4d4d);

        }];
 */
    TimeTag = sender.tag;
    FDAlertView *alert = [[FDAlertView alloc] init];
    
    RBCustomDatePickerView * contentView=[[RBCustomDatePickerView alloc]init];
    contentView.delegate=self;
    contentView.frame = CGRectMake(0, 0, 320, 300);
    alert.contentView = contentView;
    [alert show];

}
-(void)getTimeToValue:(NSString *)theTimeStr
{
    if (TimeTag == 1003) {
        [_starTime setTitle:theTimeStr forState:UIControlStateNormal];
        [_starTime setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    }else{
        self.stopTimeLab.text = theTimeStr;
        self.stopTimeLab.textColor = UIColorFromRGB(0x4d4d4d);
    }
    //NSLog(@"我获取到时间了，时间是===%@",theTimeStr);
}

- (NSString *)dateStringWithDate:(NSDate *)date DateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    NSString *str = [dateFormatter stringFromDate:date];
    return str ? str : @"";
}
- (void)popOverView:(CustomPopOverView *)pView didClickMenuIndex:(NSInteger)index
{
    if (tag == 1001) {
        
        [_jbBtn setTitle:_gzjb_arr[index] forState:UIControlStateNormal];
        [_jbBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    }else{
    
        [_jxTypeBtn setTitle:_jxtype_arr[index] forState:UIControlStateNormal];
        [_jxTypeBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    }
    
    
}
- (void)handleSwitchEvent:(ZJSwitch *)sender{
    
    if (sender.isOn) {

        //是否跟换配件  是添加 配件选择  移除_backview5  _backview6 增加_backview4 ,_backview5,_backview6
        [_backView5 removeFromSuperview];
        [_backView6 removeFromSuperview];
//搜索框
        _backView4 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView3.frame)+5, kSCREEN_WIDTH, 40)];
        _backView4.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView4];

        _NumberTF = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, kSCREEN_WIDTH-70, 30)];
        _NumberTF.placeholder = @"输入库单号";
        _NumberTF.borderStyle = UITextBorderStyleNone;
        _NumberTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _NumberTF.layer.cornerRadius = 5;
        [_backView4 addSubview:_NumberTF];
        UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
        [searchBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_backView4 addSubview:searchBtn];
        [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_NumberTF.mas_right).offset(0);
            make.top.equalTo(_backView4).offset(5);
            make.size.mas_equalTo(CGSizeMake(50, 30));
        }];
        
        
        _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView4.frame)+5, kSCREEN_WIDTH, 100)];
        _backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView5];

        UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
        PhotoUp.text = @"照片上传:";
        PhotoUp.adjustsFontSizeToFitWidth = YES;
        [_backView5 addSubview:PhotoUp];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
        self.collectionView = collectionView;
        [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        [_backView5 addSubview:collectionView];
        
        
        
        _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];

        
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView6 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView6 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);
    }else{
        
        [_backView4 removeFromSuperview];
        [_backView5 removeFromSuperview];
        [_backView6 removeFromSuperview];
        //否的话  移除_backView6，_backView4 重新添加_backView5
 //上传照片
        _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView3.frame)+5, kSCREEN_WIDTH, 100)];
        _backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView5];

        UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
        PhotoUp.text = @"照片上传:";
        PhotoUp.adjustsFontSizeToFitWidth = YES;
        [_backView5 addSubview:PhotoUp];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
        self.collectionView = collectionView;
        [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        [_backView5 addSubview:collectionView];
        
        
//提交 取消
        _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];

        
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView6 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView6 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);
    }
}
//创建tableview
- (void)inittableview{
    
    [_backView4 removeFromSuperview];
    [_backView5 removeFromSuperview];
    [_backView6 removeFromSuperview];
    if (stat == 1) {
        
        self.tableview.dataSource = self;
        self.tableview.delegate = self;
        [_scrollView addSubview:self.tableview];
//        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+400+self.tableview.frame.size.height);
        //上传照片
        _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableview.frame)+5, kSCREEN_WIDTH, 100)];
        _backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView5];

        UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
        PhotoUp.text = @"照片上传:";
        PhotoUp.adjustsFontSizeToFitWidth = YES;
        [_backView5 addSubview:PhotoUp];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
        self.collectionView = collectionView;
        [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        [_backView5 addSubview:collectionView];
        
        
        //提交 取消
        _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];

        
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView6 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView6 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];

        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);
        
    }else{
        
        
        [_backView7 removeFromSuperview];
        //重新绘制tableview的高度
        CGRect rect = self.tableview.frame;
        rect.size.height = 120*_alldata.count;
        self.tableview.frame = rect;
    
        self.tableview.dataSource = self;
        self.tableview.delegate = self;
        [_scrollView addSubview:self.tableview];

//        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+400+self.tableview.frame.size.height);
        _backView7 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableview.frame)+1, kSCREEN_WIDTH, 40)];
        _backView7.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView7];

        UIButton *addPartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [addPartBtn setTitle:@"增加配件" forState:UIControlStateNormal];
        [addPartBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        [addPartBtn addTarget:self action:@selector(addView) forControlEvents:UIControlEventTouchUpInside];
        [_backView7 addSubview:addPartBtn];
        [addPartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_backView7);
            make.top.equalTo(_backView7).offset(5);
            make.size.mas_equalTo(CGSizeMake(80, 30));
        }];
        UIImageView *addImage = [[UIImageView alloc]init];
        addImage.image = [UIImage imageNamed:@"add_b"];
        [_backView7 addSubview:addImage];
        [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_backView7);
            make.right.equalTo(addPartBtn.mas_left).offset(0);
            make.size.mas_equalTo(CGSizeMake(15, 15));
        }];
        //上传照片
        _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView7.frame)+5, kSCREEN_WIDTH, 100)];
        _backView5.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView5];

        UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
        PhotoUp.text = @"照片上传:";
        PhotoUp.adjustsFontSizeToFitWidth = YES;
        [_backView5 addSubview:PhotoUp];
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(imageSize, imageSize);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //    layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
        self.collectionView = collectionView;
        [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        [_backView5 addSubview:collectionView];
        
        
        //提交 取消
        _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
        _backView6.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:_backView6];

        
        //提交取消bttuon
        _updata = [UIButton buttonWithType:UIButtonTypeSystem];
        [_updata setTitle:@"提交" forState:UIControlStateNormal];
        [_updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
        _updata.backgroundColor = UIColorFromRGB(0x2dd500);
        [_backView6 addSubview:_updata];
        [_updata mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];
        
        UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancerl setTitle:@"取消" forState:UIControlStateNormal];
        [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        cancerl.backgroundColor = UIColorFromRGB(0xd50037);
        [_backView6 addSubview:cancerl];
        [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView6).offset(10);
            make.left.equalTo(_updata.mas_right).offset(20);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
        }];

        [self.tableview reloadData];
        _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);
    }
    
}
//增加配件btn点击事件
- (void)addView{
    
    PartChooseVC *vc = [[PartChooseVC alloc]init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    
}
//反向传过来的值
//- (void)displayData:(NSDictionary *)dic
//{
//    
//    ExpenseModel *model = [[ExpenseModel alloc]init];
//    model.djnum = _NumberTF.text;
//    model.aliph = @"1";
//    //model.PJnum = [NSString stringWithFormat:@"%ld",k];
//    model.PJbianhao = dic[@"num"];
//    model.value = [dic[@"value"]integerValue];
//    model.discrib = dic[@"disc"];
//    model.unit = dic[@"unit"];
//    [_alldata addObject:model];
//    [self inittableview];
//    
//}
- (void)notice:(NSNotification *)notification{
    
    //_alldata = [[NSMutableArray alloc]init];
    NSDictionary *dic = notification.userInfo;
    ExpenseModel *model = [[ExpenseModel alloc]init];
    model.djnum = _NumberTF.text;
    model.aliph = @"1";
    model.PJbianhao = dic[@"num"];
    model.value = [dic[@"value"]integerValue];
    model.discrib = dic[@"disc"];
    model.unit = dic[@"unit"];
    [_alldata addObject:model];
    [self inittableview];
    [[NSNotificationCenter defaultCenter]removeObserver:@"cellnotice1"];
    
}
#pragma mark-->tableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _alldata.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExpenseCell *cell = [[ExpenseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    ExpenseModel *model = _alldata[indexPath.row];
    [cell fillCellWithModel:model Index:indexPath.row];
    return cell;
}
#pragma mark-->删除代理
- (void)Refresh:(NSInteger)index
{
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"你确定要删除配件%ld吗",index+1] message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [_alldata removeObjectAtIndex:index];
        
        stat = 0;
        [self inittableview];
        
        
    }];
    UIAlertAction *no=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [alert addAction:ok];
    [alert addAction:no];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)searchBtnClick{

    if (_NumberTF.text.length == 0) {
        [self showAlert:nil message:@"请输入正确的入库单号" actionWithTitle:@"确定"];
    }else{
    NSDictionary *paramet = @{MtPcsid:_NumberTF.text};
    _mtpcsArry = [[NSMutableArray alloc]init];
    _alldata = [[NSMutableArray alloc]init];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_MtPcsBynum parameters:paramet progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        stat = [dic[@"status"]integerValue];
        if (stat == 1) {
            KdataModel *model = [[KdataModel alloc]initWithDictionary:dic[@"data"] error:nil];
            for (KmtPcsModel *MtModel in model.MtPcsDetail) {
                [_mtpcsArry addObject:MtModel];
            }
            for (NSInteger i = 0; i < _mtpcsArry.count; i++) {
                KmtPcsModel *model = _mtpcsArry[i];
                ExpenseModel *model1 = [[ExpenseModel alloc]init];
                model1.djnum = _NumberTF.text;
                model1.PJnum = [NSString stringWithFormat:@"%ld",i+1];
                model1.aliph = @"0";
                model1.PJbianhao = model.logId;
                model1.value = model.numb;
                model1.discrib = model.discrib;
                model1.unit = model.unit;
                [_alldata addObject:model1];
        }
            //成功不允许开关滑动
            _swit.userInteractionEnabled = NO;
            [self inittableview];
            
        }else if (stat == 2000){
            [self showAlert:@"温馨提示" message:@"该出库单号已被使用" actionWithTitle:@"确定"];
        }else{
            _swit.userInteractionEnabled = NO;
            PartChooseVC *vc = [[PartChooseVC alloc]init];
            vc.delegate = self;
            [self.navigationController pushViewController:vc animated:YES];
            [_backView4 removeFromSuperview];
            [_backView5 removeFromSuperview];
            [_backView6 removeFromSuperview];
            _backView7 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView3.frame)+5, kSCREEN_WIDTH, 40)];
            _backView7.backgroundColor = [UIColor whiteColor];
            [_scrollView addSubview:_backView7];

            UIButton *addPartBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [addPartBtn setTitle:@"增加配件" forState:UIControlStateNormal];
            [addPartBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
            [addPartBtn addTarget:self action:@selector(addView) forControlEvents:UIControlEventTouchUpInside];
            [_backView7 addSubview:addPartBtn];
            [addPartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_backView7);
                make.top.equalTo(_backView7).offset(5);
                make.size.mas_equalTo(CGSizeMake(80, 30));
            }];
            UIImageView *addImage = [[UIImageView alloc]init];
            addImage.image = [UIImage imageNamed:@"add_b"];
            [_backView7 addSubview:addImage];
            [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(_backView7);
                make.right.equalTo(addPartBtn.mas_left).offset(0);
                make.size.mas_equalTo(CGSizeMake(15, 15));
            }];
            //上传照片
            _backView5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView7.frame)+5, kSCREEN_WIDTH, 100)];
            _backView5.backgroundColor = [UIColor whiteColor];
            [_scrollView addSubview:_backView5];

            UILabel *PhotoUp = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 21)];
            PhotoUp.text = @"照片上传:";
            [_backView5 addSubview:PhotoUp];
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
            layout.itemSize = CGSizeMake(imageSize, imageSize);
            layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            //    layout.minimumInteritemSpacing = 10;
            layout.minimumLineSpacing = 10;
            UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(85, 10, self.view.frame.size.width-90, imageSize) collectionViewLayout:layout];
            self.collectionView = collectionView;
            [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
            self.collectionView.delegate = self;
            self.collectionView.dataSource = self;
            self.collectionView.backgroundColor = [UIColor whiteColor];
            [_backView5 addSubview:collectionView];
            
            
            //提交 取消
            _backView6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_backView5.frame)+5, kSCREEN_WIDTH, 60)];
            _backView6.backgroundColor = [UIColor whiteColor];
            [_scrollView addSubview:_backView6];

            
            //提交取消bttuon
            UIButton *updata = [UIButton buttonWithType:UIButtonTypeSystem];
            [updata setTitle:@"提交" forState:UIControlStateNormal];
            [updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
            updata.backgroundColor = UIColorFromRGB(0x2dd500);
            [_backView6 addSubview:updata];
            [updata mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_backView6).offset(10);
                make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
                make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
            }];
            
            UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
            [cancerl setTitle:@"取消" forState:UIControlStateNormal];
            [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
            cancerl.backgroundColor = UIColorFromRGB(0xd50037);
            [_backView6 addSubview:cancerl];
            [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_backView6).offset(10);
                make.left.equalTo(updata.mas_right).offset(20);
                make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
            }];
            _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, CGRectGetMaxY(_backView6.frame)+50);

        }
        [self.tableview reloadData];


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
    }
    
}
- (UITableView *)tableview {
    
    if (!_tableview) {
        
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 440, kSCREEN_WIDTH, 120*_alldata.count) style:UITableViewStylePlain];
    }
    return _tableview;
}

- (void)up{
    
    [UIView animateWithDuration:0 delay:1.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _updata.userInteractionEnabled = NO;
    } completion:^(BOOL finished) {
        _updata.userInteractionEnabled = YES;
    }];
    if (_result.text.length > 50) {
        [self showAlert:@"结果分析超过限定长度" message:nil actionWithTitle:@"确定"];
    }else if (_result.text.length < 1 || [_stopTimeLab.text isEqualToString:@"选择时间"] ||[_starTime.titleLabel.text isEqualToString:@"选择时间"] || [_jbBtn.titleLabel.text isEqualToString:@"选择级别"] || [_jxTypeBtn.titleLabel.text isEqualToString:@"选择类型"] || _lastTimeFd.text.length < 1 ){
    
            [self showAlert:@"温馨提示" message:@"请完善处理信息" actionWithTitle:@"确定"];
    }
    else{
        
        /*有配件单号的参数并请求成功*/
    if (stat == 1) {
        _parame = @{userid:[NSString stringWithFormat:@"%ld",self.userId],faid:[NSString stringWithFormat:@"%ld",self.GZid],conclusion:_result.text,statTime:_starTime.titleLabel.text,endtime:_stopTimeLab.text,fLevel:_jbBtn.titleLabel.text,ftype:_jxTypeBtn.titleLabel.text,timeL:_lastTimeFd.text,PartNum:_NumberTF.text};
    }else if (stat == 2){
        /*无配件单号的参数，不跟换配件时的参数*/
            _parame = @{userid:[NSString stringWithFormat:@"%ld",self.userId],faid:[NSString stringWithFormat:@"%ld",self.GZid],conclusion:_result.text,statTime:_starTime.titleLabel.text,endtime:_stopTimeLab.text,fLevel:_jbBtn.titleLabel.text,ftype:_jxTypeBtn.titleLabel.text,timeL:_lastTimeFd.text};
    }else{
        /*无配件单号，自己创建输入单号，并提交跟换的配件信息时的参数*/
        _listarr = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i < _alldata.count; ++i) {
            ExpenseModel *model = _alldata[i];
            NSDictionary *dic = @{Code:model.PJbianhao,Values:[NSString stringWithFormat:@"%ld",model.value]};
            [_listarr addObject:dic];
        }
        _parame = @{userid:[NSString stringWithFormat:@"%ld",self.userId],faid:[NSString stringWithFormat:@"%ld",self.GZid],conclusion:_result.text,statTime:_starTime.titleLabel.text,endtime:_stopTimeLab.text,fLevel:_jbBtn.titleLabel.text,ftype:_jxTypeBtn.titleLabel.text,timeL:_lastTimeFd.text,PartNum:_NumberTF.text,PartList:_listarr};
    }
        //NSLog(@"%@",_parame);
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:K_chuli parameters:_parame constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i = 0; i<self.imageArray.count; i++) {
            
            NSData *imageData =UIImageJPEGRepresentation(self.imageArray[i], 0.1);
            NSString *fileName = [NSString  stringWithFormat:@"image%d.jpg", i];
            [formData appendPartWithFileData:imageData name:@"GZLR" fileName:fileName mimeType:@"image/jpeg"];
            
        }
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
       // NSLog(@"%@",dic);
        NSInteger status = [dic[@"status"]integerValue];
        //NSLog(@"%ld",status);
        if (status == 1) {
            //延时操作
            [UIView animateWithDuration:0 delay:2.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                
                [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
                
            } completion:^(BOOL finished) {
                
//                //将控制器压入栈，然后去栈中查找
//                for (UIViewController *controller in self.navigationController.viewControllers) {
//                    if ([controller isKindOfClass:[WorkViewController class]]) {
//                        
//                        WorkViewController *work = (WorkViewController *)controller;
//                        [self.navigationController popToViewController:work animated:YES];
//                    }
//                }
                [self.navigationController popViewControllerAnimated:YES];

            }];
            
        }else{
            [self showAlert:@"提交失败" message:dic[@"message"] actionWithTitle:@"我知道了"];
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
        NSInteger status = responses.statusCode;
        //        NSSLog(@"%ld",(long)responses.statusCode);
        if (status >=500) {
            [self showAlert:@"提交失败" message:@"服务器出错" actionWithTitle:@"我知道了"];
        }else{
            [self showAlert:@"提交失败" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
        }
        
    }];
    }
}
#pragma mark 封装提示框
-(void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)back{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"您是否确定退出当前页面" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *OkAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:cancelAction];
    [alert addAction:OkAction];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
