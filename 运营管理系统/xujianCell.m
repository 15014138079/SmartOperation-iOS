//
//  xujianCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "xujianCell.h"
#import "Masonry.h"
@implementation xujianCell
{
    UILabel *_rightCountent;
    UIView *_view;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 10)];
        _view.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self.contentView addSubview:_view];
        UIImageView *leftIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10, self.contentView.center.y-10, 22, 30)];
        leftIcon.image = [[UIImage imageNamed:@"f_icon03"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self.contentView addSubview:leftIcon];
//        [leftIcon mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(view.mas_bottom).offset(5);
//            make.left.equalTo(self.contentView).offset(10);
//            make.size.mas_equalTo(CGSizeMake(25, 24));
//        }];
        _rightCountent = [[UILabel alloc]initWithFrame:CGRectMake(45, 15, kSCREEN_WIDTH-45, 21)];
        _rightCountent.numberOfLines = 0;
        [self.contentView addSubview:_rightCountent];
//        [_rightCountent mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(view.mas_bottom).offset(8);
//            make.left.equalTo(leftIcon.mas_right).offset(10);
//            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-45, 21));
//        }];
    }
    return self;

}
- (void)fillCellWithModel:(stationModel *)model
{
    
        _rightCountent.text = model.name;

        NSDictionary *fontDic=@{NSFontAttributeName:[UIFont systemFontOfSize:17]};
        CGSize size1=CGSizeMake(kSCREEN_WIDTH-45,0);
        CGSize titleLabelSize=[model.name boundingRectWithSize:size1 options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading   attributes:fontDic context:nil].size;
        CGFloat height = titleLabelSize.height;
    
    if (height < 21.0) {
    }else{
        _rightCountent.frame = CGRectMake(45, CGRectGetMaxY(_view.frame), kSCREEN_WIDTH-45, height);
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
