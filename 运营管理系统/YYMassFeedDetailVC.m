//
//  YYMassFeedDetailVC.m
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "YYMassFeedDetailVC.h"

@interface YYMassFeedDetailVC ()
{
    UIButton *_BSmsgBtn;
    UIButton *_techniqueBtn;
    UIButton *_CLmsgBtn;
    NSInteger tag;
    FallLineView *_Fallview;
    JoinFaluterView *_JoinFalutView;
    ServerUnusualView *_ServerFView;
    CGFloat BsHeight;
    CGFloat TeHeight;
    CGFloat ClHeight;

}

@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(nonatomic,strong)UIScrollView *scrollview;
@property(nonatomic,strong)UIView *heardView;
@property(nonatomic,strong)UIView *DetailView;
@property(nonatomic,strong)BsMessageDetailView *bsView;//报送信息
@property(nonatomic,strong)TechMessageView *techniqueView;//技术支持信息
@property(nonatomic,strong)MassMsgView *CLmsgView;//品质负责人处理信息
@property(nonatomic,strong)NSMutableArray *BSmessageArr;
@property(nonatomic,strong)NSArray *imageArray;

@end

@implementation YYMassFeedDetailVC
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:NO];
}
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"质量反馈详情";
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    self.navigationItem.leftBarButtonItem = leftItem;
    NSSLog(@"%ld",self.qualityID);
    _BSmessageArr = [[NSMutableArray alloc]init];
    
    _scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
    _scrollview.backgroundColor = [UIColor whiteColor];
    _scrollview.showsVerticalScrollIndicator = NO;
    _scrollview.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_scrollview];
    _heardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 30)];
    _heardView.backgroundColor = UIColorFromRGB(0xf7f3be);
    [_scrollview addSubview:_heardView];
    UILabel *HeardTitle = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 100, 20)];
    HeardTitle.text = @"反馈流程进度:";
    HeardTitle.font = [UIFont systemFontOfSize:15.0];
    HeardTitle.textColor = UIColorFromRGB(0x4d4d4d);
    [_heardView addSubview:HeardTitle];
    UIImageView *StataImgView = [[UIImageView alloc]init];
    StataImgView.frame = CGRectMake(CGRectGetMaxX(HeardTitle.frame)+5, 10, 10, 10);
    if (self.state < 2) {
        StataImgView.image = [UIImage imageNamed:@"feedblock_y"];
    }else{
        StataImgView.image = [UIImage imageNamed:@"feedblock_g"];
    }
    [_heardView addSubview:StataImgView];
    UILabel *messageLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(StataImgView.frame)+2, 5, kSCREEN_WIDTH-165, 20)];
    if (self.state == 0) {
       messageLab.text = @"运营管理技术部支持中";
    }else if (self.state == 1){
        messageLab.text = @"品质部客诉负责人处理中";
    }else{
       messageLab.text = @"处理完成";
    }
    messageLab.textColor = UIColorFromRGB(0x4d4d4d);
    messageLab.font = [UIFont systemFontOfSize:13.0];
    [_heardView addSubview:messageLab];
    UIImageView *closeImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"feedclose_icon"]];
    closeImgView.frame = CGRectMake(CGRectGetMaxX(messageLab.frame)+5, 5, 20, 20);
    [_heardView addSubview:closeImgView];
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(CGRectGetMaxX(messageLab.frame), 0,kSCREEN_WIDTH-(kSCREEN_WIDTH-165), 30);
    [closeBtn addTarget:self action:@selector(HeardViewHiden:) forControlEvents:UIControlEventTouchUpInside];
    [_heardView addSubview:closeBtn];
    _DetailView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_heardView.frame), kSCREEN_WIDTH, 60)];
    _DetailView.backgroundColor = [UIColor redColor];
    [_scrollview addSubview:_DetailView];
    _BSmsgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _BSmsgBtn.tag = 1001;
    _BSmsgBtn.userInteractionEnabled = NO;
    _BSmsgBtn.layer.borderWidth = 1.0f;
    _BSmsgBtn.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    _BSmsgBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    _BSmsgBtn.frame = CGRectMake(0, 0, kSCREEN_WIDTH/3, 60);
    [_BSmsgBtn setTitle:@"报送信息" forState:UIControlStateNormal];
    _BSmsgBtn.backgroundColor = UIColorFromRGB(0x0093d5);
    [_BSmsgBtn addTarget:self action:@selector(ChooseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_DetailView addSubview:_BSmsgBtn];
    _techniqueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _techniqueBtn.tag = 1002;
    _techniqueBtn.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    _techniqueBtn.layer.borderWidth = 1.0f;
    _techniqueBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [_techniqueBtn setTitle:@"  运营管理部\n技术支持信息" forState:UIControlStateNormal];
    _techniqueBtn.userInteractionEnabled = NO;
    _techniqueBtn.frame = CGRectMake(CGRectGetMaxX(_BSmsgBtn.frame), 0, kSCREEN_WIDTH/3, 60);
    _techniqueBtn.backgroundColor = [UIColor whiteColor];
    [_techniqueBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    _techniqueBtn.titleLabel.lineBreakMode = 0;
    [_techniqueBtn addTarget:self action:@selector(ChooseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_DetailView addSubview:_techniqueBtn];
    
    _CLmsgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _CLmsgBtn.tag = 1003;
    _CLmsgBtn.layer.borderWidth = 1.0f;
    _CLmsgBtn.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    _CLmsgBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    _CLmsgBtn.userInteractionEnabled = NO;
    [_CLmsgBtn setTitle:@"品质部客诉负\n责人处理信息" forState:UIControlStateNormal];
    _CLmsgBtn.frame = CGRectMake(CGRectGetMaxX(_techniqueBtn.frame), 0, kSCREEN_WIDTH/3, 60);
    _CLmsgBtn.backgroundColor = [UIColor whiteColor];
    [_CLmsgBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    _CLmsgBtn.titleLabel.lineBreakMode = 0;
    [_CLmsgBtn addTarget:self action:@selector(ChooseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_DetailView addSubview:_CLmsgBtn];
    [self NetWorking];
    tag = 1001;
    
    
}

- (void)NetWorking{
    
    [_Fallview removeFromSuperview];
    [_ServerFView removeFromSuperview];
    [_JoinFalutView removeFromSuperview];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:FeedBackDtURL parameters:@{@"qualityId":[NSString stringWithFormat:@"%ld",self.qualityID]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        FeedBackBeanModel *model = [[FeedBackBeanModel alloc]initWithDictionary:dic[@"data"][@"bean"] error:nil];
            [_BSmessageArr addObject:model];

        [self initBSmsgView];
      
        if (model.state == 1) {
            _techniqueBtn.userInteractionEnabled = YES;
            [_techniqueBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
            [self initTechniqueView];
            
        }else if(model.state == 2){
            
            _techniqueBtn.userInteractionEnabled = YES;
            [_techniqueBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
            _CLmsgBtn.userInteractionEnabled = YES;
            [_CLmsgBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
            [self initTechniqueView];
            [self InitMassMsgView];
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
         NSHTTPURLResponse * responses = (NSHTTPURLResponse *)task.response;
         NSInteger status = responses.statusCode;
         if (status >= 500) {
         _ServerFView = [[ServerUnusualView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64) AndTitle:[error userInfo][@"NSLocalizedDescription"]];
         _ServerFView.delegate = self;
         [self.view addSubview:_ServerFView];
         }else if ([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"请求超时。"]) {
         _Fallview = [[FallLineView alloc]initWithFrame:CGRectMake(0,64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
         _Fallview.delegate = self;
         [self.view addSubview:_Fallview];
         }else if([[error userInfo][@"NSLocalizedDescription"]isEqualToString:@"似乎已断开与互联网的连接。"]){
         _JoinFalutView = [[JoinFaluterView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
         _JoinFalutView.delegate = self;
         [self.view addSubview:_JoinFalutView];
         }else{
         [self showAlert:@"温馨提示" message:[error userInfo][@"NSLocalizedDescription"] actionWithTitle:@"我知道了"];
         }

         
    }];
}
/*失败页面刷新代理方法*/
- (void)FallLineRefresh:(UIButton *)sender{
    [self NetWorking];
}
- (void)ServerRefresh:(UIButton *)sender{
    [self NetWorking];
}
- (void)JoinRefersh:(UIButton *)sender{
    [self NetWorking];
}

//实例化报送信息的view
- (void)initBSmsgView{

    FeedBackBeanModel *model = _BSmessageArr[0];
    NSDictionary *dic = @{@"pson":model.userN,@"tel":model.userTel,@"findtime":model.time,@"findaddress":model.stationAddress,@"custname":model.entName,@"equipNum":model.code,@"typename":model.typeName,@"equipname":model.equipName,@"equipmodel":model.equipModel,@"value":[NSString stringWithFormat:@"%ld台",model.num],@"mainGZ":model.faultDisc,@"discrb":model.discrb};

    
    self.imageArray = [model.images componentsSeparatedByString:@","];
/*
 报送信息view
 */
    _bsView = [[BsMessageDetailView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(_DetailView.frame)+5, kSCREEN_WIDTH, 300) AndDataDictionray:dic MaxY:CGRectGetMaxY(_DetailView.frame)+5 WithImageUrl:self.imageArray];
    _bsView.delegate = self;
    if (tag == 1001) {
        [self showBSmsgView];
        [self dismissTechniqueView];
        [self dismissMassMsgView];
    }else{
        [self dismissBSmsgView];
    }
    [self.scrollview addSubview:_bsView];

    
}
#pragma mark-->BsMessageDetailViewDelegate
- (void)didSelectedImageURL:(NSArray *)arr AndIndex:(NSInteger)index{
    AlbumVC *alVC = [[AlbumVC alloc]init];
    alVC.ImageUrl = arr;
    alVC.index = index;
    [self.navigationController pushViewController:alVC animated:YES];
}
- (void)returnBSMessageViewHeight:(CGFloat)height{
    
    if (tag == 1001) {
        
      self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, height+100);
    }
    
    BsHeight = height;
}
/*
 报送信息view的状态
 */
- (void)dismissBSmsgView{
    
    _bsView.alpha = 0;
}
- (void)showBSmsgView{
    
    _bsView.alpha = 1;
}
/*
    技术支持信息view
 */
- (void)initTechniqueView{

    FeedBackBeanModel *model = _BSmessageArr[0];
    NSDictionary *techDic = @{@"pson":model.artisanName,@"tel":@"12345678911",@"clmessage":model.suggestion,@"time":model.artisanTime};
    _techniqueView = [[TechMessageView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_DetailView.frame)+5, kSCREEN_WIDTH, 300) AndDiction:techDic MaxY:CGRectGetMaxY(_DetailView.frame)+5];
    _techniqueView.delegate = self;
    if (tag == 1002) {
        [self showTechniqueView];
        [self dismissMassMsgView];
        [self dismissBSmsgView];
    }else{
        [self dismissTechniqueView];
    }
    [self.scrollview addSubview:_techniqueView];

    
}
/*
 技术支持信息view的状态
 */
- (void)dismissTechniqueView{
    
    _techniqueView.alpha = 0;
}
- (void)showTechniqueView{
    _techniqueView.alpha = 1;
}
/*
 srollview contentsize跟随_techniqueView变化
 */
- (void)returnTechMessageViewHeight:(CGFloat)height{
    
    //self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, height+100);
    TeHeight = height;
}
/*
    品质部信息view
 */
- (void)InitMassMsgView{
    
    FeedBackBeanModel *model = _BSmessageArr[0];
    NSDictionary *MassDic = @{@"pson":model.sqeName,@"tel":@"12345678911",@"time":model.sqeTime,@"scheme":model.scheme,@"result":model.measures,@"causes":model.causes};
    _CLmsgView = [[MassMsgView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_DetailView.frame)+5, kSCREEN_WIDTH, 300) AndDiction:MassDic WithMaxY:CGRectGetMaxY(_DetailView.frame)+5];
    _CLmsgView.delegate = self;
    if (tag == 1003) {
        [self showMassMsgView];
        [self dismissTechniqueView];
        [self dismissBSmsgView];
    }else{
        [self dismissMassMsgView];
    }
    [self.scrollview addSubview:_CLmsgView];

}
/*
 品质部信息view的状态
 */
- (void)dismissMassMsgView{
    _CLmsgView.alpha = 0;
}
- (void)showMassMsgView{
    _CLmsgView.alpha = 1;
}
#pragma Mark-->MassMsgViewDelegate
- (void)returnMassMsgViewHeight:(CGFloat)height{
    
    //self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, height+100);
    ClHeight = height;

}

-(void)ChooseBtnClick:(UIButton *)sender{
    tag = sender.tag;
    if (sender.tag == 1001) {
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, BsHeight+100);
        [self dismissMassMsgView];
        [self dismissTechniqueView];
        [self showBSmsgView];
        FeedBackBeanModel *model = _BSmessageArr[0];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        sender.backgroundColor = UIColorFromRGB(0x0093d5);
        [_techniqueBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        _techniqueBtn.backgroundColor = [UIColor whiteColor];
        if (model.state == 2) {
            
            [_CLmsgBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
            _CLmsgBtn.backgroundColor = [UIColor whiteColor];
        }
 
    }else if(sender.tag == 1002){
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, TeHeight+100);
        [self dismissMassMsgView];
        [self dismissBSmsgView];
        [self showTechniqueView];
        FeedBackBeanModel *model = _BSmessageArr[0];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        sender.backgroundColor = UIColorFromRGB(0x0093d5);
        [_BSmsgBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        _BSmsgBtn.backgroundColor = [UIColor whiteColor];
        _BSmsgBtn.userInteractionEnabled = YES;
        if (model.state == 2) {
            
            [_CLmsgBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
            _CLmsgBtn.backgroundColor = [UIColor whiteColor];
        }

    }else if (sender.tag == 1003){
        
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, ClHeight+100);
        [self dismissTechniqueView];
        [self dismissBSmsgView];
        [self showMassMsgView];
        _CLmsgBtn.userInteractionEnabled = YES;
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        sender.backgroundColor = UIColorFromRGB(0x0093d5);
        [_BSmsgBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        _BSmsgBtn.backgroundColor = [UIColor whiteColor];
        [_techniqueBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        _techniqueBtn.backgroundColor = [UIColor whiteColor];

    
    }

}
/*
 反馈流程中的closebtn 点击事件
 */
- (void)HeardViewHiden:(UIButton *)sender{
    [_bsView removeFromSuperview];
    [_CLmsgView removeFromSuperview];
    [_techniqueView removeFromSuperview];
    _heardView.alpha = 0;
    _DetailView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 60);

    if (tag == 1001) {
        [self initBSmsgView];
        [self initTechniqueView];
        [self InitMassMsgView];
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, BsHeight+100);
    }else if (tag == 1002){
        [self initTechniqueView];
        [self initBSmsgView];
        [self InitMassMsgView];
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, TeHeight+100);
    }else{
        [self InitMassMsgView];
        [self initTechniqueView];
        [self initBSmsgView];
        self.scrollview.contentSize = CGSizeMake(kSCREEN_WIDTH, ClHeight+100);
    }

}
#pragma mark 封装提示框
-(void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
