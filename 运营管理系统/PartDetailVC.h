//
//  PartDetailVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartDetailVC : UIViewController
@property (nonatomic,strong)NSString *logId;
@property (nonatomic,assign)NSInteger number;
@property (nonatomic,strong)NSString *unit;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSString *time;
@property (nonatomic,strong)NSString *storeCode;//仓库编码
@property (nonatomic,strong)NSString *storeName;//仓库名称

@end
