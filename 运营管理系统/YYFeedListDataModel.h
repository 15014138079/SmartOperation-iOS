//
//  YYFeedListDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/8.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYFeedListBeanlistModel.h"
@interface YYFeedListDataModel : JSONModel
@property(nonatomic,strong)NSArray <YYFeedListBeanlistModel> *beanList;
@end
