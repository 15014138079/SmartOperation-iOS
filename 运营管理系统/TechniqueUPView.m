//
//  TechniqueUPView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "TechniqueUPView.h"

@implementation TechniqueUPView
- (instancetype)initWithFrame:(CGRect)frame AndMessageDic:(NSDictionary *)dic{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self initMessageView:dic];
    }
    return self;
}

- (void)initMessageView:(NSDictionary *)dictionary{
    
    UIView *back1 = [[UIView alloc]initWithFrame:CGRectMake(0, 10, kSCREEN_WIDTH, 55)];
    back1.backgroundColor = [UIColor whiteColor];
    [self addSubview:back1];
    UILabel *nameLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 5, kSCREEN_WIDTH-24, 21)];
    nameLab.text = [NSString stringWithFormat:@"填  报  人: %@",dictionary[@"userName"]];
    nameLab.textColor = UIColorFromRGB(0x4d4d4d);
    nameLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:nameLab];
    UILabel *PhoneLab = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(nameLab.frame)+5, kSCREEN_WIDTH-24, 21)];
    PhoneLab.text = [NSString stringWithFormat:@"联系方式: %@",dictionary[@"phone"]];
    PhoneLab.textColor = UIColorFromRGB(0x4d4d4d);
    PhoneLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:PhoneLab];
    UIView *back2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back1.frame)+10, kSCREEN_WIDTH, 126)];
    back2.backgroundColor = [UIColor whiteColor];
    [self addSubview:back2];
    UILabel *DiscirbLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, 70, 21)];
    DiscirbLab.text = @"处理意见:";
    DiscirbLab.textColor = UIColorFromRGB(0x4d4d4d);
    DiscirbLab.font = [UIFont systemFontOfSize:15.0];
    DiscirbLab.textAlignment = NSTextAlignmentCenter;
    [back2 addSubview:DiscirbLab];
    _DiscribTextView = [[PHATextView alloc]initWithFrame:CGRectMake(84, 3, kSCREEN_WIDTH-87, 120)];
    _DiscribTextView.placeholder = @"输入处理意见";
    _DiscribTextView.returnKeyType = UIReturnKeyDone;
    _DiscribTextView.font = [UIFont systemFontOfSize:15.0];
    //_DiscribTextView.textColor = UIColorFromRGB(0x4d4d4d);
    _DiscribTextView.layer.borderColor = UIColorFromRGB(0xf0f0f0).CGColor;
    _DiscribTextView.layer.borderWidth = 1.0f;
    _DiscribTextView.layer.cornerRadius = 3;
    [back2 addSubview:_DiscribTextView];
    
    UIView *back3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back2.frame)+10, kSCREEN_WIDTH, 46)];
    back3.backgroundColor = [UIColor whiteColor];
    [self addSubview:back3];
    UIButton  *TechUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [TechUpBtn setTitle:@"提交" forState:UIControlStateNormal];
    TechUpBtn.backgroundColor = UIColorFromRGB(0x2dd500);
    TechUpBtn.frame = CGRectMake(kSCREEN_WIDTH/6-10, 3, kSCREEN_WIDTH/3, 40);
    [TechUpBtn addTarget:self action:@selector(TechupBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back3 addSubview:TechUpBtn];
    UIButton *TechCancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [TechCancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    TechCancelBtn.backgroundColor = UIColorFromRGB(0xd50037);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    TechCancelBtn.frame = CGRectMake(CGRectGetMaxX(TechUpBtn.frame)+20, 3, kSCREEN_WIDTH/3, 40);
    [TechCancelBtn addTarget:self action:@selector(TechrevokeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back3 addSubview:TechCancelBtn];

}
- (void)TechupBtnClick:(UIButton *)sender{
    [self.delegate TechUpBtnClick:sender];
}
- (void)TechrevokeBtnClick:(UIButton *)sender{
    [self.delegate TechRevokeClick:sender];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
