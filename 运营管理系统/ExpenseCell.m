//
//  ExpenseCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "ExpenseCell.h"
#import "Masonry.h"
@implementation ExpenseCell
{
    UILabel *_kdNum;
    UILabel *_partT;
    UILabel *_partNum;
    UILabel *_PJnumber;
    UILabel *_PJdiscreb;
    UIButton *_closePart;//删除按键
    NSInteger choseIndex;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *KD = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 60, 21)];
        KD.text = @"出库单";
        KD.adjustsFontSizeToFitWidth = YES;
        KD.textColor = UIColorFromRGB(0x4d4d4d);
        [self.contentView addSubview:KD];

        _kdNum = [[UILabel alloc]init];
        _kdNum.adjustsFontSizeToFitWidth = YES;
        _kdNum.textColor = UIColorFromRGB(0x4d4d4d);
        _kdNum.textAlignment = NSTextAlignmentRight;
        _kdNum.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:_kdNum];
        [_kdNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(0);
            make.top.equalTo(self.contentView).offset(5);
            make.size.mas_equalTo(CGSizeMake(140,21));
        }];
        UILabel *kdNumTitle = [[UILabel alloc]init];
        kdNumTitle.adjustsFontSizeToFitWidth = YES;
        kdNumTitle.textColor = UIColorFromRGB(0x4d4d4d);
        kdNumTitle.text = @"单据号:";
        kdNumTitle.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:kdNumTitle];
        [kdNumTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(5);
            make.right.equalTo(_kdNum.mas_left).offset(0);
            make.size.mas_equalTo(CGSizeMake(50, 21));
        }];
        //配件
        _partT = [[UILabel alloc]init];
        _partT.adjustsFontSizeToFitWidth = YES;
        _partT.textColor = UIColorFromRGB(0x0093d5);
        [self.contentView addSubview:_partT];
        [_partT mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(KD.mas_bottom).offset(5);
            make.left.equalTo(self.contentView).offset(10);
            make.size.mas_equalTo(CGSizeMake(60, 21));
        }];
        //配件编号
        _partNum = [[UILabel alloc]init];
        _partNum.adjustsFontSizeToFitWidth = YES;
        _partNum.textColor = UIColorFromRGB(0x4d4d4d);
        _partNum.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:_partNum];
        [_partNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_partT.mas_bottom).offset(5);
            make.left.equalTo(self.contentView).offset(10);
            make.size.mas_equalTo(CGSizeMake(140, 21));
        }];
        //只有手动添加才能删除
        _closePart = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closePart setImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
        [_closePart addTarget:self action:@selector(closePartClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_closePart];
        [_closePart mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_partT);
            make.right.equalTo(self.contentView.mas_right).offset(-10);
            make.size.mas_equalTo(CGSizeMake(20, 11));
        }];

        //配件数量
        _PJnumber = [[UILabel alloc]init];
        _PJnumber.adjustsFontSizeToFitWidth = YES;
        _PJnumber.textColor = UIColorFromRGB(0x0093d5);
        _PJnumber.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_PJnumber];
        [_PJnumber mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_partT.mas_bottom).offset(5);
            make.right.equalTo(self.contentView.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(150, 21));
        }];
        //配件描述
        _PJdiscreb = [[UILabel alloc]init];
        _PJdiscreb.adjustsFontSizeToFitWidth = YES;
        //PJdiscreb.text = model.discrib;
        _PJdiscreb.textColor = UIColorFromRGB(0xbbbbbb);
        _PJdiscreb.font = [UIFont systemFontOfSize:12.0];
        _PJdiscreb.numberOfLines = 0;
        [self.contentView addSubview:_PJdiscreb];
        [_PJdiscreb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_partNum.mas_bottom).offset(5);
            make.left.equalTo(self.contentView).offset(10);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH-20, 30));
        }];

    }
    return self;
}
- (void)fillCellWithModel:(ExpenseModel *)model Index:(NSInteger)index
{
    _kdNum.text = model.djnum;
    _partT.text = [NSString stringWithFormat:@"配件%ld",index+1];
    _partNum.text = model.PJbianhao;
    _PJnumber.text = [NSString stringWithFormat:@"x%ld%c%@%c",model.value,'(',model.unit,')'];
    _PJdiscreb.text = model.discrib;
    _closePart.alpha = [model.aliph integerValue];
    choseIndex = index;
    
}
- (void)closePartClick:(UIButton *)sender{
    
    [self.delegate Refresh:choseIndex];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
