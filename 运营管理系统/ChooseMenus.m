//
//  ChooseMenus.m
//  多级菜单
//
//  Created by 杨毅 on 16/12/1.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "ChooseMenus.h"

#define KTableViewMaxHeight 300
@implementation WSIndexPath

+ (instancetype)twIndexPathWithRow:(NSInteger)row
                                 item:(NSInteger)item
                                 rank:(NSInteger)rank{
    
    WSIndexPath *indexPath = [[self alloc] initWithRow:row item:item rank:rank];
    
    return indexPath;
}


- (instancetype)initWithRow:(NSInteger )row
                          item:(NSInteger )item
                          rank:(NSInteger )rank{
    
    if (self = [super init]) {
        
        //self.column = column;
        self.row = row;
        self.item = item;
        self.rank = rank;
        
    }
    
    return self;
}


@end
@interface ChooseMenus()<UITableViewDelegate,UITableViewDataSource>
{
//    NSArray *typeNames;
//    NSArray *typeIDs;
    NSInteger DataCount;
    NSInteger _currSelectRow;
    NSInteger _currSelectItem;
    NSInteger _currSelectRank;
    NSArray *_oneArr;
    NSArray *_TowArr;
    NSArray *_ThreeArr;
    NSInteger typeID;
}
@property (nonatomic,strong) UITableView *leftTableView;
@property (nonatomic,strong) UITableView *leftTableView1;
@property (nonatomic,strong) UITableView *leftTableView2;
@property (nonatomic,strong) UIView *View;
//@property (nonatomic,strong) UIView *View2;
//@property (nonatomic,strong) UIView *View3;
@end

@implementation ChooseMenus
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        //self.backgroundColor = [UIColor redColor];
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor grayColor].CGColor;
        UIImageView *LeftImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 21, 21)];
        LeftImage.image = [[UIImage imageNamed:@"icon0001_b"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self addSubview:LeftImage];
        UILabel *titel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 120, 21)];
        titel.text = @"故障类型选择";
        titel.textColor = UIColorFromRGB(0x4d4d4d);
        titel.textAlignment = NSTextAlignmentLeft;
        titel.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:titel];
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 43, self.frame.size.width, 1)];
        line.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line];
        self.CancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.CancelBtn.frame = CGRectMake(self.frame.size.width-80, 2, 80, 40);
        [self.CancelBtn setTitle:@"确定" forState:UIControlStateNormal];
        [self.CancelBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.CancelBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [self.CancelBtn addTarget:self action:@selector(CancelChooseView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.CancelBtn];
        [self _initialize];
        [self initView1];
    }
    return self;
}
//确定按钮代理方法
- (void)CancelChooseView:(UIButton *)sender
{
    [self.dataSource CancelChooseView:sender];
}
- (void)_initialize{
    
    //_currSelectColumn = 0;
    _currSelectItem = WSNoFound;
    _currSelectRank = WSNoFound;
    _currSelectRow = WSNoFound;
    //_isLeftOpen = NO;
    // _isRightOpen = NO;
}
- (void)initView1{
    
    [self addSubview:self.View];
    [self.View addSubview:self.leftTableView];
    [self.View addSubview:self.leftTableView1];
    [self.View addSubview:self.leftTableView2];

}
- (UIView *)View{
    if (!_View) {
        _View = [[UIView alloc]initWithFrame:CGRectMake(0, 44.0, self.frame.size.width, self.frame.size.height-44.0)];
        _View.backgroundColor = [UIColor whiteColor];
    }
    return _View;
}
- (UITableView *)leftTableView{
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        [_leftTableView registerClass:[UITableViewCell class]forCellReuseIdentifier:@"cell"];
        _leftTableView.frame = CGRectMake(0, 0, self.View.frame.size.width/3, self.View.frame.size.height);
        _leftTableView.tableFooterView = [[UIView alloc]init];
    }
    return _leftTableView;
}
- (UITableView *)leftTableView1{
    if (!_leftTableView1) {
        _leftTableView1 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView1.delegate = self;
        _leftTableView1.dataSource = self;
        _leftTableView1.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0];
        [_leftTableView1 registerClass:[UITableViewCell class]forCellReuseIdentifier:@"cell"];
        _leftTableView1.frame = CGRectMake(self.View.frame.size.width/3, 0, self.View.frame.size.width/3, self.View.frame.size.height);
        _leftTableView1.tableFooterView = [[UIView alloc]init];
    }
    return _leftTableView1;
}
- (UITableView *)leftTableView2{

    if (!_leftTableView2) {
        _leftTableView2 = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView2.delegate = self;
        _leftTableView2.dataSource = self;
        _leftTableView2.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1.0];
        [_leftTableView2 registerClass:[UITableViewCell class]forCellReuseIdentifier:@"cell"];
        _leftTableView2.frame = CGRectMake(self.View.frame.size.width/3 * 2, 0, self.View.frame.size.width/3, self.View.frame.size.height);
        _leftTableView2.tableFooterView = [[UIView alloc]init];
    }
    return _leftTableView2;
}
/*
#pragma mark -- tableViews Change -
- (void)_hiddenLeftTableViews{

    self.leftTableView.frame = CGRectMake(self.leftTableView.frame.origin.x, self.leftTableView.frame.origin.y, self.leftTableView.frame.size.width, 0);
    self.leftTableView1.frame = CGRectMake(self.leftTableView1.frame.origin.x, self.leftTableView1.frame.origin.y, self.leftTableView1.frame.size.width, 0);
    self.leftTableView2.frame = CGRectMake(self.leftTableView2.frame.origin.x, self.leftTableView2.frame.origin.y, self.leftTableView2.frame.size.width, 0);
    


    
    
}

- (void)_showLeftTableViews{
    
    self.leftTableView.frame = CGRectMake(self.leftTableView.frame.origin.x, self.leftTableView.frame.origin.y, self.leftTableView.frame.size.width, KTableViewMaxHeight);
    self.leftTableView1.frame = CGRectMake(self.leftTableView1.frame.origin.x, self.leftTableView1.frame.origin.y, self.leftTableView1.frame.size.width, KTableViewMaxHeight);
    self.leftTableView2.frame = CGRectMake(self.leftTableView2.frame.origin.x, self.leftTableView2.frame.origin.y, self.leftTableView2.frame.size.width, KTableViewMaxHeight);
    
}
*/
- (void)typeIDOne:(NSArray *)typeIDOne{
    //NSLog(@"one%@",typeIDOne);
    _oneArr = typeIDOne;
}
- (void)typeIDTow:(NSArray *)typeIDTow{
    //NSLog(@"tow%@",typeIDTow);
    _TowArr = typeIDTow;
}
- (void)typeIDThree:(NSArray *)typeIDThree{
    //NSLog(@"three%@",typeIDThree);
    _ThreeArr = typeIDThree;
}
- (void)reloadLeftTableView
{
    [self.leftTableView reloadData];
}


#pragma mark -- DataSource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    WSIndexPath *twIndexPath =[self _getTwIndexPathForNumWithtableView:tableView];
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(dropMenuView:numberWithIndexPath:)]) {
        
         DataCount =  [self.dataSource dropMenuView:self numberWithIndexPath:twIndexPath];
//        if (twIndexPath.column == 1) {
//            _rightHeight = count * 44.0;
//        }
 //       NSLog(@"%ld",count);
        return DataCount;
    }else{
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    WSIndexPath *twIndexPath = [self _getTwIndexPathForCellWithTableView:tableView indexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectedBackgroundView = [[UIView alloc] init];
    cell.selectedBackgroundView.backgroundColor =  [UIColor colorWithRed:233/255.0 green:233/255.0 blue:233/255.0 alpha:1.0];
    cell.textLabel.textColor = [UIColor colorWithWhite:0.004 alpha:1.000];
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    cell.textLabel.numberOfLines = 0;
    cell.backgroundColor = [UIColor clearColor];
    //被选中cell中text颜色
    cell.textLabel.highlightedTextColor = UIColorFromRGB(0x066db8);
    //    [UIColor colorWithRed:233/255.0 green:233/255.0 blue:233/255.0 alpha:1.0];
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(dropMenuView:titleWithIndexPath:)]) {
        
        cell.textLabel.text =  [self.dataSource dropMenuView:self titleWithIndexPath:twIndexPath];
        
    }else{
        
        cell.textLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    }
    
    
    return cell;
    
}


#pragma mark - tableView delegate -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    //[self _changeTopButton:cell.textLabel.text];
    
    if (tableView == self.leftTableView) {
        _currSelectRow = indexPath.row;
        _currSelectItem = WSNoFound;
        _currSelectRank = WSNoFound;
        //根据被点击的index取出当前对应ID
        typeID = [_oneArr[_currSelectRow]integerValue];
        
        [self.leftTableView1 reloadData];
        [self.leftTableView2 reloadData];
    }
    if (tableView == self.leftTableView1) {
        _currSelectRank = WSNoFound;
        _currSelectItem = indexPath.row;
        [self.leftTableView2 reloadData];
        //根据被点击的index取出当前对应ID
        typeID = [_TowArr[_currSelectRow][_currSelectItem]integerValue];
        
        
    }
    
    
    
    if (self.leftTableView2 == tableView) {
        
        //根据被点击的index取出当前对应ID
        typeID = [_ThreeArr[_currSelectRow][_currSelectItem][indexPath.row]integerValue];
        
        //[self bgAction:nil];
        //[self _hiddenLeftTableViews];

        
        
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (self.block !=nil) {
        
        self.block(cell.textLabel.text,typeID);
    }

    
    
}



- (WSIndexPath *)_getTwIndexPathForNumWithtableView:(UITableView *)tableView{
    
    
    if (tableView == self.leftTableView) {
        
        return  [WSIndexPath twIndexPathWithRow:WSNoFound item:WSNoFound rank:WSNoFound];
        
    }
    
    if (tableView == self.leftTableView1 && _currSelectRow != WSNoFound) {
        
        
        return [WSIndexPath twIndexPathWithRow:_currSelectRow item:WSNoFound rank:WSNoFound];
    }
    
    if (tableView == self.leftTableView2 && _currSelectRow != WSNoFound && _currSelectItem != WSNoFound) {
        return [WSIndexPath twIndexPathWithRow:_currSelectRow item:_currSelectItem  rank:WSNoFound];
    }
    

    
    
    return  0;
}

- (WSIndexPath *)_getTwIndexPathForCellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    
    if (tableView == self.leftTableView) {
        
        return  [WSIndexPath twIndexPathWithRow:indexPath.row item:WSNoFound rank:WSNoFound];
        
    }
    
    if (tableView == self.leftTableView1) {

        
        return [WSIndexPath twIndexPathWithRow:_currSelectRow item:indexPath.row rank:WSNoFound];
    }
    
    if (tableView == self.leftTableView2) {
        return [WSIndexPath twIndexPathWithRow:_currSelectRow item:_currSelectItem  rank:indexPath.row];
    }
    

    
    
    return  [WSIndexPath twIndexPathWithRow:indexPath.row item:WSNoFound rank:WSNoFound];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
}
- (void)TitleBlock:(ReturnBlock)block{
    
    self.block = block;
}
@end
