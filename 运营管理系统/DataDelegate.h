//
//  DataDelegate.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/24.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataDelegate <NSObject>

//委托指定对象将注册界面内的输入的用户名进行显示
- (void)displayData:(NSDictionary *)dic;
@end
