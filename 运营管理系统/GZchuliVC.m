//
//  GZchuliVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "GZchuliVC.h"
#import "chuliChildeVC.h"
#import "MassagerVC.h"
@interface GZchuliVC ()
{
    chuliChildeVC *_chuliVC;
    MassagerVC *_MVC;
    UIButton *_btn1;
    UIButton *_btn2;
}
@end

@implementation GZchuliVC

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    //设置tabbarItem
    
    UIImageView *backView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    backView.image = [[UIImage imageNamed:@"bg-1"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];//bg-1
    backView.userInteractionEnabled = YES;
    self.navigationItem.titleView = backView;
    self.view.backgroundColor = [UIColor whiteColor];
    UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:@[@"处理",@"信息"]];
    segment.frame = CGRectMake((kSCREEN_WIDTH-240)/2, 10, 120, 30);
    segment.tintColor = [UIColor whiteColor];
    segment.selectedSegmentIndex = 0;
    [segment addTarget:self action:@selector(handelSegementControlAction:)forControlEvents:(UIControlEventValueChanged)];
    self.navigationItem.titleView = segment;

//    _btn1 = [UIButton buttonWithType:UIButtonTypeCustom /*自定义按钮,会去除按钮原有的样式*/];
//    _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    UIImage *check1 = [[UIImage imageNamed:@"left_w"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIImage *uncheck1 = [[UIImage imageNamed:@"left_b"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    UIImage *check2 = [[UIImage imageNamed:@"right_w"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIImage *uncheck2 = [[UIImage imageNamed:@"right_b"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    //背景图片
//    [_btn1 setBackgroundImage:uncheck1 forState:UIControlStateNormal];
//    [_btn2 setBackgroundImage:uncheck2 forState:UIControlStateNormal];
//    
//    //实现选择
//    //1.设置UIControlStateSelected状态
//    //UIControlStateSelected 指的是按钮被选中
//    [_btn1 setBackgroundImage:check1 forState:UIControlStateSelected];
//    [_btn2 setBackgroundImage:check2 forState:UIControlStateSelected];
//    //2.给按钮添加事件处理,在处理函数中改变selected状态
//    [_btn1 addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
//    [_btn2 addTarget:self action:@selector(selectedChange:) forControlEvents:UIControlEventTouchUpInside];
//    
//    
//    _btn1.frame = CGRectMake(0, 0, 60, 30);
//    _btn2.frame = CGRectMake(60, 0, 60, 30);
//    _btn1.tag = 1000;
//    _btn2.tag = 1001;
//    _btn1.selected = YES;
//    [backView addSubview:_btn1];
//    [backView addSubview:_btn2];
    _chuliVC = [[chuliChildeVC alloc]init];
    _MVC = [[MassagerVC alloc]init];
    [self.view addSubview:_chuliVC.view];
    [self addChildViewController:_chuliVC];

}
- (void)handelSegementControlAction:(UISegmentedControl *)segment{
    switch (segment.selectedSegmentIndex) {
        case 0:
            [self.view addSubview:_chuliVC.view];
            [self addChildViewController:_chuliVC];
            break;
        case 1:
            [self.view addSubview:_MVC.view];
            [self addChildViewController:_MVC];
            break;
        default:
            break;
    }
}
//- (void)selectedChange:(UIButton *)sender{
//    
//    _btn1.selected = NO;
//    _btn2.selected = NO;
//    sender.selected = YES;
//    switch (sender.tag) {
//        case 1000:
//            [self.view addSubview:_chuliVC.view];
//            [self addChildViewController:_chuliVC];
//            [_MVC.view removeFromSuperview];
//            
//            break;
//        case 1001:
//            [self.view addSubview:_MVC.view];
//            [self addChildViewController:_MVC];
//            [_chuliVC.view removeFromSuperview];
//            
//            break;
//            
//        default:
//            break;
//    }
//}
- (void)back{

    [self.navigationController popViewControllerAnimated:YES];
}

@end
