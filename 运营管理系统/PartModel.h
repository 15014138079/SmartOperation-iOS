//
//  PartModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/29.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "PartDataModel.h"
@interface PartModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)PartDataModel *data;
@end
