//
//  SearchViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/23.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "AlarmTableViewCell.h"

#import "AFNetworking.h"
#import "CJCalendarViewController.h"
#import "TroubleDetailVC.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
//弹出选择框
#import "MHActionSheet.h"
#import "JoinFaluterView.h"
#import "FallLineView.h"
#import "ServerUnusualView.h"
#import "NothingDataView.h"
#import "YYAlarmDataModel.h"
#import "ChooseMenus.h"
#import "TypeDataModel.h"
@interface SearchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CalendarViewControllerDelegate,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate,NothingViewDelegate,ChooseMenusDataSource>
@property (nonatomic,assign)NSInteger userId;
@end
