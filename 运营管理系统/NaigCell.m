//
//  NaigCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/11/8.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "NaigCell.h"
#import "Masonry.h"
@implementation NaigCell
{
    UILabel *_name;
    UILabel *_distance;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initView];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)initView{
    _name = [[UILabel alloc]initWithFrame:CGRectMake(10, 12, kSCREEN_WIDTH/3*2, 20)];
    _name.textColor = UIColorFromRGB(0x4d4d4d);
    _name.font = [UIFont systemFontOfSize:15.0];
    _name.numberOfLines = 0;
    [self.contentView addSubview:_name];
    _distance = [[UILabel alloc]init];
    _distance.textColor = UIColorFromRGB(0x0093d5);
    _distance.font = [UIFont systemFontOfSize:15.0];
    _distance.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_distance];
    [_distance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_name);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
}
- (void)fillCellWithModel:(NaigModel *)model
{
    _name.text = model.title;
    _distance.text = [NSString stringWithFormat:@"%.2fKM",model.distance];
//        NSDictionary *fontDic=@{NSFontAttributeName:[UIFont systemFontOfSize:18]};
//        CGSize size1=CGSizeMake(kSCREEN_WIDTH,0);
//        CGSize titleLabelSize=[model.title boundingRectWithSize:size1 options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading   attributes:fontDic context:nil].size;
//    
//    
//    
//        if (titleLabelSize.height < 40) {
//            
//            
//        }else{ //重新绘制高度
//
//    
//            //标题重新设置
//            _name.frame=CGRectMake(10,0,kSCREEN_WIDTH/2, titleLabelSize.height);
//        }


}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
