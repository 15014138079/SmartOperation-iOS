//
//  dataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "stationModel.h"
@interface dataModel : JSONModel
@property (nonatomic,strong)NSArray<stationModel> *Stationlist;
@end
