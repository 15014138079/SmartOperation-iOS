//
//  ExpandDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/30.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "XJhisPsonListModel.h"
@interface ExpandDataModel : JSONModel
@property (nonatomic, assign, getter=isExpend) BOOL expand;
@property (nonatomic,strong)NSArray <XJhisPsonListModel> *beanList;
@end
