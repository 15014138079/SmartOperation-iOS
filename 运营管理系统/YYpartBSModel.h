//
//  YYpartBSModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYpartDataModel.h"
@interface YYpartBSModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)YYpartDataModel *data;
@end
