//
//  YYUpPsonBeanList.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYUpPsonBeanList <NSObject>

@end
@interface YYUpPsonBeanList : JSONModel
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)NSString *userN;
@property (nonatomic,strong)NSString *postName;
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,strong)NSString *tel;
@end
/*
 {"id":1386,"level":7,"tel":"13919269796","name":"沙立雨","levelName":"运营主任"}
 */
