//
//  XYhistroyVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XYhistroyVC.h"
#import "XJhistoryListVC.h"
#import "XJStationDetailVC.h"
@interface XYhistroyVC ()
{
    XJhistoryListVC *_ListVC;
    XJStationDetailVC *_StationMessageVC;
}
@end

@implementation XYhistroyVC
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UISegmentedControl *seg = [[UISegmentedControl alloc]initWithItems:@[@"巡检历史",@"站点详情"]];
    seg.frame = CGRectMake(0, 0, 100, 30);
    seg.tintColor = [UIColor whiteColor];
    [seg addTarget:self action:@selector(nextVC:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = seg;
    seg.selectedSegmentIndex = 0;
    _ListVC = [[XJhistoryListVC alloc]init];
    _ListVC.userID = self.userID;
    _ListVC.stationId = self.stationId;
    _StationMessageVC = [[XJStationDetailVC alloc]init];
    _StationMessageVC.MessageArray = self.stationMessageArray;

    [self.view addSubview:_ListVC.view];
    [self addChildViewController:_ListVC];

}
-(void)nextVC:(UISegmentedControl *)sender{
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.view addSubview:_ListVC.view];
            [self addChildViewController:_ListVC];
            [_StationMessageVC.view removeFromSuperview];
            break;
        case 1:
            [self.view addSubview:_StationMessageVC.view];
            [self addChildViewController:_StationMessageVC];
            [_ListVC.view removeFromSuperview];
            
            break;

        default:
            break;
    }
    
}
- (void)backVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
