//
//  PartCompanyCell.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/14.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYbeanListModel.h"
@interface PartCompanyCell : UITableViewCell
- (void)fillCellWithModel:(YYbeanListModel *)model;
@end
