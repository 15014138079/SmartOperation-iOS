//
//  YYFeedMessageReportVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/10.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TechniqueUPView.h"
#import "CharacterUPView.h"
#import "AFNetworking.h"
#import "YXKit.h"
#import "MBProgressHUD.h"
#import "FeedBackBeanModel.h"
#import "AlbumVC.h"
#import "BsMessageDetailView.h"
#import "TechMessageView.h"
#import "MassMsgView.h"
#import "FallLineView.h"//连接超时
#import "JoinFaluterView.h"//连接失败
#import "ServerUnusualView.h"//服务器出错
@interface YYFeedMessageReportVC : UIViewController<TechniqueUPViewDelegate,CharacterUPViewDelegate,BsMessageDetailViewDelegate,TechMessageViewDelegate,JoinFalutDelegate,ServerUnsualDelegate,FallLineDelegate>
@property (nonatomic,assign)NSInteger state;
@property (nonatomic,assign)NSInteger feedBackID;//反馈信息id
@end
