//
//  MassMessageView.m
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "MassMessageView.h"

static NSString *collectionViewCellId = @"collectionViewCellId";
static CGFloat imageSize = 80;
@implementation MassMessageView
{
    NSInteger values;
}
- (instancetype)initWithFrame:(CGRect)frame AndDataDictionary:(NSDictionary *)dataDic
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        //NSSLog(@"%@",dataDic);
        self.imageArray = [NSMutableArray array];
        [self initView:dataDic];
    }
    return self;
}
- (void)initView:(NSDictionary *)dic{
    //section1
    UIView *back1 = [[UIView alloc]initWithFrame:CGRectMake(0, 5, kSCREEN_WIDTH, 55)];
    back1.backgroundColor = [UIColor whiteColor];
    [self addSubview:back1];
    UILabel *nameLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 5, kSCREEN_WIDTH-24, 21)];
    nameLab.text = [NSString stringWithFormat:@"填  报  人: %@",dic[@"pson"]];
    nameLab.textColor = UIColorFromRGB(0x4d4d4d);
    nameLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:nameLab];
    UILabel *PhoneLab = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(nameLab.frame)+5, kSCREEN_WIDTH-24, 21)];
    PhoneLab.text = [NSString stringWithFormat:@"联系方式: %@",dic[@"tel"]];
    PhoneLab.textColor = UIColorFromRGB(0x4d4d4d);
    PhoneLab.font = [UIFont systemFontOfSize:15.0];
    [back1 addSubview:PhoneLab];
    //section2
    UIView *back2 = [[UIView alloc]init];
    back2.backgroundColor = [UIColor whiteColor];
    [self addSubview:back2];
    _FinderTimeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _FinderTimeBtn.frame = CGRectMake(84, 3, kSCREEN_WIDTH-87, 21);
    _FinderTimeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_FinderTimeBtn setTitle:@"选择时间" forState:UIControlStateNormal];
    [_FinderTimeBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    _FinderTimeBtn.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _FinderTimeBtn.layer.cornerRadius = 2;
    _FinderTimeBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [_FinderTimeBtn addTarget:self action:@selector(finderTimeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back2 addSubview:_FinderTimeBtn];
    UILabel *FindTimeTtitle = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, 70, 21)];
    FindTimeTtitle.text = @"发生时间:";
    FindTimeTtitle.textColor = UIColorFromRGB(0x4d4d4d);
    FindTimeTtitle.font = [UIFont systemFontOfSize:15.0];
    [back2 addSubview:FindTimeTtitle];
    UILabel *FindAddress = [[UILabel alloc]initWithFrame:CGRectZero];
    FindAddress.text = dic[@"adderss"];
    FindAddress.textColor = UIColorFromRGB(0x4d4d4d);
    FindAddress.backgroundColor = UIColorFromRGB(0xf0f0f0);
    FindAddress.layer.cornerRadius = 2;
    FindAddress.numberOfLines = 0;
    FindAddress.font = [UIFont systemFontOfSize:15.0];
    CGRect rect1 = [FindAddress.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-87, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:FindAddress.font,NSFontAttributeName, nil] context:nil];
    FindAddress.frame = CGRectMake(84, CGRectGetMaxY(_FinderTimeBtn.frame)+3, kSCREEN_WIDTH-87, rect1.size.height);
    [back2 addSubview:FindAddress];
    UILabel *FindAddressT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(FindTimeTtitle.frame)+3, 70, 21)];
    FindAddressT.text = @"发生地点:";
    FindAddressT.textColor = UIColorFromRGB(0x4d4d4d);
    FindAddressT.font = [UIFont systemFontOfSize:15.0];
    [back2 addSubview:FindAddressT];
    UILabel *CustomerName = [[UILabel alloc]initWithFrame:CGRectZero];
    CustomerName.text = dic[@"custmeN"];
    CustomerName.textColor = UIColorFromRGB(0x4d4d4d);
    CustomerName.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CustomerName.layer.cornerRadius = 2;
    CustomerName.numberOfLines = 0;
    CustomerName.font = [UIFont systemFontOfSize:15.0];
    CGRect rect = [CustomerName.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-87, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:CustomerName.font,NSFontAttributeName, nil] context:nil];
    CustomerName.frame = CGRectMake(84, CGRectGetMaxY(FindAddress.frame)+3, kSCREEN_WIDTH-87, rect.size.height);
    [back2 addSubview:CustomerName];
    UILabel *CustomerNameT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(FindAddress.frame)+3, 70, 21)];
    CustomerNameT.text = @"客户名称:";
    CustomerNameT.textColor = UIColorFromRGB(0x4d4d4d);
    CustomerNameT.font = [UIFont systemFontOfSize:15.0];
    [back2 addSubview:CustomerNameT];
    back2.frame = CGRectMake(0, CGRectGetMaxY(back1.frame)+5, kSCREEN_WIDTH, CGRectGetMaxY(CustomerName.frame)+3);
    
    //section3
    UIView *back3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back2.frame)+5, kSCREEN_WIDTH, 130)];
    back3.backgroundColor = [UIColor whiteColor];
    [self addSubview:back3];
    UILabel *ProductNameT = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, 70, 21)];
    ProductNameT.text = @"产品名称:";
    ProductNameT.textColor = UIColorFromRGB(0x4d4d4d);
    ProductNameT.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:ProductNameT];
    _ChooseProductBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    _ChooseProductBTN.titleLabel.font = [UIFont systemFontOfSize:15.0];
    _ChooseProductBTN.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _ChooseProductBTN.frame = CGRectMake(84, 3, kSCREEN_WIDTH-87, 21);
    [_ChooseProductBTN setTitle:@"选择产品" forState:UIControlStateNormal];
    [_ChooseProductBTN setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    [_ChooseProductBTN addTarget:self action:@selector(chooseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back3 addSubview:_ChooseProductBTN];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(84, CGRectGetMaxY(_ChooseProductBTN.frame)+1, kSCREEN_WIDTH-87, 1)];
    line.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [back3 addSubview:line];
    UILabel *EquipmentNumT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(ProductNameT.frame)+3, 70, 21)];
    EquipmentNumT.text = @"设备编号:";
    EquipmentNumT.textColor = UIColorFromRGB(0x4d4d4d);
    EquipmentNumT.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:EquipmentNumT];
    _EquipeNum = [[UILabel alloc]initWithFrame:CGRectMake(84, CGRectGetMaxY(_ChooseProductBTN.frame)+3, kSCREEN_WIDTH-87, 21)];
    //_EquipeNum.text = @"123456789";
    _EquipeNum.font = [UIFont systemFontOfSize:15.0];
    _EquipeNum.textColor = UIColorFromRGB(0x4d4d4d);
    _EquipeNum.backgroundColor = UIColorFromRGB(0xf0f0f0);
    //EquipeNum.layer.cornerRadius = 2;
    [back3 addSubview:_EquipeNum];
    UILabel *MonitorTypeT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(EquipmentNumT.frame)+3, 70, 21)];
    MonitorTypeT.text = @"监测类型:";
    MonitorTypeT.textColor = UIColorFromRGB(0x4d4d4d);
    MonitorTypeT.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:MonitorTypeT];
    _MonitorType = [[UILabel alloc]initWithFrame:CGRectMake(84, CGRectGetMaxY(_EquipeNum.frame)+3, kSCREEN_WIDTH-87, 21)];
    //_MonitorType.text = @"xxx监测";
    _MonitorType.textColor = UIColorFromRGB(0x4d4d4d);
    _MonitorType.font = [UIFont systemFontOfSize:15.0];
    _MonitorType.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [back3 addSubview:_MonitorType];
    UILabel *ProductModelT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(MonitorTypeT.frame)+3, 70, 21)];
    ProductModelT.text = @"产品型号:";
    ProductModelT.textColor = UIColorFromRGB(0x4d4d4d);
    ProductModelT.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:ProductModelT];
    _ProductModel = [[UILabel alloc]initWithFrame:CGRectMake(84, CGRectGetMaxY(_MonitorType.frame)+3, kSCREEN_WIDTH-87, 21)];
    //_ProductModel.text = @"xxx型号";
    _ProductModel.textColor = UIColorFromRGB(0x4d4d4d);
    _ProductModel.font = [UIFont systemFontOfSize:15.0];
    _ProductModel.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [back3 addSubview:_ProductModel];
    UILabel *MaintainNumT = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(ProductModelT.frame)+3, 70, 21)];
    MaintainNumT.text = @"维修数量:";
    MaintainNumT.textColor = UIColorFromRGB(0x4d4d4d);
    MaintainNumT.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:MaintainNumT];
    UIButton *cutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cutBtn.frame = CGRectMake(CGRectGetMaxX(MaintainNumT.frame)+5, CGRectGetMaxY(_ProductModel.frame)+5, 20, 20);
    [cutBtn setImage:[UIImage imageNamed:@"decrease_normal@2x"] forState:UIControlStateNormal];
//    [cutBtn setTitle:@"-" forState:UIControlStateNormal];
//    [cutBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
    [cutBtn addTarget:self action:@selector(subtractClick) forControlEvents:UIControlEventTouchUpInside];
//    cutBtn.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
//    cutBtn.layer.borderWidth = 1.0f;
    [back3 addSubview:cutBtn];
    _NumberTF = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(cutBtn.frame)+3, CGRectGetMaxY(_ProductModel.frame)+2, 40, 25)];
    _NumberTF.enabled = NO;
    _NumberTF.text = @"1";
    _NumberTF.textColor = UIColorFromRGB(0x0093d5);
    _NumberTF.textAlignment = NSTextAlignmentCenter;
    _NumberTF.layer.borderWidth = 1.0f;
    _NumberTF.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
    _NumberTF.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:_NumberTF];
    UILabel *UnitLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_NumberTF.frame), CGRectGetMaxY(_ProductModel.frame)+5, 20, 21)];
    UnitLab.text = @"台";
    UnitLab.textColor = UIColorFromRGB(0x4d4d4d);
    UnitLab.textAlignment = NSTextAlignmentCenter;
    UnitLab.font = [UIFont systemFontOfSize:15.0];
    [back3 addSubview:UnitLab];
    
    UIButton *AddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    AddBtn.frame = CGRectMake(CGRectGetMaxX(UnitLab.frame), CGRectGetMaxY(_ProductModel.frame)+5, 20, 20);
    [AddBtn setImage:[UIImage imageNamed:@"increase_highlight@2x"] forState:UIControlStateNormal];
//    [AddBtn setTitle:@"+" forState:UIControlStateNormal];
//    [AddBtn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
    [AddBtn addTarget:self action:@selector(addClick) forControlEvents:UIControlEventTouchUpInside];
//    AddBtn.layer.borderColor = UIColorFromRGB(0x0093d5).CGColor;
//    AddBtn.layer.borderWidth = 1.0f;
    [back3 addSubview:AddBtn];
    
    //section4
    UIView *back4 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back3.frame)+5, kSCREEN_WIDTH, 30)];
    back4.backgroundColor = [UIColor whiteColor];
    [self addSubview:back4];
    UILabel *MainGzT = [[UILabel alloc]initWithFrame:CGRectMake(12, 4, 70, 21)];
    MainGzT.text = @"主要故障:";
    MainGzT.textColor = UIColorFromRGB(0x4d4d4d);
    MainGzT.font = [UIFont systemFontOfSize:15.0];
    MainGzT.textAlignment = NSTextAlignmentCenter;
    [back4 addSubview:MainGzT];
    _GuzhangTF = [[UITextField alloc]initWithFrame:CGRectMake(84, 4, kSCREEN_WIDTH-87, 21)];
    _GuzhangTF.placeholder = @"小于50个字符";
    _GuzhangTF.textColor = UIColorFromRGB(0x4d4d4d);
    _GuzhangTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _GuzhangTF.font = [UIFont systemFontOfSize:15.0];
    [back4 addSubview:_GuzhangTF];
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(84, CGRectGetMaxY(_GuzhangTF.frame)+1, kSCREEN_WIDTH-87, 1)];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [back4 addSubview:line2];
    
    //section5
    UIView *back5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back4.frame), kSCREEN_WIDTH, 66)];
    back5.backgroundColor = [UIColor whiteColor];
    [self addSubview:back5];
    UILabel *DiscirbLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, 70, 21)];
    DiscirbLab.text = @"事件描述:";
    DiscirbLab.textColor = UIColorFromRGB(0x4d4d4d);
    DiscirbLab.font = [UIFont systemFontOfSize:15.0];
    DiscirbLab.textAlignment = NSTextAlignmentCenter;
    [back5 addSubview:DiscirbLab];
    _DiscribTextView = [[PHATextView alloc]initWithFrame:CGRectMake(84, 3, kSCREEN_WIDTH-87, 60)];
    _DiscribTextView.placeholder = @"输入事件描述信息";
    _DiscribTextView.returnKeyType = UIReturnKeyDone;
    _DiscribTextView.font = [UIFont systemFontOfSize:15.0];
    //_DiscribTextView.textColor = UIColorFromRGB(0x4d4d4d);
    _DiscribTextView.layer.borderColor = UIColorFromRGB(0xf0f0f0).CGColor;
    _DiscribTextView.layer.borderWidth = 1.0f;
    _DiscribTextView.layer.cornerRadius = 3;
    [back5 addSubview:_DiscribTextView];
    
    //section6
    UIView *back6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back5.frame), kSCREEN_WIDTH, 130)];
    back6.backgroundColor = [UIColor whiteColor];
    [self addSubview:back6];
    UILabel *BadPhotoLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 3, 70, 21)];
    BadPhotoLab.text = @"不良图片:";
    BadPhotoLab.textColor = UIColorFromRGB(0x4d4d4d);
    BadPhotoLab.font = [UIFont systemFontOfSize:15.0];
    BadPhotoLab.textAlignment = NSTextAlignmentCenter;
    [back6 addSubview:BadPhotoLab];
//    UIImageView *PhotoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"add"]];
//    PhotoView.frame = CGRectMake(12, CGRectGetMaxY(BadPhotoLab.frame)+3, 60, 60);
//    [back6 addSubview:PhotoView];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(imageSize, imageSize);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    layout.minimumLineSpacing = 10;
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(BadPhotoLab.frame), self.frame.size.width-22, imageSize + 20) collectionViewLayout:layout];
    self.collectionView = collectionView;
    [self.collectionView registerClass:[KKUploadPhotoCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellId];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [back6 addSubview:collectionView];
    
    //section7
    UIView *back7 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(back6.frame)+5, kSCREEN_WIDTH, 46)];
    back7.backgroundColor = [UIColor whiteColor];
    [self addSubview:back7];
    _UpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_UpBtn setTitle:@"提交" forState:UIControlStateNormal];
    _UpBtn.backgroundColor = UIColorFromRGB(0x2dd500);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    _UpBtn.frame = CGRectMake(kSCREEN_WIDTH/6-10, 3, kSCREEN_WIDTH/3, 40);
    [_UpBtn addTarget:self action:@selector(UpBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back7 addSubview:_UpBtn];
    UIButton *CancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [CancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    CancelBtn.backgroundColor = UIColorFromRGB(0xd50037);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    CancelBtn.frame = CGRectMake(CGRectGetMaxX(_UpBtn.frame)+20, 3, kSCREEN_WIDTH/3, 40);
    [CancelBtn addTarget:self action:@selector(revokeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [back7 addSubview:CancelBtn];
}
//时间选择
- (void)finderTimeBtnClick:(UIButton *)sender{
    
    [self.delegate FinderTimeChoose:sender];
}
//产品选择
- (void)chooseBtnClick:(UIButton *)sender{
    
    [self.delegate ProductChoose:sender];
}
- (void)subtractClick{
    
    if ([_NumberTF.text isEqualToString:@"1"]) {
        values = 1;
        _NumberTF.text = [NSString stringWithFormat:@"%ld",values];
    }else{
        values = values-1;
        _NumberTF.text = [NSString stringWithFormat:@"%ld",values];
    }
}
- (void)addClick{
    values++;
    _NumberTF.text = [NSString stringWithFormat:@"%ld",values];
}
#pragma mark  UICollectionView数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.imageArray.count < 5) {
        return self.imageArray.count + 1;
    }
    return self.imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    KKUploadPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellId forIndexPath:indexPath];
    //添加子控件,设置布局与控件图片
    [self addAndSetSubViews:cell indexPath:indexPath];
    return cell;
}

- (void)addAndSetSubViews:(KKUploadPhotoCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    [self.delegate MasViewAddAndSetSubViews:cell indexPath:indexPath];
    /*
    //清空子控件,解决重用问题
    NSArray *subviews = [[NSArray alloc] initWithArray:cell.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
    UIImageView *imageView = [[UIImageView alloc]init];
    [cell.contentView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    cell.tag = 11; //根据tag值设定是否可点击,11可点击,初始全部可点击
    cell.imageView = imageView;
    cell.backgroundColor = [UIColor whiteColor];
    if(indexPath.row == self.imageArray.count){
        imageView.image = [UIImage imageNamed:@"add"];
    }else{
        imageView.image = nil;
    }
    
    UIButton *cancleBtn = [[UIButton alloc]init];
    cell.cancleBtn = cancleBtn;
    [cell.contentView addSubview: cancleBtn];
    [cancleBtn setImage:[UIImage imageNamed:@"circle_delete"] forState:UIControlStateNormal];
    cancleBtn.hidden = YES;//初始将删除按钮隐藏
    cell.cancleBtn.tag = indexPath.row;
    [cell.cancleBtn addTarget:self action:@selector(cancleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.imageView.frame = CGRectMake(0, 0, imageSize, imageSize);
    cell.cancleBtn.frame = CGRectMake(60, 0, 20, 20);
    
    if (self.imageArray.count > indexPath.row) {
        if ([self.imageArray[indexPath.row] isKindOfClass:[UIImage class]]) {
            cell.imageView.image = nil;
            cell.imageView.image = self.imageArray[indexPath.row];
            cell.cancleBtn.hidden = NO;
            cell.tag = 10; //初始设置tag为11,当为10时,表示已经添加图片
        }
    }*/
    
}

#pragma mark  collectionView代理方法,添加照片
//点击collectionView跳转到相册
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate MasViewCollectionView:collectionView didSelectItemAtIndexPath:indexPath];
    /*
    if ([collectionView cellForItemAtIndexPath:indexPath].tag == 11) {
        [[KKPhotoPickerManager shareInstace] showActionSheetInView:self fromController:self completionBlock:^(NSMutableArray *imageArray) {
            for (int i = 0; i<imageArray.count; i++) {
                if (self.imageArray.count < 5) {
                    UIImage *image = imageArray[i];
                    [self.imageArray addObject:image]; //上传图片保存到数组
                }
            }
            [self.collectionView reloadData];
        }];
    }*/
    
}
- (void)UpBtnClick:(UIButton *)sender{
    [self.delegate upBtnClick:sender];
}
- (void)revokeBtnClick:(UIButton *)sender{
    [self.delegate RevokeClick:sender];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
