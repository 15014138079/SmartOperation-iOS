//
//  YYEquipListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/11.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol YYEquipListModel <NSObject>

@end
@interface YYEquipListModel : JSONModel
@property (nonatomic,strong)NSString *equipmodel;
@property (nonatomic,assign)float percent;
@property (nonatomic,assign)NSInteger value;
@end
/*
 "model": "CODmax",
 "percent": 0.667,
 "value": 2
 */
