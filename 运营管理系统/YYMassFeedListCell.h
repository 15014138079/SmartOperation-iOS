//
//  YYMassFeedListCell.h
//  jkljlkjlk
//
//  Created by 杨毅 on 17/1/18.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYFeedListBeanlistModel.h"

@interface YYMassFeedListCell : UITableViewCell
- (void)fillCellWithModel:(YYFeedListBeanlistModel *)model;
@end
