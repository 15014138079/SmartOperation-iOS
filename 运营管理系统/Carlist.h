//
//  Carlist.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Carlist : NSObject
@property(nonatomic,strong)NSString *Brand;//车辆品牌
@property(nonatomic,strong)NSString *BuyTime;//购买日期
@property(nonatomic,strong)NSString *plateNum;//车牌号
@property(nonatomic,assign)NSInteger carID;
@property(nonatomic,strong)NSString *Cmodel;
@property(nonatomic,strong)NSString *repName;//申请人
@end
