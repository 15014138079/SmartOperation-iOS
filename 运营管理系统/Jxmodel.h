//
//  Jxmodel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/10.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol Jxmodel <NSObject>

@end
@interface Jxmodel : JSONModel
@property (nonatomic,strong)NSString *fuc;
@property (nonatomic,assign)NSInteger state;
@property (nonatomic,strong)NSString *checktime;
@property (nonatomic,strong)NSString *HDpson;
@end
/*
 "fuc": "啊考虑考虑",
 "state": null,
 "checktime": "2016-11-08 00:00:00",
 "HDpson": "周金磊"
 */