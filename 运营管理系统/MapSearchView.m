//
//  MapSearchView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/1/6.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "MapSearchView.h"
#import "AFNetworking.h"
#import "MJRefresh.h"
#import "YYmapDatamodel.h"
#import "psonCell.h"
@interface MapSearchView()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *_StationDataSource;
    NSInteger ID;
    UITextField *_searchTF;
    NSDictionary *_paramet;
}
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@end
@implementation MapSearchView
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[self imageResize:[UIImage imageNamed:@"back_02"] andResizeTo:self.frame.size]];
        _StationDataSource = [[NSMutableArray alloc]init];
        UIView *backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 45)];
        backview.backgroundColor = [UIColor whiteColor];
        [self addSubview:backview];
        _searchTF = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, kSCREEN_WIDTH-80, 25)];
        _searchTF.backgroundColor = UIColorFromRGB(0xf0f0f0);
        _searchTF.placeholder = @"🔍搜索";
        [backview addSubview:_searchTF];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(CGRectGetMaxX(_searchTF.frame)+10, 10, 50, 25);
        [btn setTitle:@"搜索" forState:UIControlStateNormal];
        [btn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(SearClick:) forControlEvents:UIControlEventTouchUpInside];
        [backview addSubview:btn];
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 46, kSCREEN_WIDTH, self.frame.size.height-46) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.tableFooterView = [[UIView alloc]init];
        [self addSubview:_tableView];
        //_paramet = @{@"userId":@1407,@"pageNum":@0,@"keyword":_searchTF.text};
        
    }
    return self;
}
- (void)SearClick:(UIButton *)sender{
    [_StationDataSource removeAllObjects];
    _paramet = @{@"userId":@1470,@"pageNum":@0,@"keyword":_searchTF.text};
    //NSSLog(@"%@",_paramet);
    [self networking];
}
- (void)networking{
    
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:@"http://192.168.2.52:8080/OMMS/servlet/queryPsonByUserId.do" parameters:_paramet progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        YYmapDatamodel *datamodel = [[YYmapDatamodel alloc]initWithDictionary:dic[@"data"] error:nil];
        for (YYmapPsonListModel *model in datamodel.beanList) {
            
            [_StationDataSource addObject:model];
        }
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 44.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _StationDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

        psonCell *cell = [[psonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = UIColorFromRGB(0xf0f0f0);
            YYmapPsonListModel *model = _StationDataSource[indexPath.row];
            [cell fillCellWithModel:model];
    
        return cell;

    

}

-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
