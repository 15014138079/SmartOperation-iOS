//
//  partCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/13.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "partCell.h"

@implementation partCell
{
    __weak IBOutlet UILabel *logId;
    __weak IBOutlet UILabel *tm;
    __weak IBOutlet UILabel *discrib;
    
}
- (void)fillCellWithModel:(MslistModel *)model
{
    logId.text = model.logId;
    tm.text = [NSString stringWithFormat:@"%ld%c%@%c",model.numb,'(',model.unit,')'];
    discrib.text = model.discrib;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
