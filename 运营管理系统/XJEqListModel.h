//
//  XJEqListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/3.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol XJEqListModel <NSObject>
@end
@interface XJEqListModel : JSONModel
@property(nonatomic,assign)NSInteger equipId;
@property(nonatomic,strong)NSString *equipName;
@property(nonatomic,assign)NSInteger entId;
@property(nonatomic,strong)NSString *entName;
@property(nonatomic,strong)NSString *code;
@property(nonatomic,strong)NSString *stationType;
@property(nonatomic,strong)NSString *stationAddress;
@property(nonatomic,strong)NSString *equipmodel;
@end
/*
 "equipId":10881,
 "equipName":"YX-GIR(SO2,NO,O2)",
 "entId":3020,
 "entName":"焦作煤业（集团）中马建材有限责任公司",
 "code":"",
 "stationType":"废气监测",
 "stationAddress":"焦作市马村区跃进西街300号（中马村矿院内）",
 "equipmodel":"Amtax Compact"
 */
