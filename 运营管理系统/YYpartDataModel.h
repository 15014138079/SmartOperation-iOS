//
//  YYpartDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYbeanListModel.h"
@interface YYpartDataModel : JSONModel
@property (nonatomic,strong)NSArray <YYbeanListModel> *beanList;
@end
