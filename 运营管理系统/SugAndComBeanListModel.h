//
//  SugAndComBeanListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/21.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol SugAndComBeanListModel <NSObject>

@end

@interface SugAndComBeanListModel : JSONModel
@property (nonatomic,assign)NSInteger smtId;
@property (nonatomic,assign)NSInteger ID;
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *time;
@property (nonatomic,strong)NSString *smtPs;
@property (nonatomic,strong)NSString *branchName;
@property (nonatomic,assign)NSInteger anonymity;
@property (nonatomic,assign)NSInteger supportState;
@property (nonatomic,strong)NSString *suggestion;
@property (nonatomic,assign)NSInteger supportNum;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,assign)NSInteger aspectOn;

@end
/*
 "smtId": 1101,
 "id": 2,
 "title": "测试结果一下",
 "time": "2017-02-21 11:18:23",
 "smtPs": "郑博",
 "branchName": "甘肃分公司",
 "anonymity": 0,
 "state": 1,
 "suggestion": "图图看看咯咯哦哦哦哦",
 "supportNum": 0,
 "type": 2,
 "aspectOn": 1

 */
