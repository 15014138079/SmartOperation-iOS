//
//  CarfenPei.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarfenPei.h"
#import "UIViewController+LewPopupViewController.h"
#import "LewPopupViewAnimationFade.h"
#import "LewPopupViewAnimationSlide.h"
#import "LewPopupViewAnimationSpring.h"
#import "LewPopupViewAnimationDrop.h"
#import "Masonry.h"
#import "YXKit.h"


@interface CarfenPei ()

@property (nonatomic, strong) NSArray *titleMenus;

@end
@implementation CarfenPei



- (instancetype)initWithBounds:(CGRect)bounds titleMenus:(NSArray *)titles
{
    self = [super initWithFrame:bounds];
    if (self) {
        // Initialization code
        [[NSBundle mainBundle] loadNibNamed:[[self class] description] owner:self options:nil];
        _innerView.frame = bounds;
        [self addSubview:_innerView];

        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[[UIImage imageNamed:@"close"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];

        btn.frame = CGRectMake(_innerView.frame.size.width-30,20, 20, 20);
        [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        UIImageView *LeftIcon = [[UIImageView alloc]initWithFrame:CGRectMake(15, 50, 32, 24)];
        LeftIcon.image = [[UIImage imageNamed:@"g_car_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self addSubview:LeftIcon];
        UILabel *title = [[UILabel alloc]init];
        title.text = @"车辆分配";
        title.font = [UIFont systemFontOfSize:21.0];
        title.textColor = UIColorFromRGB(0x4d4d4d);
        [self addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(55);
            make.left.equalTo(LeftIcon.mas_right).offset(10);
            make.size.mas_equalTo(CGSizeMake(100, 21));
        }];
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(LeftIcon.mas_bottom).offset(5);
            make.left.equalTo(self).offset(20);
            make.size.mas_equalTo(CGSizeMake(self.frame.size.width-40, 1));
        }];
        NSArray *leftTitle = @[@"车辆品牌:",@"车辆型号:",@"车牌号码:",@"购买时间:",@"     负责人:"];
        self.titleMenus = titles;
        for (NSInteger i = 0; i < leftTitle.count; ++i) {
            UILabel *leftT = [[UILabel alloc]initWithFrame:CGRectMake(30,120+i*(21 + 15) , 90, 21)];
            leftT.text = leftTitle[i];
            leftT.textColor = UIColorFromRGB(0x4d4d4d);
            leftT.font = [UIFont systemFontOfSize:19.0];
            [self addSubview:leftT];
            if (i == 2) {
                
                _textTF = [[UITextField alloc]initWithFrame:CGRectMake(120,120+i*(21 + 15) ,self.frame.size.width/5*3-30, 21)];
                _textTF.font = [UIFont systemFontOfSize:17.0];
                _textTF.borderStyle = UITextBorderStyleNone;
                _textTF.placeholder = @"输入车牌号码";
                [self addSubview:_textTF];
                UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(120, 141+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
                line1.backgroundColor = UIColorFromRGB(0x4d4d4d);
                [self addSubview:line1];
                UILabel *xing1 = [[UILabel alloc]init];
                xing1.text = @"*";
                xing1.textColor = [UIColor redColor];
                [self addSubview:xing1];
                [xing1 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(line1.mas_top).offset(-30);
                    make.left.equalTo(_textTF.mas_right).offset(3);
                    make.size.mas_equalTo(CGSizeMake(10, 10));
                        }];

            }else if(i==0){
                _btn1 = [UIButton buttonWithType:UIButtonTypeSystem];
                _btn1.frame = CGRectMake(120,120+i*(21 + 15) ,self.frame.size.width/5*3-30, 21);
                [_btn1 setTitle:@"选择车辆品牌" forState:UIControlStateNormal];
                [_btn1 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
                
                _btn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                _btn1.titleLabel.font = [UIFont systemFontOfSize:17.0];
                [_btn1 addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
                        [self addSubview:_btn1];
                UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(120, 141+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
                line1.backgroundColor = UIColorFromRGB(0x4d4d4d);
                [self addSubview:line1];
                UILabel *xing2 = [[UILabel alloc]init];
                xing2.text = @"*";
                xing2.textColor = [UIColor redColor];
                [self addSubview:xing2];
                [xing2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(line1.mas_top).offset(-30);
                    make.left.equalTo(_btn1.mas_right).offset(3);
                    make.size.mas_equalTo(CGSizeMake(10, 10));
                }];
                _btn1.tag = 1001;
            }else if (i == 1){
            
                _btn2 = [UIButton buttonWithType:UIButtonTypeSystem];
                _btn2.frame = CGRectMake(120,120+i*(21 + 15) ,self.frame.size.width/5*3-30, 21);
                [_btn2 setTitle:@"选择车辆型号" forState:UIControlStateNormal];
                [_btn2 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
                
                _btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                _btn2.titleLabel.font = [UIFont systemFontOfSize:17.0];
                [_btn2 addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:_btn2];
                UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(120, 141+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
                line1.backgroundColor = UIColorFromRGB(0x4d4d4d);
                [self addSubview:line1];
                UILabel *xing2 = [[UILabel alloc]init];
                xing2.text = @"*";
                xing2.textColor = [UIColor redColor];
                [self addSubview:xing2];
                [xing2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(line1.mas_top).offset(-30);
                    make.left.equalTo(_btn2.mas_right).offset(3);
                    make.size.mas_equalTo(CGSizeMake(10, 10));
                }];
                _btn2.tag = 1002;
            }else if(i == 3){
                _btn3 = [UIButton buttonWithType:UIButtonTypeSystem];
                _btn3.frame = CGRectMake(120,120+i*(21 + 15) ,self.frame.size.width/5*3-30, 21);
                [_btn3 setTitle:@"选择购买时间" forState:UIControlStateNormal];
                [_btn3 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
                
                _btn3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                _btn3.titleLabel.font = [UIFont systemFontOfSize:17.0];
                [_btn3 addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:_btn3];
                UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(120, 141+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
                line1.backgroundColor = UIColorFromRGB(0x4d4d4d);
                [self addSubview:line1];
                UILabel *xing2 = [[UILabel alloc]init];
                xing2.text = @"*";
                xing2.textColor = [UIColor redColor];
                [self addSubview:xing2];
                [xing2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(line1.mas_top).offset(-30);
                    make.left.equalTo(_btn3.mas_right).offset(3);
                    make.size.mas_equalTo(CGSizeMake(10, 10));
                }];
                _btn3.tag = 1003;
            
            } else{
                UILabel *rightC = [[UILabel alloc]initWithFrame:CGRectMake(120,120+i*(21 + 15) ,self.frame.size.width/5*3, 21)];
                rightC.text = self.titleMenus[i];
                rightC.textColor = UIColorFromRGB(0xb6b6b6);
                rightC.font = [UIFont systemFontOfSize:19.0];
                [self addSubview:rightC];
                UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(120, 141+i*(21 + 15), self.frame.size.width/5*3-30, 1)];
                line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
                [self addSubview:line1];
            }

            
        }

        /*保存*/
        UIButton *preserve = [UIButton buttonWithType:UIButtonTypeSystem];
        preserve.frame = CGRectMake(self.frame.size.width/6, 320, self.frame.size.width/3*2, 40);
        [preserve setTitle:@"提交" forState:UIControlStateNormal];
        [preserve setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        preserve.backgroundColor = UIColorFromRGB(0x2dd500);
        preserve.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [preserve addTarget:self action:@selector(updata) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:preserve];

        
    }
    return self;
}


+ (instancetype)defaultPopupView{
    
    return [[self alloc]init];
}

- (void)choose:(UIButton *)sender{
    
    [self.delegate choose:sender];

    
}

- (void)updata{
    
    NSString *CarPai = _textTF.text;
    NSString *PayTime = _btn3.titleLabel.text;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:CarPai forKey:@"carpaiN"];
    [user setObject:PayTime forKey:@"buyTime"];

    [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交完成" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];

    
}
- (void)dismiss{
    LewPopupViewAnimationSlide *animation = [[LewPopupViewAnimationSlide alloc]init];
    animation.type = LewPopupViewAnimationSlideTypeTopBottom;
    [_parentVC lew_dismissPopupViewWithanimation:animation];
}

@end
