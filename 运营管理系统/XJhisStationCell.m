//
//  XJhisStationCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJhisStationCell.h"

@implementation XJhisStationCell
{
    __weak IBOutlet UILabel *StationName;
    
    __weak IBOutlet UILabel *PrincipalName;
    __weak IBOutlet UILabel *StationAddress;
}
- (void)fillCellWithModel:(XJhisStationListModel *)model
{
    
    StationName.text = model.stationName;
    PrincipalName.text = [NSString stringWithFormat:@"负责人:%@",model.principalName];
    StationAddress.text = [NSString stringWithFormat:@"站点位置:%@",model.stationAddress];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    StationName.lineBreakMode = NSLineBreakByTruncatingMiddle;
    StationAddress.lineBreakMode = NSLineBreakByTruncatingMiddle;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
