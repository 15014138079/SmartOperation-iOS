//
//  YYbeanListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYbeanListModel <NSObject>

@end
@interface YYbeanListModel : JSONModel
@property (nonatomic,assign)NSInteger branchId;
@property (nonatomic,strong)NSString *storeName;
@property (nonatomic,strong)NSString *principalName;
@property (nonatomic,strong)NSString *branchName;
@property (nonatomic,strong)NSString *principalPhone;
@property (nonatomic,strong)NSString *storeCode;

@end
/*
 "storeName": "湖南（长沙）仓",
 "principalName": "何文俊",
 "branchId": 4,
 "branchName": "湖南分公司",
 "principalPhone": "15200385503",
 "storeCode": "SYXY120"
 */
