//
//  CarExtendVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/10.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarExtendVC.h"
#import "AFNetworking.h"
#import "CustomPopOverView.h"
#import "Masonry.h"
#import "CustomPopOverView.h"
#import "AppDelegate.h"
#import "YXKit.h"
@interface CarExtendVC ()<CustomPopOverViewDelegate>
{
    UIScrollView *_scorllView;
    NSMutableArray *_carName;
    NSMutableArray *_carBand;
    NSMutableArray *_vehicleModel_arr;
    NSInteger tag;
    NSInteger indexPath;
    UIButton *_btn1;
    UIButton *_btn2;
    UITextView *_textView;
}
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@end

@implementation CarExtendVC
- (AFHTTPSessionManager *)manager{
    
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"车辆申用";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    //获取注册界面保存的数据
    self.userId = delegate.userId;
    self.userN = delegate.username;
    _carName = [[NSMutableArray alloc]init];
    _carBand = [[NSMutableArray alloc]init];
    _vehicleModel_arr = [[NSMutableArray alloc]init];//车辆列表中的车辆型号数组
}
/*
- (void)load{

    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_car parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *carList = dic[@"data"][@"carlist"];
        for (NSInteger i = 0; i<carList.count; ++i) {
            
            [_carName addObject:carList[i][@"name"]];
            [_vehicleModel_arr addObject:carList[i][@"vehicleModel"]];

        }

        for (NSInteger j = 0; j < _vehicleModel_arr.count; j++) {
            
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            [arr addObjectsFromArray:_vehicleModel_arr[j]];
            
            NSMutableArray *carBand1 = [[NSMutableArray alloc]init];
            
            for (NSInteger i = 0; i < arr.count; i++) {
                
                [carBand1 addObject:arr[i][@"name"]];
            }
            [_carBand addObject:carBand1];
        }
        [self initScorllView];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}*/
- (void)initScorllView{
    
    _scorllView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-100)];
    _scorllView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _scorllView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+100);
    // 隐藏水平滚动条
    _scorllView.showsVerticalScrollIndicator = YES;
    _scorllView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_scorllView];
    UIView *backView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 15, kSCREEN_WIDTH, 80)];
    backView1.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:backView1];
    NSArray *arr1 = @[@"申请人:",@"部    门:"];
    NSArray *arr2 = @[self.userN,@"运营管理部"];
    for (NSInteger i = 0; i < arr1.count; ++i) {
        
        UILabel *letfT = [[UILabel alloc]initWithFrame:CGRectMake(15, 20 + i * (21 + 10), 80, 21)];
        letfT.text = arr1[i];
        letfT.textColor = UIColorFromRGB(0x4d4d4d);
        [backView1 addSubview:letfT];
        UILabel *rightContent = [[UILabel alloc]initWithFrame:CGRectMake(100, 20 + i * (21 + 10), 100, 21)];
        rightContent.textColor = UIColorFromRGB(0x4d4d4d);
        rightContent.text = arr2[i];
        [backView1 addSubview:rightContent];
    }
    UIView *backView2 = [[UIView alloc]init];
    backView2.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView1.mas_bottom).offset(15);
        make.left.equalTo(_scorllView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 85));
    }];
    NSArray *leftT_arr = @[@"车辆品牌:",@"车辆型号:"];

    for (NSInteger i = 0; i < leftT_arr.count; ++i) {
        
        UILabel *leftT = [[UILabel alloc]initWithFrame:CGRectMake(15, 20 + i * (21 + 10), 80, 21)];
        leftT.text = leftT_arr[i];
        leftT.textColor = UIColorFromRGB(0x4d4d4d);
        [backView2 addSubview:leftT];
        }
    _btn1 = [UIButton buttonWithType:UIButtonTypeSystem];
    _btn1.frame = CGRectMake(100, 15, kSCREEN_WIDTH/2, 30);
    //[_btn1 setTitle:rigthC_arr[i] forState:UIControlStateNormal];
    _btn1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    //设置btn标题左对齐
    _btn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btn1 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _btn1.tag = 1001;
    [_btn1 addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [backView2 addSubview:_btn1];
    
    _btn2 = [UIButton buttonWithType:UIButtonTypeSystem];
    _btn2.frame = CGRectMake(100, 50, kSCREEN_WIDTH/2, 30);
    //[_btn1 setTitle:rigthC_arr[i] forState:UIControlStateNormal];
    _btn2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    //设置btn标题左对齐
    _btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btn2 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
    _btn2.tag = 1002;
    [_btn2 addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [backView2 addSubview:_btn2];

    UIView *backView3 = [[UIView alloc]init];
    backView3.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:backView3];
    [backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView2.mas_bottom).offset(20);
        make.left.equalTo(_scorllView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 100));
    }];
    UILabel *leftT = [[UILabel alloc]initWithFrame:CGRectMake(15, 20, 80, 21)];
    leftT.text = @"备注信息:";
    leftT.textColor = UIColorFromRGB(0x4d4d4d);
    [backView3 addSubview:leftT];
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(100, 10, kSCREEN_WIDTH/3*2, 80)];
    _textView.text = @"输入申用用途及备注信息";
    _textView.textColor = UIColorFromRGB(0xb6b6b6);
    _textView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _textView.font = [UIFont systemFontOfSize:17.0];
    [backView3 addSubview:_textView];
    
   UIView *backView4 = [[UIView alloc]init];
    backView4.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:backView4];
    [backView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView3.mas_bottom).offset(15);
        make.left.equalTo(_scorllView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
    }];
    //提交取消bttuon
    UIButton *updata = [UIButton buttonWithType:UIButtonTypeSystem];
    [updata setTitle:@"提交" forState:UIControlStateNormal];
    [updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    updata.backgroundColor = UIColorFromRGB(0x2dd500);
    [updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
    [backView4 addSubview:updata];
    [updata mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView4).offset(10);
        make.left.equalTo(_scorllView).offset((kSCREEN_WIDTH/3-20)/2);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
    
    UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancerl setTitle:@"取消" forState:UIControlStateNormal];
    [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    cancerl.backgroundColor = UIColorFromRGB(0xd50037);
    [backView4 addSubview:cancerl];
    [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView4).offset(10);
        make.left.equalTo(updata.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];

}
- (void)choose:(UIButton *)sender{
    
    tag = sender.tag;

    if (sender.tag == 1001) {

        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, kSCREEN_WIDTH/2, 44*3) titleMenus:_carName];
        view.containerBackgroudColor = [UIColor clearColor];
        view.delegate = self;
        [view showFrom:sender alignStyle:CPAlignStyleLeft];
        
    }else{
        
        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, kSCREEN_WIDTH/2, 44*3) titleMenus:_carBand[indexPath]];
        view.containerBackgroudColor = [UIColor clearColor];
        view.delegate = self;
        [view showFrom:sender alignStyle:CPAlignStyleLeft];
    }
}

- (void)popOverView:(CustomPopOverView *)pView didClickMenuIndex:(NSInteger)index
{


    if (tag == 1001) {
        indexPath = index;
        [_btn1 setTitle:_carName[index] forState:UIControlStateNormal];
        [_btn1 setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        //每次跟换车辆品牌 清空车辆型号  重新选择车辆型号
        [_btn2 setTitle:nil forState:UIControlStateNormal];
    }else{
        
        [_btn2 setTitle:_carBand[indexPath][index] forState:UIControlStateNormal];
        [_btn2 setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    }
    
        

    
}
/*
//提交申请
- (void)up{
    
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *time = [dateFormatter stringFromDate:currentDate];
    
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_carUpdate parameters:@{userid:[NSString stringWithFormat:@"%ld",self.userId],updataTime:time,Carmodel:_btn2.titleLabel.text,brand:_btn1.titleLabel.text,disc:_textView.text} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSInteger status = [dic[@"status"]integerValue];
        NSLog(@"%ld",status);
        if (status == 1) {

            [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
        }else{
            
            [[YXAlertHUD stateHUDWithHUDTyep:1 message:@"提交失败" textColor:[UIColor redColor] iconColor:[UIColor redColor]] show];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}*/
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
