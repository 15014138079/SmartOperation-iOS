//
//  YYAlarmListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
@protocol YYAlarmListModel <NSObject>

@end
@interface YYAlarmListModel : JSONModel
@property (nonatomic,assign)NSInteger faultTypeId;
@property (nonatomic,assign)NSInteger equipId;
@property (nonatomic,strong)NSString *time;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSString *name;

@property (nonatomic,assign)NSInteger gzID;
@property (nonatomic,assign)NSInteger state;
@property (nonatomic,strong)NSString *faultTypeName;
@property (nonatomic,assign)NSInteger stationId;
@property (nonatomic,assign)NSInteger changePsonId;

@property (nonatomic,strong)NSString *stationName;
@property (nonatomic,assign)NSInteger findId;
@property (nonatomic,assign)NSInteger faultApprovalId;
@end
/*
 "faultTypeId": 254,
 "equipId": 8918,
 "time": "2016-12-23 12:30:15",
 "discrib": "湿度传感器报警导致设备停止测量。",
 "name": "湿度报警",
 "gzid": 254,
 "state": 2,
 "faultTypeName": "探测器、传感器故障",
 "stationId": 2382,
 "changePsonId": 0,
 "findId": 1102,
 "stationName": "武威市泰达市政管理建设有限责任公司进水口"
 */
