//
//  YYXJhistoryListCell.m
//  XJHistory
//
//  Created by 杨毅 on 16/12/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "YYXJhistoryListCell.h"

@implementation YYXJhistoryListCell
{
    UILabel *_XJtitle;
    UILabel *_XJstateLab;
    UILabel *_XJuserName;
    UILabel *_time;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        _XJtitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, kSCREEN_WIDTH-20, 21)];
        _XJtitle.textColor = UIColorFromRGB(0x4d4d4d);
        _XJtitle.text = @"站点名称";
        _XJtitle.textAlignment = NSTextAlignmentLeft;
        _XJtitle.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _XJtitle.font = [UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:_XJtitle];

        _XJuserName = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_XJtitle.frame), 100, 21)];
        _XJuserName.font = [UIFont systemFontOfSize:13.0];
        _XJuserName.textColor = UIColorFromRGB(0x4d4d4d);
        _XJuserName.text = @"username";
        _XJuserName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_XJuserName];
        _XJstateLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_XJuserName.frame), CGRectGetMaxY(_XJtitle.frame), 30, 21)];
        _XJstateLab.font = [UIFont systemFontOfSize:13.0];
        _XJstateLab.text = @"正常";
        [self.contentView addSubview:_XJstateLab];
        _time = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_XJstateLab.frame)+5, CGRectGetMaxY(_XJtitle.frame), kSCREEN_WIDTH-150, 21)];
        _time.textColor = UIColorFromRGB(0x4d4d4d);
        _time.font = [UIFont systemFontOfSize:13.0];
        _time.text = @"2016-10-12 00:00:00";
        _time.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_time];

    }
    return self;
}
- (void)fillCellWithModel:(YYXJhisListModel *)model
{
    _XJtitle.text = model.stationName;
    if (model.resultStatus == 0) {

//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:@"巡检结果:异常"];
//        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x4d4d4d) range:NSMakeRange(0, 5)];
//        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xd50037) range:NSMakeRange(5, 2)];
        _XJstateLab.text = @"异常" ;
        _XJstateLab.textColor = UIColorFromRGB(0xd50037);
    }else{
//        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:@"巡检结果:正常"];
//        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x4d4d4d) range:NSMakeRange(0, 5)];
//        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x2dd500) range:NSMakeRange(5, 2)];
        _XJstateLab.text = @"正常";
        _XJstateLab.textColor = UIColorFromRGB(0x2dd500);
    }
    _XJuserName.text = [NSString stringWithFormat:@"巡检人:%@",model.inspectUser];
    _time.text = model.commitTime;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
