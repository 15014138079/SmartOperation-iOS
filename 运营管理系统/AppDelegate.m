//
//  AppDelegate.m
//  运营管理系统
//
//  Created by 杨毅 on 16/8/26.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "AlarmViewController.h"
#import "NavigationController.h"
#import "TabBarViewController.h"
#import <Bugly/Bugly.h>

@interface AppDelegate ()


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    NSString *executableFile = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleExecutableKey];    //获取项目名称
//    
//    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];      //获取项目版本号
//    NSLog(@"%@,%@",executableFile,version);
    //[self VersionButton];
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
   // 全局设置
    id appearnce = [UINavigationBar appearance];
    UIColor *color = UIColorFromRGB(0x0093d5);
    [appearnce setBarTintColor:color];
    [appearnce setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.0],NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //启动页停留时间
    [NSThread sleepForTimeInterval:1.0];
    //[self.window makeKeyAndVisible];
   NSString *doucumentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    //2.拼接上文件路径
    NSString *filtPath = [doucumentsPath stringByAppendingPathComponent:@"Log.txt"];
    NSSLog(@"%@",filtPath);
    //3.字典从文件中读取数据
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:filtPath];
    if (dic != nil) {
        
        if (![[dic objectForKey:@"username"] isKindOfClass:[NSNull class]] && ![[dic objectForKey:@"password"] isKindOfClass:[NSNull class]]) {
            TabBarViewController *tab = [[TabBarViewController alloc]init];
            self.window.rootViewController = tab;
        }else{
            ViewController *vc = [[ViewController alloc]init];
            self.window.rootViewController = vc;

        }
        
    }else{
        
        ViewController *vc = [[ViewController alloc]init];
        self.window.rootViewController = vc;
    }



    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    //    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    
    //设置导航条的透明状态
    //1.添加背景图片, 透明 --> 不透明
    
    //2.修改属性,默认是透明
    //nav.navigationBar.translucent = NO;
    //导航条是否透明,决定了内部viewcontroller的原点坐标
    //透明情况下,原点坐标为屏幕的左上角(0,0)
    //不透明情况下,原点坐标为导航条左下角(0,64)
    
//新版界面
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
// 
//    self.window.backgroundColor = [UIColor whiteColor];
//    [self.window makeKeyAndVisible];
//    self.window.rootViewController = [[NavigationController alloc] initWithRootViewController:[[TabBarViewController alloc]init]];//根视图
    
    [Bugly startWithAppId:@"5f2e735e46"];

    
    return YES;
}



//- (void)VersionButton{
//    //获取发布版本的version
//    NSString *string = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://itunes.apple.com/lookup?id=1176817914"] encoding:NSUTF8StringEncoding error:nil];
//    if (string != nil && [string length]>0 && [string rangeOfString:@"version"].length == 7) {
//        [self checkAppUpdate:string];
//        
//    }
//    NSLog(@"%@",string);
//}
//- (void)checkAppUpdate:(NSString *)appInfo{
//    //获取当前版本
//    NSString *version = [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleVersion"];
//    NSString *appInfo1 = [appInfo substringFromIndex:[appInfo rangeOfString:@"\"version\":"].location+10];
//    appInfo1 = [[appInfo1 substringFromIndex:[appInfo1 rangeOfString:@","].location]stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//
//    /*
//     判断，如果当前版本与发布版本不同，则进入更新。
//     */
//    NSLog(@"a%@",appInfo1);
//    NSLog(@"v%@",version);
//    if (![appInfo1 isEqualToString:version]) {
//        
//        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"发现新版本"] message:nil preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"下次再说" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            NSLog(@"点击了取消");
//        }];
//        
//        UIAlertAction *OKAction  = [UIAlertAction actionWithTitle:@"去更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            NSLog(@"点击了知道了");
//            NSURL * url = [NSURL URLWithString:@"https://itunes.apple.com/cn/app/ying-feng-huan-jing-yun-ying/id1176817914?mt=8&uo=4"];//itunesURL = trackViewUrl的内容
//            [[UIApplication sharedApplication] openURL:url];
//        }];
//        [alertVC addAction:cancelAction];
//        [alertVC addAction:OKAction];
//        UIWindow *aW = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
//        aW.rootViewController = [[UIViewController alloc]init];
//        aW.windowLevel = UIWindowLevelAlert + 1;
//        [aW makeKeyAndVisible];
//        [aW.rootViewController presentViewController:alertVC animated:YES completion:nil];
//
//
//    }else{
//        NSLog(@"最新版本");
//        }
//}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
