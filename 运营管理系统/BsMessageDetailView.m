//
//  BsMessageDetailView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "BsMessageDetailView.h"
#import "Masonry.h"
#import "FaultPhotoView.h"
@interface BsMessageDetailView()<FaultPhotoViewDelegate>
{
    CGFloat MaxY;
    UILabel *_content1;
    UIView *_line0;
    UIView *_line3;
    UIView *_line5;
    UIView *_line6;

    NSArray *_allImageArr;
}
@end
@implementation BsMessageDetailView
- (instancetype)initWithFrame:(CGRect)frame AndDataDictionray:(NSDictionary *)dic MaxY:(CGFloat)Y WithImageUrl:(NSArray *)imageUrlArray{
    self = [super initWithFrame:frame];
    if (self) {
        MaxY = Y;
        _allImageArr = imageUrlArray;
        [self initviews:dic];
    }
    return self;
}

- (void)initviews:(NSDictionary *)dictionary{

    NSArray *arr1 = @[@"填  报  人",@"联系方式",@"发生时间"];
    NSArray *arr2 = @[dictionary[@"pson"],dictionary[@"tel"],dictionary[@"findtime"]];
    for (NSInteger i = 0; i < arr1.count; i++) {
        _content1 = [[UILabel alloc]initWithFrame:CGRectZero];
        _content1.text = arr2[i];
        _content1.textColor = UIColorFromRGB(0x4d4d4d);
        _content1.font = [UIFont systemFontOfSize:15.0];
        _content1.numberOfLines = 0;
        _content1.backgroundColor = UIColorFromRGB(0xf0f0f0);

        _content1.frame = CGRectMake(100,i*(5+18)+2, kSCREEN_WIDTH-105, 18);
        [self addSubview:_content1];
        _line0 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_content1.frame)+2, kSCREEN_WIDTH, 1)];
        _line0.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:_line0];
        UILabel *leftTitle = [[UILabel alloc]init];
        leftTitle.text = arr1[i];
        leftTitle.textColor = UIColorFromRGB(0x4d4d4d);
        leftTitle.font = [UIFont systemFontOfSize:15.0];
        leftTitle.textAlignment = NSTextAlignmentCenter;
        [self addSubview:leftTitle];
        [leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_content1);
            make.left.equalTo(self).offset(5);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
    }
    UILabel *FindAddressLab = [[UILabel alloc]initWithFrame:CGRectZero];
    FindAddressLab.text = dictionary[@"findaddress"];
    FindAddressLab.textColor = UIColorFromRGB(0x4d4d4d);
    FindAddressLab.font = [UIFont systemFontOfSize:15.0];
    FindAddressLab.numberOfLines = 0;
    FindAddressLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect rect1 = [FindAddressLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:FindAddressLab.font,NSFontAttributeName, nil] context:nil];
    FindAddressLab.frame = CGRectMake(100, CGRectGetMaxY(_line0.frame)+3, kSCREEN_WIDTH-105, rect1.size.height);
    [self addSubview:FindAddressLab];
    UILabel *FindAddressT = [[UILabel alloc]init];
    FindAddressT.text = @"发生地址";
    FindAddressT.textColor = UIColorFromRGB(0x4d4d4d);
    FindAddressT.font = [UIFont systemFontOfSize:15.0];
    FindAddressT.textAlignment = NSTextAlignmentCenter;
    [self addSubview:FindAddressT];
    [FindAddressT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(FindAddressLab);
        make.left.equalTo(self).offset(5);
        make.size.mas_equalTo(CGSizeMake(90, 18));
    }];
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(FindAddressLab.frame)+2, kSCREEN_WIDTH, 1)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:line1];

    UILabel *cusNameLab = [[UILabel alloc]initWithFrame:CGRectZero];
    cusNameLab.text = dictionary[@"custname"];
    cusNameLab.textColor = UIColorFromRGB(0x4d4d4d);
    cusNameLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
    cusNameLab.font = [UIFont systemFontOfSize:15.0];
    cusNameLab.numberOfLines = 0;
    cusNameLab.textAlignment = NSTextAlignmentLeft;
    
    CGRect rect2 = [cusNameLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:cusNameLab.font,NSFontAttributeName, nil] context:nil];
    cusNameLab.frame = CGRectMake(100, CGRectGetMaxY(line1.frame)+3, kSCREEN_WIDTH-105, rect2.size.height);
    [self addSubview:cusNameLab];
    UILabel *cusNameT = [[UILabel alloc]init];
    cusNameT.text = @"客户名称";
    cusNameT.textColor = UIColorFromRGB(0x4d4d4d);
    cusNameT.font = [UIFont systemFontOfSize:15.0];
    cusNameT.textAlignment = NSTextAlignmentCenter;
    [self addSubview:cusNameT];
    [cusNameT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(cusNameLab);
        make.left.equalTo(self).offset(5);
        make.size.mas_equalTo(CGSizeMake(90, 18));
    }];
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(cusNameLab.frame)+2, kSCREEN_WIDTH, 1)];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:line2];

    NSArray *arr3 = @[@"设备编码",@"监测类型",@"产品名称",@"产品型号",@"维修数量"];
    NSArray *arr4 = @[dictionary[@"equipNum"],dictionary[@"typename"],dictionary[@"equipname"],dictionary[@"equipmodel"],dictionary[@"value"]];
    for (NSInteger i = 0; i < arr3.count; i++) {
        UILabel *content2 = [[UILabel alloc]initWithFrame:CGRectMake(100, CGRectGetMaxY(line2.frame)+3+i*(5+18), kSCREEN_WIDTH-105, 18)];
        content2.text = arr4[i];
        content2.textColor = UIColorFromRGB(0x4d4d4d);
        content2.backgroundColor = UIColorFromRGB(0xf0f0f0);
        content2.textAlignment = NSTextAlignmentLeft;
        content2.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:content2];
        UILabel *TitleLab2 = [[UILabel alloc]init];
        TitleLab2.text = arr3[i];
        TitleLab2.textColor = UIColorFromRGB(0x4d4d4d);
        TitleLab2.font = [UIFont systemFontOfSize:15.0];
        TitleLab2.textAlignment = NSTextAlignmentCenter;
        [self addSubview:TitleLab2];
        [TitleLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(5);
            make.centerY.equalTo(content2);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
        _line3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(content2.frame)+2, kSCREEN_WIDTH, 1)];
        _line3.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:_line3];
    }

    UILabel *MainGZLab = [[UILabel alloc]initWithFrame:CGRectZero];
    MainGZLab.text = dictionary[@"mainGZ"];
    MainGZLab.textColor = UIColorFromRGB(0x4d4d4d);
    MainGZLab.font = [UIFont systemFontOfSize:15.0];
    MainGZLab.numberOfLines = 0;
    MainGZLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect rect3 = [MainGZLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:MainGZLab.font,NSFontAttributeName, nil] context:nil];
    MainGZLab.frame = CGRectMake(100, CGRectGetMaxY(_line3.frame)+3, kSCREEN_WIDTH-105, rect3.size.height);
    [self addSubview:MainGZLab];
    UILabel *MainGZTitle = [[UILabel alloc]init];
    MainGZTitle.text = @"主要故障";
    MainGZTitle.textColor = UIColorFromRGB(0x4d4d4d);
    MainGZTitle.font = [UIFont systemFontOfSize:15.0];
    MainGZTitle.textAlignment = NSTextAlignmentCenter;
    [self addSubview:MainGZTitle];
    [MainGZTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(MainGZLab);
        make.left.equalTo(self).offset(5);
        make.size.mas_equalTo(CGSizeMake(90, 18));
    }];
    UIView *line4 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(MainGZLab.frame)+2, kSCREEN_WIDTH, 1)];
    line4.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:line4];

    UILabel *Discrb = [[UILabel alloc]initWithFrame:CGRectZero];
    Discrb.text = dictionary[@"discrb"];
    Discrb.textColor = UIColorFromRGB(0x4d4d4d);
    Discrb.font = [UIFont systemFontOfSize:15.0];
    Discrb.numberOfLines = 0;
    Discrb.backgroundColor = UIColorFromRGB(0xf0f0f0);
    CGRect rect4 = [Discrb.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:Discrb.font,NSFontAttributeName, nil] context:nil];
    Discrb.frame = CGRectMake(100, CGRectGetMaxY(line4.frame)+3, kSCREEN_WIDTH-105, rect4.size.height);
    [self addSubview:Discrb];
    UILabel *DiscrbTitle = [[UILabel alloc]init];
    DiscrbTitle.text = @"事件描述";
    DiscrbTitle.textColor = UIColorFromRGB(0x4d4d4d);
    DiscrbTitle.font = [UIFont systemFontOfSize:15.0];
    DiscrbTitle.textAlignment = NSTextAlignmentCenter;
    [self addSubview:DiscrbTitle];
    [DiscrbTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(Discrb);
        make.left.equalTo(self).offset(5);
        make.size.mas_equalTo(CGSizeMake(90, 18));
    }];
    _line5 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(Discrb.frame)+2, kSCREEN_WIDTH, 1)];
    _line5.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:_line5];
    
    FaultPhotoView *photoView = [[FaultPhotoView alloc]initWithFrame:CGRectMake(100, CGRectGetMaxY(_line5.frame)+2, kSCREEN_WIDTH-105, 100) imageurl:_allImageArr];
    photoView.delegate = self;
    photoView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:photoView];
    UILabel *BidPhotoLab = [[UILabel alloc]init];
    BidPhotoLab.text = @"不良图片";
    BidPhotoLab.textColor = UIColorFromRGB(0x4d4d4d);
    BidPhotoLab.font = [UIFont systemFontOfSize:15.0];
    BidPhotoLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:BidPhotoLab];
    [BidPhotoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(photoView);
        make.left.equalTo(self).offset(5);
        make.size.mas_equalTo(CGSizeMake(90, 18));
    }];
    _line6 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(photoView.frame)+2, kSCREEN_WIDTH, 1)];
    _line6.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self addSubview:_line6];
    
    
    
}
- (void)didSelectPhotoURL:(NSArray *)FautImageURL andIndex:(NSInteger)index{
    
    [self.delegate didSelectedImageURL:FautImageURL AndIndex:index];
}
//重新绘制view的高度
- (void)layoutSubviews{
    
    self.frame = CGRectMake(0, MaxY, kSCREEN_WIDTH, CGRectGetMaxY(_line6.frame));
    [self.delegate returnBSMessageViewHeight:CGRectGetMaxY(_line6.frame)];
}


@end
