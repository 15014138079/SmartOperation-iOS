//
//  GZcountProjectCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PtypeModel.h"
@interface GZcountProjectCell : UITableViewCell
- (void)fillCellWithModel:(PtypeModel *)model;
@end
