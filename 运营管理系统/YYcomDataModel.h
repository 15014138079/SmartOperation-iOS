//
//  YYcomDataModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/15.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYFgsModel.h"
@interface YYcomDataModel : JSONModel
@property (nonatomic,strong)NSArray <YYFgsModel> *Fgsdata;
@end
