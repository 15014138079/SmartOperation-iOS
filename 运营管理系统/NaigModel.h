//
//  NaigModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/11/8.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NaigModel : NSObject
@property (nonatomic,assign)float lat;
@property (nonatomic,assign)float lnt;
@property (nonatomic,strong)NSString *phone;
@property (nonatomic,strong)NSString *title;//站点名字
@property (nonatomic,strong)NSString *subtitle;//站点位置
@property (nonatomic,strong)NSString *pson;//负责人
@property (nonatomic,strong)NSString *opcenter;//所属运营中心
@property (nonatomic,strong)NSString *adress;//站点位置
@property (nonatomic,assign)NSInteger state;//大头针标记
@property (nonatomic,assign)NSInteger sationN;//站点数
@property (nonatomic,assign)float distance;//距离
@end
/*
 @"latitude":[NSString stringWithFormat:@"%f",model.lat],
 @"longitude":[NSString stringWithFormat:@"%f",model.lnt],
 @"phone":model.phone,
 @"title":model.stationName,
 @"subtitle":model.address,
 @"pson":model.principalName,
 @"opcenter":model.opCenName,
 @"adress":model.address,
 @"state":@3
 */
