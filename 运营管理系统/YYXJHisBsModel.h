//
//  YYXJHisBsModel.h
//  XJHistory
//
//  Created by 杨毅 on 16/12/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "YYXJdataModel.h"
@interface YYXJHisBsModel : JSONModel
@property (nonatomic,strong)NSString *message;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)YYXJdataModel *data;
@end
