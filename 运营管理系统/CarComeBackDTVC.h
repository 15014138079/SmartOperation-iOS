//
//  CarComeBackDTVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/11.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarComeBackDTVC : UIViewController
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)NSString *band;
@property (nonatomic,strong)NSString *plateNum;
@property (nonatomic,strong)NSString *repName;
@property (nonatomic,strong)NSString *Cmodel;
@property (nonatomic,assign)NSInteger carId;
@end
