//
//  MassMsgView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/13.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "MassMsgView.h"
#import "Masonry.h"
@interface MassMsgView()
{
    CGFloat MaxY;
    UILabel *_MassTitle;
    UILabel *_AlterWays;
    UIView *_line4;
}
@end
@implementation MassMsgView
- (instancetype)initWithFrame:(CGRect)frame AndDiction:(NSDictionary *)dic WithMaxY:(CGFloat)Y
{
    self = [super initWithFrame:frame];
    if (self) {
        MaxY = Y;
        NSArray *titleArray = @[@"填  报  人",@"联系方式",@"审核时间"];
        NSArray *conteneArray = @[dic[@"pson"],dic[@"tel"],dic[@"time"]];
        for (NSInteger i = 0; i < titleArray.count; i++) {
            _MassTitle = [[UILabel alloc]initWithFrame:CGRectMake(5, i*(5+18), 90, 18)];
            _MassTitle.textColor = UIColorFromRGB(0x4d4d4d);
            _MassTitle.text = titleArray[i];
            _MassTitle.textAlignment = NSTextAlignmentCenter;
            _MassTitle.font = [UIFont systemFontOfSize:15.0];
            [self addSubview:_MassTitle];
            UILabel *MassContentLab = [[UILabel alloc]initWithFrame:CGRectMake(100, i*(5+18), kSCREEN_WIDTH-105, 18)];
            MassContentLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
            MassContentLab.textColor = UIColorFromRGB(0x4d4d4d);
            MassContentLab.text = conteneArray[i];
            MassContentLab.font = [UIFont systemFontOfSize:15.0];
            [self addSubview:MassContentLab];
            UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_MassTitle.frame)+2, kSCREEN_WIDTH, 1)];
            line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [self addSubview:line1];
        }
        UILabel *CLsuggestion = [[UILabel alloc]init];
        CLsuggestion.text = dic[@"scheme"];
        CLsuggestion.numberOfLines = 0;
        CLsuggestion.textColor = UIColorFromRGB(0x4d4d4d);
        CLsuggestion.font = [UIFont systemFontOfSize:15.0];
        CLsuggestion.backgroundColor = UIColorFromRGB(0xf0f0f0);
        CGRect rect = [CLsuggestion.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:CLsuggestion.font, NSFontAttributeName,nil] context:nil];
        CLsuggestion.frame = CGRectMake(100, CGRectGetMaxY(_MassTitle.frame)+5, kSCREEN_WIDTH-105, rect.size.height);
        [self addSubview:CLsuggestion];
        UILabel *SuggestionLab = [[UILabel alloc]init];
        SuggestionLab.textAlignment = NSTextAlignmentCenter;
        SuggestionLab.text = @"处置方案";
        SuggestionLab.textColor = UIColorFromRGB(0x4d4d4d);
        SuggestionLab.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:SuggestionLab];
        [SuggestionLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(CLsuggestion);
            make.left.equalTo(self).offset(5);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
        UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(CLsuggestion.frame)+2, kSCREEN_WIDTH, 1)];
        line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line2];
        
        
        UILabel *ResultContent = [[UILabel alloc]init];
        ResultContent.text = dic[@"result"];
        ResultContent.numberOfLines = 0;
        ResultContent.textColor = UIColorFromRGB(0x4d4d4d);
        ResultContent.font = [UIFont systemFontOfSize:15.0];
        ResultContent.backgroundColor = UIColorFromRGB(0xf0f0f0);
        CGRect rect1 = [ResultContent.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:ResultContent.font, NSFontAttributeName,nil] context:nil];
        ResultContent.frame = CGRectMake(100, CGRectGetMaxY(CLsuggestion.frame)+5, kSCREEN_WIDTH-105, rect1.size.height);
        [self addSubview:ResultContent];
        UILabel *ResultLab = [[UILabel alloc]init];
        ResultLab.textAlignment = NSTextAlignmentCenter;
        ResultLab.text = @"原因分析";
        ResultLab.textColor = UIColorFromRGB(0x4d4d4d);
        ResultLab.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:ResultLab];
        [ResultLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(ResultContent);
            make.left.equalTo(self).offset(5);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
        UIView *line3 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(ResultContent.frame)+2, kSCREEN_WIDTH, 1)];
        line3.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:line3];
        
        
        _AlterWays = [[UILabel alloc]init];
        _AlterWays.text = dic[@"causes"];
        _AlterWays.numberOfLines = 0;
        _AlterWays.textColor = UIColorFromRGB(0x4d4d4d);
        _AlterWays.font = [UIFont systemFontOfSize:15.0];
        _AlterWays.backgroundColor = UIColorFromRGB(0xf0f0f0);
        CGRect rect2 = [_AlterWays.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-105, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_AlterWays.font, NSFontAttributeName,nil] context:nil];
        _AlterWays.frame = CGRectMake(100, CGRectGetMaxY(ResultContent.frame)+5, kSCREEN_WIDTH-105, rect2.size.height);
        [self addSubview:_AlterWays];
        UILabel *AlterLab = [[UILabel alloc]init];
        AlterLab.textAlignment = NSTextAlignmentCenter;
        AlterLab.text = @"整改措施";
        AlterLab.textColor = UIColorFromRGB(0x4d4d4d);
        AlterLab.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:AlterLab];
        [AlterLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_AlterWays);
            make.left.equalTo(self).offset(5);
            make.size.mas_equalTo(CGSizeMake(90, 18));
        }];
        _line4 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_AlterWays.frame)+2, kSCREEN_WIDTH, 1)];
        _line4.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self addSubview:_line4];
    }
    return self;
}
- (void)layoutSubviews{
    self.frame = CGRectMake(0, MaxY, kSCREEN_WIDTH, CGRectGetMaxY(_line4.frame)+2);
    [self.delegate returnMassMsgViewHeight:CGRectGetMaxY(_line4.frame)+2];
}

@end
