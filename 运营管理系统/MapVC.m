//
//  MapVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/9/12.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "MapVC.h"
#import "MapView.h"
#import "Item.h"
#import "TestMapCell.h"
#import "AppDelegate.h"
#import "Masonry.h"
#import "MapSearchVC.h"
#import "AFNetworking.h"
#import "MapStation.h"
#import "pson.h"
#import "BranchOfcList.h"
#import "MbaseModel.h"
#import "testMapPsonCell.h"
#import "NaigModel.h"
#import "NaigCell.h"
#import "MBProgressHUD.h"
#import "YYmapDatamodel.h"
#import "YYmapStationDataModel.h"
#import <MapKit/MapKit.h>
@interface MapVC ()<MapViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
   // UIView *_StationDTview;//站点详情
    UIView *_AllStaionView;//所有附近站点背景
    NSDictionary *_dictionary;
    UIButton *_NeigBtn;//附近
    NSInteger _NeigBtnTag;
    NSString *_NeigURL;
    
    NSString *_place;
    CGFloat lnt;
    CGFloat lat;
    CLLocationManager *_LocationManager; //定位管理器
    MKMapView *_OriginalMapview;
    UIView *_OriginalView;
    
}
@property (nonatomic,strong)MapView *mapView;
@property (nonatomic,strong)NSMutableArray *annotations;
@property (nonatomic,strong)UITableView *tableview;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)NSMutableArray *allStation;
@property (nonatomic,strong)NSMutableArray *allBran;
@property (nonatomic,strong)NSMutableArray *allPson;
@property (nonatomic,strong)UIView  *StationDTview;
@property (nonatomic,strong)UIView  *NagihView;
@property (nonatomic,strong)NSMutableArray *naigPsonArr;
@property (nonatomic,strong)NSMutableArray *naigStationArr;


@end

@implementation MapVC
- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"定位查询";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.userID = delegate.userId;


    _annotations = [[NSMutableArray alloc]init];
    _naigStationArr = [[NSMutableArray alloc]init];

    _OriginalView = [[UIView alloc]initWithFrame:self.view.bounds];
    _OriginalView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_OriginalView];

    _OriginalMapview = [[MKMapView alloc]initWithFrame:self.view.bounds];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_OriginalMapview animated:YES];
    _OriginalMapview.showsUserLocation = YES;
    _OriginalMapview.userTrackingMode = MKUserTrackingModeFollow;
    [_OriginalView addSubview:_OriginalMapview];
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(10, 70, kSCREEN_WIDTH-20, 50)];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.shadowColor = [UIColor grayColor].CGColor;
    backView.layer.shadowOffset = CGSizeMake(0, 0);
    backView.layer.shadowOpacity = 1.0f;
    [_OriginalView addSubview:backView];
    UIButton *search = [UIButton buttonWithType:UIButtonTypeCustom];
    [search setTitle:@"搜索人员、站点" forState:UIControlStateNormal];
    [search setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    search.frame = CGRectMake(10, 0, kSCREEN_WIDTH-80, 50);
    search.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [search addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:search];
    UIImageView *searchImage = [[UIImageView alloc]init];
    searchImage.image = [[UIImage imageNamed:@"seach_icon_b"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [backView addSubview:searchImage];
    [searchImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(search.mas_right).offset(0);
        make.centerY.equalTo(backView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];

    [self dingwei];
    
    
}
- (void)dingwei{
    //实例化定位管理器
    _LocationManager = [[CLLocationManager alloc] init];
    
    _LocationManager.delegate = self;
    
    //请求用户同意定位
    //只在前台定位
    //在项目的配置文件Info.plist中添加以下内容
    //NSLocationWhenInUseUsageDescription
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] > 8.0) {
        [_LocationManager requestWhenInUseAuthorization];
    }
    
    //允许在后台定位
    //NSLocationAlwaysUsageDescription
    //[_manager requestAlwaysAuthorization];
    
    //设置定位的精度和更新频率
    /*
     kCLLocationAccuracyBestForNavigation  导航
     kCLLocationAccuracyBest;              较好的精度
     kCLLocationAccuracyNearestTenMeters;  10米
     kCLLocationAccuracyHundredMeters;     100米
     kCLLocationAccuracyKilometer;         1000米
     kCLLocationAccuracyThreeKilometers;   3000米
     */
    _LocationManager.desiredAccuracy = kCLLocationAccuracyBest; //定位的精度
    
    _LocationManager.distanceFilter = 100; //更新频率
    if ([CLLocationManager locationServicesEnabled]) { // 判断是否打开了位置服务
        //启动定位
        [_LocationManager startUpdatingLocation]; // 开始更新位置
    }else{
        
    }
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([_LocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_LocationManager requestWhenInUseAuthorization];     //NSLocationWhenInUseDescription
                [_LocationManager requestAlwaysAuthorization];
            }
            break;
        default:
            break;
            
            
    }
}
/** 不能获取位置信息时调用*/
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
    [MBProgressHUD hideHUDForView:_OriginalMapview animated:YES];
}
//定位成功后会执行的函数
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    
    CLLocation *currentLocation = [locations firstObject];
    //NSLog(@"%@",currentLocation);
    
    CLLocation *newLocation = locations[0];
    CLLocationCoordinate2D oldCoordinate = newLocation.coordinate;

    //NSSLog(@"旧的经度：%f,旧的纬度：%f",oldCoordinate.longitude,oldCoordinate.latitude);
    lat = oldCoordinate.latitude;
    lnt = oldCoordinate.longitude;
    
    //获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *array, NSError *error)
     {
         if (array.count > 0)
         {
             CLPlacemark *placemark = [array firstObject];
             
             //NSSLog(@"%@",placemark.administrativeArea);//省份
             //获取城市
             NSString *city = placemark.locality;
             if (!city) {
                 //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                 city = placemark.administrativeArea;
             }
             //NSString  *cityName = city;
             
             //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
             NSMutableString *str = [[NSMutableString alloc]init];
             [str appendString:placemark.administrativeArea];
             if (placemark.locality.length>0){
                 [str appendString:placemark.locality];
             }
             if (placemark.subLocality.length>0) {
                 [str appendString:placemark.subLocality];
             }
             if (placemark.thoroughfare.length>0) {
                 [str appendString:placemark.thoroughfare];
             }
             if (placemark.subThoroughfare.length>0) {
                 [str appendString:placemark.subThoroughfare];
             }
             //_place = str;//城市地区街道子街道
             _place = [NSString stringWithFormat:@"%@",str];
             
             /*_addressL.text = _place;
             if (_addressL.text.length < 1) {
                 self.refreshBtn.frame = CGRectMake(26, 0,kSCREEN_WIDTH-52, 40);
                 
             }else{
                 self.refreshBtn.frame = CGRectMake(CGRectGetMaxX(_addressL.frame), 0,80, 40);
             }*/
             
         }else if (error == nil && [array count] == 0)
         {
             [MBProgressHUD hideHUDForView:_OriginalMapview animated:YES];
             NSLog(@"No results were returned.");
         }else if (error != nil)
         {
             [MBProgressHUD hideHUDForView:_OriginalMapview animated:YES];
             NSLog(@"An error occurred = %@", error);
         }

//         if (lat == 0 && lnt == 0) {
//             CGFloat lat1 = 28.0803;
//             CGFloat lnt1 = 113.023;
//             NSString *adress1 = @"长沙市雨花区";
//             [self.annotations addObject:@{@"latitude":[NSString stringWithFormat:@"%f",lat1],
//                                           @"longitude":[NSString stringWithFormat:@"%f",lnt1],@"title":@"你当前的位置",@"phone":@"",@"subtitle":adress1,@"state":@"0"}];
             //CLLocationCoordinate2D center = CLLocationCoordinate2DMake(lat/*纬度*/, lnt/*经度*/);
             //MKCoordinateSpan span = MKCoordinateSpanMake(1, 1);
             //MKCoordinateRegion region = MKCoordinateRegionMake(center,span);
//             MKCoordinateRegion adjustedRegion = [_mapView.mapView regionThatFits:region];
//             MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
            // [_OriginalMapview setRegion:region animated:YES];
//         }else{
//             
//             [self.annotations addObject:@{@"latitude":[NSString stringWithFormat:@"%f",lat],
//                                           @"longitude":[NSString stringWithFormat:@"%f",lnt],@"title":@"你当前的位置",@"phone":@"",@"subtitle":_place,@"state":@"0"}];
//             //        _mapView.mapView.centerCoordinate = CLLocationCoordinate2DMake(lat, lnt);
//             CLLocationCoordinate2D center = CLLocationCoordinate2DMake(lat/*纬度*/, lnt/*经度*/);
//             MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center,10000 ,10000 );
//             MKCoordinateRegion adjustedRegion = [_mapView.mapView regionThatFits:region];
//             [_mapView.mapView setRegion:adjustedRegion animated:YES];
////             MKCoordinateSpan span = MKCoordinateSpanMake(1, 1);
////             MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
//             //[_mapView.mapView setRegion:region animated:YES];
//         }
//         
//         [_mapView beginLoad];

     }];
    
    [manager stopUpdatingLocation];
    [MBProgressHUD hideHUDForView:_OriginalMapview animated:YES];
    
}
- (void)initMapview{
    
    [_OriginalView removeFromSuperview];
    self.mapView = [[MapView alloc] initWithDelegate:self];
    [self.view addSubview:_mapView];
    //self.mapView.mapView.showsUserLocation = YES;
    [_mapView setFrame:self.view.bounds];
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(10, 70, kSCREEN_WIDTH-20, 50)];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.shadowColor = [UIColor grayColor].CGColor;
    backView.layer.shadowOffset = CGSizeMake(0, 0);
    backView.layer.shadowOpacity = 1.0f;
    [self.view addSubview:backView];
    UIButton *search = [UIButton buttonWithType:UIButtonTypeCustom];
    [search setTitle:@"搜索人员、站点" forState:UIControlStateNormal];
    [search setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    search.frame = CGRectMake(10, 0, kSCREEN_WIDTH-80, 50);
    search.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [search addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:search];
    UIImageView *searchImage = [[UIImageView alloc]init];
    searchImage.image = [[UIImage imageNamed:@"seach_icon_b"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [backView addSubview:searchImage];
    [searchImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(search.mas_right).offset(0);
        make.centerY.equalTo(backView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];

}

- (void)searchClick{
    

    MapSearchVC *vc = [[MapSearchVC alloc]init];
    vc.Delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    [self.StationDTview removeFromSuperview];
    [self.NagihView removeFromSuperview];
    [_AllStaionView removeFromSuperview];

}
#pragma mark -
#pragma mark delegate

- (NSInteger)numbersWithCalloutViewForMapView
{

    return self.annotations.count;
}

- (CLLocationCoordinate2D)coordinateForMapViewWithIndex:(NSInteger)index
{
    Item *item = [[Item alloc] initWithDictionary:[self.annotations objectAtIndex:index]];
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [item.latitude doubleValue];
    coordinate.longitude = [item.longitude doubleValue];
    return coordinate;
}

- (UIImage *)baseMKAnnotationViewImageWithIndex:(NSInteger)index
{
    //2 人员  0 自己 3 站点
   
    if ([self.annotations[index][@"state"]integerValue] == 2) {
        return [[UIImage imageNamed:@"user_a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
    }else if ([self.annotations[index][@"state"]integerValue]==3){
        
        return [[UIImage imageNamed:@"site_a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else{
        
        return [[UIImage imageNamed:@"position"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
}

- (UIView *)mapViewCalloutContentViewWithIndex:(NSInteger)index
{
    
//2 人员 1分公司 0 自己 3 站点

    if ([self.annotations[index][@"state"]integerValue]==2) {
            Item *item = [[Item alloc] initWithDictionary:[self.annotations objectAtIndex:index]];
        testMapPsonCell *cell = [[[NSBundle mainBundle]loadNibNamed:@"testMapPsonCell" owner:self options:nil]objectAtIndex:0];
        cell.layer.cornerRadius = 10;
        cell.title.text = item.title;
        cell.LastUpTime.text = item.time;
        if (_NeigBtnTag != 0) {
            cell.subtitle.text = item.address;
        }else{
            cell.subtitle.text = item.subtitle;
        }
        cell.phoneNum.text = [NSString stringWithFormat:@"Tel:%@",item.phone];
        return cell;
    }else{
    Item *item = [[Item alloc] initWithDictionary:[self.annotations objectAtIndex:index]];
    TestMapCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TestMapCell" owner:self options:nil] objectAtIndex:0];
    cell.title.text = item.title;
    cell.subtitle.text = item.subtitle;
        return cell;
    }
}

- (void)calloutViewDidSelectedWithIndex:(NSInteger)index
{
    NSSLog(@"dianjile");

}
- (void)AnnotationViewHiden{
   
    [self close];
}
- (void)AnnotationViewShow:(NSInteger)index{
    
   
    if (_dictionary != nil && _NeigBtnTag != 2002) {
        [self initStationview];
    }else{
        [self initNagiView];
    }
}
//搜索返回
- (void)displayData:(NSDictionary *)dic
{

   //2 人员 1分公司 0 自己 3 站点
    [self.annotations removeAllObjects];
    [self.mapView removeFromSuperview];
    _dictionary = dic;
    [self.annotations addObject:dic];
    
    
    [self initMapview];
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake([dic[@"latitude"]doubleValue], [dic[@"longitude"]doubleValue]);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center,10000 ,10000 );
    MKCoordinateRegion adjustedRegion = [_mapView.mapView regionThatFits:region];
    [_mapView.mapView setRegion:adjustedRegion animated:YES];

    [_mapView beginLoad];

}

//实例化返回时底部弹出框框
- (void)initStationview{
    
    self.StationDTview = [[UIView alloc]init];
    self.StationDTview.backgroundColor = [UIColor whiteColor];
    self.StationDTview.layer.shadowColor = [UIColor grayColor].CGColor;
    self.StationDTview.layer.shadowOffset = CGSizeMake(0, 0);
    self.StationDTview.layer.shadowOpacity = 1.0f;
    [self.view addSubview:self.StationDTview];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[[UIImage imageNamed:@"close_0102"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.StationDTview addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.StationDTview).offset(10);
        make.right.equalTo(self.StationDTview.mas_right).offset(-5);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        icon.image = [[UIImage imageNamed:@"avatar"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else if([_dictionary[@"state"]integerValue] == 3){
        icon.image = [[UIImage imageNamed:@"site_01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }

    [self.StationDTview addSubview:icon];
    UILabel *stationName = [[UILabel alloc]initWithFrame:CGRectZero];
    stationName.text = _dictionary[@"title"];
    stationName.numberOfLines = 0;
    stationName.textColor = UIColorFromRGB(0x0093d5);
    CGRect rect = [stationName.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-60, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:stationName.font,NSFontAttributeName, nil] context:nil];
    CGFloat height = rect.size.height;
    if ([_dictionary[@"state"]integerValue] == 2) {
        stationName.frame = CGRectMake(35, 10, 80, 21);
    }else{
        stationName.frame = CGRectMake(35, 10, kSCREEN_WIDTH-60, height);
    }
    [_StationDTview addSubview:stationName];
    //人员存在岗位标示
    if ([_dictionary[@"state"]integerValue] == 2) {
        UILabel *postLab = [[UILabel alloc]init];
        postLab.text = [NSString stringWithFormat:@"%c%@%c",'(',_dictionary[@"post"],')'];
        postLab.font = [UIFont systemFontOfSize:15.0];
        postLab.textColor = UIColorFromRGB(0x4d4d4d);
        [_StationDTview addSubview:postLab];
        [postLab mas_makeConstraints:^(MASConstraintMaker *make) {
            //make.top.equalTo(_StationDTview).offset(10);
            make.centerY.equalTo(stationName);
            make.left.equalTo(stationName.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(120, 21));
        }];
    }
    //左边标题名字
    UILabel *leftT1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(stationName.frame)+5, kSCREEN_WIDTH/4+20, 21)];
    leftT1.text = @"所属运营中心";
    leftT1.font = [UIFont systemFontOfSize:15.0];
    leftT1.textColor = UIColorFromRGB(0x4d4d4d);
    leftT1.textAlignment = NSTextAlignmentRight;
    [self.StationDTview addSubview:leftT1];
    
    UILabel *leftT2 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT1.frame)+10,kSCREEN_WIDTH/4+20 , 21)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        leftT2.text = @"负责站点数";

    }else if([_dictionary[@"state"]integerValue] == 3){

        leftT2.text = @"负责人";
    }

    leftT2.font = [UIFont systemFontOfSize:15.0];
    leftT2.textColor = UIColorFromRGB(0x4d4d4d);
    leftT2.textAlignment = NSTextAlignmentRight;
    [self.StationDTview addSubview:leftT2];

    //联系方式
    UILabel *leftT4 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT2.frame)+10,kSCREEN_WIDTH/4+20 , 21)];
    /*if ([_dictionary[@"state"]integerValue] == 2) {
        
        leftT4.text = @"当前任务";
        leftT4.textColor = UIColorFromRGB(0xf0f0f0);
        
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        leftT4.text = @"联系方式";
        leftT4.textColor = UIColorFromRGB(0x4d4d4d);
    }*/
    leftT4.text = @"联系方式";
    leftT4.textColor = UIColorFromRGB(0x4d4d4d);
    leftT4.font = [UIFont systemFontOfSize:15.0];
    //leftT4.textColor = UIColorFromRGB(0x4d4d4d);
    leftT4.textAlignment = NSTextAlignmentRight;
    [self.StationDTview addSubview:leftT4];

    
    UILabel *leftT3 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT4.frame)+10, kSCREEN_WIDTH/4+20, 21)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        leftT3.text = @"当前任务";
        leftT3.textColor = UIColorFromRGB(0xf0f0f0);
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        leftT3.text = @"详细地址";
        leftT3.textColor = UIColorFromRGB(0x4d4d4d);
    }
    leftT3.font = [UIFont systemFontOfSize:15.0];
    //leftT3.textColor = UIColorFromRGB(0x4d4d4d);
    leftT3.textAlignment = NSTextAlignmentRight;
    [self.StationDTview addSubview:leftT3];

    //右边内容
    UILabel *content1 = [[UILabel alloc]initWithFrame:CGRectMake(leftT1.frame.size.width+10, CGRectGetMaxY(stationName.frame)+5, 200, 21)];
    if ([_dictionary[@"state"]integerValue] == 3) {
        content1.text = _dictionary[@"opcenter"];
    }else if([_dictionary[@"state"]integerValue] == 2){
        content1.text = _dictionary[@"subtitle"];
    }
    
    content1.font = [UIFont systemFontOfSize:15.0];
    content1.textColor = UIColorFromRGB(0x4d4d4d);
    [self.StationDTview addSubview:content1];

    UILabel *content2 = [[UILabel alloc]initWithFrame:CGRectMake(leftT2.frame.size.width+10, CGRectGetMaxY(content1.frame)+10, 60, 21)];
    if ([_dictionary[@"state"]integerValue] == 3) {
        
        content2.text = _dictionary[@"pson"];
        
    }else if([_dictionary[@"state"]integerValue] == 2){
        
        content2.text = _dictionary[@"stationNum"];
    }
    content2.font = [UIFont systemFontOfSize:15.0];
    content2.textColor = UIColorFromRGB(0x4d4d4d);
    [self.StationDTview addSubview:content2];

    UILabel *content4 = [[UILabel alloc]initWithFrame:CGRectMake(leftT4.frame.size.width+10, CGRectGetMaxY(content2.frame)+10, 120, 21)];
    /*if ([_dictionary[@"state"]integerValue] == 2) {
        
        content4.text = @"待巡检站点[5]";
        content4.textColor = UIColorFromRGB(0xf0f0f0);
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        content4.text = _dictionary[@"phone"];
        content4.textColor = UIColorFromRGB(0x4d4d4d);
    }*/
    content4.text = _dictionary[@"phone"];
    content4.textColor = UIColorFromRGB(0x4d4d4d);
   // content4.textColor = UIColorFromRGB(0x4d4d4d);
    content4.font = [UIFont systemFontOfSize:15.0];
    [self.StationDTview addSubview:content4];

    
    UILabel *content3 = [[UILabel alloc]initWithFrame:CGRectZero];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        content3.text = @"待巡检站点[5]";
        content3.textColor = UIColorFromRGB(0xf0f0f0);

        
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        content3.text = _dictionary[@"adress"];
        content3.textColor = UIColorFromRGB(0x4d4d4d);
    }
    content3.font = [UIFont systemFontOfSize:15.0];
    //content3.textColor = UIColorFromRGB(0x4d4d4d);
    content3.numberOfLines = 0;
    CGRect rect1 = [content3.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:content3.font,NSFontAttributeName, nil] context:nil];
    CGFloat height1 = rect1.size.height;
    content3.frame = CGRectMake(leftT3.frame.size.width+10, CGRectGetMaxY(content4.frame)+10, kSCREEN_WIDTH/4*3-20, height1);
    [self.StationDTview addSubview:content3];

    //横线
    for (NSInteger i = 0; i < 4; ++i) {
     
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(stationName.frame) + i * 30, kSCREEN_WIDTH, 1)];
            line.backgroundColor = UIColorFromRGB(0xf0f0f0);
            [self.StationDTview addSubview:line];
        
    }
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(content3.frame)+5, kSCREEN_WIDTH, 1)];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.StationDTview addSubview:line2];

    
    //竖线
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(kSCREEN_WIDTH/4+25, CGRectGetMaxY(stationName.frame), 1, 107 +content3.frame.size.height)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.StationDTview addSubview:line1];
    //附近的站点
    _NeigBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        [_NeigBtn setTitle:@"附近站点" forState:UIControlStateNormal];
        _NeigBtnTag = 2001;
    }else if([_dictionary[@"state"]integerValue] == 3){
        _NeigBtnTag = 2002;
        [_NeigBtn setTitle:@"附近的人" forState:UIControlStateNormal];
    }
    _NeigBtn.frame = CGRectMake((kSCREEN_WIDTH-80)/2,CGRectGetMaxY(content3.frame)+10, 80, 30);
    [_NeigBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    [_NeigBtn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [self.StationDTview addSubview:_NeigBtn];

    
    
    UIImageView *icon1 = [[UIImageView alloc]init];
    icon1.image = [UIImage imageNamed:@"position"];
    [self.StationDTview addSubview:icon1];
    [icon1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_NeigBtn.mas_left).offset(-10);
        make.centerY.equalTo(_NeigBtn);
        make.size.mas_equalTo(CGSizeMake(21, 28));
    }];
    
    self.StationDTview.frame = CGRectMake(0, kSCREEN_HEIGHT-CGRectGetMaxY(_NeigBtn.frame)-10, kSCREEN_WIDTH, CGRectGetMaxY(_NeigBtn.frame)+10);
    
    
}
//附近站点or人员按键
- (void)next{
    
    [self.StationDTview removeFromSuperview];
    [self.NagihView removeFromSuperview];
    _AllStaionView = [[UIView alloc]initWithFrame:CGRectMake(0, kSCREEN_HEIGHT/3*2+50, kSCREEN_WIDTH,kSCREEN_HEIGHT/3-50)];
    _AllStaionView.backgroundColor = [UIColor whiteColor];
    _AllStaionView.layer.shadowColor = [UIColor grayColor].CGColor;
    _AllStaionView.layer.shadowOffset = CGSizeMake(0, 0);
    _AllStaionView.layer.shadowOpacity = 1.0f;
    [self.view addSubview:_AllStaionView];
    /*
    if (_naigPsonArr.count == 0) {
        UILabel *tishi = [[UILabel alloc]init];
        tishi.text = @"没有您要的信息";
        tishi.textAlignment = NSTextAlignmentCenter;
        [_AllStaionView addSubview:tishi];
        [tishi mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_AllStaionView);
            make.left.equalTo(_AllStaionView).offset(0);
            make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 21));
        }];
    }else{*/
        _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 45, kSCREEN_WIDTH, _AllStaionView.frame.size.height-45) style:UITableViewStylePlain];
        _tableview.delegate = self;
        _tableview.dataSource = self;
        _tableview.tableFooterView = [[UIView alloc]init];
    //}

//    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 45)];
//    headerView.backgroundColor = [UIColor whiteColor];
    UIButton *backbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backbtn setImage:[[UIImage imageNamed:@"return_0101"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [backbtn addTarget:self action:@selector(backStationview) forControlEvents:UIControlEventTouchUpInside];
    [_AllStaionView addSubview:backbtn];
    [backbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_AllStaionView).offset(10);
        make.left.equalTo(_AllStaionView).offset(10);
        make.size.mas_equalTo(CGSizeMake(10, 20));
    }];
    UIButton *closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closebtn setImage:[[UIImage imageNamed:@"close_0102"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [closebtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [_AllStaionView addSubview:closebtn];
    [closebtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_AllStaionView).offset(7);
        make.right.equalTo(_AllStaionView.mas_right).offset(-5);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    UIImageView *icon = [[UIImageView alloc]init];
    icon.image = [[UIImage imageNamed:@"avatar"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_AllStaionView addSubview:icon];
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backbtn);
        make.left.equalTo(backbtn.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(21, 21));
    }];
    UILabel *heardTitle = [[UILabel alloc]init];
    heardTitle.text = _NeigBtn.titleLabel.text;
    heardTitle.textColor = UIColorFromRGB(0x0093d5);
    [_AllStaionView addSubview:heardTitle];
    [heardTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(10);
        make.top.equalTo(_AllStaionView).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 21));
    }];
    //self.tableview.tableHeaderView = headerView;
    [_AllStaionView addSubview:_tableview];

//    if (_naigPsonArr.count == 0) {
//        [self showAlert:@"温馨提示" message:@"没有您要的数据" actionWithTitle:nil];
//    }
    //2001 附近的站点 2002 附近的人
    [self neigNetworking];
}
- (void)neigNetworking{
    //lat=21.3596&lnt=110.254&userId=1103
    CLLocation *orig = [[CLLocation alloc]initWithLatitude:[_dictionary[@"latitude"]doubleValue] longitude:[_dictionary[@"longitude"]doubleValue]];
    if (_NeigBtnTag == 2001) {
        _NeigURL = K_neigPsonURL;
    }else if (_NeigBtnTag == 2002){
        _NeigURL = K_neigStaionURL;
    }else{
        
    }
    MBProgressHUD *HUD1 = [MBProgressHUD showHUDAddedTo:_AllStaionView animated:YES];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 15;
    [_manager POST:_NeigURL parameters:@{@"lat":[NSString stringWithFormat:@"%f",[_dictionary[@"latitude"]doubleValue]],@"lnt": [NSString stringWithFormat:@"%f",[_dictionary[@"longitude"]doubleValue]],@"userId":[NSString stringWithFormat:@"%ld",self.userID]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [_naigStationArr removeAllObjects];
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (_NeigBtnTag == 2001) {
            YYmapStationDataModel *StationModel = [[YYmapStationDataModel alloc]initWithDictionary:dic[@"data"] error:nil];
            for (YYstationListModel *model in StationModel.beanList) {
                NSDictionary *stationDic = @{@"latitude":[NSString stringWithFormat:@"%f",model.lat],
                                             @"longitude":[NSString stringWithFormat:@"%f",model.lnt],
                                             @"phone":model.phone,
                                             @"title":model.stationName,
                                             @"subtitle":model.address,
                                             @"pson":model.principalName,
                                             @"opcenter":model.opCenName,
                                             @"adress":model.address,
                                             @"state":@3};
                [self.annotations addObject:stationDic];
                NaigModel *Nmodel = [[NaigModel alloc]init];
                Nmodel.lat = model.lat;
                Nmodel.lnt = model.lnt;
                Nmodel.phone = model.phone;
                Nmodel.title = model.stationName;
                Nmodel.subtitle = model.address;
                Nmodel.pson = model.principalName;
                Nmodel.opcenter = model.opCenName;
                Nmodel.adress = model.address;
                Nmodel.sationN = 0;
                Nmodel.state = 3;
                CLLocation *dist = [[CLLocation alloc]initWithLatitude:model.lat longitude:model.lnt];
                CLLocationDistance kilometers = [orig distanceFromLocation:dist]/1000;
                if (kilometers < 50 || kilometers == 50){

                    Nmodel.distance = kilometers;
                }

                [_naigStationArr addObject:Nmodel];
            }
        }else{
            
            YYmapDatamodel *datamodel = [[YYmapDatamodel alloc]initWithDictionary:dic[@"data"] error:nil];
            for (YYmapPsonListModel *model in datamodel.beanList) {
                NSDictionary *stationDic = @{@"latitude":[NSString stringWithFormat:@"%f",model.lat],
                                             @"longitude":[NSString stringWithFormat:@"%f",model.lnt],
                                             @"phone":model.phone,
                                             @"title":model.username,
                                             @"subtitle":model.address,
                                             @"pson":model.post,
                                             @"opcenter":model.opCenName,
                                             @"adress":model.address,
                                             @"state":@2};
                [self.annotations addObject:stationDic];
                NaigModel *Nmodel = [[NaigModel alloc]init];
                Nmodel.lat = model.lat;
                Nmodel.lnt = model.lnt;
                Nmodel.phone = model.phone;
                Nmodel.title = model.username;
                Nmodel.subtitle = model.address;
                Nmodel.pson = model.post;
                Nmodel.opcenter = model.opCenName;
                Nmodel.adress = model.address;
                Nmodel.sationN = model.stationNum;
                Nmodel.state = 2;
                CLLocation *dist = [[CLLocation alloc]initWithLatitude:model.lat longitude:model.lnt];
                CLLocationDistance kilometers = [orig distanceFromLocation:dist]/1000;
                if (kilometers < 50 || kilometers == 50){
                    
                    Nmodel.distance = kilometers;
                }
                
                [_naigStationArr addObject:Nmodel];
            }
        }
        if (_naigStationArr.count == 0 && _NeigBtnTag == 2001) {
            UILabel *lab = [[UILabel alloc]init];
            lab.text = @"没有附近的站点";
            lab.textColor = UIColorFromRGB(0x4d4d4d);
            lab.textAlignment = NSTextAlignmentCenter;
            [_AllStaionView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(_AllStaionView);
                make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 21));
            }];
        }else if (_naigStationArr.count == 0 && _NeigBtnTag == 2002){
            UILabel *lab = [[UILabel alloc]init];
            lab.text = @"没有附近的人";
            lab.textAlignment = NSTextAlignmentCenter;
            lab.textColor = UIColorFromRGB(0x4d4d4d);
            [_AllStaionView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(_AllStaionView);
                make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 21));
            }];
        }else{
        }
        //NSSLog(@"%@",_naigStationArr);
        [_mapView beginLoad];
        [self.tableview reloadData];
        [MBProgressHUD hideHUDForView:_AllStaionView animated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#pragma mark-->tablebiew

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 44.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    

        return _naigStationArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NaigCell *cell = [[NaigCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    cell.backgroundColor = UIColorFromRGB(0xf0f0f0);
    
    NaigModel *model = _naigStationArr[indexPath.row];
   
    [cell fillCellWithModel:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
/*2001
 Nmodel.lat = model.lat;
 Nmodel.lnt = model.lnt;
 Nmodel.phone = model.phone;
 Nmodel.title = model.stationName;
 Nmodel.subtitle = model.address;
 Nmodel.pson = model.principalName;
 Nmodel.opcenter = model.opCenName;
 Nmodel.adress = model.address;
 Nmodel.state = 3;
 */
    /*
     2002
     Nmodel.lat = model.lat;
     Nmodel.lnt = model.lnt;
     Nmodel.phone = model.phone;
     Nmodel.title = model.username;
     Nmodel.subtitle = model.address;
     Nmodel.pson = model.post;
     Nmodel.opcenter = model.opCenName;
     Nmodel.adress = model.address;
     Nmodel.state = 2;
     */
    NaigModel *model = _naigStationArr[indexPath.row];
    if (_NeigBtnTag == 2001) {
        NSDictionary *dic = @{@"latitude":[NSString stringWithFormat:@"%f",model.lat],
                              @"longitude":[NSString stringWithFormat:@"%f",model.lnt],
                              @"phone":model.phone,
                              @"title":model.title,
                              @"subtitle":model.adress,
                              @"pson":model.pson,
                              @"opcenter":model.opcenter,
                              @"adress":model.adress,
                              @"state":@3};
        _dictionary = dic;
    }else{
        NSDictionary *dic = @{@"latitude":[NSString stringWithFormat:@"%f",model.lat],
                              @"longitude":[NSString stringWithFormat:@"%f",model.lnt],
                              @"phone":model.phone,
                              @"title":model.title,
                              @"subtitle":[NSString stringWithFormat:@"%ld",model.sationN],
                              @"pson":model.pson,
                              @"opcenter":model.opcenter,
                              @"adress":model.adress,
                              @"state":@2};
        _dictionary = dic;
    }
    
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(model.lat, model.lnt);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center,10000 ,10000 );
    MKCoordinateRegion adjustedRegion = [_mapView.mapView regionThatFits:region];
    [_mapView.mapView setRegion:adjustedRegion animated:YES];

    [_mapView beginLoad];
    [self close];
}
//附近
- (void)initNagiView{
  
    self.NagihView = [[UIView alloc]init];
    self.NagihView.backgroundColor = [UIColor whiteColor];
    self.NagihView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.NagihView.layer.shadowOffset = CGSizeMake(0, 0);
    self.NagihView.layer.shadowOpacity = 1.0f;
    [self.view addSubview:self.NagihView];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[[UIImage imageNamed:@"close_0102"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.NagihView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.NagihView).offset(10);
        make.right.equalTo(self.NagihView.mas_right).offset(-5);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        icon.image = [[UIImage imageNamed:@"avatar"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else if([_dictionary[@"state"]integerValue] == 3){
        icon.image = [[UIImage imageNamed:@"site_01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    [self.NagihView addSubview:icon];
    UILabel *stationName = [[UILabel alloc]initWithFrame:CGRectZero];
    stationName.text = _dictionary[@"title"];
    stationName.numberOfLines = 0;
    stationName.textColor = UIColorFromRGB(0x0093d5);
    CGRect rect = [stationName.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-60, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:stationName.font,NSFontAttributeName, nil] context:nil];
    CGFloat height = rect.size.height;
    if ([_dictionary[@"state"]integerValue] == 2) {
        stationName.frame = CGRectMake(35, 10, 80, 21);
    }else{
        stationName.frame = CGRectMake(35, 10, kSCREEN_WIDTH-60, height);
    }
    [self.NagihView addSubview:stationName];
    //人员存在岗位标示
    if ([_dictionary[@"state"]integerValue] == 2) {
        UILabel *postLab = [[UILabel alloc]init];
        postLab.text = [NSString stringWithFormat:@"%c%@%c",'(',_dictionary[@"pson"],')'];
        postLab.font = [UIFont systemFontOfSize:15.0];
        postLab.textColor = UIColorFromRGB(0x4d4d4d);
        [self.NagihView addSubview:postLab];
        [postLab mas_makeConstraints:^(MASConstraintMaker *make) {
            //make.top.equalTo(_StationDTview).offset(10);
            make.centerY.equalTo(stationName);
            make.left.equalTo(stationName.mas_right).offset(5);
            make.size.mas_equalTo(CGSizeMake(120, 21));
        }];
    }
    //左边标题名字
    UILabel *leftT1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(stationName.frame)+5, kSCREEN_WIDTH/4+20, 21)];
    leftT1.text = @"所属运营中心";
    leftT1.font = [UIFont systemFontOfSize:15.0];
    leftT1.textColor = UIColorFromRGB(0x4d4d4d);
    leftT1.textAlignment = NSTextAlignmentRight;
    [self.NagihView addSubview:leftT1];
    
    UILabel *leftT2 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT1.frame)+10,kSCREEN_WIDTH/4+20 , 21)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        leftT2.text = @"负责站点数";
        
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        leftT2.text = @"负责人";
    }
    
    leftT2.font = [UIFont systemFontOfSize:15.0];
    leftT2.textColor = UIColorFromRGB(0x4d4d4d);
    leftT2.textAlignment = NSTextAlignmentRight;
    [self.NagihView addSubview:leftT2];
    
    //联系方式
    UILabel *leftT4 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT2.frame)+10,kSCREEN_WIDTH/4+20 , 21)];
    /*if ([_dictionary[@"state"]integerValue] == 2) {
     
     leftT4.text = @"当前任务";
     leftT4.textColor = UIColorFromRGB(0xf0f0f0);
     
     }else if([_dictionary[@"state"]integerValue] == 3){
     
     leftT4.text = @"联系方式";
     leftT4.textColor = UIColorFromRGB(0x4d4d4d);
     }*/
    leftT4.text = @"联系方式";
    leftT4.textColor = UIColorFromRGB(0x4d4d4d);
    leftT4.font = [UIFont systemFontOfSize:15.0];
    //leftT4.textColor = UIColorFromRGB(0x4d4d4d);
    leftT4.textAlignment = NSTextAlignmentRight;
    [self.NagihView addSubview:leftT4];
    
    
    UILabel *leftT3 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leftT4.frame)+10, kSCREEN_WIDTH/4+20, 21)];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        leftT3.text = @"当前任务";
        leftT3.textColor = UIColorFromRGB(0xf0f0f0);
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        leftT3.text = @"详细地址";
        leftT3.textColor = UIColorFromRGB(0x4d4d4d);
    }
    leftT3.font = [UIFont systemFontOfSize:15.0];
    //leftT3.textColor = UIColorFromRGB(0x4d4d4d);
    leftT3.textAlignment = NSTextAlignmentRight;
    [self.NagihView addSubview:leftT3];
    
    //右边内容
    UILabel *content1 = [[UILabel alloc]initWithFrame:CGRectMake(leftT1.frame.size.width+10, CGRectGetMaxY(stationName.frame)+5, 200, 21)];
    if ([_dictionary[@"state"]integerValue] == 3) {
        content1.text = _dictionary[@"opcenter"];
    }else if([_dictionary[@"state"]integerValue] == 2){
        content1.text = _dictionary[@"opcenter"];
    }
    
    content1.font = [UIFont systemFontOfSize:15.0];
    content1.textColor = UIColorFromRGB(0x4d4d4d);
    [self.NagihView addSubview:content1];
    
    UILabel *content2 = [[UILabel alloc]initWithFrame:CGRectMake(leftT2.frame.size.width+10, CGRectGetMaxY(content1.frame)+10, 60, 21)];
    if ([_dictionary[@"state"]integerValue] == 3) {
        
        content2.text = _dictionary[@"pson"];
        
    }else if([_dictionary[@"state"]integerValue] == 2){
        
        content2.text = _dictionary[@"subtitle"];
    }
    content2.font = [UIFont systemFontOfSize:15.0];
    content2.textColor = UIColorFromRGB(0x4d4d4d);
    [self.NagihView addSubview:content2];
    
    UILabel *content4 = [[UILabel alloc]initWithFrame:CGRectMake(leftT4.frame.size.width+10, CGRectGetMaxY(content2.frame)+10, 120, 21)];
    /*if ([_dictionary[@"state"]integerValue] == 2) {
     
     content4.text = @"待巡检站点[5]";
     content4.textColor = UIColorFromRGB(0xf0f0f0);
     }else if([_dictionary[@"state"]integerValue] == 3){
     
     content4.text = _dictionary[@"phone"];
     content4.textColor = UIColorFromRGB(0x4d4d4d);
     }*/
    content4.text = _dictionary[@"phone"];
    content4.textColor = UIColorFromRGB(0x4d4d4d);
    // content4.textColor = UIColorFromRGB(0x4d4d4d);
    content4.font = [UIFont systemFontOfSize:15.0];
    [self.NagihView addSubview:content4];
    
    
    UILabel *content3 = [[UILabel alloc]initWithFrame:CGRectZero];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        content3.text = @"待巡检站点[5]";
        content3.textColor = UIColorFromRGB(0xf0f0f0);
        
        
    }else if([_dictionary[@"state"]integerValue] == 3){
        
        content3.text = _dictionary[@"adress"];
        content3.textColor = UIColorFromRGB(0x4d4d4d);
    }
    content3.font = [UIFont systemFontOfSize:15.0];
    //content3.textColor = UIColorFromRGB(0x4d4d4d);
    content3.numberOfLines = 0;
    CGRect rect1 = [content3.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH/4*3-20, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:content3.font,NSFontAttributeName, nil] context:nil];
    CGFloat height1 = rect1.size.height;
    content3.frame = CGRectMake(leftT3.frame.size.width+10, CGRectGetMaxY(content4.frame)+10, kSCREEN_WIDTH/4*3-20, height1);
    [self.NagihView addSubview:content3];
    
    //横线
    for (NSInteger i = 0; i < 4; ++i) {
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(stationName.frame) + i * 30, kSCREEN_WIDTH, 1)];
        line.backgroundColor = UIColorFromRGB(0xf0f0f0);
        [self.NagihView addSubview:line];
        
    }
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(content3.frame)+5, kSCREEN_WIDTH, 1)];
    line2.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.NagihView addSubview:line2];
    
    
    //竖线
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(kSCREEN_WIDTH/4+25, CGRectGetMaxY(stationName.frame), 1, 107 +content3.frame.size.height)];
    line1.backgroundColor = UIColorFromRGB(0xf0f0f0);
    [self.NagihView addSubview:line1];
    //附近的站点
    _NeigBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([_dictionary[@"state"]integerValue] == 2) {
        
        [_NeigBtn setTitle:@"附近站点" forState:UIControlStateNormal];
        _NeigBtnTag = 2001;
    }else if([_dictionary[@"state"]integerValue] == 3){
        _NeigBtnTag = 2002;
        [_NeigBtn setTitle:@"附近的人" forState:UIControlStateNormal];
    }
    _NeigBtn.frame = CGRectMake((kSCREEN_WIDTH-80)/2,CGRectGetMaxY(content3.frame)+10, 80, 30);
    [_NeigBtn setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
    [_NeigBtn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [self.NagihView addSubview:_NeigBtn];
    
    
    
    UIImageView *icon1 = [[UIImageView alloc]init];
    icon1.image = [UIImage imageNamed:@"position"];
    [self.NagihView addSubview:icon1];
    [icon1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_NeigBtn.mas_left).offset(-10);
        make.centerY.equalTo(_NeigBtn);
        make.size.mas_equalTo(CGSizeMake(21, 28));
    }];
    
    self.NagihView.frame = CGRectMake(0, kSCREEN_HEIGHT-CGRectGetMaxY(_NeigBtn.frame)-10, kSCREEN_WIDTH, CGRectGetMaxY(_NeigBtn.frame)+10);

}
//返回上一级view移除当前view
- (void)backStationview{
    
    [_AllStaionView removeFromSuperview];
    [self initStationview];
}
//删除按键
- (void)close{
    
    [_AllStaionView removeFromSuperview];
    [self.StationDTview removeFromSuperview];
    [self.NagihView removeFromSuperview];
}
#pragma mark 封装提示框
- (void)showAlert:(NSString *)Title message:(NSString *)message actionWithTitle:(NSString *)actionWithTitle{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:Title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:actionWithTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)backVC{

    [self.navigationController popViewControllerAnimated:YES];
}
@end
