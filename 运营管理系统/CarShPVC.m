//
//  CarShPVC.m
//  运营管理系统
//
//  Created by 杨毅 on 16/10/11.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "CarShPVC.h"
#import "Masonry.h"
#import "AFNetworking.h"
#import "BSMassageModel.h"
#import "ZJSwitch.h"
#import "AppDelegate.h"
#import "YXKit.h"
#import "CarfenPei.h"
#import "LewPopupViewController.h"
#import "KSDatePicker.h"//日历选择器
#import "CustomPopOverView.h"
@interface CarShPVC ()<CarfenPeiDelegate,CustomPopOverViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_backView3;
    UIView *_backView4;
    UIView *_backView5;
    UIView *_backView6;
    UIView *_backView7;
    
    UIView *_backView8;
    UIView *_backView9;
    ZJSwitch *_swi;
    
    NSArray *_content_arr;
    UITextView *_bztextView;//审核备注信息
    NSInteger state;//上报状态
    UIButton *_btn;
    
    CarfenPei *_view;
    NSArray *_titleMeus;//传给view的数据
    NSInteger carS;//派发车状态 0 不派发 1派发
    
    NSMutableArray *_carid;
    NSMutableArray *_carBandid;
    NSMutableArray *_vehicleModel_arr;
    NSMutableArray *_carname;
    NSMutableArray *_carBand;
    NSInteger tag;
    NSInteger indexPath;
    
    //提交参数 车辆ID 车辆型号ID
    NSInteger Cid;
    NSInteger Mid;

}
@property(nonatomic,strong)AFHTTPSessionManager *manager;
@end

@implementation CarShPVC
- (AFHTTPSessionManager *)manager{
    
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"车辆审批确认";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIApplication *app = [UIApplication sharedApplication];
    AppDelegate *delegate = app.delegate;
    self.ID = delegate.userId;
    self.level = delegate.level;
    self.userN = delegate.username;
    carS = 0;
}
#if 0
- (void)starLoad{
    

    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_carShBasMas parameters:@{carid:[NSString stringWithFormat:@"%ld",self.ID]} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dic);
        
        NSDictionary *xq = dic[@"data"][@"xq"];
        _content_arr = @[xq[@"pson"],@"运营管理部",xq[@"CarBrand"],xq[@"CarModel"],xq[@"remark"]];
        //_apid = [fau[@"ApPersonId"]integerValue];
        _titleMeus = @[@"",@"",@"",@"",self.userN];
        [self initScorlleview];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
#endif
- (void)initScorlleview{
    
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-120);
    _scrollView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _scrollView.contentSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT+100);
    [self.view addSubview:_scrollView];
    // 隐藏水平滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    //故障基本信息背景
    UIView *backView1 = [[UIView alloc]init];
    backView1.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView1];
    [backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_scrollView).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 50));
    }];
    //申用基本信息图标 d_icon01   编辑图标d_icon04 大小30*30
    UIImageView *gzMimageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    gzMimageView.image = [[UIImage imageNamed:@"d_icon01"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [backView1 addSubview:gzMimageView];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(45, 15, 150, 21)];
    title.text = @"申用基本信息";
    title.font = [UIFont systemFontOfSize:21.0];
    title.textColor = UIColorFromRGB(0x0093d5);
    [backView1 addSubview:title];
    
    //基本信息详情背景
    UIView *backView2 = [[UIView alloc]init];
    backView2.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView1.mas_bottom).offset(2);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH,160));
    }];
    
    
    NSArray *leftTitle_arr = @[@"申请人",@"部门",@"车辆品牌",@"车辆型号",@"申用备注"];
    
    
    for (NSInteger i = 0; i < leftTitle_arr.count; ++i) {
        
        
        UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 10 + i * (21+10), 80, 21)];
        titleName.font = [UIFont systemFontOfSize:19.0];
        titleName.textColor = UIColorFromRGB(0x4d4d4d);
        titleName.text = leftTitle_arr[i];
        [backView2 addSubview:titleName];

            UILabel *content = [[UILabel alloc]initWithFrame:CGRectMake(100, 5 + i * (30+1), kSCREEN_WIDTH/4*3, 30)];
            content.text = _content_arr[i];
            content.backgroundColor = UIColorFromRGB(0xf0f0f0);
            content.font = [UIFont systemFontOfSize:19.0];
            content.textColor = UIColorFromRGB(0x4d4d4d);
            [backView2 addSubview:content];
        
    }
    //故障审批审核
    _backView3 = [[UIView alloc]init];
    _backView3.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView3];
    [_backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView2.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 50));
    }];
    UIImageView *editImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    editImageView.image = [[UIImage imageNamed:@"d_icon04"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_backView3 addSubview:editImageView];
    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(45, 15, 150, 21)];
    title2.text = @"故障审核审批";
    title2.font = [UIFont systemFontOfSize:21.0];
    title2.textColor = UIColorFromRGB(0x0093d5);
    [_backView3 addSubview:title2];
    
    
    //审核审批具体信息背景
    _backView4 = [[UIView alloc]init];
    _backView4.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView4];
    [_backView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView3.mas_bottom).offset(2);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 77));
    }];
    NSArray *leftT = @[@"是否批准",@"审核时间"];
    for (NSInteger i = 0; i < leftT.count; ++i) {
        UILabel *titleName = [[UILabel alloc]initWithFrame:CGRectMake(15, 10 + i * (21+15), 80, 21)];
        titleName.font = [UIFont systemFontOfSize:19.0];
        titleName.textColor = UIColorFromRGB(0x4d4d4d);
        titleName.text = leftT[i];
        [_backView4 addSubview:titleName];
        if (i == 0) {
            
            _swi = [[ZJSwitch alloc]initWithFrame:CGRectMake(100, 5, 60, 31)];
            _swi.tintColor = [UIColor lightGrayColor];
            _swi.onText = @"是";
            _swi.offText = @"否";
            [_swi addTarget:self action:@selector(handleSwitch:) forControlEvents:UIControlEventValueChanged];
            [_backView4 addSubview:_swi];
        }else{
            
            NSDate *currentDate = [NSDate date];//获取当前时间，日期
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
            UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(100, 5 + i * (31+5), kSCREEN_WIDTH/2, 30)];
            timeLab.text = [dateFormatter stringFromDate:currentDate];
            timeLab.backgroundColor = UIColorFromRGB(0xf0f0f0);
            timeLab.font = [UIFont systemFontOfSize:19.0];
            timeLab.textColor = UIColorFromRGB(0x4d4d4d);
            [_backView4 addSubview:timeLab];
        }
    }
    //默认没有指派人员 先显示备注栏
    _backView6 = [[UIView alloc]init];
    _backView6.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView6];
    [_backView6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView4.mas_bottom).offset(0);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 85));
    }];
    UILabel *shTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 80, 21)];
    shTitle.textColor = UIColorFromRGB(0x4d4d4d);
    shTitle.text = @"审核备注";
    shTitle.font = [UIFont systemFontOfSize:19.0];
    [_backView6 addSubview:shTitle];
    _bztextView = [[UITextView alloc]initWithFrame:CGRectMake(100, 0, kSCREEN_WIDTH/4*3-10, 80)];
    _bztextView.font = [UIFont systemFontOfSize:19.0];
    _bztextView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    _bztextView.text = @"输入处理意见";
    _bztextView.textColor = UIColorFromRGB(0x4d4d4d);
    [_backView6 addSubview:_bztextView];
    
    _backView7 = [[UIView alloc]init];
    _backView7.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_backView7];
    [_backView7 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView6.mas_bottom).offset(10);
        make.left.equalTo(_scrollView).offset(0);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH, 60));
    }];
    //提交取消bttuon
    UIButton *updata = [UIButton buttonWithType:UIButtonTypeSystem];
    [updata setTitle:@"提交" forState:UIControlStateNormal];
    [updata setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [updata addTarget:self action:@selector(up) forControlEvents:UIControlEventTouchUpInside];
    updata.backgroundColor = UIColorFromRGB(0x2dd500);
    [_backView7 addSubview:updata];
    [updata mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView7).offset(10);
        make.left.equalTo(_scrollView).offset((kSCREEN_WIDTH/3-20)/2);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
    
    UIButton *cancerl = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancerl setTitle:@"取消" forState:UIControlStateNormal];
    [cancerl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancerl addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    cancerl.backgroundColor = UIColorFromRGB(0xd50037);
    [_backView7 addSubview:cancerl];
    [cancerl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView7).offset(10);
        make.left.equalTo(updata.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREEN_WIDTH/3, 40));
    }];
}
- (void)handleSwitch:(ZJSwitch *)sender{
    //加权限
    if (sender.isOn && (self.level == 7 || self.level == 8)) {
        state = 0;
        _btn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_btn setTitle:@"车辆分配" forState:UIControlStateNormal];
        _btn.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [_btn setTitleColor:UIColorFromRGB(0x0093d5) forState:UIControlStateNormal];
        //button 标题对齐方式
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_btn addTarget:self action:@selector(updataDt) forControlEvents:UIControlEventTouchUpInside];
        [_backView4 addSubview:_btn];
        [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backView4).offset(5);
            make.left.equalTo(_swi.mas_right).offset(10);
            make.size.mas_equalTo(CGSizeMake(120, 30));
        }];

        
    }else{
        state = 1;
        [_btn removeFromSuperview];
    }
}
//车辆分配按键
- (void)updataDt{
    
    carS = 1;
   
    
 
}
/*
//获取车辆品牌 型号 网络请求
- (void)carLoad{

    _carid = [[NSMutableArray alloc]init];
    _carBand = [[NSMutableArray alloc]init];
    _carBandid = [[NSMutableArray alloc]init];
    _carname = [[NSMutableArray alloc]init];
    _vehicleModel_arr = [[NSMutableArray alloc]init];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager POST:K_car parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *carList = dic[@"data"][@"carlist"];
        for (NSInteger i = 0; i<carList.count; ++i) {
            
            [_carname addObject:carList[i][@"name"]];
            [_carid addObject:carList[i][@"id"]];
            [_vehicleModel_arr addObject:carList[i][@"vehicleModel"]];
            
        }
        for (NSInteger j = 0; j < _vehicleModel_arr.count; j++) {
            
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            [arr addObjectsFromArray:_vehicleModel_arr[j]];
            
            NSMutableArray *carBand1 = [[NSMutableArray alloc]init];
            NSMutableArray *carBid = [[NSMutableArray alloc]init];
            for (NSInteger i = 0; i < arr.count; i++) {
                
                [carBand1 addObject:arr[i][@"name"]];
                [carBid addObject:arr[i][@"id"]];
            }
            [_carBandid addObject:carBid];
            [_carBand addObject:carBand1];
        }

       _view = [[CarfenPei alloc]initWithBounds:CGRectMake(0, 15, kSCREEN_WIDTH, kSCREEN_HEIGHT-15) titleMenus:_titleMeus];
        _view.parentVC = self;
        _view.delegate = self;
        LewPopupViewAnimationSlide *animation = [[LewPopupViewAnimationSlide alloc]init];
        
        animation.type = LewPopupViewAnimationSlideTypeBottomBottom;
        [self lew_presentPopupView:_view animation:animation dismissed:^{
            NSLog(@"动画结束");}];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}*/
/*
- (void)up{
    

    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [_manager POST:K_carShQR parameters:@{carid:[NSString stringWithFormat:@"%ld",Cid],userid:[NSString stringWithFormat:@"%ld",self.ID],sbState:[NSString stringWithFormat:@"%ld",state],updataTime:[dateFormatter stringFromDate:currentDate],disc:_bztextView.text,carStata:[NSString stringWithFormat:@"%ld",carS],carMid:[NSString stringWithFormat:@"%ld",Mid],carBuyTime:_view.btn3.titleLabel.text,carNum:_view.textTF.text} progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            NSInteger status = [dic[@"status"]integerValue];
            NSLog(@"%ld",status);
            if (status == 1) {
                
                [[YXAlertHUD stateHUDWithHUDTyep:0 message:@"提交成功" textColor:[UIColor blackColor] iconColor:[UIColor greenColor]] show];
            }else{
                
                [[YXAlertHUD stateHUDWithHUDTyep:1 message:@"提交失败" textColor:[UIColor redColor] iconColor:[UIColor redColor]] show];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        
    });
}*/
- (void)choose:(UIButton *)sender{
    
    tag = sender.tag;
// tag = 1000 选择品牌 1001 型号 1003 购买时间
    if (sender.tag == 1003) {
        //x,y 值无效，默认是居中的
        KSDatePicker* picker = [[KSDatePicker alloc] initWithFrame:CGRectMake(0, 0, _view.frame.size.width - 40, 300)];
        
        //配置中心，详情见KSDatePikcerApperance
        
        picker.appearance.radius = 5;
        
        //设置回调
        picker.appearance.resultCallBack = ^void(KSDatePicker* datePicker,NSDate* currentDate,KSDatePickerButtonType buttonType){
            
            if (buttonType == KSDatePickerButtonCommit) {
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd"];
                
                [sender setTitle:[NSString stringWithFormat:@"%@ 00:00:00",[formatter stringFromDate:currentDate]] forState:UIControlStateNormal];
            }
        };
        // 显示
        [picker show];
    }else if (sender.tag == 1001) {
        
        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, kSCREEN_WIDTH/2, 44*3) titleMenus:_carname];
        view.containerBackgroudColor = [UIColor clearColor];
        view.delegate = self;
        [view showFrom:sender alignStyle:CPAlignStyleLeft];
        
    }else{
        
        CustomPopOverView *view = [[CustomPopOverView alloc]initWithBounds:CGRectMake(0, 0, kSCREEN_WIDTH/2, 44*3) titleMenus:_carBand[indexPath]];
        view.containerBackgroudColor = [UIColor clearColor];
        view.delegate = self;
        [view showFrom:sender alignStyle:CPAlignStyleLeft];
    }



}
- (void)popOverView:(CustomPopOverView *)pView didClickMenuIndex:(NSInteger)index
{


    if (tag == 1001) {
        indexPath = index;
        
        [_view.btn1 setTitle:_carname[index] forState:UIControlStateNormal];
        [_view.btn1 setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        [_view.btn2 setTitle:@"选择车辆型号" forState:UIControlStateNormal];
        [_view.btn2 setTitleColor:UIColorFromRGB(0xb6b6b6) forState:UIControlStateNormal];
        Cid = [_carid[index]integerValue];
    }else if(tag == 1002){

        [_view.btn2 setTitle:_carBand[indexPath][index] forState:UIControlStateNormal];
        [_view.btn2 setTitleColor:UIColorFromRGB(0x4d4d4d) forState:UIControlStateNormal];
        Mid = [_carBandid[indexPath][index]integerValue];
    }else {
    
        
    }
    
    
    
    
}
- (void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
