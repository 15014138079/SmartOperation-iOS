//
//  YYXJhistoryVC.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "XJhisBranchCell.h"
#import "AppDelegate.h"
#import "XJBranchDataModel.h"
#import "XJhisUserVC.h"
#import "XJhisPsonDataModel.h"
#import "XJhisPsonCell.h"
#import "XJhisStationDataModel.h"
#import "XYhistroyVC.h"
#import "ZJAlertListView.h"
#import "NothingDataView.h"
#import "ServerUnusualView.h"
#import "JoinFaluterView.h"
#import "FallLineView.h"
#import "MBProgressHUD.h"
@interface YYXJhistoryVC : UIViewController<UITableViewDelegate,UITableViewDataSource,ZJAlertListViewDelegate,ZJAlertListViewDatasource,JoinFalutDelegate,FallLineDelegate,NothingViewDelegate,ServerUnsualDelegate>

@end
