//
//  XJBranchListModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "XJhisPsonListModel.h"
@protocol XJBranchListModel <NSObject>

@end
@interface XJBranchListModel : JSONModel
@property (nonatomic,strong)NSString *storeName;
@property (nonatomic,strong)NSString *principalName;
@property (nonatomic,assign)NSInteger branchId;
@property (nonatomic,strong)NSString *branchName;
@property (nonatomic,strong)NSString *principalPhone;
@property (nonatomic,strong)NSString *storeCode;
@property (nonatomic, assign, getter=isExpend) BOOL expand;
@property (nonatomic,strong)NSArray <XJBranchListModel> *beanList;
@end
/*
 "storeName": "甘肃（临夏）仓",
 "principalName": "沙立雨",
 "branchId": 24,
 "branchName": "甘肃分公司",
 "principalPhone": "13919269796",
 "storeCode": "SYXY123"
 */
