//
//  XJLRViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Masonry.h"
#import "AFNetworking.h"
//录入人名字来源
#import "AppDelegate.h"
#import "ZJSwitch.h"
#import "UIImage+GIF.h"

#import "LewPopupViewController.h"
#import "PopupView.h"
#import "YXKit.h"
#import "MBProgressHUD.h"
//出库单号模型
#import "KbaseModel.h"
#import "KdataModel.h"
#import "KmtPcsModel.h"
#import "ExpenseCell.h"
#import "ExpenseModel.h"
//图片上传
#import "KKUploadPhotoCollectionViewCell.h"
#import "KKPhotoPickerManager.h"
//配件选择
#import "PartChooseVC.h"
//弹出选择框
#import "MHActionSheet.h"
//时间选择
#import "FDAlertView.h"
#import "RBCustomDatePickerView.h"
//故障类型选择
#import "TypeDataModel.h"
#import "SectionOneFaultModel.h"
#import "SectionTowFaultModel.h"
#import "SectionThreeFaultModel.h"
#import "ChooseMenus.h"
#import <CoreLocation/CoreLocation.h>
#import "PHATextView.h"
#import "XJEqDataModel.h"//设备模型
#import "YYUpPsonDataModel.h"

@interface XJLRViewController : UIViewController<DataDelegate,PopupViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,closeDelegate,sendTheValueDelegate,ChooseMenusDataSource,CLLocationManagerDelegate,UITextViewDelegate>
@property(assign ,nonatomic)NSInteger stationid;
@property(strong ,nonatomic)NSMutableArray *eqidArray;
@property(strong ,nonatomic)NSString *username;
@property(assign ,nonatomic)NSInteger userId;
@property(strong,nonatomic)NSMutableArray *SBArry;
@end
