//
//  SuggestionView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "SuggestionView.h"

@interface SuggestionView()
{
    NSInteger typeTag;
}
@end

@implementation SuggestionView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initview];
    }
    return self;
}
- (void)initview{
    UIView *backview = [[UIView alloc]initWithFrame:CGRectMake(10, 0, kSCREEN_WIDTH-20, 40)];
    backview.backgroundColor = UIColorFromRGB(0xffffff);
    backview.layer.cornerRadius = 5;
    [self addSubview:backview];
    self.leftbackview = [[UIView alloc]initWithFrame:CGRectMake(1, 1, (kSCREEN_WIDTH-22)/2, 38)];
    self.leftbackview.backgroundColor = UIColorFromRGB(0xffffff);
    self.leftbackview.layer.cornerRadius = 3;
    [backview addSubview:self.leftbackview];

    self.leftIcon = [[UIImageView alloc]initWithFrame:CGRectMake((kSCREEN_WIDTH-22)/4-20, 9, 20, 20)];
    self.leftIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_checked"];
    [self.leftbackview addSubview:self.leftIcon];
    self.leftTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftIcon.frame), 8, 40, 21)];
    self.leftTitle.text = @"建议";
    self.leftTitle.textColor = UIColorFromRGB(0x4d4d4d);
    self.leftTitle.textAlignment = NSTextAlignmentCenter;
    [self.leftbackview addSubview:self.leftTitle];
    self.leftBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    //self.leftBTN.userInteractionEnabled = NO;
    self.leftBTN.frame = self.leftbackview.frame;

    [self.leftBTN addTarget:self action:@selector(chooseComplain:) forControlEvents:UIControlEventTouchUpInside];
    [backview addSubview:self.leftBTN];
    
    
    self.rightbackview = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftbackview.frame), 1, (kSCREEN_WIDTH-22)/2, 38)];
    self.rightbackview.backgroundColor = UIColorFromRGB(0xeaeaea);
    self.rightbackview.layer.cornerRadius = 3;
    [backview addSubview:self.rightbackview];
    self.rightIcon = [[UIImageView alloc]initWithFrame:CGRectMake((kSCREEN_WIDTH-22)/4-20, 9, 20, 20)];
    self.rightIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_unchecked"];
    [self.rightbackview addSubview:self.rightIcon];
    self.rightTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.rightIcon.frame), 8, 40, 21)];
    self.rightTitle.text = @"投诉";
    self.rightTitle.textColor = UIColorFromRGB(0xbfbfc4);
    self.rightTitle.textAlignment = NSTextAlignmentCenter;
    [self.rightbackview addSubview:self.rightTitle];
    self.rightBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBTN.frame = self.rightbackview.frame;
    [self.rightBTN addTarget:self action:@selector(chooseSuggestion:) forControlEvents:UIControlEventTouchUpInside];
    [backview addSubview:self.rightBTN];
    self.leftBTN.tag = 1;
    self.rightBTN.tag = 2;
    typeTag = self.leftBTN.tag;
    self.QuestionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.QuestionBtn.backgroundColor = UIColorFromRGB(0xffffff);
    self.QuestionBtn.frame = CGRectMake(10, CGRectGetMaxY(backview.frame)+10, kSCREEN_WIDTH-20, 40);
    [self.QuestionBtn setTitle:@"  问题类型" forState:UIControlStateNormal];
    [self.QuestionBtn setTitleColor:UIColorFromRGB(0xbfbfc4) forState:UIControlStateNormal];
    self.QuestionBtn.layer.cornerRadius = 5;
    self.QuestionBtn.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.QuestionBtn.layer.borderWidth = 0.5;
    self.QuestionBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
    self.QuestionBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.QuestionBtn addTarget:self action:@selector(QuestionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.QuestionBtn];
    
    self.QuestionTitleTF = [[UITextField alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.QuestionBtn.frame)+10, kSCREEN_WIDTH-20, 40)];
    self.QuestionTitleTF.backgroundColor = UIColorFromRGB(0xffffff);
    self.QuestionTitleTF.borderStyle = UITextBorderStyleRoundedRect;
    self.QuestionTitleTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.QuestionTitleTF.placeholder = @"  填写投诉或者建议标题";
    self.QuestionTitleTF.textColor = UIColorFromRGB(0x4d4d4d);
    [self addSubview:self.QuestionTitleTF];
    
    self.DiscribTextView = [[PHATextView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.QuestionTitleTF.frame)+10, kSCREEN_WIDTH-20, 100)];
    self.DiscribTextView.placeholder = @"  填写投诉或建议详细内容";
    self.DiscribTextView.returnKeyType = UIReturnKeyDone;
    self.DiscribTextView.font = [UIFont systemFontOfSize:16.0];
    //_DiscribTextView.textColor = UIColorFromRGB(0x4d4d4d);
    self.DiscribTextView.layer.borderColor = UIColorFromRGB(0xb6b6b6).CGColor;
    self.DiscribTextView.layer.borderWidth = 1.0f;
    self.DiscribTextView.layer.cornerRadius = 3;
    [self addSubview:self.DiscribTextView];
    
    _submitBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [_submitBTN setTitle:@"提交" forState:UIControlStateNormal];
    _submitBTN.backgroundColor = UIColorFromRGB(0x2dd500);
    //UpBtn.titleLabel.font = [UIFont systemFontOfSize:1]
    _submitBTN.frame = CGRectMake(kSCREEN_WIDTH/6-10, CGRectGetMaxY(self.DiscribTextView.frame)+15, kSCREEN_WIDTH/3, 40);
    _submitBTN.layer.cornerRadius = 3;
   
    [_submitBTN addTarget:self action:@selector(submitBTNClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_submitBTN];
    _CancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_CancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    _CancelBtn.backgroundColor = UIColorFromRGB(0xd50037);
    _CancelBtn.frame = CGRectMake(CGRectGetMaxX(_submitBTN.frame)+20, CGRectGetMaxY(self.DiscribTextView.frame)+15, kSCREEN_WIDTH/3, 40);
    _CancelBtn.layer.cornerRadius = 3;
    [_CancelBtn addTarget:self action:@selector(revokeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_CancelBtn];
    
    
    //响应者  responder
    //第一响应者  first responder 用户正在操作的控件
    //如果文本框称为第一响应者,则系统会自动弹出键盘
    //如果想要隐藏键盘,只需要放弃第一响应者即可
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    
    [tap addTarget:self action:@selector(hideKeyboard)];
    
    [self addGestureRecognizer:tap];

}
- (void)hideKeyboard{
    
    [self.DiscribTextView resignFirstResponder];
    [self.QuestionTitleTF resignFirstResponder];
}
- (void)chooseComplain:(UIButton *)sender{
    
    typeTag = sender.tag;
    self.leftBTN.userInteractionEnabled = NO;
    self.rightBTN.userInteractionEnabled = YES;
    self.leftIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_checked"];
    self.leftbackview.backgroundColor = UIColorFromRGB(0xffffff);
    self.leftTitle.textColor = UIColorFromRGB(0x4d4d4d);
    
    self.rightIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_unchecked"];
    self.rightbackview.backgroundColor = UIColorFromRGB(0xeaeaea);
    self.rightTitle.textColor = UIColorFromRGB(0xbfbfc4);
    [self.delegate UpType:sender andTag:typeTag];
    [self.DiscribTextView resignFirstResponder];
    [self.QuestionTitleTF resignFirstResponder];
}
- (void)chooseSuggestion:(UIButton *)sender{
    
    typeTag = sender.tag;
    self.leftBTN.userInteractionEnabled = YES;
    self.rightBTN.userInteractionEnabled = NO;
    self.rightIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_checked"];
    self.rightbackview.backgroundColor = UIColorFromRGB(0xffffff);
    self.rightTitle.textColor = UIColorFromRGB(0x4d4d4d);
    self.leftIcon.image = [UIImage imageNamed:@"icon_image_correction_check_box_unchecked"];
    self.leftbackview.backgroundColor = UIColorFromRGB(0xeaeaea);
    self.leftTitle.textColor = UIColorFromRGB(0xbfbfc4);
    [self.delegate UpType:sender andTag:typeTag];
    [self.DiscribTextView resignFirstResponder];
    [self.QuestionTitleTF resignFirstResponder];
}
- (void)QuestionBtnClick:(UIButton *)sender{
    [self.DiscribTextView resignFirstResponder];
    [self.QuestionTitleTF resignFirstResponder];
    [self.delegate ChooseQuestionType:sender];
}
- (void)submitBTNClick:(UIButton *)sender{
    [self.DiscribTextView resignFirstResponder];
    [self.QuestionTitleTF resignFirstResponder];
    [self.delegate SuggestionViewSubmitClick:sender];
}
- (void)revokeBtnClick:(UIButton *)sender{
    
    [self.delegate SuggestionViewCancelClick:sender];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
