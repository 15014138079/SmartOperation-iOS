//
//  XJhisBranchCell.m
//  运营管理系统
//
//  Created by 杨毅 on 16/12/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "XJhisBranchCell.h"

@implementation XJhisBranchCell
{

    __weak IBOutlet UILabel *BranchofOCname;
}
- (void)fillCellWithModel:(XJBranchListModel *)model{
    BranchofOCname.text = model.branchName;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
