//
//  SuggestionAndComplainAddVC.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestionView.h"
#import "HideNameOrShowPopView.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "MHActionSheet.h"
#import "SuggesDataModel.h"
#import "YXKit.h"
@interface SuggestionAndComplainAddVC : UIViewController<SuggestionViewDelegate,HideNameOrShowPopViewDelegate>

@end
