//
//  YMTabBarViewController.m
//  OneOwnerPurchase
//
//  Created by Yumukim on 15/11/16.
//  Copyright © 2015年 Yumukim. All rights reserved.
//

#import "TabBarViewController.h"
#import "SearchViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBar.tintColor = [UIColor grayColor];
    self.tabBar.barTintColor = [[UIColor alloc]initWithRed:247/255.0 green:246/255.0 blue:246/255.0 alpha:1];
    self.tabBar.layer.shadowColor = [UIColor grayColor].CGColor;
    self.tabBar.layer.shadowOffset = CGSizeMake(0, -1);
    self.tabBar.layer.shadowRadius = 2;
    self.tabBar.layer.shadowOpacity = 0.4;

    //工作模块
    self.FirstController = [[WorkViewController alloc] init];
    UINavigationController *s = [[UINavigationController alloc]initWithRootViewController:self.FirstController];
    //故障模块
    self.SecondController = [[AlarmViewController alloc] init];
    UINavigationController *f = [[UINavigationController alloc]initWithRootViewController:self.SecondController];
    //查询模块
    self.ThirdController = [[YunYinViewController alloc] init];
    UINavigationController *t = [[UINavigationController alloc]initWithRootViewController:self.ThirdController];
    //设置模块
    self.ForthController = [[SettingsViewController alloc]init];
    UINavigationController *four = [[UINavigationController alloc]initWithRootViewController:self.ForthController];
    
    
    self.viewControllers = @[s,f,t,four];
    
    NSArray * TabTitle = @[@"工作",@"故障",@"查询",@"设置"];
    
    /**注意默认情况下UITabBarController在加载子视图时是懒加载的，所以这里调用一次所有的Controller，否则在第一次展示时只有第一个控制器tab图标，其他Controller的tab图标不会显示
     */
    
    for (int i = 0; i < self.viewControllers.count; i++)
    {
        self.viewControllers[i].tabBarItem.title = TabTitle[i];
        self.viewControllers[i].tabBarItem.image = [[UIImage imageNamed:[NSString stringWithFormat:@"icon00%d_b",i+1]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.viewControllers[i].tabBarItem.selectedImage = [[UIImage imageNamed:[NSString stringWithFormat:@"icon00%d",i+1]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        //徽标
       // self.viewControllers[0].tabBarItem.badgeValue = [NSString stringWithFormat:@""];

    }
  

}

@end
