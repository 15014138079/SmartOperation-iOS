//
//  GZupdataViewController.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/22.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYUpPsonDataModel.h"
@interface GZupdataViewController : UIViewController
@property (nonatomic,strong)NSString *funcName;
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,assign)NSInteger leve;
//故障列表编辑传递的信息
@property (nonatomic,assign)NSInteger faultTypeId;//故障类型id
@property (nonatomic,assign)NSInteger equipId;//设备id
@property (nonatomic,strong)NSString *GZtitle;
@property (nonatomic,strong)NSString *faultTypeName;//故障类型名称
@property (nonatomic,assign)NSInteger stationId;//站点id
@property (nonatomic,strong)NSString *stationN;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,assign)NSInteger GZID;
@property (nonatomic,strong)NSString *ocTime;//上报时间
@end
