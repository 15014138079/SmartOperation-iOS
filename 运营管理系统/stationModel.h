//
//  stationModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/9/28.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import "JSONModel.h"
#import "equeipsModel.h"

@protocol stationModel <NSObject>

@end

@interface stationModel : JSONModel
@property (nonatomic,strong)NSString *address;
@property (nonatomic,strong)NSString *buildT;
@property (nonatomic,strong)NSString *code;
@property (nonatomic,assign)NSInteger conffient;
@property (nonatomic,strong)NSString *discrib;
@property (nonatomic,strong)NSArray <equeipsModel> *equips;
@property (nonatomic,assign)NSInteger Id;
@property (nonatomic,strong)NSString *lat;
@property (nonatomic,strong)NSString *lnt;
@property (nonatomic,strong)NSString *locppt;
@property (nonatomic,assign)NSInteger monthCost;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,assign)NSInteger routCycle;
@property (nonatomic,strong)NSString *siteLevel;
@property (nonatomic,assign)NSInteger state;
@end
/*
 "address": "贵州省清镇市第一中学正则楼楼顶",
 "buildT": null,
 "code": "GY08003",
 "conffient": 1,
 "discrib": "",
 "equips": [],
 "id": 2315,
 "lat": "26.559772",
 "lnt": "106.472877",
 "locppt": "",
 "monthCost": 0,
 "name": "接官厅",
 "routCycle": 0,
 "siteLevel": "A",
 "state": 0
 */
