//
//  YYgzDeatailConList.h
//  运营管理系统
//
//  Created by 杨毅 on 17/1/12.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "JSONModel.h"

@protocol YYgzDeatailConList <NSObject>


@end

@interface YYgzDeatailConList : JSONModel
@property (nonatomic,strong)NSString *confirmTime;
@property (nonatomic,strong)NSString *confirmPsonName;
@property (nonatomic,assign)NSInteger confirmState;
@property (nonatomic,strong)NSString *confirmRemark;
@end
