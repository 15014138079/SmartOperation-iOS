//
//  SugDetailView.m
//  运营管理系统
//
//  Created by 杨毅 on 17/2/22.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import "SugDetailView.h"
@interface SugDetailView()
{
    __weak IBOutlet UIView *TitleAndContentBackView;
    __weak IBOutlet UILabel *titleLab;
    __weak IBOutlet UILabel *ContentLab;
    __weak IBOutlet UIView *line1;
    __weak IBOutlet UIView *line2;
    __weak IBOutlet UIView *line3;
    __weak IBOutlet UIView *line4;
    __weak IBOutlet UIImageView *supportUpImageview;
    __weak IBOutlet UIImageView *PsonImageview;
    __weak IBOutlet UIImageView *branchCompanyImageview;
    __weak IBOutlet UIImageView *submitTimeImageview;
    __weak IBOutlet UILabel *SupportNumberLab;
    __weak IBOutlet UILabel *PsonLab;
    __weak IBOutlet UILabel *CompanyLab;
    __weak IBOutlet UILabel *TimeLab;
    __weak IBOutlet UIImageView *typeImageview;
    
}
@end
@implementation SugDetailView
- (instancetype)initWithFrame:(CGRect)frame AndDetailData:(NSDictionary *)detailDic{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
        switch ([detailDic[@"aspectOn"]integerValue]) {
            case 1:
                titleLab.text = [NSString stringWithFormat:@"%c技术问题%c%@",'[',']',detailDic[@"title"]];
                break;
            case 2:
                titleLab.text = [NSString stringWithFormat:@"%c行政问题%c%@",'[',']',detailDic[@"title"]];
                break;
            case 3:
                titleLab.text = [NSString stringWithFormat:@"%c备件问题%c%@",'[',']',detailDic[@"title"]];
                break;
            case 4:
                titleLab.text = [NSString stringWithFormat:@"%c体系管理问题%c%@",'[',']',detailDic[@"title"]];
                break;
            case 5:
                titleLab.text = [NSString stringWithFormat:@"%c其他%c%@",'[',']',detailDic[@"title"]];
                break;
                
            default:
                break;
        }

        titleLab.numberOfLines = 0;
        CGRect titleRect = [titleLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-30, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:titleLab.font,NSFontAttributeName, nil] context:nil];
        titleLab.frame = CGRectMake(5, 0, kSCREEN_WIDTH-30, titleRect.size.height);
        
        ContentLab.numberOfLines = 0;
        ContentLab.text = detailDic[@"content"];
        CGRect contentRect = [ContentLab.text boundingRectWithSize:CGSizeMake(kSCREEN_WIDTH-30, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:ContentLab.font,NSFontAttributeName, nil] context:nil];
        ContentLab.frame = CGRectMake(5, CGRectGetMaxY(titleLab.frame)+5, kSCREEN_WIDTH-30, contentRect.size.height);
        TitleAndContentBackView.frame = CGRectMake(0, 0, kSCREEN_WIDTH-20, CGRectGetMaxY(ContentLab.frame)+5);
        line1.frame = CGRectMake(0, CGRectGetMaxY(TitleAndContentBackView.frame), kSCREEN_WIDTH-20, 1);
        supportUpImageview.frame = CGRectMake(5, CGRectGetMaxY(line1.frame)+3, 20, 20);
        supportUpImageview.image = [UIImage imageNamed:detailDic[@"supimage"]];
        SupportNumberLab.text = detailDic[@"supportN"];
        SupportNumberLab.frame = CGRectMake(30,CGRectGetMaxY(line1.frame)+3, kSCREEN_WIDTH-50, 21);
        
        line2.frame = CGRectMake(0, CGRectGetMaxY(supportUpImageview.frame)+3, kSCREEN_WIDTH-20, 1);
        PsonImageview.frame = CGRectMake(5, CGRectGetMaxY(line2.frame)+3, 20, 20);
        PsonImageview.image = [UIImage imageNamed:@"user-male"];
        
        PsonLab.text = detailDic[@"pson"];
        PsonLab.frame = CGRectMake(30, CGRectGetMaxY(line2.frame)+3, kSCREEN_WIDTH-50, 21);
        line3.frame = CGRectMake(0, CGRectGetMaxY(PsonImageview.frame)+3, kSCREEN_WIDTH-20, 1);
        branchCompanyImageview.frame = CGRectMake(5, CGRectGetMaxY(line3.frame)+3, 20, 20);
        branchCompanyImageview.image = [UIImage imageNamed:@"company"];
        
        CompanyLab.text = detailDic[@"branchCom"];
        CompanyLab.frame = CGRectMake(30, CGRectGetMaxY(line3.frame)+3, kSCREEN_WIDTH-50, 21);
        line4.frame = CGRectMake(0, CGRectGetMaxY(branchCompanyImageview.frame)+3, kSCREEN_WIDTH-20, 1);
        submitTimeImageview.frame = CGRectMake(5, CGRectGetMaxY(line4.frame)+3, 20, 20);
        submitTimeImageview.image = [UIImage imageNamed:@"calender"];
        
        TimeLab.text = detailDic[@"time"];
        TimeLab.frame = CGRectMake(30, CGRectGetMaxY(line4.frame)+3, kSCREEN_WIDTH-50, 21);
        
        typeImageview.frame = CGRectMake(kSCREEN_WIDTH-110, CGRectGetMinY(line1.frame)-10, 80, 57);
        if ([detailDic[@"sugTypeID"]integerValue] == 1) {
            typeImageview.image = [UIImage imageNamed:@"suggested_seal"];
        }else{
            typeImageview.image = [UIImage imageNamed:@"complaint_seal"];
        }
        
        
    }
    return self;
}
- (void)layoutSubviews{
    
    self.frame = CGRectMake(10, 10, kSCREEN_WIDTH-20, CGRectGetMaxY(submitTimeImageview.frame)+3);
    [self.delegate returnSugDetailViewHeight:CGRectGetMaxY(submitTimeImageview.frame)+3];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
