//
//  MenuModel.h
//  运营管理系统
//
//  Created by 杨毅 on 16/8/27.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuModel : NSObject
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *icon;
@end
