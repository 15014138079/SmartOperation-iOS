//
//  SuggestionView.h
//  运营管理系统
//
//  Created by 杨毅 on 17/2/20.
//  Copyright © 2017年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PHATextView.h"

@class SuggestionView;
@protocol SuggestionViewDelegate <NSObject>
- (void)UpType:(UIButton *)sender andTag:(NSInteger)typetag;
- (void)ChooseQuestionType:(UIButton *)sender;
- (void)SuggestionViewSubmitClick:(UIButton *)sender;//提交被点击
- (void)SuggestionViewCancelClick:(UIButton *)sender;//取消被点击
@end


@interface SuggestionView : UIView
@property(nonatomic,strong)UIView *leftbackview;
@property(nonatomic,strong)UIView *rightbackview;
@property(nonatomic,strong)UIImageView *leftIcon;
@property(nonatomic,strong)UIImageView *rightIcon;
@property(nonatomic,strong)UILabel *leftTitle;
@property(nonatomic,strong)UILabel *rightTitle;
@property(nonatomic,strong)UIButton *leftBTN;
@property(nonatomic,strong)UIButton *rightBTN;
@property(nonatomic,strong)UIButton *QuestionBtn;
@property(nonatomic,strong)UIButton *submitBTN;
@property(nonatomic,strong)UIButton *CancelBtn;
@property(nonatomic,strong)UITextField *QuestionTitleTF;
@property(nonatomic,strong)PHATextView *DiscribTextView;
@property(nonatomic,weak)id <SuggestionViewDelegate> delegate;

@end
