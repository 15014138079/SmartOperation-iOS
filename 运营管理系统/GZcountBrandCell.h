//
//  GZcountBrandCell.h
//  运营管理系统
//
//  Created by 杨毅 on 16/10/17.
//  Copyright © 2016年 杨毅. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "brandModel.h"
@interface GZcountBrandCell : UITableViewCell
- (void)fillCellWithModel:(brandModel *)model;
@end
